<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('rp_titik')) {
  function rp_titik($uang = '')
  {
    $rp     = "";
    $digit  = strlen($uang);
  
    while($digit > 3)
    {
      $rp       = "." . substr($uang,-3) . $rp;
      $lebar    = strlen($uang) - 3;
      $uang     = substr($uang, 0, $lebar);
      $digit    = strlen($uang);  
    }
    $rp     = $uang . $rp;
    return $rp;
  }
}

if ( ! function_exists('rp_titik_komplit')) {
  function rp_titik_komplit($uang = '')
  {
    $rp     = "";
    $digit  = strlen($uang);
  
    while($digit > 3)
    {
      $rp       = "." . substr($uang,-3) . $rp;
      $lebar    = strlen($uang) - 3;
      $uang     = substr($uang, 0, $lebar);
      $digit    = strlen($uang);  
    }
    $rp     = $uang . $rp;
    return 'Rp'.$rp.',00';
  }
}



?>