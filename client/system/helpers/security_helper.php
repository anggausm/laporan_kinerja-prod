<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package    CodeIgniter
 * @author    EllisLab Dev Team
 * @copyright    Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright    Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license    https://opensource.org/licenses/MIT    MIT License
 * @link    https://codeigniter.com
 * @since    Version 1.0.0
 * @filesource
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * CodeIgniter Security Helpers
 *
 * @package        CodeIgniter
 * @subpackage    Helpers
 * @category    Helpers
 * @author        EllisLab Dev Team
 * @link        https://codeigniter.com/user_guide/helpers/security_helper.html
 */

// ------------------------------------------------------------------------

if (!function_exists('xss_clean')) {
    /**
     * XSS Filtering
     *
     * @param    string
     * @param    bool    whether or not the content is an image file
     * @return    string
     */
    function xss_clean($str, $is_image = false)
    {
        return get_instance()->security->xss_clean($str, $is_image);
    }
}

// ------------------------------------------------------------------------

if (!function_exists('sanitize_filename')) {
    /**
     * Sanitize Filename
     *
     * @param    string
     * @return    string
     */
    function sanitize_filename($filename)
    {
        return get_instance()->security->sanitize_filename($filename);
    }
}

// --------------------------------------------------------------------

if (!function_exists('do_hash')) {
    /**
     * Hash encode a string
     *
     * @todo    Remove in version 3.1+.
     * @deprecated    3.0.0    Use PHP's native hash() instead.
     * @param    string    $str
     * @param    string    $type = 'sha1'
     * @return    string
     */
    function do_hash($str, $type = 'sha1')
    {
        if (!in_array(strtolower($type), hash_algos())) {
            $type = 'md5';
        }

        return hash($type, $str);
    }
}

// ------------------------------------------------------------------------

if (!function_exists('strip_image_tags')) {
    /**
     * Strip Image Tags
     *
     * @param    string
     * @return    string
     */
    function strip_image_tags($str)
    {
        return get_instance()->security->strip_image_tags($str);
    }
}

// ------------------------------------------------------------------------

if (!function_exists('encode_php_tags')) {
    /**
     * Convert PHP tags to entities
     *
     * @param    string
     * @return    string
     */
    function encode_php_tags($str)
    {
        return str_replace(array('<?', '?>'), array('&lt;?', '?&gt;'), $str);
    }
}

function enc_data($string)
{
    $output = false;
    /*
     * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
    [security]
    encryption_key=111111111111111111111111 ;
    iv=Xp8BFF8hpFpy6BxK24PjTwtM;
    encryption_mechanism=aes-256-cbc
     */
    //  $security = array('encryption_key'=> '', 'iv'=> '', 'encryption_mechanism'=>'');
    date_default_timezone_set('Asia/Jakarta');
    $d = date('d');
    $m = date('m');
    $y = date('Y');
    $secret_key = '111111111111111111111111';
    $secret_iv = $d . '8BFF8hpFpy' . $m . 'xK2' . $y . 'wtM';
    $encrypt_method = 'aes-256-cbc';
    // hash
    $key = hash("sha256", $secret_key);
    // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
    $iv = substr(hash("sha256", $secret_iv), 0, 16);
    //do the encryption given text/string/number
    $result = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($result);
    return $output;
}
function dec_data($string)
{
    $output = false;
    /*
     * read security.ini file & get encryption_key | iv | encryption_mechanism value for generating encryption code
     */
    $d = date('d');
    $m = date('m');
    $y = date('Y');
    $secret_key = '111111111111111111111111';
    $secret_iv = $d . '8BFF8hpFpy' . $m . 'xK2' . $y . 'wtM';
    $encrypt_method = 'aes-256-cbc';
    // hash
    $key = hash("sha256", $secret_key);
    // iv – encrypt method AES-256-CBC expects 16 bytes – else you will get a warning
    $iv = substr(hash("sha256", $secret_iv), 0, 16);
    //do the decryption given text/string/number
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}
/* XSS FRIENDLY */
if (!function_exists('enc_data_url')) {
    /**
     * simple method to encrypt plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     * IVs should be random and generated by a CSPRNG.
     * IVs should not be reused. That is, don't encrypt plaintext "A" and plaintext "B" with the same IV. Every record should have its own IV.
     * The IV is not a secret like the key. It can be stored in plaintext along with the cipher text.
     * @param string $string: string
     *
     * @return string
     */
    function enc_data_url($string)
    {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'e6q1tGv2OgcwUXQE1pAF';
        $secret_iv = 'dc4788e795angadjwdqIr' . date('d') . 'hskAm' . date('m');
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64url_encode($output);
        return $output;
    }
}
if (!function_exists('dec_data_url')) {
    /**
     * simple method to decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     *
     * @param string $string: string
     *
     * @return string
     */
    function dec_data_url($string)
    {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'e6q1tGv2OgcwUXQE1pAF';
        $secret_iv = 'dc4788e795angadjwdqIr' . date('d') . 'hskAm' . date('m');
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64url_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}
if (!function_exists('base64url_encode')) {
    /**
     * Encode data to Base64URL
     * @param string $data
     * @return boolean|string
     **/
    function base64url_encode($data)
    {
        // First of all you should encode $data to Base64 string
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $url = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($url, '=');
    }
}
if (!function_exists('base64url_decode')) {
    /**
     * Decode data from Base64URL
     * @param string $data
     * @param boolean $strict
     * @return boolean|string
     **/
    function base64url_decode($data, $strict = false)
    {
        // Convert Base64URL to Base64 by replacing “-” with “+” and “_” with “/”
        $b64 = strtr($data, '-_', '+/');

        // Decode Base64 string and return the original data
        return base64_decode($b64, $strict);
    }
}
