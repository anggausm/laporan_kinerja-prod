<?php

/*
 * Author : Wahyu Budi Santosa
 * ak : dudoks
 */
defined('BASEPATH') or exit('No direct script access allowed');

class CI_App
{

    public function __construct()
    {
        $this->ddk = &get_instance();
    }

    public function index()
    {
        print "CURL Dudoks 1.0";
    }

	public function isdm_server()
	{
		// return "https://isdm.usm.ac.id/server/index.php/";
		return "http://192.168.19.200/server/index.php/";
	}
    public function server_portal()
    {
        return "https://portal.usm.ac.id/";
    }
    public function sso_app()
    {
        return "http://192.168.176.205/index.php/";
    }
    public function server_file()
    {
        return "https://apps.usm.ac.id/fl/";
        //   return "http://172.16.99.209/";
    }

    public function lokasi_file()
    {
        return "https://apps.usm.ac.id/fl/";
    }

    // public function server_file()
    // {
    //     return "http://img.usm.ac.id/file_v2/";
    // }
    public function base_server()
    {
        return "http://192.168.176.101/index.php/";
    }
    public function sia_server()
    {
        return "http://172.16.98.203/index.php/";
    }
    public function server_sso()
    {
        //172.16.98.101
        return "http://192.168.176.205/index.php/";
        //return "http://172.16.99.205/index.php/";
    }

    public function base_client()
    {
        return "https://apps.usm.ac.id/kpg/";
    }

    public function id_aplikasi()
    {
        return "98654375831928832";
    }

    public function token($value = '')
    {
        $token = $this->ddk->session->userdata('sso_token');
        if (empty($token)) {
            $this->ddk->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
        } else {
            return $token;
        }
    }

    public function cek_token($value = '')
    {
        $token = $this->ddk->session->userdata('sso_token');
        if (empty($token)) {
            $this->ddk->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
            return redirect(base_url());
        }

        $rs = json_decode($this->ddk->curl->request('PUT', $this->sso_app() . 'api/cek_token', 'sso_token', $json_data = ''), 'TRUE');
        if ($rs['result']['status'] == '0') {
            $this->ddk->session->set_flashdata('pesan', 'Maaf Session anda telah berahir, silahkan login kembali.');
            return redirect(base_url());
        }

        if (empty($rs)) {
            $this->ddk->session->set_flashdata('pesan', 'Maaf Session anda telah berahir, silahkan login kembali.1');
            return redirect(base_url());
        }
        return $rs;

    }

    public function cekRequest($request)
    {
// digunakan untuk mengecek reques apa yang di ijinkan untuk mengakses halaman ini.
        error_reporting('All');
        $cek_rq = $_SERVER['REQUEST_METHOD'];
        if ($cek_rq == $request) {
            return true;
        } else {
            return show_error('Maaf untuk Request data anda belum bisa kami proses.', '201', 'Reques di tolak...');
            exit();
        }
    }

    public function AksesMenu($menu_key = '', $akses = '')
    {
        // error_reporting('all');
        $session = $this->ddk->session->userdata('munu_crud');
        $proses = array_search($menu_key, array_column($session, 'menu_key'));
        $rs_data = $session[$proses];
        if ($rs_data[$akses] == '1') {
            return $session[$proses];
        } else {
            // $data['pesan'] = "";
            // return $this->ddk->tema->backend('backend/error_page/index', $data);
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Maaf anda belum memiliki akses di sistem ini.');
            return redirect(base_url());
        }
    }

    public function cekTokenAkses()
    {
        $rs_session = $this->ddk->session->userdata();
        if (empty($rs_session['sso_token'])) {
            error_reporting('all');
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Silahkan login terlebih dahulu, untuk mengakses data di sistem kami.');
            return redirect(base_url());
        }
        if (empty($rs_session)) {
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Maaf anda belum memiliki akses di sistem ini.');
            return redirect(base_url());
        }
        $token = $rs_session['sso_token'];
        $id_aplikasi = $rs_session['menu']['0']['id_aplikasi'];

        //cek_token
        $token = json_decode($this->ddk->curl->request('PUT', $this->sso_app() . 'api/cek_token', 'sso_token'), true);
        if ($this->id_aplikasi() != $id_aplikasi) {
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Maaf anda belum memiliki akses di sistem ini.');
            return redirect(base_url());
        }

        if ($token['result']['id_app'] == $id_aplikasi) {

        } else {
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Maaf anda belum memiliki akses di sistem ini.');
            return redirect(base_url());
        }

    }

    public function cek_token_rest()
    {
        $Authorization = $this->ddk->input->get_request_header('Authorization');

        $token = json_decode($this->ddk->curl->request_manual_tkn('PATCH', $this->base_server() . 'api/cek_token', $Authorization, ''), true);

        if ($token['result']['id_app'] == $this->id_aplikasi()) {

        } else {
            $this->ddk->session->unset_userdata('base_token');
            $this->ddk->session->unset_userdata('sso_token');
            $this->ddk->session->set_flashdata('pesan', 'Maaf anda belum memiliki akses di sistem ini.');
            return redirect(base_url());
        }
    }

    public function respons_data($data, $pesan, $status)
    {
//response data json
        $arr = array('result' => $data, 'message' => $pesan, 'status' => $status);
        $this->ddk->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // ->_display();
    }
    public function akses_root()
    {

    }

    public function get_server($nm_server = '')
    {
        return $this->ddk->session->userdata($nm_server);
    }

}
