<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Wfh extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
//==== ALLOWING CORS
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'e8a7b815-7035-11ea-9bcb-1cb72c27dd68';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }
    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['tgl'] = date('d-m-Y');
        $data['hari'] = $this->namahari();
        $data['jam'] = date("H:i:s");
        $os =    $this->getOS();

        if ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'Android') {
            $notifikasi = "0";
        } elseif ($os == 'BlackBerry') {
            $notifikasi = "0";
        } elseif ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'iPad') {
            $notifikasi = "1";
        } else {
            $notifikasi = "1";
        }
        $ip =  "";//$this->get_ip();

        if ($ip['isp'] == "Universitas Semarang") {
            $data['latitude'] = $ip['latitude'];
            $data['longitude'] = $ip['longitude'];
        } else {
            $data['latitude'] = "";
            $data['longitude'] = "";
       }
        $data['notifikasi'] = $notifikasi;

       $cek_presensi = $this->cek_presensi();


    // return print_r($cek_presensi);
        if (!empty($cek_presensi['result'])) {
            redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $cek_presensi['result']['id_absen']);
        }

       $this->tema->backend('absensi/wfh/index', $data);
    }
    public function get_ip()
    {
        error_reporting('All');
        $ip = $_SERVER['HTTP_X_REAL_IP'];
        $iptolocation = json_decode($this->curl->request('GET', "https://ip-api.com/json/$ip", 'base_token', ""), true);
        if ($iptolocation['isp'] == "Universitas Semarang") {
            $kordinat = array("isp" => "Universitas Semarang", "latitude" => "-6.9817454", "longitude" => "110.452002");
        } else {
            $kordinat = array("isp" => "", "latitude" => "", "longitude" => "");
        }
        return $kordinat;
    }

    public function presensi()
    {
        $data = array();
        $input = $this->input->post();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        if (empty($input)) {
            redirect(base_url() . 'absensi/wfh/');
        }

        if (empty($input["longitude"]) or empty($input["latitude"])) {
            $this->session->set_flashdata('pesan', "Maaf kordinattidak terdeteksi, Pastikan GPS HP aktif, dan ijin lokasi pada browser sudah aktif.");
            return redirect(base_url() . 'absensi/wfh/');
        }
        //$this->getOS()
        $arr = array("latitude" => $input["latitude"], "longitude" => $input["longitude"], "keterangan" => $this->getOS(), "latitude2" => $input['latitude2'], "longitude2" => $input['longitude2']);
        $presensi = $this->insert_presensi($arr);
        $this->session->set_flashdata('pesan', $presensi['message']);
        redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $presensi['result']['id_absen']);
    }
    public function getOS()
    {
        return true;
        $user_agent = $_SERVER["HTTP_USER_AGENT"];
        $os_platform = "Unknown OS Platform";
        $os_array = array(
            '/windows nt 10/i' => 'Windows 10',
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile',
        );
        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }
        return $os_platform;
    }
    public function mulai_kerja($id_presensi = '')
    {
        $data = array();
        if (empty($id_presensi)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $os = $this->getOS();

        if ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'Android') {
            $notifikasi = "0";
        } elseif ($os == 'BlackBerry') {
            $notifikasi = "0";
        } elseif ($os == 'iPhone') {
            $notifikasi = "0";
        } elseif ($os == 'iPad') {
            $notifikasi = "1";
        } else {
            $notifikasi = "1";
        }
        $ip = $this->get_ip();

        if ($ip['isp'] == "Universitas Semarang") {
            $data['latitude'] = $ip['latitude'];
            $data['longitude'] = $ip['longitude'];
        } else {
            $data['latitude'] = "";
            $data['longitude'] = "";
        }
        $data['notifikasi'] = $notifikasi;
        $data['wfh'] = $this->get_wfh(array('id_absen' => $id_presensi));
        $this->tema->backend('absensi/wfh/mulai_kerja', $data);
    }

    public function add_task_list()
    {
        $data = array();
        $input = $this->input->post();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        if (empty($input)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $insert_task = $this->insert_task($input);
        $this->session->set_flashdata('pesan', $presensi['message']);
        redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $insert_task['result']['id_absen']);
    }

    public function laporan($id_task = '')
    {
        $data = array();
        if (empty($id_task)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['task'] = $this->get_task(array('id_task' => $id_task));
        $this->tema->backend('absensi/wfh/laporan', $data);
    }
    public function update_laporan()
    {
        $data = array();
        $input = $this->input->post();
        $file = $_FILES['nama_file'];

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');

        if (empty($input)) {
            redirect(base_url() . 'absensi/wfh/');
        }

        // if (!empty($file) and $file['size'] < 1000) {
        //     $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan.. Ukuran File tidak sesuai standar kami');
        //     redirect(base_url() . 'absensi/wfh');
        // };

        $upload = json_decode($this->curl->upload($this->server_file . 'upload/laporan_pekerjaan', $file, 'sso_token'), 'TRUE');
        // return print_r($upload);
        if ($upload['status'] == '201') {
            $this->session->set_flashdata('pesan', $upload['message']);
            return redirect(base_url() . 'akd/dosen/presensi/kuliah/');
        }
        $array = array('id_task' => $input['id_task'], 'judul_kerjaan' => $input['judul_kerjaan'], 'keterangan' => $input['keterangan'], 'jam_riport' => $input['jam_riport'], 'dokumen' => $upload['nm_file'], 'link' => $input['link']);
        $task_riport = $this->task_riport($array);
        $this->session->set_flashdata('pesan', $presensi['message']);
        redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $task_riport['result']['id_absen']);
    }

    public function selesai_kerja()
    {
        $input = $this->input->post();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        if (empty($input)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $input = $this->input->post();
        $rs = array('latitude' => $input['latitude'], 'longitude' => $input['longitude'], 'keterangan' => $this->getOS()); 

        $selesai = $this->selesai($input['latitude'], $input['longitude'], $this->getOS(), $input['latitude2'], $input['longitude2']);
        $this->session->set_flashdata('pesan', $selesai['message']);

       

        redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $task_riport['result']['id_absen']);
    }
    public function delete_laporan($id_task = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        if (empty($id_task)) {
            redirect(base_url() . 'absensi/wfh/');
        }
        $delete_task = $this->delete_task(array('id_task' => $id_task));
        $this->session->set_flashdata('pesan', $presensi['message']);
        redirect(base_url() . 'absensi/wfh/mulai_kerja/' . $delete_task['result']['id_absen']);
    }
    public function namahari()
    {
        $tgl = date('d');
        $bln = date('m');
        $thn = date('Y');
        $info = date('w', mktime(0, 0, 0, $bln, $tgl, $thn));
        if ($info == '0') {
            $hari = "Minggu";
        } elseif ($info == '1') {
            $hari = "Senin";
        } elseif ($info == '2') {
            $hari = "Selasa";
        } elseif ($info == '3') {
            $hari = "Rabu";
        } elseif ($info == '4') {
            $hari = "Kamis";
        } elseif ($info == '5') {
            $hari = "Jumat";
        } elseif ($info == '6') {
            $hari = "Sabtu";
        } elseif ($info == '7') {

        }
        return $hari;
    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->get_download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents\
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

/*=============================================================
========================= Get Data  ===========================

=============================================================*/
    public function insert_presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/presensi', 'base_token', $data), true);
    }
    public function selesai($latitude = '', $longitude = '', $keterangan = '', $latitude2 = '', $longitude2 = '')
    {
        $data = json_encode(array("latitude" => $latitude, "longitude" => $longitude, "keterangan" => $keterangan, "latitude2" => $latitude2, "longitude2" => $longitude2));
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/selesai', 'base_token', $data), true);
    }
    public function cek_presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/cek_presensi', 'base_token', $data), true);
    }
    public function get_wfh($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/get_wfh', 'base_token', $data), true);
    }

    public function insert_task($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/insert_task', 'base_token', $data), true);
    }
    public function delete_task($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/wfh/delete_task', 'base_token', $data), true);
    }
    public function get_task($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/get_task', 'base_token', $data), true);
    }
    public function task_riport($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/wfh/task_riport', 'base_token', $data), true);
    }
    //
    public function get_download($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/wfh/download', 'base_token', $data), true);
    }
}
