<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ddk extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();

    }
    public function token_login($value = '')
    {
        $date = md5(base64_encode(date('m-Y-d')));
        return $date;
    }

    public function index()
    {

        $data = array();
        $data['token'] = $this->token_login();
        $this->tema->frond('frond_page/home/konten', $data);
    }

}
