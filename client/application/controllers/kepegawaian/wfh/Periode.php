<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Periode extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '2138e7a2-98a9-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['piket'] = $this->get_data();
        $this->tema->backend('kepegawaian/wfh/periode/index', $data);

    }

    public function simpan()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $input = $this->input->post();
        $simpan = $this->add_data($input);
        $this->session->set_flashdata('pesan', $simpan['message']);
        redirect(base_url() . 'kepegawaian/wfh/periode');

    }

    public function hapus_data($id = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'd');
        $delete = $this->delete_data(array("id_piket_wfh" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kepegawaian/wfh/periode');

    }
    public function edit_data($id = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $data['edit'] = $this->get_edit_data(array("id_piket_wfh" => dec_data($id)));
        $data['piket'] = $this->get_data();
        $this->tema->backend('kepegawaian/wfh/periode/edit', $data);
    }
    public function update()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $input = $this->input->post();
        $update = $this->update_data(array("id_piket_wfh" => dec_data($input['id_piket_wfh']), "periode_wfh" => $input["periode_wfh"], "tgl_mulai" => $input["tgl_mulai"], "tgl_selesai" => $input["tgl_selesai"], "keterangan" => $input["keterangan"]));
        $this->session->set_flashdata('pesan', $update['message']);
        redirect(base_url() . 'kepegawaian/wfh/periode');
    }
    // public function kelas_hapus()
    // {
    //     $this->apilib->AksesMenu($this->appUrl, 'd');
    //     $this->session->set_flashdata('pesan', 'Maaf terjadi masalah pada proses penghapusan kelas, silahkan ulangi lagi');
    //     redirect(base_url() . 'pasca/tu/jadwal');
    // }

    /*
     * transaksi
     */

    public function get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/periode/get', 'base_token', $data), true);
    }
    public function add_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kpg/wfh/periode/add', 'base_token', $data), true);
    }
    public function get_edit_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/wfh/periode/edit', 'base_token', $data), true);
    }
    public function update_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/wfh/periode/update', 'base_token', $data), true);
    }
    public function delete_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kpg/wfh/periode/delete', 'base_token', $data), true);
    }

}
