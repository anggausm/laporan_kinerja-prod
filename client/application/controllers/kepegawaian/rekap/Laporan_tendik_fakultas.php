<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Laporan_tendik_fakultas extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'dd3281ba-9581-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['fakultas'] = $this->get_fak();
        $this->tema->backend('kepegawaian/rekap/tendik_fakultas/index', $data);
    }

    public function anggota($id = '')
    {
        if (empty($id)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/rekap/laporan_tendik_fakultas');
        }
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['kode_fakultas'] = dec_data($id);
        $data['struktural'] = $this->list_anggota(array("kode_fakultas" => dec_data($id)));

        $this->tema->backend('kepegawaian/rekap/tendik_fakultas/anggota', $data);

    }

    public function rincian_kerja($id = '', $user_key = '', $bulan = '')
    {
        if (empty($id)) {
            $this->session->set_flashdata('pesan', "Maaf proses belum bisa kami lanjutkan.");
            redirect(base_url() . 'kepegawaian/rekap/laporan_tendik_fakultas');
        }
        if (empty($bulan)) {
            $bulan = date('Y-m');
        }
        $data['tahun'] = date('Y');
        $data['bulan'] = date('m');

        $data['kode_fakultas'] = dec_data($id);
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pekerjaan'] = $this->presensi(array("user_key" => dec_data($user_key), "kode_fakultas" => dec_data($id), "bulan" => $bulan));
        $this->tema->backend('kepegawaian/rekap/tendik_fakultas/rincian_kerja', $data);

    }
    public function bulan()
    {
        $input = $this->input->post();
        $data['kode_fakultas'] = $input['kode_fakultas'];
        $bulan = $input['tahun'] . '-' . $input['bulan'];
        $data['tahun'] = $input['tahun'];
        $data['bulan'] = $input['bulan'];
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pekerjaan'] = $this->presensi(array("user_key" => $input['user_key'], "kode_fakultas" => $input['kode_fakultas'], "bulan" => $bulan));
        $this->tema->backend('kepegawaian/rekap/tendik_fakultas/rincian_kerja', $data);

    }

    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->download(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }
    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_fak($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/tendik_fakultas/fak', 'base_token', $data), true);
    }
    public function list_anggota($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/tendik_fakultas/list_anggota', 'base_token', $data), true);
    }
    public function presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/tendik_fakultas/presensi', 'base_token', $data), true);
    }
    public function download($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/lap/tendik_fakultas/download', 'base_token', $data), true);
    }

}
