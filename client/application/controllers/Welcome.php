<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        //$this->load->view('welcome_message');
        // $data = array();
        $data['token'] = md5("ddk007");
        $this->tema->frond('frond_page/home/konten', $data);
        $this->session->sess_destroy();
        redirect($this->app->server_portal());
    }

    public function tc()
    {
        print "yes";
    }


    // public function cek_base_token($base_token = '')
    // {
    //    // return print $this->app->base_server() . 'api/cek_token';
    //     $base_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF9hcHAiOiI5ODY1NDM3NTgzMTkyODgzMiIsInVzZXJuYW1lIjoid2JzYW50b3NhIiwic3RhdHVzIjoiMSIsImtvZGVfa2h1c3VzIjpudWxsLCJ1c2VyX2tleSI6IjQ3MmVjMTJhLTgyMTItMTFlYS1iNThlLTU2Y2I4NzlmMmQ1NSIsImxldmVsX2tleSI6IjJmMmI3YjlhLTcwMDgtMTFlYS05MjE0LTFjYjcyYzI3ZGQ2OCIsImlhdCI6MTY2NDM1NjI3MCwiZXhwIjoxNjY0MzcwNjcwfQ.fiqvhcPo6aCJOu2XFH-zdH-jTkhCbZo17EfGtLfQQ8I";
    //     return print $this->app->base_server() . 'api/cek_token';
    //     return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    // }

}
