<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ajuan_jafa extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '9dc639fc-b528-11ea-95bf-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
    }

    public function index()
    {
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['get_data']   = $this->get_data();
        $this->tema->backend('sistem_lldikti/jafa/index', $data);
    }

    public function tambah()
    {
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['dosen']      = $this->get_dosen();
        $data['jafa']       = $this->get_jafa();
        $this->tema->backend('sistem_lldikti/jafa/tambah', $data);
    }

    public function simpan()
    {
        $input = $this->input->post();
        if (empty($input)) {
            redirect(base_url() . 'sistem_lldikti/ajuan_jafa');
        }

        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'c');
        $array          = array('user_key'      => $input['dosen'], 
                                'bidang_ilmu'   => $input['bidang'],
                                'id_jafa'       => $input['jafa']);
        
        $insert_notif   = $this->simpan_data($array);

        $this->session->set_flashdata('pesan', $insert_notif['message']);
        redirect(base_url() . 'sistem_lldikti/ajuan_jafa');
    }
 
    public function detail()
    {
        $input  = $this->input->post();
        $data   = array();
        if (empty($this->input->post())) {
            redirect(base_url() . 'sistem_lldikti/ajuan_jafa');
        }
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $data['status']     = $this->get_status(array('id' => dec_data($this->input->post('id'))));
        $data['detail']     = $this->get_detail(array('id_jenis' => dec_data($this->input->post('id'))));
        $this->tema->backend('sistem_lldikti/jafa/detail', $data);
    }

    public function edit()
    {
        $input              = $this->input->post();
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $ambil              = $this->get_edit(array('id_data' => dec_data($input['id'])));
        $data['ajuan']      = $ambil['result']['rs'];
        $data['dosen']      = $this->get_dosen();
        $data['jafa']       = $this->get_jafa();
        $data['jenis']      = $this->get_jenis();
        // print_r($data);
        $this->tema->backend('sistem_lldikti/jafa/edit', $data);
    }

    public function update()
    {
        $input = $this->input->post();
        if (empty($input)) {
            redirect(base_url() . 'sistem_lldikti/ajuan_jafa');
        }

        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'u');
        $array          = array('id_data'       => dec_data($input['id']),
                                'user_key'      => $input['dosen'], 
                                'bidang_ilmu'   => $input['bidang'],
                                'id_jafa'       => $input['jafa'],
                                'id_jafa_ajuan' => $input['status_ajuan']);
        
        $update_notif   = $this->update_data($array);
        // print_r($update_notif);
        $this->session->set_flashdata('pesan', $update_notif['message']);
        redirect(base_url() . 'sistem_lldikti/ajuan_jafa');
    }

    // =============================================== Get Data   ============================================================= 

    
    public function get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/default', 'base_token', $data), true);
    }

    public function get_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/detail', 'base_token', $data), true);
    }

    public function get_dosen($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_dosen', 'base_token', $data), true);
    }
    
    public function get_jafa($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_jafa', 'base_token', $data), true);
    }

    public function get_jenis($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_jenis', 'base_token', $data), true);
    }

    public function get_status($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_status', 'base_token', $data), true);
    }

    public function get_edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/get_edit', 'base_token', $data), true);
    }

    public function simpan_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/lldikti/jafa/simpan', 'base_token', $data), true);
    }

    public function update_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/lldikti/jafa/update', 'base_token', $data), true);
    }
}
