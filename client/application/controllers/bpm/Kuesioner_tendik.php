<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kuesioner_tendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '';
        $this->base_url = $this->app->get_server('base_server');
        $this->link = 'bpm/kuesioner_tendik/';
        $this->view = 'bpm/kuesioner_tendik/';
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $sudah_mengisi = $this->_get_sudah_mengisi()['result'];
        $dimensi_indikator_item = $this->_get_dimensi_indikator_item()['result'];
        if ($sudah_mengisi['st_sudah_mengisi'] == '1') {
            return redirect(base_url() . $this->link . 'sudah_mengisi');
        }
        $data['dimensi_indikator_item'] = $dimensi_indikator_item;
        $this->tema->backend($this->view . 'index', $data);
    }

    public function sudah_mengisi() {
        $data = array();
        $this->tema->backend($this->view . 'sudah_mengisi', $data);
    }

    public function simpan() {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'c');
        $input = $this->input->post();

        $penilaian = $input['data'];
        $arr = array(
            'penilaian' => $penilaian,
        );
        $insert = $this->_insert_bpm($arr);
        if ($insert['result'] === false or empty($insert)) {
            $this->session->set_flashdata('type', 'danger');
            $this->session->set_flashdata('pesan', $insert['message']);
        } else {
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('pesan', $insert['message']);
            return redirect(base_url() . $this->link . 'sudah_mengisi');
        }        
    }

    /* BASE API */

    public function _get_sudah_mengisi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/bpm/kuesioner_tendik/get_sudah_mengisi', 'base_token', $data), true);
    }
    public function _get_dimensi_indikator_item($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/bpm/kuesioner_tendik/get_dimensi_indikator_item', 'base_token', $data), true);
    }
    public function _insert_bpm($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/bpm/kuesioner_tendik/insert_bpm', 'base_token', $data), true);
    }
}