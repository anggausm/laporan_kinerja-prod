<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Presensi_online extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
//==== ALLOWING CORS
        date_default_timezone_set('Asia/Jakarta');
        $this->app->cek_token_rest();
        $this->appUrl = 'e8a7b815-7035-11ea-9bcb-1cb72c27dd68';
        $this->base_url = $this->app->base_server();
        $this->load->helper('download');
        $this->server_file = $this->app->server_file();

    }
    public function index()
    {
    }

    public function insert_presensi()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"));
        if (empty($put_data)) {
            return $this->app->respons_data(array(), "maaf reques adnd atidak bisa kami lanjutkan, kordinat posisi anda masih kosong", '200');
        }
        $data = json_encode(array("latitude" => $put_data->latitude, "longitude" => $put_data->longitude, "keterangan" => $put_data->keterangan));
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/presensi', $token, $data), true);
        return $this->app->respons_data($rs['result'], $rs['message'], '200');
    }
    public function selesai()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"));
        $data = json_encode(array("latitude" => $put_data->latitude, "longitude" => $put_data->longitude, "keterangan" => $put_data->keterangan));
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/selesai', $token, $data), true);
        return $this->app->respons_data($rs['result'], $rs['message'], $rs['status']);
    }
    public function cek_presensi()
    {
        $this->app->cekRequest('PATCH');
        $data = json_encode($array);
        $token = $this->input->get_request_header('Authorization');
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/cek_presensi_mobile', $token, $data), true);

        if ($rs['result']['id_presensi'] == '0') {
            return $this->app->respons_data(array("id_absen" => null, "detail" => null), "Silahkan melakukan presensi...", '200');
        } else {
            return $this->app->respons_data(array("id_absen" => $rs['result']['id_presensi'], "detail" => $rs['result']['detail_presensi']), $rs['message'], '200');
        }

    }

    /**
    LAPORAN KERJA
     **/

    //Download Data

    public function get_rencana_kerja()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"), true);

        $get_wfh = $this->get_wfh(array('id_absen' => $put_data['id_absen']), $token);
        return $this->app->respons_data($get_wfh['result'], $get_wfh['message'], '200');
    }
    public function get_wfh($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/get_wfh', $token, $data), true);
    }

    public function download_files($nama_file = '')
    {
        // $token = $this->input->get_request_header('Authorization');
        // $put_data = json_decode(file_get_contents("php://input"), true);

        // $rs_data = $this->get_download(array("id_task" => $id_task), $token);
        // $nama_file = $rs_data['result']['dokumen'];
        // $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents\
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

    public function get_download($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/wfh/download', $token, $data), true);
    }

    // Tambah rencana kera
    public function add_task_list()
    {

        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $input = json_decode(file_get_contents("php://input"), true);
        if (empty($input)) {
            return $this->app->respons_data(array(), "Data tidak boleh kosong", '200');
        }
        $insert_task = $this->insert_task($input, $token);
        return $this->app->respons_data($insert_task['result'], $insert_task['message'], '200');

    }
    public function insert_task($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/insert_task', $token, $data), true);
    }

    public function laporan()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $input = json_decode(file_get_contents("php://input"), true);

        if (empty($input['id_task'])) {
            return $this->app->respons_data(array(), "Maaf ID presensi belum ada", '200');
        }
        $data = $this->get_task(array('id_task' => $input['id_task']), $token);
        return $this->app->respons_data($data['result'], $data['message'], '200');
    }

    public function get_task($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/get_task', $token, $data), true);
    }

    public function update_laporan()
    {
        $this->app->cekRequest('POST');
        $token = $this->input->get_request_header('Authorization');
        $input = file_get_contents("php://input");

        $data = array();
        $input = $this->input->post();
        $file = $_FILES['nama_file'];

        if (empty($_POST)) {
            return $this->app->respons_data(array(), "Maaf data gagal di proses", '200');
        }

        $upload = json_decode($this->curl->upload_manual($this->server_file . 'upload/laporan_pekerjaan2', $file, $token), 'TRUE');

        if ($upload['status'] == '201') {
            return $this->app->respons_data(array(), "File gagal di upload", '200');
        }

        $array = array('id_task' => $_POST['id_task'], 'judul_kerjaan' => $_POST['judul_kerjaan'], 'keterangan' => $_POST['keterangan'], 'jam_riport' => $_POST['jam_riport'], 'dokumen' => $upload['nm_file'], 'link' => $_POST['link']);
        $task_riport = $this->task_riport($array, $token);
        return $this->app->respons_data(array(), $task_riport['message'], '200');
    }

    public function task_riport($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/task_riport', $token, $data), true);
    }

    public function delete_laporan()
    {
        $this->app->cekRequest('DELETE');
        $token = $this->input->get_request_header('Authorization');
        $input = json_decode(file_get_contents("php://input"), true);
        if (empty($input['id_task'])) {
            return $this->app->respons_data(array(), "Maaf data gagal di proses", '200');
        }
        $delete_task = $this->delete_task(array('id_task' => $input['id_task']), $token);
        return $this->app->respons_data($delete_task['result'], $delete_task['message'], '200');
    }

    public function delete_task($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('DELETE', $this->base_url . 'api/wfh/delete_task', $token, $data), true);
    }

    ///REST DATA

}
