<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Presensi_piket extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
//==== ALLOWING CORS
        date_default_timezone_set('Asia/Jakarta');
        $this->app->cek_token_rest();
        $this->appUrl = '1efcc005-965e-11ea-aca3-56cb879f2d55';
        $this->base_url = $this->app->base_server();

    }
    public function index()
    {
    }

    public function insert_presensi()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"));
        if (empty($put_data)) {
            return $this->app->respons_data(array(), "maaf reques adnd atidak bisa kami lanjutkan, kordinat posisi anda masih kosong", '200');
        }
        $data = json_encode(array("latitude" => $put_data->latitude, "longitude" => $put_data->longitude, "keterangan" => $put_data->keterangan));
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/piket/presensi', $token, $data), true);
        return $this->app->respons_data($rs['result'], $rs['message'], '200');
    }
    public function selesai()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"));
        $data = json_encode(array("latitude" => $put_data->latitude, "longitude" => $put_data->longitude, "keterangan" => $put_data->keterangan));
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/piket/selesai', $token, $data), true);
        return $this->app->respons_data($rs['result'], $rs['message'], $rs['status']);
    }
    public function cek_presensi()
    {
        $this->app->cekRequest('PATCH');
        $data = json_encode($array);
        $token = $this->input->get_request_header('Authorization');
        $rs = json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/piket/cek_presensi_mobile', $token, $data), true);

        if ($rs['result']['id_presensi'] == '0') {
            return $this->app->respons_data(array("id_absen" => null, "detail" => null), "Silahkan melakukan presensi...", '200');
        } else {
            return $this->app->respons_data(array("id_absen" => $rs['result']['id_presensi'], "detail" => $rs['result']['detail_presensi']), $rs['message'], '200');
        }

    }
}
