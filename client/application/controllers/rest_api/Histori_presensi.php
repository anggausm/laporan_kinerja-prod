<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Histori_presensi extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
//==== ALLOWING CORS
        date_default_timezone_set('Asia/Jakarta');
        $this->app->cek_token_rest();
        $this->appUrl = '662c130b-71c2-11ea-8d6b-1cb72c27dd68';
        $this->base_url = $this->app->base_server();
        $this->load->helper('download');
        $this->server_file = $this->app->server_file();

    }
    public function index()
    {
    }

    public function get_histori_default()
    {
        $this->app->cekRequest('PUT');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"), true);

        $data = $this->get_default($token);
        return $this->app->respons_data($data['result'], $data['message'], '200');
    }

    public function get_histori_presensi()
    {
        $this->app->cekRequest('PUT');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"), true);

        $data = $this->histori_laporan(array('bulan' => $put_data['bulan'], 'tahun' => $put_data['tahun']), $token);
        return $this->app->respons_data($data['result'], $data['message'], '200');
    }

    public function detail_presensi()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"), true);

        $data = $this->get_wfh(array('id_absen' => $put_data['id_absen']), $token);
        return $this->app->respons_data($data['result'], $data['message'], '200');
    }

    public function download_task()
    {
        $this->app->cekRequest('PUT');
        $token = $this->input->get_request_header('Authorization');
        $put_data = json_decode(file_get_contents("php://input"), true);

        $data = $this->get_wfh(array('id_task' => $put_data['id_task']), $token);
        return $this->app->respons_data($data['result'], $data['message'], '200');
    }

    //GET FROM API SERVER
    public function get_default($token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/histori/default', $token, $data), true);
    }
    public function histori_laporan($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/histori/histori_laporan', $token, $data), true);
    }
    public function get_wfh($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->base_url . 'api/wfh/get_wfh', $token, $data), true);
    }

    public function get_download($array = '', $token = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/wfh/download', $token, $data), true);
    }
}

// $route['api/presensi/histori/default']['PUT'] ="rest_api/histori_presensi/get_histori_default";
// $route['api/presensi/histori/laporan']['PUT'] ="rest_api/histori_presensi/get_histori_presensi";
// $route['api/presensi/histori/detail']['PATCH'] ="rest_api/histori_presensi/detail_presensi";
// $route['api/presensi/histori/download']['PUT'] ="rest_api/histori_presensi/download_task";