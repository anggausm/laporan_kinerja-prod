<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Download extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->server_file = $this->app->server_file();

    }
    public function index()
    {
    }

    public function lap_kerja($nama_file = '')
    {

        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents\
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }
}
