<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Beranda extends CI_Controller
{
//construktor
    public function __construct()
    {
        parent::__construct();
        $this->base_url = $this->app->base_server();
        error_reporting("ALL");

    }
    public function index()
    {

    }

    public function get_banner()
    {
        $this->app->cekRequest('PUT');
        $token = "";
        $data = "";
        $rs = json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/banner', $token, $data), true);
        return $this->app->respons_data($rs['result'], "banner portal USM", '200');
    }

    public function get_biodata()
    {
        $this->app->cekRequest('PUT');
        $token = $this->input->get_request_header('Authorization');
        $data = json_decode(file_get_contents("php://input"), true);
        $rs = json_decode($this->curl->request_manual_tkn('PUT', $this->base_url . 'api/biodata/tendik', $token, $data), true);
        return $this->app->respons_data($rs['result'], "biodata user", '200');
    }

}