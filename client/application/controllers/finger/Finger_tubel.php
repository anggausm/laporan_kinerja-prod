<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_tubel extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_tubel();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_tubel/';
        $this->link = 'finger/finger_tubel/';
        $this->isdm_url = $this->app->isdm_server();
    }

    public function index($id = '')
    {
        $data = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'r');
        $idne           = empty(dec_data($id)) ? '' : dec_data($id);
        $data['idne']   = $idne;
        $data['edit']   = $this->_edit(array("id" => $idne))['result'];
        $data['doskar'] = $this->_isdm_doskar_list()['result'];
        $data['dt']     = $this->_get_data()['result'];

        $this->tema->backend($this->view . 'index', $data);
    }

    public function insert()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();

        // ambil detai doskar dari isdm
        $doskar         = $this->_isdm_doskar_detail(array('user_key' => $post['key']))['result'];
        $post['nama']   = $doskar['nama_doskar'];

        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        if (empty($doskar['nama_doskar'])) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Nama Dosen/Karyawan tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $aksi   = $this->_insert($post);

        
        // return print(json_encode($post));



        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) 
        {
            $this->session->set_flashdata('type', 'alert-warning');
        }
        else
        {
            $this->session->set_flashdata('type', 'alert-success');
        }
        
        return redirect(base_url($this->link));
    }

    public function update()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();

        // ambil detai doskar dari isdm
        $doskar         = $this->_isdm_doskar_detail(array('user_key' => $post['key']))['result'];
        $post['nama']   = $doskar['nama_doskar'];
        
        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        if (empty($doskar['nama_doskar'])) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Nama Dosen/Karyawan tidak boleh kosong.");
            return redirect(base_url($this->link));
        }



        $post['id'] = dec_data($post['id_belajar']);

        // return print(json_encode($post));

        $aksi       = $this->_update($post);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link));
    }


    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_tubel/get_data', 'base_token', $data), true);
    }

    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_tubel/edit', 'base_token', $data), true);
    }

    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_tubel/simpan', 'base_token', $data), true);
    }

    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_tubel/update', 'base_token', $data), true);
    }

    
    public function _isdm_doskar_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    
    public function _isdm_doskar_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
}