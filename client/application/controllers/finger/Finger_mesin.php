<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_mesin extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_mesin();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_mesin/';
        $this->link = 'finger/finger_mesin/';
    }

    public function index($id_mesin = '')
    {
        $data = array();
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'r');
        $id_msn             = empty(dec_data($id_mesin)) ? '' : dec_data($id_mesin);
        $data['id_mesin']   = $id_msn;
        $data['dt']         = $this->_edit(array("id_mesin" => $id_msn))['result'];
        $data['mesin']      = $this->_get_mesin()['result'];

        $this->tema->backend($this->view . 'index', $data);
    }

    public function insert()
    {
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'c');
        $post = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $aksi   = $this->_insert($post);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) 
        {
            $this->session->set_flashdata('type', 'alert-warning');
        }
        else
        {
            $this->session->set_flashdata('type', 'alert-success');
        }
        
        return redirect(base_url($this->link));
    }

    public function update()
    {
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'u');
        $post = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $post['id'] = dec_data($post['id_mesin']);
        $aksi       = $this->_update($post);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link));
    }


    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _get_mesin($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_mesin/get_data', 'base_token', $data), true);
    }

    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_mesin/edit', 'base_token', $data), true);
    }

    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_mesin/simpan', 'base_token', $data), true);
    }

    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_mesin/update', 'base_token', $data), true);
    }
}