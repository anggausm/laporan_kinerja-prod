<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_ijin extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_ijin();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_ijin/';
        $this->link = 'finger/finger_ijin/';
        $this->isdm_url = $this->app->isdm_server();
    }

    public function index()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'r');
        $post           = $this->input->post();

        if (!empty($post['bulan'] && empty(!$post['tahun']))) {
            $bt         = "".$post['tahun']."-".$post['bulan']."";
        } else {
            $bt         = date('Y-m');
        }

        $parameter      = array('bulan_tahun' => $bt);
        $data['periode']= $parameter;
        $data['dt']     = $this->_get_data($parameter)['result'];

        $this->tema->backend($this->view . 'index', $data);
    }
   
    public function tambah()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $data['doskar'] = $this->_isdm_doskar_list()['result'];
        $data['jenis']  = $this->_jenis_ijin()['result'];

        $this->tema->backend($this->view . 'tambah', $data);
    }

    public function simpan()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        if (empty($post['akhir'])) {
            $akhir = $post['awal'];
        } else {
            $akhir = $post['akhir'];
        }

        if ($akhir < $post['awal']) {
            $this->session->set_flashdata('pesan', 'Tanggal akhir tidak boleh kurang dari tanggal awal');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        // ambil detai doskar
        $doskar = $this->_isdm_doskar_detail(array('user_key' => $post['user_key']))['result'];

        // hitung lama ijin
        $tanggal_1  = date_create($post['awal']);
        $tanggal_2  = date_create($akhir);
        $selisih    = date_diff($tanggal_1, $tanggal_2);
        $lama       = $selisih->days + 1;

        $simpan = array('user_key'      => $post['user_key'],
                        'id_doskar'     => $doskar['id_doskar'],
                        'nama_doskar'   => $doskar['nama_doskar'],
                        'tanggal_awal'  => $post['awal'],
                        'tanggal_akhir' => $akhir,
                        'jenis_ijin'    => $post['jenis'],
                        'keterangan'    => $post['keterangan'],
                        'lama'          => $lama,
                        'status'        => 'Acc'
                    );

        $aksi   = $this->_insert($simpan);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }
   
    public function edit()
    {
        
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        if (empty($post['akhir'])) {
            $akhir = $post['awal'];
        } else {
            $akhir = $post['akhir'];
        }

        if ($post['awal'] < $akhir) {
            $this->session->set_flashdata('pesan', 'Tanggal akhir tidak boleh kurang dari tanggal awal');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }
        
        $data['doskar'] = $this->_isdm_doskar_list()['result'];
        $data['jenis']  = $this->_jenis_ijin()['result'];
        $data['dt']     = $this->_edit(array('id_ijin' => dec_data($post['id_ijin'])))['result'];

        $this->tema->backend($this->view . 'edit', $data);
    }
 
    public function update() 
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        if (empty($post['akhir'])) {
            $akhir = $post['awal'];
        } else {
            $akhir = $post['akhir'];
        }

        if ($akhir < $post['awal']) {
            $this->session->set_flashdata('pesan', 'Tanggal akhir tidak boleh kurang dari tanggal awal');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        // ambil detai doskar
        $doskar = $this->_isdm_doskar_detail(array('user_key' => $post['user_key']))['result'];

        // hitung lama ijin
        $tanggal_1  = date_create($post['awal']);
        $tanggal_2  = date_create($akhir);
        $selisih    = date_diff($tanggal_1, $tanggal_2);
        $lama       = $selisih->days + 1;

        $update['set'] = array('user_key'  => $post['user_key'],
                        'id_doskar'     => $doskar['id_doskar'],
                        'nama_doskar'   => $doskar['nama_doskar'],
                        'tanggal_awal'  => $post['awal'],
                        'tanggal_akhir' => $akhir,
                        'jenis_ijin'    => $post['jenis'],
                        'keterangan'    => $post['keterangan'],
                        'lama'          => $lama
                    );
        $update['where'] = array('id_ijin' => dec_data($post['id_ijin']));

        $aksi   = $this->_update($update);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }
 
    public function hapus() 
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        $delete['dt']    = $this->_edit(array('id_ijin' => dec_data($post['id_ijin'])))['result'];
        $delete['where'] = array('id_ijin' => dec_data($post['id_ijin']));

        // return print_r($delete);

        $aksi   = $this->_delete($delete);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }
    

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    

    public function _jenis_ijin($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_ijin/get_jenis', 'base_token', $data), true);
    }
    
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_ijin/get_data', 'base_token', $data), true);
    }

    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_ijin/simpan', 'base_token', $data), true);
    }

    public function _edit($array = "")
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_ijin/edit', 'base_token', $data), true);
    }

    public function _update($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_ijin/update', 'base_token', $data), true);
    }

    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kpg/finger_ijin/delete', 'base_token', $data), true);
    }

    // API TO ISDM
    public function _isdm_doskar_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_doskar_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
}
