<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_rekappresensi extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_rekappresensi();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_rekappresensi/';
        $this->link = 'finger/finger_rekappresensi/';
        $this->isdm_url = $this->app->isdm_server();
    }

    public function index()
    {
        $data['crud'] = $this->app->AksesMenu($this->menu_key, 'r');
        $post         = $this->input->post();

        if (empty($post['akhir'])) {
            $akhir = $post['awal'];
        } else {
            $akhir = $post['akhir'];
        }

        if ($akhir < $post['awal']) {
            $this->session->set_flashdata('pesan', 'Tanggal akhir tidak boleh kurang dari tanggal awal');
            $this->session->set_flashdata('type', 'alert-warning');

            return redirect(base_url($this->link));
        }

        $data['periode'] = array('awal' => $post['awal'], 'akhir' => $post['akhir']);
        $data['detail']  = $this->_get_data(array('awal' => $post['awal'], 'akhir' => $akhir))['result'];

        $this->tema->backend($this->view . 'index', $data);
    }

    public function info($user_key = '', $tgl_awal = '', $tgl_akhir = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->menu_key, 'r');
        $key    = dec_data_url($user_key);
        $awal   = dec_data_url($tgl_awal);
        $akhir  = dec_data_url($tgl_akhir);

        $data['periode'] = array('awal' => $awal, 'akhir' => $akhir, 'user_key' => $key);
        $data['dt']      = $this->_get_detail(array('awal' => $awal, 'akhir' => $akhir, 'user_key' => $key))['result'];

        $this->tema->backend($this->view . 'info', $data);
    }

    public function update()
    {
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'u');
        $post = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $send['id']     = dec_data($post['id_transport']);
        $send['status'] = dec_data($post['hitung_transport']);

        $aksi           = $this->_update($send);

        $url            = "" . $post['key'] . "/" . $post['awal'] . "/" . $post['akhir'] . "";
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link . 'info' . $url));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link . 'info/' . $url));
    }

    public function download_transport_excel()
    {
        $data['crud'] = $this->app->AksesMenu($this->menu_key, 'r');
        $post         = $this->input->post();
        $awal         = dec_data($post['awal']);
        $akhir        = dec_data($post['akhir']);
        $data['periode']      = array('awal' => $awal, 'akhir' => $akhir);
        $data['transport']    = $this->_get_data(array('awal' => $awal, 'akhir' => $akhir))['result'];
        
        $this->load->view($this->view . 'rekap_excel', $data);
    }


    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_rekap_presensi/get_data', 'base_token', $data), true);
    }

    public function _get_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_rekap_presensi/detail', 'base_token', $data), true);
    }

    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_rekap_presensi/update', 'base_token', $data), true);
    }



    public function _isdm_doskar_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_doskar_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
}
