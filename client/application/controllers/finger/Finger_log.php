<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_log extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_log();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_log/';
        $this->link = 'finger/finger_log/';
    }

    public function index()
    {
        $data['crud'] = $this->app->AksesMenu($this->menu_key, 'r');
        $post         = $this->input->post();

        if (empty($post['akhir'])) {
            $akhir = $post['awal'];
        } else {
            $akhir = $post['akhir'];
        }

        if ($akhir < $post['awal']) {
            $this->session->set_flashdata('pesan', 'Tanggal akhir tidak boleh kurang dari tanggal awal');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        $data['awal'] = $post['awal'];
        $data['akhir']= $akhir;
        $data['dt']   = $this->_get_data(array('finger_id' => $post['finger_id'], 'awal' => $post['awal'], 'akhir' => $akhir))['result'];

        $this->tema->backend($this->view . 'index', $data);
    }
    /*
    public function insert()
    {
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'c');
        $post = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $aksi   = $this->_insert($post);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) 
        {
            $this->session->set_flashdata('type', 'alert-warning');
        }
        else
        {
            $this->session->set_flashdata('type', 'alert-success');
        }
        
        return redirect(base_url($this->link));
    }

    public function update()
    {
        $data['crud']       = $this->app->AksesMenu($this->menu_key, 'u');
        $post = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $post['id'] = dec_data($post['id_mesin']);
        $aksi       = $this->_update($post);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link));
    }
    */

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_logs/get_data', 'base_token', $data), true);
    }

    /*
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_logs/edit', 'base_token', $data), true);
    }

    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_logs/simpan', 'base_token', $data), true);
    }

    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_logs/update', 'base_token', $data), true);
    }
    */
}