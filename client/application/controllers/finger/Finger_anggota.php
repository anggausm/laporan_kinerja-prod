<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_anggota extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_anggota();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_anggota/';
        $this->link = 'finger/finger_anggota/';
        $this->isdm_url = $this->app->isdm_server();
    }

    public function index()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'r');
        $data['dt']     = $this->_get_data()['result'];

        $this->tema->backend($this->view . 'index', $data);
    }
   
    public function tambah()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $data['doskar'] = $this->_isdm_doskar_list()['result'];
        $data['dt']     = $this->_add()['result'];

        $this->tema->backend($this->view . 'tambah', $data);
    }

    public function simpan()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        // ambil detai doskar
        $doskar = $this->_isdm_doskar_detail(array('user_key' => $post['user_key']))['result'];

        $simpan = array('user_key'       => $post['user_key'],
                        'id_doskar'      => $doskar['id_doskar'],
                        'finger_id'      => $post['finger_id'],
                        'nickname'       => $post['nickname'],
                        'id_setting_jam' => $post['jam'],
                        'id_tarif'       => $post['tarif'],
                        'shift'          => $post['shift'],
                        'status'         => 'Finjer'
                    );

        $aksi   = $this->_insert($simpan);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }
   
    public function edit()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }
        
        $data['doskar']     = $this->_isdm_doskar_list()['result'];
        $data['id_peserta'] = dec_data($post['id_peserta']);
        $data['edit']       = $this->_edit(array('id_peserta' => dec_data($post['id_peserta'])))['result'];

        $this->tema->backend($this->view . 'edit', $data);
    }
 
    public function update() 
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        $post['id_peserta'] = dec_data($post['id_peserta']);
        $aksi               = $this->_update($post);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }
 
    public function hapus() 
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Data tidak boleh kosong.');
            $this->session->set_flashdata('type', 'alert-warning');
            
            return redirect(base_url($this->link));
        }

        $delete['dt']           = $this->_edit(array('id_peserta' => dec_data($post['id_peserta'])))['result']['dt'];
        $delete['id_peserta']   = dec_data($post['id_peserta']);

        // return print_r($delete);

        $aksi   = $this->_delete($delete);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        
        return redirect(base_url($this->link));
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    

    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_anggota/get_data', 'base_token', $data), true);
    }
    
    public function _add($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_anggota/tambah', 'base_token', $data), true);
    }
    
    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_anggota/simpan', 'base_token', $data), true);
    }

    public function _edit($array = "")
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_anggota/edit', 'base_token', $data), true);
    }

    public function _update($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_anggota/update', 'base_token', $data), true);
    }

    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kpg/finger_anggota/delete', 'base_token', $data), true);
    }

    // API TO ISDM
    public function _isdm_doskar_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_doskar_detail($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
}