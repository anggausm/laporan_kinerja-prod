<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_harikhusus extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_harikhusus();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_harikhusus/';
        $this->link = 'finger/finger_harikhusus/';
    }

    public function index($id = '')
    {
        $data = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'r');
        $idne           = empty(dec_data($id)) ? '' : dec_data($id);
        $data['idne']   = $idne;
        $data['edit']   = $this->_edit(array("id" => $idne))['result'];
        $data['dt']     = $this->_get_data()['result']['data'];
        $data['jenis']  = $this->_get_data()['result']['jenis'];

        $this->tema->backend($this->view . 'index', $data);
    }

    public function insert()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();


        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $aksi   = $this->_insert($post);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) 
        {
            $this->session->set_flashdata('type', 'alert-warning');
        }
        else
        {
            $this->session->set_flashdata('type', 'alert-success');
        }
        
        return redirect(base_url($this->link));
    }

    public function update()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'u');
        $post           = $this->input->post();
        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $post['id'] = dec_data($post['id']);
        $aksi       = $this->_update($post);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link));
    }

    public function destroy($id = '')
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'd');
        $post           = array('id' => dec_data($id));

        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $aksi = $this->_destroy($post);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
            return redirect(base_url($this->link));
        }
        $this->session->set_flashdata('type', 'alert-success');
        return redirect(base_url($this->link));
    }


    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_harikhusus/get_data', 'base_token', $data), true);
    }

    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_harikhusus/edit', 'base_token', $data), true);
    }

    public function _insert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_harikhusus/simpan', 'base_token', $data), true);
    }

    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kpg/finger_harikhusus/update', 'base_token', $data), true);
    }

    public function _destroy($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kpg/finger_harikhusus/delete', 'base_token', $data), true);
    }
}