<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_transport extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kepegawaian_olahtransport();
        $this->base_url = $this->app->get_server('base_server');
        $this->view = 'backend/kepegawaian/finger_transport/';
        $this->link = 'finger/finger_transport/';
    }

    public function index($get_tgl = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'r');
        $post           = $this->input->post();
        $tgl_pilih      = empty($post['tanggal']) ? dec_data($get_tgl) : $post['tanggal'];
        // $tgl_lalu   = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
        $tgl            = empty($tgl_pilih) ? date('Y-m-d') : $tgl_pilih;

        $data['tanggal']    = $tgl;
        $data['transport']  = $this->_get_data(array("tanggal" => $tgl))['result'];

        $this->tema->backend($this->view . 'index', $data);
    }

    public function olah_transport()
    {
        $data['crud']   = $this->app->AksesMenu($this->menu_key, 'c');
        $post           = $this->input->post();
        $tgl            = dec_data($post['tanggal']);

        if (empty($post)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', "Maaf, Data tidak boleh kosong.");
            return redirect(base_url($this->link));
        }

        $arr  = array('tanggal' => $tgl, 'jenis' => $post['jenis']);          
        $aksi = $this->_get_olah($arr);

        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('type', 'alert-warning');
        }
        $this->session->set_flashdata('type', 'alert-success');

        return redirect(base_url($this->link . 'index/' . $post['tanggal']));
    }

    // =============================================================
    // ========================= Get Data  =========================
    // =============================================================
    public function _get_data($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_olah/get_data', 'base_token', $data), true);
    }

    public function _get_olah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kpg/finger_olah/get_olah', 'base_token', $data), true);
    }
}