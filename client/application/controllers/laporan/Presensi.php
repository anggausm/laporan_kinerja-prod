<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Presensi extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'e3380e53-87c0-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['rs_data'] = $this->anggota();
        $this->tema->backend('laporan/presensi/index', $data);
    }

    public function detail_presesni()
    {
        $input = $this->input->post();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        if (empty($input)) {
            redirect(base_url() . 'laporan/presensi');
        }
        if (!empty($this->input->post('bulan')) or !empty($this->input->post('tahun'))) {
            $data['bulan'] = $this->input->post('bulan');
            $data['tahun'] = $this->input->post('tahun');
            $arr = array("user_key" => dec_data($input['user_key']), "id_sub_struktural" => dec_data($input['id_sub_struktural']), "bulan" => $this->input->post('bulan'), "tahun" => $this->input->post('tahun'));
        } else {
            $data['bulan'] = date('m');
            $data['tahun'] = date('Y');
            $arr = array("user_key" => dec_data($input['user_key']), "id_sub_struktural" => dec_data($input['id_sub_struktural']));
        }
        $data['base_url'] = base_url();
        $data_presensi = $this->data_presensi($arr);
        $data['rs_data'] = $data_presensi;
        $this->tema->backend('laporan/presensi/presensi', $data);
    }

    public function get_task_list()
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $this->app->cekRequest('PUT');
        $data = file_get_contents("php://input");
        return print $this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/detail_task', 'base_token', $data);
        // return print $this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/anggota', 'base_token', $data);

    }
    public function download_files($id = '')
    {
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $rs_data = $this->download_file(array("id_task" => $id));
        $nama_file = $rs_data['result']['dokumen'];
        $file = $rs_data['result']['jdl_file'];
        $server_file = $this->server_file . 'file/laporan_pekerjaan/' . $nama_file;
        $data = file_get_contents("$server_file"); // Read the file's contents
        $ext = explode('.', $nama_file);
        $name = date('Ymd') . '_' . $file . '.' . $ext[1];
        return force_download($name, $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function anggota($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/anggota', 'base_token', $data), true);
    }
    public function data_presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/data_presensi', 'base_token', $data), true);
    }
    public function detail_task($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/detail_task', 'base_token', $data), true);
    }
    public function download_file($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/laporan/presensi/download_file', 'base_token', $data), true);
    }

}
