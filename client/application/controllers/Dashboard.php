<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->url_sso = $this->app->sso_app();
        $this->server_file = $this->app->server_file();
        $this->sia_url = $this->app->sia_server();
        $this->tendik_client = 'https://apps.usm.ac.id/tdk/';

    }

    public function index($base_token = '', $sso_token = '')
    {
        $this->token = $this->app->cek_token();
        $data = array();
        $data['menu'] = $this->session->userdata('menu');
        $sudah_mengisi_kuesioner_bpm = $this->_get_sudah_mengisi_kuesioner_bpm()['result'];
        if ($sudah_mengisi_kuesioner_bpm['st_sudah_mengisi'] == '0') {
            return redirect($this->tendik_client . 'bpm/kuesioner_tendik');
        }

        $data['sudah_mengisi_kuesioner_bpm'] = $sudah_mengisi_kuesioner_bpm;

        $this->tema->backend('dashboard/base', $data);

    } 

    public function app($base_token = '', $sso_token = '')
    {
     //return print $base_token; 
        //return print $surat_token . '<br>' . $sso_token;
        $data = array();
        if (empty($base_token) or empty($sso_token)) { 
            $this->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
            redirect($this->app->server_portal());
        }
        $bt = $this->cek_base_token($base_token); 


       
        // $sso = $this->cek_sso_token($sso_token);

        if ($bt['result']['id_app'] == $this->app->id_aplikasi()) {
            $this->session->set_userdata(array('sso_token' => $sso_token));
            return redirect(base_url() . 'app/routes/' . $bt['result']['id_app'] . '/' . $bt['result']['level_key']);
        }
        // $this->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
        // redirect($this->app->server_portal());

    }

    public function cek_base_token($base_token = '')
    {
       // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }
    public function cek_sso_token($sso_token = '')
    {
        $json_data = json_encode(array());
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->server_sso() . 'api/cek/global_token', $sso_token, $json_data), true);
    }

    /* WS SIA */
    public function _get_sudah_mengisi_kuesioner_bpm($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->sia_url . 'api/bpm/kuesioner_tendik/get_sudah_mengisi', 'sia_token', $data), true);
    }

}
