<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Unit_lembaga extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '7a9bff75-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['data'] = $this->_restruktur_unit_lembaga();
        $this->tema->backend('restruktur/unit_lembaga/index', $data);
    }
    public function edit($id_sub_struktural = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'u');
        $edit = $this->_edit(array('id_sub_struktural' => $id_sub_struktural));
        $data['struktural'] = $edit['struktural'];
        $data['anggota'] = $edit['anggota'];
        $data['doskar_usm'] = $this->_doskar_usm();
        $this->tema->backend('restruktur/unit_lembaga/edit', $data);
    }
    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'user_key' => $post['doskar_usm'],
            'id_sub_struktural' => $post['id_sub_struktural'],
        );
        $aksi = $this->_simpan($data);
        $this->session->set_flashdata('pesan', $aksi['message']);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        return redirect(site_url("restruktur/unit_lembaga/edit/$post[id_sub_struktural]"), 'refresh');
    }

    public function delete($id_sub_struktural, $id_angota_struktural)
    {
        $data = array(
            'id_angota_struktural' => $id_angota_struktural,
        );
        $aksi = $this->_delete($data);
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('type', 'danger');
        } else {
            $this->session->set_flashdata('type', 'success');
        }
        $this->session->set_flashdata('pesan', $aksi['message']);
        return redirect(site_url("restruktur/unit_lembaga/edit/$id_sub_struktural"), 'refresh');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _restruktur_unit_lembaga($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/unit_lembaga', 'base_token', $data), true)['result'];
    }
    public function _edit($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/unit_lembaga/edit', 'base_token', $data), true)['result'];
    }
    public function _simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/restruktur/unit_lembaga/simpan', 'base_token', $data), true);
    }
    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/restruktur/unit_lembaga/delete', 'base_token', $data), true);
    }

    public function _doskar_usm($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/restruktur/unit_lembaga/doskar_usm', 'base_token', $data), true)['result'];
    }
}
