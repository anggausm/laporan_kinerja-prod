<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * @property CI_App $app
 * @property CI_Session $session
 * @property Lib_menu_key $Lib_menu_key
 */
class Penilaian_diri extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->base_url = $this->app->get_server('base_server');
        $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->isdm_url = $this->app->isdm_server();
        $this->view = 'backend/kinerja/kependidikan/penilaian_diri/';
        $this->link = 'kinerja/kependidikan/penilaian_diri/';
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $session    = $this->session->userdata();
        $user       = $this->cek_user($session['base_token'])['result'];
        $data['profil'] = $this->_isdm_profil(array('user_key' => $user['user_key']))['result'];
        $data['periode'] = $this->_periode()['result'];
        $data['tanya']  = $this->_pernyataan()['result']['data'];
        $data['button'] = $this->_pernyataan()['result']['status'];

        // return print_r($data);

        $this->tema->backend($this->view . 'index', $data);
    }

    public function simpan()
    {
        $periode = $this->_periode()['result'];

        if (date('Y-m-d') >= $periode['pengisian_mulai'] && date('Y-m-d') <= $periode['pengisian_selesai']) {
            $data    = array();
            $post         = $this->input->post();

            foreach ($post['nilai'] as $key => $value) {
                $jawaban[] = array('id_pertanyaan' => $key, 'nilai' => $value['nilai']);
            }
            $response  = $this->_save($post);

            if ($response['result'] === true) {
                $this->session->set_flashdata('pesan', 'Data berhasil disimpan');
                $this->session->set_flashdata('type', 'alert-success');
            } else {
                $this->session->set_flashdata('pesan', 'Data gagal disimpan.');
                $this->session->set_flashdata('type', 'alert-danger');
            }
        } else {
            $this->session->set_flashdata('pesan', 'Data gagal disimpan, diluar periode pengisian.');
            $this->session->set_flashdata('type', 'alert-danger');
        }

        return redirect(base_url($this->link));
    }



    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function cek_user($base_token = '')
    {
        // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }

    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _pernyataan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/kependidikan/pertanyaan', 'base_token', $data), true);
    }

    public function _periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/kependidikan/pertanyaan/periode', 'base_token', $data), true);
    }

    public function _save($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/kependidikan/pertanyaan/simpan', 'base_token', $data), true);
    }
}
