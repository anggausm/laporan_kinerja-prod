<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * @property CI_App $app
 * @property CI_Session $session
 * @property Lib_menu_key $Lib_menu_key
 */

class Penilaian_universitas extends CI_Controller
{
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->base_url = $this->app->get_server('base_server');
        // $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->isdm_url = $this->app->isdm_server();
        $this->view = 'backend/kinerja/kependidikan/penilaian_universitas/';
        $this->link = 'kinerja/kependidikan/penilaian_universitas/';
    }

    public function index()
    {
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $session    = $this->session->userdata();
        $user       = $this->_cek_token($session['base_token'])['result'];

        $get_nilai = $this->_get_asesi()['result'];
        $data['data']  = $get_nilai;
        $data['detail_kinerja']['status_pimpinan']  = '1';
        $this->tema->backend($this->view . 'index', $data);
    }
    public function detail($user_key, $jenis_pekerjaan)
    {
        $jenis_pekerjaan = dec_data_url($jenis_pekerjaan);
        $user_key = dec_data_url($user_key);
        $arr = array('jenis_pekerjaan' => $jenis_pekerjaan, 'dinilai_user_key' => $user_key);
        $get_pertanyaan = $this->_get_pertanyaan($arr)['result'];

        $data['profil'] = $this->_isdm_profil(array('user_key' => $user_key))['result'];
        $data['jenis_pekerjaan'] = $jenis_pekerjaan;
        $data['user_key'] = $user_key;
        $data['tanya'] = $get_pertanyaan['pertanyaan'];
        $data['disabled'] = $get_pertanyaan['disabled'];
        $data['periode'] = $get_pertanyaan['periode'];
        $data['kinerja_ajuan'] = $get_pertanyaan['kinerja_ajuan'];
        $data['disabled_verifikasi'] = $get_pertanyaan['disabled_verifikasi'];
        $this->tema->backend($this->view . 'form_dosen', $data);
    }
    public function simpan()
    {
        $input = $this->input->post();
        $input['jenis_pekerjaan'] = dec_data_url($input['jenis_pekerjaan']);
        $input['dinilai_user_key'] = dec_data_url($input['dinilai_user_key']);
        $arr_simpan = array();
        // RE Data DECRYPT
        foreach ($input['kinerja_nilai'] as $key => $value) {
            foreach ($value as $key2 => $value2) {
                // jangan dec_data_url
                if ($key2 == 'jumlah_univ' || $key2 == 'keterangan_univ') {
                    $value2 = $value2;
                } else {
                    $value2 = dec_data($value2);
                }
                $arr_simpan[$key][$key2] = $value2;
            }
        }
        $datas['jenis_pekerjaan'] = $input['jenis_pekerjaan'];
        $datas['dinilai_user_key'] = $input['dinilai_user_key'];
        $datas['kinerja_nilai'] = $arr_simpan;
        $response  = $this->_save($datas);

        $this->session->set_flashdata('pesan', $response['message']);
        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        return redirect(base_url($this->link . 'detail/' . enc_data_url($input['dinilai_user_key']) . '/' . enc_data_url($input['jenis_pekerjaan'])));
    }
    public function verifikasi()
    {
        $input = $this->input->post();
        $input['jenis_pekerjaan'] = dec_data_url($input['jenis_pekerjaan']);
        $input['dinilai_user_key'] = dec_data_url($input['dinilai_user_key']);
        $response  = $this->_verifikasi($input);
        $this->session->set_flashdata('pesan', $response['message']);
        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        return redirect(base_url($this->link . 'detail/' . enc_data_url($input['dinilai_user_key']) . '/' . enc_data_url($input['jenis_pekerjaan'])));
    }



    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function _cek_token($base_token = '')
    {
        // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }

    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _isdm_detail_kinerja_akses($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_detail_kinerja_akses', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _isdm_doskar_list($array = array())
    {
        $array['keperluan'] = 'penilaian_kinerja';
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _get_asesi($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/kependidikan/penilaian_universitas/get_asesi', 'base_token', $data), true);
    }
    public function _get_pertanyaan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/kependidikan/penilaian_universitas/get_pertanyaan', 'base_token', $data), true);
    }
    public function _save($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/kependidikan/penilaian_universitas/save', 'base_token', $data), true);
    }
    public function _verifikasi($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/kependidikan/penilaian_universitas/verifikasi', 'base_token', $data), true);
    }
}
