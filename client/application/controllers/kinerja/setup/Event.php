<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Event extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '84480e89-64da-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->title = 'Event';

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        // if (!empty($this->input->post())) {
        //     $data['bulan'] = $this->input->post('bulan');
        //     $data['tahun'] = $this->input->post('tahun');
        //     $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        // } else {
        //     $data['bulan'] = date('m');
        //     $data['tahun'] = date('Y');
            
        //     $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        // }
        $data['event'] =   $this->get_default()['result'];

         $data['jenis'] = array(
            array('id_stat' => 'Harmoni', 'nama_stat' => 'Harmoni'),
            array('id_stat' => 'Integritas', 'nama_stat' => 'Integritas')
        );

        $this->tema->backend('backend/kinerja/setup_event/index', $data);
    }

     public function tambah()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pa'] =  $this->_periode_aktif()['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/setup_event/tambah', $data);
    }

    public function simpan()
    {
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'c');
        $post = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/setup/event/');
        }

                 $datainsert = array(
                        'nama_event' => $post['nama_event'],
                        'tgl_awal' => $post['tgl_awal'],
                        'tgl_akhir' => $post['tgl_akhir'],
                        'jam_awal' => $post['jam_awal'],
                        'jam_akhir' => $post['jam_akhir'],
                        'lokasi_event' => $post['lokasi'],
                        'ket' => $post['ket']
                    );
                 
                $aksi = $this->_simpan($datainsert);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/setup/event/index');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/setup/event/index');
                }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];
        $data['event'] =   $this->get_default()['result'];

         $data['jenis'] = array(
            array('id_stat' => 'Harmoni', 'nama_stat' => 'Harmoni'),
            array('id_stat' => 'Integritas', 'nama_stat' => 'Integritas')
        );

        $this->tema->backend('backend/kinerja/setup_event/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        
    
            $data_update = array(
            	'id_event' => dec_data($post['id_event']),
                'nama_event' => $post['nama_event'],
                'tgl_awal' => $post['tgl_awal'],
                'tgl_akhir' => $post['tgl_akhir'],
                'jam_awal' => $post['jam_awal'],
                'jam_akhir' => $post['jam_akhir'],
                'lokasi_event' => $post['lokasi'],
                'ket' => $post['ket']
            );
            // var_dump($data_update); die();

            $aksi = $this->_update($data_update);
             
            if ($aksi['result'] === false) {
                $id_log = trim($post['id']);
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/setup/event/edit/'. $post['id_event']);
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/setup/event/index');
            }
        
        
    }

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/setup/event/index');
        }

        $delete         = $this->_hapus(array("id" => dec_data($id)));
         if ($delete['result'] === false) {
            $this->session->set_flashdata('error', $delete['message']);
            redirect(base_url() . 'kinerja/setup/event/index');
        }
        else {
            $this->session->set_flashdata('pesan', $delete['message']);
            redirect(base_url() . 'kinerja/setup/event/index');
        }
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/event/default', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/setup/event/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/event/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/setup/event/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/setup/event/hapus', 'base_token', $data), true);
    }

    public function _periode_aktif($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_aktif', 'base_token', $data), true);
    }
}
