<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property CI_App $app
 * @property CI_Tema $tema
 * @property CI_Curl $curl
 * @property CI_Input $input
 */
class Asesor extends CI_Controller
{
    public $sso_url, $appUrl, $base_url, $view, $link, $isdm_url;
    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        // $this->appUrl = '845f00eb-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->isdm_url = $this->app->isdm_server();
        $this->view = 'backend/kinerja/setup_asesor/';
        $this->link = 'kinerja/setup/asesor/';
    }

    public function index()
    {
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['periode'] = $this->_get_periode()['result'];
        $data['kategori_doskar'] = array('All', 'Tenaga Pendidik', 'Tenaga Penunjang', 'Tenaga Kependidikan');
        $data['data'] = $this->_doskar_list()['result'];

        $this->tema->backend($this->view . 'index', $data);
    }
    public function json()
    {
        $input = $this->input->post();
        $dinilai_id_doskar = empty($input['dinilai_id_doskar']) ? '' : $input['dinilai_id_doskar'];
        $table = empty($input['table']) ? '' : $input['table'];
        $doskar = $this->_doskar_list()['result'];
        usort($doskar, function ($a, $b) {
            // return $a['id_doskar'] <=> $b['id_doskar'];
            return strcmp($a['fak_unit'], $b['fak_unit']);
        });
        $filter = array(
            'bulan_tahun' => empty($input['bulan_tahun']) ? '2024-12' : $input['bulan_tahun'],
        );
        $get_data = $this->_get_data($filter)['result'];
        $list_asesor = array();
        foreach ($get_data['asesor'] as $key => $value) {
            $list_asesor[$value['dinilai_id_doskar']][] = $value;
        }
        $cari_asesor = empty($list_asesor[$dinilai_id_doskar]) ? array() : $list_asesor[$dinilai_id_doskar];
        $no = 1;
        $data = array();
        foreach ($doskar as $key => $value) {
            // YANG AKAN DINILAI JANGAN MUNCUL DI MODAL TAMBAH
            if ($table == 'modal_tambah' && $value['id_doskar'] == $dinilai_id_doskar) {
                continue;
            }
            $value['no'] = $no++;
            $search_asesor = empty($list_asesor[$value['id_doskar']]) ? array() : $list_asesor[$value['id_doskar']];
            $search_asesor_teman = array();
            $search_asesor_pimpinan = array();
            $search_asesor_universitas = array();
            foreach ($search_asesor as $key2 => $val2) {
                if ($val2['kategori'] == 'teman') {
                    $search_asesor_teman[] = $val2;
                }
                if ($val2['kategori'] == 'pimpinan') {
                    $search_asesor_pimpinan[] = $val2;
                }
                if ($val2['kategori'] == 'universitas') {
                    $search_asesor_universitas[] = $val2;
                }
            }
            $value['search_asesor_teman'] =  $search_asesor_teman;
            $value['search_asesor_pimpinan'] =  $search_asesor_pimpinan;
            $value['search_asesor_universitas'] =  $search_asesor_universitas;
            $allow_tambah = '1';
            foreach ($cari_asesor as $key2 => $val2) {
                if ($val2['penilai_id_doskar'] == $value['id_doskar']) {
                    $allow_tambah = '0';
                }
            }
            $value['allow_tambah'] =  $allow_tambah;
            $data[] = $value;
        }
        $response['data'] = $data;
        $response['asesor'] = $get_data['asesor'];
        echo json_encode($response);
    }
    public function ajax_update()
    {
        $input = $this->input->post();
        $aksi = $this->_insert($input);
        $response['message'] = $aksi['message'];
        $response['type'] = 'alert-success';
        if (empty($aksi['result'])) {
            $response['type'] = 'alert-warning';
        }
        echo json_encode($response);
    }

    public function ajax_delete($id_asesor)
    {
        $data = array(
            'id_asesor' => $id_asesor,
        );
        $aksi = $this->_delete($data);
        $response['message'] = $aksi['message'];
        $response['type'] = 'alert-success';
        if (empty($aksi['result'])) {
            $response['type'] = 'alert-warning';
        }
        echo json_encode($response);
    }

    public function _get_data($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/asesor/get_data', 'base_token', $data), true);
    }
    public function _get_periode($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/asesor/get_periode', 'base_token', $data), true);
    }
    public function _insert($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/setup/asesor/insert', 'base_token', $data), true);
    }
    public function _delete($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/setup/asesor/delete', 'base_token', $data), true);
    }
    public function _doskar_list($array = array())
    {
        $array['keperluan'] = 'penilaian_kinerja';
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
}
