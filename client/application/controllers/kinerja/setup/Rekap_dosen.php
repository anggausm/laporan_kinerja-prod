<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_dosen extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        // $this->appUrl = '845f00eb-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->isdm_url = $this->app->isdm_server();
        $this->view = 'backend/kinerja/setup_rekap_dosen/';
        $this->link = 'kinerja/setup/rekap_dosen/';
        $this->load->helper('excel');
    }

    public function index()
    {
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $input = $this->input->post();
        if (!empty($input['submit']) && $input['submit'] == 'excel') {
            return $this->_download_excel();
        }
        if (!empty($input['submit']) && $input['submit'] == 'pdf') {
            return $this->_download_pdf();
        }
        $data['id_unit'] = empty($input['id_unit']) ? '' : $input['id_unit'];
        $data['nama_unit'] = empty($input['nama_unit']) ? '' : $input['nama_unit'];
        $filter_doskar = $this->_filter_doskar();
        foreach ($filter_doskar as $key => $value) {
            $data[$key] = $value;
        }
        $this->tema->backend($this->view . 'index', $data);
    }
    public function detail_pertanyaan($user_key)
    {
        $is_akses_univ = $this->_is_akses_univ();
        if (!$is_akses_univ) {
            $this->session->set_flashdata('pesan', "aa");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $data['cek_token'] = $this->_cek_token()['result'];
        $jenis_pekerjaan = 'Tenaga Pendidik';
        $user_key = dec_data_url($user_key);
        $arr = array('dinilai_user_key' => $user_key, 'jenis_pekerjaan' => $jenis_pekerjaan);
        $get_pertanyaan = $this->_get_pertanyaan($arr)['result'];

        $data['profil'] = $this->_isdm_profil(array('user_key' => $user_key))['result'];
        $data['jenis_pekerjaan'] = $jenis_pekerjaan;
        $data['user_key'] = $user_key;
        $data['tanya'] = $get_pertanyaan['pertanyaan'];
        $data['disabled'] = $get_pertanyaan['disabled'];
        $data['periode'] = $get_pertanyaan['periode'];
        $data['kinerja_ajuan'] = $get_pertanyaan['kinerja_ajuan'];
        $this->tema->backend($this->view . 'form_dosen', $data);
    }
    public function ajukan($user_key)
    {
        $user_key = dec_data_url($user_key);
        $response  = $this->_ajukan(array('user_key' => $user_key));
        echo "<pre>";
        print_r($response);
        echo "</pre>";
        die();
        $this->session->set_flashdata('pesan', $response['message']);
        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
        }
        return redirect(base_url($this->link . 'detail_pertanyaan/' . enc_data_url($user_key)));
    }
    public function cek_link()
    {
        $is_akses_univ = $this->_is_akses_univ();
        if (!$is_akses_univ) {
            return redirect(base_url($this->link));
        }
        // var_dump(!$this->_hasHttpOrHttps('https://refpress.org/ref-vol21-a301/        https://repository.usm.ac.id/files/journalint/B206/20240309031508-The-Role-of-Financial-Self-Efficacy-in-Moderating-the-Influence-of-Investment-Knowledge-and-Financial-Literacy-on-Investment-Intention-in-the-Capital-Market-among-Millennials.pdf'));
        // die();
        $get_nilai = $this->_get_data()['result'];
        foreach ($get_nilai['cek_link'] as $key => $value) {
            if (!empty($value['link_dokumen'])) {
                $pattern = '/(?=http)/'; // Lookahead assertion for 'http' or 'https'
                $value['link_dokumen'] = str_replace(' ', '', $value['link_dokumen']);
                $parts = preg_split($pattern, $value['link_dokumen'], -1, PREG_SPLIT_NO_EMPTY);

                // Add 'http' or 'https' back to the start of each part
                foreach ($parts as &$part) {
                    if ($this->_checkUrlAccess($part)) {
                        // echo "URL is accessible: $part<br>";
                    } else {
                        echo "id_nilai $value[id_nilai] :  URL is not accessible: $part<br>";
                    }
                }

                // if (!$this->_hasHttpOrHttps($value['link_dokumen'])) {
                //     echo "Data tidak dapat diajukan, dimohon untuk mengisi Link Dokumen dengan https://";
                // }
            }
        }
    }
    function _hasHttpOrHttps($string)
    {
        // Use a regular expression to check if the string starts with http or https
        return preg_match('/^(http|https):\/\//', $string);
    }
    function _checkUrlAccess($url)
    {
        $ch = curl_init($url);

        // Set curl options
        curl_setopt($ch, CURLOPT_NOBODY, true); // We don't need the body of the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Don't output the response
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); // Timeout after 10 seconds
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Follow redirects
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'); // Mimic a real browser
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Skip SSL verification for this example
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // Skip host verification for this example
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language: en-US,en;q=0.5',
            'Referer: https://www.google.com', // Add a referer
        ]);

        // Execute the request
        $result = curl_exec($ch);

        if ($result === false) {
            // If there's an error, get the error message
            $error = curl_error($ch);
        } else {
            // Check the HTTP response code
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            // var_dump($httpCode);
            $error = null;
        }

        // Close the curl session
        curl_close($ch);

        if ($error) {
            // Output the error message if there is one
            echo "Error accessing URL: $url\n";
            echo "Error message: $error\n";
            return false;
        } else {
            // Return true if the HTTP code is 200 (OK)
            return $httpCode === 200;
        }
    }

    public function _download_excel()
    {
        $is_akses_univ = $this->_is_akses_univ();
        if (!$is_akses_univ) {
            $this->session->set_flashdata('pesan', "aa");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $input = $this->input->post();
        $nama_unit = empty($input['id_unit']) ? 'SEMUA' : $input['nama_unit'];
        $filter_doskar = $this->_filter_doskar();
        try {
            $this->load->library('phpexcel');
            $filepath = "file/kinerja/rekap_dosen.xlsx";
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $this->phpexcel = $objReader->load($filepath);
            $objWorksheet = $this->phpexcel->setActiveSheetIndex(0);
            $objWorksheet->setCellValue(generateExcelColumnLetters(28) . '6', 'ID Doskar');

            $objWorksheet->setCellValue('A3', $nama_unit);
            $letter = 1;
            // $objWorksheet->getColumnDimension(generateExcelColumnLetters($letter++))->setAutoSize(true);
            $no = 1;
            $line = 7;
            foreach ($filter_doskar['filter_doskar'] as $value) {
                $letter = 1;
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $no);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nama_doskar']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_akademik']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pendidikan']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_penelitian']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pkm']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_penunjang']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_core_value']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=SUM(C' . $line . ':H' . $line . ')');
                // UBAH RUMUS ; DENGAN ,
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(I' . $line . '>35, "A", IF(I' . $line . '>=26, "B", IF(I' . $line . '>=15, "C", "D")))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_akademik']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_pendidikan']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_penelitian']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_pkm']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_penunjang']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan_core_value']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=SUM(K' . $line . ':P' . $line . ')');
                // UBAH RUMUS ; DENGAN ,
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(Q' . $line . '>35, "A", IF(Q' . $line . '>=26, "B", IF(Q' . $line . '>=15, "C", "D")))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_akademik']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_pendidikan']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_penelitian']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_pkm']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_penunjang']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_univ_core_value']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=SUM(S' . $line . ':X' . $line . ')');
                // UBAH RUMUS ; DENGAN ,
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(Y' . $line . '>35, "A", IF(Y' . $line . '>=26, "B", IF(Y' . $line . '>=15, "C", "D")))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(Y' . $line . '>35, 100, IF(Y' . $line . '>=26, 75, IF(Y' . $line . '>=15, 50, 25)))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['id_doskar']);
                $line++;
                $no++;
            }
            $nm_file = 'rekap_kinerja_dosen_' . date('Ymd') . '_' . date('His');
            $nm_file = strtolower($nm_file);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=' . $nm_file . '.xlsx');
            header('Cache-Control: max-age=0');
            // output
            $obj_writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
            return $obj_writer->save('php://output');
        } catch (Exception $e) {
            var_dump($e);
            die();
        }
    }
    public function _download_pdf()
    {
        $input = $this->input->post();
        $is_akses_univ = $this->_is_akses_univ();
        if (empty($input['id_unit'])) {
            $this->session->set_flashdata('pesan', "Silahkan pilih unit terlebih dahulu");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $filter_doskar = $this->_filter_doskar();
        foreach ($filter_doskar as $key => $value) {
            $data[$key] = $value;
        }
        $cek_token = $data['cek_token'];
        $periode = $this->_get_periode_aktif()['result'];
        if (empty($periode['pengisian_selesai']) || empty($periode['penilaian_selesai'])) {
            $this->session->set_flashdata('pesan', "Data Periode tidak ada");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $date_now = date('Y-m-d');
        if (!in_array($cek_token['username'], array('angga', 'GDOS089'))) {
            if ($date_now < $periode['pengisian_selesai']) {
                $this->session->set_flashdata('pesan', "Belum dapat mendownload, dikarenakan periode pengisian belum selesai");
                $this->session->set_flashdata('type', 'alert-danger');
                return redirect(base_url($this->link));
            }
            if ($date_now < $periode['penilaian_selesai']) {
                $this->session->set_flashdata('pesan', "Belum dapat mendownload, dikarenakan periode penilaian belum selesai");
                $this->session->set_flashdata('type', 'alert-danger');
                return redirect(base_url($this->link));
            }
        }
        $arr = array('user_key' => $cek_token['user_key'], 'is_akses_univ' => $is_akses_univ, 'id_unit' => $input['id_unit']);
        $kinerja_akses = $this->_isdm_detail_kinerja_akses($arr)['result'];
        $data['kinerja_akses'] = $kinerja_akses;
        try {
            $mpdf = new \Mpdf\Mpdf(['format' => 'A4', 'orientation' => 'P']);
            $mpdf->defaultfooterline = 0;
            // $mpdf->SetFooter('Halaman {PAGENO}');
            $html = $this->load->view($this->view . 'cetak_pdf', $data, true);
            // echo "<pre>";
            // print_r($html);
            // echo "</pre>";
            // die();
            $mpdf->WriteHTML($html);
            $nm_file = 'rekap_kinerja_dosen_';
            $nm_file .=  date('Ymd') . '_' . date('His');
            $nm_file .= '.pdf';
            if (in_array($cek_token['username'], array('angga'))) {
                $mpdf->Output($nm_file, 'I');
            } else {
                $mpdf->Output($nm_file, 'D');
            }
        } catch (\Mpdf\MpdfException $e) {
            // Handle mPDF exceptions
            echo 'An error occurred: ' . $e->getMessage();
        } catch (\Exception $e) {
            // Handle other general exceptions
            echo 'An unexpected error occurred: ' . $e->getMessage();
        }
    }
    public function _filter_doskar()
    {
        $input = $this->input->post();
        $cek_token = $this->_cek_token()['result'];
        $kinerja_akses = $this->_isdm_detail_kinerja_akses(array('user_key' => $cek_token['user_key']))['result'];
        $kinerja_akses['id_unit'] = empty($kinerja_akses['id_unit']) ? 'zzzzz' : $kinerja_akses['id_unit'];
        $is_akses_univ = $this->_is_akses_univ();
        // JIKA LEVEL BIASA SESUAI UNIT
        if ($is_akses_univ == false) {
            $list_unit_user = empty($kinerja_akses['list_unit_user']) ? array() : $kinerja_akses['list_unit_user'];
        }
        $doskar = $this->_doskar_list()['result'];
        $order = ['A11A', 'B11A', 'B13B', 'B21A', 'C11A', 'C41A', 'C51A', 'D11A', 'F11A', 'G11A', 'G21A', 'G31A', 'G41A', 'B31A', 'A31A', 'F31A', 'A', 'B', 'C', 'D', 'F', 'G', 'PS', 'RT', 'PERPUS', 'MHS', 'SAKTI', 'TU', 'BAAK', 'BAUK', 'KP', 'SHKH', 'BAUK-KEU', 'SPMB', 'KPG', 'LPPM', 'PUSKOM', 'SIO', 'BPM', 'BBJ']; // Specific order of IDs 
        usort($doskar, function ($a, $b) use ($order) {
            $orderMap = array_flip($order); // Creates a map with values as keys and their positions as values
            $posA = $orderMap[$a['id_unit']] ?? null; // Use null if not found
            $posB = $orderMap[$b['id_unit']] ?? null; // Use null if not found

            if ($posA === null && $posB === null) {
                // Both are not in the order, sort alphabetically
                return $a['id_unit'] <=> $b['id_unit'];
            } elseif ($posA === null) {
                // $a is not in the order, put it after $b
                return 1;
            } elseif ($posB === null) {
                // $b is not in the order, put it after $a
                return -1;
            } else {
                // Both are in the order, sort by the custom order
                return $posA <=> $posB;
            }
        });

        $unit = array();
        $filter_doskar = array();
        foreach ($doskar as $key => $value) {
            $matchesJenis = $value['jenis_pekerjaan'] == 'Tenaga Pendidik';
            if ($matchesJenis) {
                if (empty($unit[$value['id_unit']])) {
                    $unit[$value['id_unit']] = array('id_unit' => $value['id_unit'], 'nama_unit' => $value['nama_prodi']);
                }
            }
            $matchesFakUnit = empty($input['id_unit']) || $value['id_unit'] == $input['id_unit'];
            if ($matchesJenis && $matchesFakUnit) {
                $filter_doskar[] = $value;
            }
        }

        $get_nilai = $this->_get_data()['result']['rekap'];
        $re_nilai = array();
        foreach ($get_nilai as $key => $value) {
            $re_nilai[$value['dinilai_user_key']] = $value;
        }
        foreach ($filter_doskar as $key => $value) {
            $cari_nilai = empty($re_nilai[$value['user_key']]) ? array() : $re_nilai[$value['user_key']];
            // DIRI SENDIRI
            $filter_doskar[$key]['nilai_akademik'] = empty($cari_nilai['nilai_akademik']) ? 0 : $cari_nilai['nilai_akademik'];
            $filter_doskar[$key]['nilai_pendidikan'] = empty($cari_nilai['nilai_pendidikan']) ? 0 : $cari_nilai['nilai_pendidikan'];
            $filter_doskar[$key]['nilai_penelitian'] = empty($cari_nilai['nilai_penelitian']) ? 0 : $cari_nilai['nilai_penelitian'];
            $filter_doskar[$key]['nilai_pkm'] = empty($cari_nilai['nilai_pkm']) ? 0 : $cari_nilai['nilai_pkm'];
            $filter_doskar[$key]['nilai_penunjang'] = empty($cari_nilai['nilai_penunjang']) ? 0 : $cari_nilai['nilai_penunjang'];
            $filter_doskar[$key]['nilai_core_value'] = empty($cari_nilai['nilai_core_value']) ? 0 : $cari_nilai['nilai_core_value'];
            $total_nilai = empty($cari_nilai['total_nilai']) ? 0 : $cari_nilai['total_nilai'];
            $filter_doskar[$key]['total_nilai'] = $total_nilai;
            $grade = '';
            $kinerja = '';
            if ($total_nilai > 35) {
                $grade = 'A';
                $kinerja = '100';
            } elseif ($total_nilai >= 26) {
                $grade = 'B';
                $kinerja = '75';
            } elseif ($total_nilai >= 15) {
                $grade = 'C';
                $kinerja = '50';
            } else {
                $grade = 'D';
                $kinerja = '25';
            }
            $filter_doskar[$key]['grade'] = $grade;
            $filter_doskar[$key]['kinerja'] = $kinerja;

            // FAKULTAS
            $filter_doskar[$key]['nilai_pimpinan_akademik'] = empty($cari_nilai['nilai_pimpinan_akademik']) ? 0 : $cari_nilai['nilai_pimpinan_akademik'];
            $filter_doskar[$key]['nilai_pimpinan_pendidikan'] = empty($cari_nilai['nilai_pimpinan_pendidikan']) ? 0 : $cari_nilai['nilai_pimpinan_pendidikan'];
            $filter_doskar[$key]['nilai_pimpinan_penelitian'] = empty($cari_nilai['nilai_pimpinan_penelitian']) ? 0 : $cari_nilai['nilai_pimpinan_penelitian'];
            $filter_doskar[$key]['nilai_pimpinan_pkm'] = empty($cari_nilai['nilai_pimpinan_pkm']) ? 0 : $cari_nilai['nilai_pimpinan_pkm'];
            $filter_doskar[$key]['nilai_pimpinan_penunjang'] = empty($cari_nilai['nilai_pimpinan_penunjang']) ? 0 : $cari_nilai['nilai_pimpinan_penunjang'];
            $filter_doskar[$key]['nilai_pimpinan_core_value'] = empty($cari_nilai['nilai_pimpinan_core_value']) ? 0 : $cari_nilai['nilai_pimpinan_core_value'];
            $total_pimpinan_nilai = empty($cari_nilai['total_pimpinan_nilai']) ? 0 : $cari_nilai['total_pimpinan_nilai'];
            $filter_doskar[$key]['total_pimpinan_nilai'] = $total_pimpinan_nilai;
            $grade_pimpinan = '';
            $kinerja_pimpinan = '';
            if ($total_pimpinan_nilai > 35) {
                $grade_pimpinan = 'A';
                $kinerja_pimpinan = '100';
            } elseif ($total_pimpinan_nilai >= 26) {
                $grade_pimpinan = 'B';
                $kinerja_pimpinan = '75';
            } elseif ($total_pimpinan_nilai >= 15) {
                $grade_pimpinan = 'C';
                $kinerja_pimpinan = '50';
            } else {
                $grade_pimpinan = 'D';
                $kinerja_pimpinan = '25';
            }
            $filter_doskar[$key]['grade_pimpinan'] = $grade_pimpinan;
            $filter_doskar[$key]['kinerja_pimpinan'] = $kinerja_pimpinan;

            // UNIV
            $filter_doskar[$key]['nilai_univ_akademik'] = empty($cari_nilai['nilai_univ_akademik']) ? 0 : $cari_nilai['nilai_univ_akademik'];
            $filter_doskar[$key]['nilai_univ_pendidikan'] = empty($cari_nilai['nilai_univ_pendidikan']) ? 0 : $cari_nilai['nilai_univ_pendidikan'];
            $filter_doskar[$key]['nilai_univ_penelitian'] = empty($cari_nilai['nilai_univ_penelitian']) ? 0 : $cari_nilai['nilai_univ_penelitian'];
            $filter_doskar[$key]['nilai_univ_pkm'] = empty($cari_nilai['nilai_univ_pkm']) ? 0 : $cari_nilai['nilai_univ_pkm'];
            $filter_doskar[$key]['nilai_univ_penunjang'] = empty($cari_nilai['nilai_univ_penunjang']) ? 0 : $cari_nilai['nilai_univ_penunjang'];
            $filter_doskar[$key]['nilai_univ_core_value'] = empty($cari_nilai['nilai_univ_core_value']) ? 0 : $cari_nilai['nilai_univ_core_value'];
            $total_univ_nilai = empty($cari_nilai['total_univ_nilai']) ? 0 : $cari_nilai['total_univ_nilai'];
            $filter_doskar[$key]['total_univ_nilai'] = $total_univ_nilai;
            $grade_univ = '';
            $kinerja_univ = '';
            if ($total_univ_nilai > 35) {
                $grade_univ = 'A';
                $kinerja_univ = '100';
            } elseif ($total_univ_nilai >= 26) {
                $grade_univ = 'B';
                $kinerja_univ = '75';
            } elseif ($total_univ_nilai >= 15) {
                $grade_univ = 'C';
                $kinerja_univ = '50';
            } else {
                $grade_univ = 'D';
                $kinerja_univ = '25';
            }
            $filter_doskar[$key]['grade_univ'] = $grade_univ;
            $filter_doskar[$key]['kinerja_univ'] = $kinerja_univ;
            $filter_doskar[$key]['list_nilai'] = $cari_nilai;
        }
        $re_unit = array();
        foreach ($unit as $key => $value) {
            if (
                $is_akses_univ === false
            ) {
                foreach ($list_unit_user as $key2 => $val2) {
                    if ($value['id_unit'] == $val2['id_unit']) {
                        $re_unit[] = $value;
                    }
                }
            } else if ($is_akses_univ === true) {
                $re_unit[] = $value;
            }
        }
        $re_filter_doskar = array();
        foreach ($filter_doskar as $key => $value) {
            if ($is_akses_univ === false) {
                foreach ($list_unit_user as $key2 => $val2) {
                    if ($value['id_unit'] == $val2['id_unit']) {
                        $re_filter_doskar[] = $value;
                    }
                }
            } else {
                $re_filter_doskar[] = $value;
            }
        }
        $result['cek_token'] = $cek_token;
        $result['kinerja_akses'] = $kinerja_akses;
        $result['is_akses_univ'] = $is_akses_univ;
        $result['filter_doskar'] = $re_filter_doskar;
        $result['unit'] = $re_unit;
        $result['doskar'] = $doskar;
        $result['get_nilai'] = $get_nilai;
        return $result;
    }
    public function _get_pertanyaan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/kependidikan/penilaian_pimpinan/get_pertanyaan', 'base_token', $data), true);
    }
    public function _get_periode_aktif($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/general/get_periode_aktif', 'base_token', $data), true);
    }
    public function _get_data($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/rekap_dosen/get_data', 'base_token', $data), true);
    }
    public function _ajukan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/dosen/rekap/ajukan', 'base_token', $data), true);
    }
    public function _doskar_list($array = array())
    {
        $array['keperluan'] = 'penilaian_kinerja';
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _isdm_profil($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _isdm_detail_kinerja_akses($array = array())
    {
        $data = json_encode($array);
        $aksi = json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_detail_kinerja_akses', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
        return $aksi;
    }
    public function _cek_token()
    {
        $base_token = $this->session->userdata('base_token');
        // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }
    /* mengijinkan level berikut melihat semua unit */
    public function _is_akses_univ()
    {
        // 'Kepegawain', 'Kepegawaian 2', 'Pimpinan Universitas', 'Kepegawaian (pimpinan)','Tester APP'
        if (in_array($this->session->userdata('nm_level'), array('Kepegawain', 'Kepegawaian 2', 'Pimpinan Universitas', 'Kepegawaian (pimpinan)', 'Tester APP'))) {
            return true;
        }
        return false;
    }
}
