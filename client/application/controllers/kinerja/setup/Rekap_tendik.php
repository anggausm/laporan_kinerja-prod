<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_tendik extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '69e80275-6f25-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->title = 'Rekap Penilaian Pegawai';
        // $this->load->library(array('phpexcel','session'));

    }

    public function index()
    {
        $data = array();
        $data['crud']  = $this->app->AksesMenu($this->appUrl, 'r');
    
        $periode        = $this->get_periode();
        $data['periode']= $periode['result']['all_periode'];

        $this->tema->backend('backend/kinerja/rekap_tendik/index', $data);
    }

    public function data()
    {
        $data             = array();
        $data['crud']     = $this->app->AksesMenu($this->appUrl, 'r');
        $post             = $this->input->post();
        $data['list']    = $this->_list_rekap((array("periode" => $post['periode'])))['result'];
       
        $data['id_periode'] = $post['periode'];
        $periode        = $this->get_periode();
        $data['periode']  = $periode['result']['all_periode'];   

        $this->tema->backend('backend/kinerja/rekap_tendik/data', $data);

    }

        public function export_excel($periode = '')
    { 
        $data              = array();
        $data['crud']      = $this->app->AksesMenu($this->appUrl, 'r');
        $tahun             = dec_data($periode);
   
        $datas             = $this->_list_rekap((array("periode" => $tahun )))['result'];
        $data_periode      = $this->get_periode((array("periode" => $tahun)));
        $periode           = $data_periode['result']['data_periode'];  

        $this->load->helper('excel');
        $this->load->library('phpexcel');
        $filepath          = "file/kinerja/rekap_penilaian_tendik.xlsx";
       
        $objReader      = PHPExcel_IOFactory::createReader('Excel2007');
        $this->phpexcel = $objReader->load($filepath);
        $objWorksheet   = $this->phpexcel->setActiveSheetIndex(0);

        $objWorksheet->setCellValue('A1', strtoupper("Rekap Penilaian Kinerja Pegawai"));
        $objWorksheet->mergeCells('A1:F1');
        $objWorksheet->getStyle("A1:F1")->getFont()->setBold(true);
        $objWorksheet->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objWorksheet->setCellValue('A2', 'PERIODE ' .strtoupper(tanggal_indonesia($periode['pengisian_mulai'])). ' SAMPAI ' . strtoupper(tanggal_indonesia($periode['pengisian_selesai'])));
        $objWorksheet->mergeCells('A2:F2');
        $objWorksheet->getStyle("A2:F2")->getFont()->setBold(true);
        $objWorksheet->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objWorksheet->getColumnDimension('A')->setAutoSize(true);
        $objWorksheet->getColumnDimension('B')->setAutoSize(true);
        $objWorksheet->getColumnDimension('C')->setAutoSize(true);
        $objWorksheet->getColumnDimension('D')->setAutoSize(true);
        $objWorksheet->getColumnDimension('E')->setAutoSize(true);
        $objWorksheet->getColumnDimension('F')->setAutoSize(true);
        
        
        
        $line = 5;
        $no = 1;
        
        foreach ($datas as $value) {
            $objWorksheet->setCellValue('A' . $line, "$no");
            $objWorksheet->setCellValue('B' . $line, strtoupper($value['nama_doskar']));
            $objWorksheet->setCellValue('C' . $line, strtoupper($value['nama_unit']));
            $objWorksheet->setCellValue('D' . $line, strtoupper($value['skor']));
            $objWorksheet->setCellValue('E' . $line, strtoupper($value['nilai']));
            $objWorksheet->setCellValue('F' . $line, strtoupper($value['kategori']));

            $line = $line + 1;
            $no = $no + 1;
           
        }
            $no++;                       
            

        $nama_file = 'REKAP_PENILAIAN_KINERJA_' . date('YmdHis');
        $nm_file = preg_replace("/[^a-zA-Z0-9_]/", '_', $nama_file);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=' . $nm_file . '.xlsx');
        header('Cache-Control: max-age=0');
        // output
        $obj_writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
        return $obj_writer->save('php://output');
        
    }

    //  public function export_excel($periode = '')
    // {
    //     $data['crud'] = $this->app->AksesMenu($this->menu_key, 'r');
    //     $post         = $this->input->post();
    //     // $awal         = dec_data($post['awal']);
    //     // $akhir        = dec_data($post['akhir']);
    //     // $data['periode']      = array('awal' => $awal, 'akhir' => $akhir);
    //     // $data['transport']    = $this->_get_data(array('awal' => $awal, 'akhir' => $akhir))['result'];
    //      $tahun             = dec_data($periode);
    //     $data['rekap'] = $this->_list_rekap((array("periode" => $tahun )))['result'];
        
    //     // $this->load->view($this->view . 'rekap_excel', $data);
    //     $this->tema->backend('backend/kinerja/rekap_tendik/rekap_excel', $data);
    // }

     public function validasi()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pa'] =  $this->_periode_aktif()['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/rekap_tendik/tambah', $data);
    }

    public function simpan()
    {
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'c');
        $post = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/setup/event/');
        }

                 $datainsert = array(
                        'nama_event' => $post['nama_event'],
                        'tgl_awal' => $post['tgl_awal'],
                        'tgl_akhir' => $post['tgl_akhir'],
                        'jam_awal' => $post['jam_awal'],
                        'jam_akhir' => $post['jam_akhir'],
                        'lokasi_event' => $post['lokasi'],
                        'ket' => $post['ket']
                    );
                 
                $aksi = $this->_simpan($datainsert);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/setup/event/index');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/setup/event/index');
                }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];
        $data['event'] =   $this->get_default()['result'];

         $data['jenis'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Wajib'),
            array('id_stat' => '2', 'nama_stat' => 'Pilihan')
        );

        // $data['pa'] =  $this->_periode_aktif()['result'];
        // if (!empty($this->input->post())) {
        //     $data['bulan'] = $this->input->post('bulan');
        //     $data['tahun'] = $this->input->post('tahun');
        //     $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        // } else {
        //     $data['bulan'] = date('m');
        //     $data['tahun'] = date('Y');
            
        //     $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        // }
        // $data['pa'] =  $this->_periode_aktif()['result'];

        $this->tema->backend('backend/kinerja/rekap_tendik/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        
    
            $data_update = array(
            	'id_event' => dec_data($post['id_event']),
                'nama_event' => $post['nama_event'],
                'tgl_awal' => $post['tgl_awal'],
                'tgl_akhir' => $post['tgl_akhir'],
                'jam_awal' => $post['jam_awal'],
                'jam_akhir' => $post['jam_akhir'],
                'lokasi_event' => $post['lokasi'],
                'ket' => $post['ket']
            );
            // var_dump($data_update); die();

            $aksi = $this->_update($data_update);
             
            if ($aksi['result'] === false) {
                $id_log = trim($post['id']);
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/setup/event/edit/'. $post['id_event']);
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/setup/event/index');
            }
        
        
    }

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'rekap_tendik/index');
        }

        $delete         = $this->_hapus(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'rekap_tendik/index');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function _list_rekap($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/rekap_tendik/default', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/setup/event/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/event/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/setup/event/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/setup/event/hapus', 'base_token', $data), true);
    }

    public function _periode_aktif($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_aktif', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }
}
