<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_kependidikan extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        // $this->appUrl = '845f00eb-884d-11ea-b58e-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->isdm_url = $this->app->isdm_server();
        $this->view = 'backend/kinerja/setup_rekap_kependidikan/';
        $this->link = 'kinerja/setup/rekap_kependidikan/';
        $this->load->helper('excel');
    }

    public function index()
    {
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $input = $this->input->post();
        if (!empty($input['submit']) && $input['submit'] == 'excel') {
            return $this->_download_excel();
        }
        if (!empty($input['submit']) && $input['submit'] == 'pdf') {
            return $this->_download_pdf();
        }
        $data['fak_unit'] = empty($input['fak_unit']) ? '' : $input['fak_unit'];
        $data['nama_unit'] = empty($input['nama_unit']) ? '' : $input['nama_unit'];
        $filter_doskar = $this->_filter_doskar();
        foreach ($filter_doskar as $key => $value) {
            $data[$key] = $value;
        }
        $this->tema->backend($this->view . 'index', $data);
    }
    /* DOWNLOAD EXCEL HANYA UNTUK AKSES PIMPINAN */
    public function _download_excel()
    {
        $is_akses_univ = $this->_is_akses_univ();
        if (!$is_akses_univ) {
            $this->session->set_flashdata('pesan', "aa");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $input = $this->input->post();
        $nama_unit = empty($input['fak_unit']) ? 'SEMUA' : $input['nama_unit'];
        $filter_doskar = $this->_filter_doskar();
        try {
            $this->load->library('phpexcel');
            $filepath = "file/kinerja/rekap_kependidikan.xlsx";
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $this->phpexcel = $objReader->load($filepath);
            $objWorksheet = $this->phpexcel->setActiveSheetIndex(0);

            $objWorksheet->setCellValue('A3', $nama_unit);
            $letter = 1;
            // $objWorksheet->getColumnDimension(generateExcelColumnLetters($letter++))->setAutoSize(true);
            $objWorksheet->setCellValue(generateExcelColumnLetters(11) . '6', 'ID Doskar');
            $no = 1;
            $line = 8;
            foreach ($filter_doskar['filter_doskar'] as $value) {
                $letter = 1;
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $no);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nama_doskar']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_diri']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_rekan_1']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_rekan_2']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_rekan_3']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai_pimpinan']);
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=SUM(C' . $line . ':G' . $line . ')/5');
                // UBAH RUMUS ; DENGAN ,
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(H' . $line . '>=81, "A", IF(H' . $line . '>=71, "B", IF(H' . $line . '>=56, "C", "D")))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=IF(H' . $line . '>=81,100,IF(H' . $line . '>=71,75,IF(H' . $line . '>=56,50,25)))');
                $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['id_doskar']);
                $line++;
                $no++;
            }
            $nm_file = 'rekap_kinerja_kependidikan_' . date('Ymd') . '_' . date('His');
            $nm_file = strtolower($nm_file);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=' . $nm_file . '.xlsx');
            header('Cache-Control: max-age=0');
            // output
            $obj_writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
            return $obj_writer->save('php://output');
        } catch (Exception $e) {
            var_dump($e);
            die();
        }
    }
    public function _download_pdf()
    {
        $input = $this->input->post();
        $is_akses_univ = $this->_is_akses_univ();
        if (empty($input['fak_unit'])) {
            $this->session->set_flashdata('pesan', "Silahkan pilih unit terlebih dahulu");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $cek_token = $this->_cek_token()['result'];
        $periode = $this->_get_periode_aktif()['result'];
        if (empty($periode['pengisian_selesai']) || empty($periode['penilaian_selesai'])) {
            $this->session->set_flashdata('pesan', "Data Periode tidak ada");
            $this->session->set_flashdata('type', 'alert-danger');
            return redirect(base_url($this->link));
        }
        $date_now = date('Y-m-d');
        if (!in_array($cek_token['username'], array('angga', 'GDOS089'))) {
            if ($date_now < $periode['pengisian_selesai']) {
                $this->session->set_flashdata('pesan', "Belum dapat mendownload, dikarenakan periode pengisian belum selesai");
                $this->session->set_flashdata('type', 'alert-danger');
                return redirect(base_url($this->link));
            }
            if ($date_now < $periode['penilaian_selesai']) {
                $this->session->set_flashdata('pesan', "Belum dapat mendownload, dikarenakan periode penilaian belum selesai");
                $this->session->set_flashdata('type', 'alert-danger');
                return redirect(base_url($this->link));
            }
        }
        $arr = array('user_key' => $cek_token['user_key'], 'is_akses_univ' => $is_akses_univ, 'id_unit' => $input['fak_unit']);
        $kinerja_akses = $this->_isdm_detail_kinerja_akses($arr)['result'];
        $filter_doskar = $this->_filter_doskar();
        foreach ($filter_doskar as $key => $value) {
            $data[$key] = $value;
        }
        $data['kinerja_akses'] = $kinerja_akses;
        try {
            $mpdf = new \Mpdf\Mpdf(['format' => 'A4', 'orientation' => 'P']);
            $mpdf->defaultfooterline = 0;
            // $mpdf->SetFooter('Halaman {PAGENO}');
            $html = $this->load->view($this->view . 'cetak_pdf', $data, true);
            // echo "<pre>";
            // print_r($html);
            // echo "</pre>";
            // die();
            $mpdf->WriteHTML($html);
            $nm_file = 'rekap_kinerja_dosen_';
            $nm_file .=  date('Ymd') . '_' . date('His');
            $nm_file .= '.pdf';
            if (in_array($cek_token['username'], array('angga'))) {
                $mpdf->Output($nm_file, 'I');
            } else {
                $mpdf->Output($nm_file, 'D');
            }
        } catch (\Mpdf\MpdfException $e) {
            // Handle mPDF exceptions
            echo 'An error occurred: ' . $e->getMessage();
        } catch (\Exception $e) {
            // Handle other general exceptions
            echo 'An unexpected error occurred: ' . $e->getMessage();
        }
    }
    public function _filter_doskar()
    {
        $input = $this->input->post();

        $this->benchmark->mark('code_start');
        $bench_step = 1;
        $cek_token = $this->_cek_token()['result'];
        $this->benchmark->mark('step_' . $bench_step++);

        $kinerja_akses = $this->_isdm_detail_kinerja_akses(array('user_key' => $cek_token['user_key']))['result'];
        $is_akses_univ = $this->_is_akses_univ();
        // JIKA LEVEL BIASA SESUAI UNIT
        if ($is_akses_univ == false) {
            $list_unit_user = empty($kinerja_akses['list_unit_user']) ? array() : $kinerja_akses['list_unit_user'];
        }
        $this->benchmark->mark('step_' . $bench_step++);
        $doskar = $this->_doskar_list()['result'];
        $this->benchmark->mark('step_' . $bench_step++);

        $order = ['A', 'B', 'C', 'D', 'F', 'G', 'PS', 'PS', 'RT', 'PERPUS', 'MHS', 'SAKTI', 'TU', 'BAAK', 'BAUK', 'KP', 'SHKH', 'BAUK-KEU', 'SPMB', 'KPG', 'LPPM', 'PUSKOM', 'SIO', 'BPM', 'BBJ']; // Specific order of IDs 
        usort($doskar, function ($a, $b) use ($order) {
            $orderMap = array_flip($order); // Creates a map with values as keys and their positions as values
            $posA = $orderMap[$a['fak_unit']] ?? null; // Use null if not found
            $posB = $orderMap[$b['fak_unit']] ?? null; // Use null if not found

            if ($posA === null && $posB === null) {
                // Both are not in the order, sort alphabetically
                return $a['fak_unit'] <=> $b['fak_unit'];
            } elseif ($posA === null) {
                // $a is not in the order, put it after $b
                return 1;
            } elseif ($posB === null) {
                // $b is not in the order, put it after $a
                return -1;
            } else {
                // Both are in the order, sort by the custom order
                return $posA <=> $posB;
            }
        });

        $unit = array();
        $filter_doskar = array();
        foreach ($doskar as $key => $value) {
            $matchesJenis = $value['jenis_pekerjaan'] != 'Tenaga Pendidik';
            if ($matchesJenis) {
                if (empty($unit[$value['fak_unit']])) {
                    $unit[$value['fak_unit']] = array('fak_unit' => $value['fak_unit'], 'nama_unit' => $value['nama_unit']);
                }
            }
            $matchesFakUnit = empty($input['fak_unit']) || $value['fak_unit'] == $input['fak_unit'];
            if ($matchesJenis && $matchesFakUnit) {
                $filter_doskar[] = $value;
            }
        }

        $this->benchmark->mark('step_' . $bench_step++);
        $get_nilai = $this->_get_data()['result'];
        $this->benchmark->mark('step_' . $bench_step++);
        $re_nilai = array();
        foreach ($get_nilai as $key => $value) {
            $re_nilai[$value['dinilai_user_key']][] = $value;
        }
        foreach ($filter_doskar as $key => $value) {
            $cari_nilai = empty($re_nilai[$value['user_key']]) ? array() : $re_nilai[$value['user_key']];
            $filter_doskar[$key]['nilai_diri'] = 0;
            $filter_doskar[$key]['nilai_rekan_1'] = 0;
            $filter_doskar[$key]['nilai_rekan_2'] = 0;
            $filter_doskar[$key]['nilai_rekan_3'] = 0;
            $filter_doskar[$key]['nilai_pimpinan'] = 0;
            $index_rekan = 1;
            foreach ($cari_nilai as $key2 => $val2) {
                if ($val2['kategori'] == 'Diri Sendiri') {
                    $filter_doskar[$key]['nilai_diri'] = $val2['jm_nilai'];
                } elseif ($val2['kategori'] == 'Pimpinan') {
                    $filter_doskar[$key]['nilai_pimpinan'] = $val2['jm_nilai'];
                } elseif ($val2['kategori'] == 'Rekan Kerja') {
                    $filter_doskar[$key]['nilai_rekan_' . $index_rekan] = $val2['jm_nilai'];
                    $index_rekan++;
                }
            }
            $total_nilai = ($filter_doskar[$key]['nilai_diri'] + $filter_doskar[$key]['nilai_rekan_1'] +  $filter_doskar[$key]['nilai_rekan_2'] + $filter_doskar[$key]['nilai_rekan_3']  + $filter_doskar[$key]['nilai_pimpinan']) / 5;
            $filter_doskar[$key]['total_nilai'] = $total_nilai;
            $grade = '';
            $kinerja = '';
            if ($total_nilai >= 81) {
                $grade = 'A';
                $kinerja = '100';
            } elseif ($total_nilai >= 71) {
                $grade = 'B';
                $kinerja = '75';
            } elseif ($total_nilai >= 56) {
                $grade = 'C';
                $kinerja = '50';
            } else {
                $grade = 'D';
                $kinerja = '25';
            }
            $filter_doskar[$key]['grade'] = $grade;
            $filter_doskar[$key]['kinerja'] = $kinerja;
            $filter_doskar[$key]['list_nilai'] = $cari_nilai;
        }
        $re_unit = array();
        foreach ($unit as $key => $value) {
            if (
                $is_akses_univ === false
            ) {
                foreach ($list_unit_user as $key2 => $val2) {
                    if ($value['fak_unit'] == $val2['id_unit']) {
                        $re_unit[] = $value;
                    }
                }
            } else if ($is_akses_univ === true) {
                $re_unit[] = $value;
            }
        }
        $re_filter_doskar = array();
        foreach ($filter_doskar as $key => $value) {
            if ($is_akses_univ === false) {
                foreach ($list_unit_user as $key2 => $val2) {
                    if ($value['id_unit'] == $val2['id_unit']) {
                        $re_filter_doskar[] = $value;
                    }
                }
            } else {
                $re_filter_doskar[] = $value;
            }
        }
        $this->benchmark->mark('step_' . $bench_step++);
        $result['kinerja_akses'] = $kinerja_akses;
        $result['is_akses_univ'] = $is_akses_univ;
        $result['filter_doskar'] = $re_filter_doskar;
        $result['unit'] = $re_unit;
        $result['doskar'] = $doskar;
        $result['get_nilai'] = $get_nilai;
        $this->benchmark->mark('code_end');
        $memory_usage = $this->benchmark->memory_usage();
        if ($cek_token['username'] == 'angga') {
            $no = 1;
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('code_start', 'step_1') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_1', 'step_2') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_2', 'step_3') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_3', 'step_4') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_4', 'step_5') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_5', 'step_6') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('step_6', 'code_end') . '<br>');
            // print_r($no++ . ' : ' . $this->benchmark->elapsed_time('code_start', 'code_end') . '<br>');
            // print_r($memory_usage . '<br>');
            // die();
        }
        return $result;
    }
    public function _get_periode_aktif($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/general/get_periode_aktif', 'base_token', $data), true);
    }
    public function _get_data($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/setup/rekap_kependidikan/get_data', 'base_token', $data), true);
    }
    public function _doskar_list($array = array())
    {
        $array['keperluan'] = 'penilaian_kinerja';
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _isdm_detail_kinerja_akses($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_detail_kinerja_akses', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }
    public function _cek_token()
    {
        $base_token = $this->session->userdata('base_token');
        // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }
    /* mengijinkan level berikut melihat semua unit */
    public function _is_akses_univ()
    {
        // 'Kepegawain', 'Kepegawaian 2', 'Pimpinan Universitas', 'Kepegawaian (pimpinan)','Tester APP'
        if (in_array($this->session->userdata('nm_level'), array('Kepegawain', 'Kepegawaian 2', 'Pimpinan Universitas', 'Kepegawaian (pimpinan)', 'Tester APP'))) {
            return true;
        }
        return false;
    }
}
