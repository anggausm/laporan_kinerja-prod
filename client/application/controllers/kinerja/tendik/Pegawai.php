<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pegawai extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'c41bab23-4fd2-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        // $this->isdm_url = "http://192.168.19.200/server/index.php/";
        $this->isdm_url = $this->app->isdm_server();
        $this->load->helper('download');
        $this->token    = $this->app->cek_token();
        $this->link = 'kinerja/tendik/pegawai/';
        $this->userkey = $this->token['result']['user_key'];

    }

    public function index()
    {
        $data               = array();
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $periods            = $this->get_periode();
        $data['periode']    = $periods['result']['all_periode'];
        $data['p_aktif']    = $periods['result']['periode_now'];
   
         // $data['personel']       = $this->_isdm_profil((array("user_key" => $this->userkey)))['result'];

        $data['tendik']     = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

        if(empty($data['tendik'])){
            $datapeg            = $this->sinkron_peg((array("user_key" => $this->userkey)))['result'];
            if($datapeg['result'] == true){
                $data['tendik']     = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
            }
        }

        $this->tema->backend('backend/kinerja/tendik_pegawai/index', $data);
    }

    public function sinkron_peg($array = '')
    {
        $post = $array;
        
        $data['tendik']         = $this->_cek_pegawai((array("user_key" => $post['user_key'])))['result'];

         if(empty($data['tendik'])){ //jika data pegawai tidak ada, maka insert

                $personel       = $this->_isdm_kependidikan_list()['result'];

                foreach($personel as $jab){

                    if($jab['user_key'] ==  $post['user_key']) {
                        $dt['id_doskar'] = $jab['id_doskar'];
                        $dt['user_key'] = $jab['user_key'];
                        $dt['nama_doskar'] = $jab['nama_doskar'];
                        $dt['nis'] = $jab['nis'];
                        $dt['nama_unit'] = $jab['nama_unit'];
                        $dt['pekerjaan'] = $jab['pekerjaan'];
                        $dt['jabatan'] = $jab['jabatan_struktural'];
                        $dt['gol'] = $jab['gol'];
                        $dt['tmt_usm'] = $jab['tmt_usm'];
                        $dt['status_kerja'] = $jab['status_kerja'];
                        }
                    }

                    if(empty($dt)){
                        $this->session->set_flashdata('error', 'Data belum lengkap, silahkan hubungi bagian kepegawaian.');
                      
                    }

        
                   $pjp = $this->_cek_jenis_pegawai((array("pekerjaan" => $dt['pekerjaan'], "jab_struk" => $dt['jabatan'] )))['result'];
                   
                        if(!empty($dt['jabatan'])){
                            $dt['jenis_pegawai'] = '2';
                        } else {
                            $dt['jenis_pegawai'] = $pjp['id_jenis_pegawai'];
                        }

                $insert     = $this->_simpan($dt);

                if($insert['result']== false)
                {
                    return false;
                } else {
                    return true;
                }

            } else { //jika data mahasiswa sudah ada maka update

               $personel       = $this->_isdm_kependidikan_list()['result'];

                foreach($personel as $jab){
                    if($jab['user_key'] ==  $post['user_key']) {
                        $dt['id_doskar'] = $jab['id_doskar'];
                        $dt['user_key'] = $jab['user_key'];
                        $dt['nama_doskar'] = $jab['nama_doskar'];
                        $dt['nis'] = $jab['nis'];
                        $dt['nama_unit'] = $jab['nama_unit'];
                        $dt['pekerjaan'] = $jab['pekerjaan'];
                        $dt['jabatan'] = $jab['jabatan_struktural'];
                        $dt['gol'] = $jab['gol'];
                        $dt['tmt_usm'] = $jab['tmt_usm'];
                        $dt['status_kerja'] = $jab['status_kerja'];
                        }
                    }

                    if(empty($dt)){
                        $this->session->set_flashdata('error', 'Data belum lengkap, silahkan hubungi bagian kepegawaian.');
                      
                    }
        
                   $pjp = $this->_cek_jenis_pegawai((array("pekerjaan" => $dt['pekerjaan'], "jab_struk" => $dt['jabatan'] )))['result'];
                  
                        if(!empty($dt['jabatan'])){
                            $dt['jenis_pegawai'] = '2';
                        } else {
                            $dt['jenis_pegawai'] = $pjp['id_jenis_pegawai'];
                        }
                $insert     = $this->_update($dt);

                if($insert['result']== false)
                {
                    return false;
                } else {
                    return true;
                }

            } 
    }


    public function dashboard()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $post = $this->input->post();

       $periods = $this->get_periode();
        $data['periode'] = $periods['result']['all_periode'];
        
        $data['periode_dipilih'] = $post['periode'];
        $data['p_aktif'] = $this->get_data_periode((array("id" => $post['periode'])))['result'];

        $data['personel']   = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

         $data['edit'] = $this->get_skp_peg((array("jabatan" => $data['personel']['jenis_pegawai'])))['result'];
               
        $finger = $this->data_presensi((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
          $data['presensi']    = $finger['result']['presensi'];
    

         $data['rincian'] = $this->get_rincian((array("jabatan" => $data['personel']['jabatan'], "periode" => $data['periode_dipilih'])))['result'];
         foreach($data['rincian'] as $key){
            $data['period'] = $key['periode'];
            $data['nm_period'] = $key['nama_periode'];
            $data['jenis_peg'] = $key['jenis_pegawai'];
         }

         $log_book = $this->data_log_book((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'], "id_periode" => $post['periode'])));
            $data['periode_log']    = $log_book['result']['plog'];
            $data['rekap_log']     = $log_book['result']['rlog'];
         //sertifikats
         $serkom = $this->data_sertifikat((array("jenis" => '2', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['sertifikat']    = $serkom['result']['sertp'];
            $data['sert_komp']    = $serkom['result']['sertj'];
            $data['sert_1']    = $serkom['result']['sert1']; //sertifikat prestasi core value no 2.4
            $data['sert_event']    = $serkom['result']['sert2']; // sertifikat untuk poin tambahan 3.2
    
         $serjur = $this->data_sertifikat((array("jenis" => '1', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
         $data['sert_juara']    = $serjur['result']['sertj'];
         //sks
         $sk_pan = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['sk_panitia']    = $sk_pan['result']['skpanitia'];
         
         $sk_peg = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['sk_pegawai']    = $sk_peg['result']['skpegawai'];

         //ijazah
         $ijazah= $this->data_ijazah((array("periode" => $data['periode_dipilih'])));
         $data['ijazah'] = $ijazah['result']['ijazah_terakhir'];
         
         //event
         $event1 = $this->data_event((array("jenis_event" => '1', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['event_wajib']    = $event1['result']['event1'];
            
            foreach($data['event_wajib'] as $ew){
                $data['foto2'] = $ew['file_foto'];
            }
            $data['event_pilihan']    = $event1['result']['event2'];
             foreach($data['event_pilihan'] as $ep){
                $data['foto1'] = $ep['file_foto'];
            }

            //nilai kuesioner
            $data['kuesioner'] = $this->nilai_kuesioner((array("periode" => $data['periode_dipilih'])))['result'];

            //cek_skp_peg
            $data['cek_skp_peg'] = $this->cek_skp_pegawai((array("periode" => $data['periode_dipilih'])))['result'];

       if($data['personel']['jenis_pegawai'] == 1){

            $this->tema->backend('backend/kinerja/tendik_pegawai/dashboard_tendik_ns', $data);
        } else if($data['personel']['jenis_pegawai'] == 2){
            $data['lpp'] = $this->data_lpp((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'], "id_periode" => $post['periode'])));
         
            $this->tema->backend('backend/kinerja/tendik_pegawai/dashboard_tendik_s', $data);
        } else if($data['personel']['jenis_pegawai'] == 3){
            $this->tema->backend('backend/kinerja/tendik_pegawai/dashboard_tunjang_1', $data);
        }  else if(data['personel']['jenis_pegawai'] == 4){
            $this->tema->backend('backend/kinerja/tendik_pegawai/dashboard_tunjang_1', $data);
        } else {
            $this->tema->backend('backend/kinerja/tendik_pegawai/dashboard', $data);
        }
        
    }

    public function form_spek_peg($id_periode='')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

         $data['personel']   = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];        
         $data['p_aktif'] = $this->get_data_periode((array("id" => dec_data($id_periode))))['result'];
         $log_book = $this->data_log_book((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'], "id_periode" => $id_periode)));
            $data['periode_log']    = $log_book['result']['plog'];
            $data['rekap_log']      = $log_book['result']['rlog'];

         $serkom    = $this->data_sertifikat((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'], "jenis" => '2', "periode" => $data['periode_dipilih'])));
         $data['sert_komp']    = $serkom['result']['sertj'];

         $sk    = $this->data_sk((array("awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'],"jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'])));
         $data['sk_pegawai']    = $sk['result']['skpegawai'];

         $ijazah= $this->data_ijazah((array("periode" => dec_data($id_periode))));
         $data['ijazah'] = $ijazah['result']['ijazah_terakhir'];

         if($data['jenis_peg'] == 2){
            $data['lpp']       = $this->data_lpp((array("jabatan" => $jabatan, "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'] )))['result'];
         }
    
        $this->tema->backend('backend/kinerja/tendik_pegawai/form_spek_peg', $data);
    }


    //================= Ijazah

     public function tambah_ijazah()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
         $ijazah= $this->data_ijazah();
         $data['ijazah'] = $ijazah['result']['all_ijazah'];
        $data['personel']   = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];  

        $data['jenis_ijazah'] = array(
            array('id_stat' => 'SD', 'nama_stat' => 'SD'),
            array('id_stat' => 'MI', 'nama_stat' => 'MI'),
            array('id_stat' => 'SMP', 'nama_stat' => 'SMP'),
            array('id_stat' => 'MTs', 'nama_stat' => 'MTs'),
            array('id_stat' => 'SMA', 'nama_stat' => 'SMA'),
            array('id_stat' => 'SMK', 'nama_stat' => 'SMK'),
            array('id_stat' => 'MA', 'nama_stat' => 'MA'),
            array('id_stat' => 'D1', 'nama_stat' => 'D1'), 
            array('id_stat' => 'D2', 'nama_stat' => 'D2'),
            array('id_stat' => 'D3', 'nama_stat' => 'D3'),
            array('id_stat' => 'D4', 'nama_stat' => 'D4'),
            array('id_stat' => 'S1', 'nama_stat' => 'S1'),
            array('id_stat' => 'S2', 'nama_stat' => 'S2'),
            array('id_stat' => 'S3', 'nama_stat' => 'S3')
        );
      
        $this->tema->backend('backend/kinerja/tendik_pegawai/form_ijazah', $data);
    }

    public function simpan_ijazah()
    {
        $post = $this->input->post();
        $file           = $_FILES['file_ijazah'];

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
        }

            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'Ijazah_'.$post['id_peg'].'_'.$post['jenjang'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/ijazah/';
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            $this->upload->initialize($config);

             if ($this->upload->do_upload('file_ijazah')) {
            $data = array(
                'jenjang_pendidikan' => $post['jenjang'],
                'tanggal_ijazah' => $post['tgl_jazah'],
                'nama_instansi' => $post['nama_sekolah'],
                'no_ijazah' => $post['nomor'],
                'tahun_masuk' => $post['t_masuk'],
                'tahun_lulus' => $post['t_lulus'],
                'jurusan' => $post['jurusan'],
                'tempat_belajar' => $post['tempat_belajar'],
                'file' => $upload_name
            );

            $aksi = $this->_simpan_ijazah($data);
            
            if ($aksi['result'] === false) {
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            }
        } else {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            }
        
    }

    public function edit_ijazah($id_ijazah = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $ijazah= $this->data_ijazah();
         $data['ijazah'] = $ijazah['result']['all_ijazah'];
        $data['edit']          = $this->get_ijazah_edit((array("id_ijazah" => dec_data($id_ijazah))))['result'];
        $data['personel']       = $this->_cek_pegawai((array("user_key" =>$this->userkey)))['result'];

        $data['jenis_ijazah'] = array(
            array('id_stat' => 'SD', 'nama_stat' => 'SD'),
            array('id_stat' => 'MI', 'nama_stat' => 'MI'),
            array('id_stat' => 'SMP', 'nama_stat' => 'SMP'),
            array('id_stat' => 'MTs', 'nama_stat' => 'MTs'),
            array('id_stat' => 'SMA', 'nama_stat' => 'SMA'),
            array('id_stat' => 'SMK', 'nama_stat' => 'SMK'),
            array('id_stat' => 'MA', 'nama_stat' => 'MA'),
            array('id_stat' => 'D1', 'nama_stat' => 'D1'), 
            array('id_stat' => 'D2', 'nama_stat' => 'D2'),
            array('id_stat' => 'D3', 'nama_stat' => 'D3'),
            array('id_stat' => 'D4', 'nama_stat' => 'D4'),
            array('id_stat' => 'S1', 'nama_stat' => 'S1'),
            array('id_stat' => 'S2', 'nama_stat' => 'S2'),
            array('id_stat' => 'S3', 'nama_stat' => 'S3')
        );

        $data['lokasi'] = array(
            array('id_lok' => 'Dalam Negeri', 'nama_lok' => 'Dalam Negeri'),
            array('id_lok' => 'Luar Negeri', 'nama_lok' => 'Luar Negeri')
        );
  
        $this->tema->backend('backend/kinerja/tendik_pegawai/edit_ijazah', $data);
    }

    public function update_ijazah()
    {
        $post = $this->input->post();
        $file           = $_FILES['file_ijazah'];

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
        }

         if (!empty($file['name'])) {
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'Ijazah_'.$post['id_peg'].'_'.$post['jenjang'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/ijazah/';
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            $file_ijz = $upload_name;

             if ($this->upload->do_upload('file_ijazah')) {
                        $file_lama = './file/uploads/ijazah/'.$post['file_lama'];
            
                        if (is_file($file_lama)) {
                            chmod($file_lama, 0777);
                            unlink($file_lama);
                        }
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return redirect(base_url() . 'kinerja/tendik/pegawai/edit_ijazah/' . $post['id_ijazah']);
                    }
             } else {
                $file_ijz = $post['file_lama'];
             }

            $data = array(
                'id_ijazah' => dec_data($post['id_ijazah']),
                'jenjang_pendidikan' => $post['jenjang'],
                'tanggal_ijazah' => $post['tgl_jazah'],
                'nama_instansi' => $post['nama_sekolah'],
                'no_ijazah' => $post['nomor'],
                'tahun_masuk' => $post['t_masuk'],
                'tahun_lulus' => $post['t_lulus'],
                'jurusan' => $post['jurusan'],
                'tempat_belajar' => $post['tempat_belajar'],
                'file' => $file_ijz
            );

            $aksi = $this->_update_ijazah($data);
            
            if ($aksi['result'] === false) {
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            }
                
    }

     public function hapus_ijazah($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah/');
        }

        $edit         = $this->get_ijazah_edit((array("id_ijazah" => dec_data($id))))['result'];

        $file_foto_lama      = './file/uploads/ijazah/'. $edit['file'];

        if (is_file($file_foto_lama)) {
                            chmod($file_foto_lama, 0777);
                            unlink($file_foto_lama);
                        } 

        $delete         = $this->_hapus_ijazah(array("id" => dec_data($id)));

        if ($delete['result'] === false) {
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            } else {
                 $this->session->set_flashdata('pesan', $delete['message']);
                redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah');
            }
       
    }

 //============================= sertifikat 

     public function tambah_sertifikat()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $sertifikats        = $this->data_sertifikat();
        $data['sertifikat'] = $sertifikats['result']['sert'];
       
        $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
        
         $data['tingkat'] = array(
            array('id_stat' => 'lokal', 'nama_stat' => 'Lokal'),
            array('id_stat' => 'provinsi', 'nama_stat' => 'Provinsi'),
            array('id_stat' => 'nasional', 'nama_stat' => 'Nasional'),
            array('id_stat' => 'internasional', 'nama_stat' => 'Internasional')
        );

         $data['jenis'] = array(
            array('id_jenis' => '4', 'nama_jenis' => 'Juara'),
            array('id_jenis' => '2', 'nama_jenis' => 'Kompetensi/Keahlian'),
            array('id_jenis' => '1', 'nama_jenis' => 'Panitia'),
            array('id_jenis' => '3', 'nama_jenis' => 'Peserta')            
        );

        $this->tema->backend('backend/kinerja/tendik_pegawai/form_sertifikat', $data);
    }

    public function simpan_sertifikat()
    {
        $post = $this->input->post();
        $data['sertifikat']        = $this->data_sertifikat()['result'];

        $file           = $_FILES['file_sertif'];
    
        $namfol = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
        }

         $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'Sertifikat_'.$post['id_peg'].'_'.$post['jenis'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sertifikat/'.$namfol;
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if (!is_dir('file/uploads/sertifikat'))
                {
                    mkdir('./file/uploads/sertifikat', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sertifikat/' .$namfol))
                {
                    mkdir('./file/uploads/sertifikat/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }


            $this->upload->initialize($config);

            if ($this->upload->do_upload('file_sertif')) {
                $data = array(
                    'jenis_sertifikat' => $post['jenis'],
                    'no_sertifikat' => $post['nomor'],
                    'nama_sertifikat' => $post['judul'],
                    'tanggal_sertifikat' => $post['tgl_sert'],
                    'tanggal_berlaku' => $post['tgl_akhir'],
                    'penyelenggara' => $post['penyelenggara'],
                    'tingkat_sertifikat' => $post['tingkat'],
                    'nm_file' => $upload_name,
                    'nm_folder' => $namfol,
                    'ket' => $post['keterangan']
                );

                $aksi = $this->_simpan_sertifikat($data);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
                }
         } else {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
            }
        
    }

    public function edit_sertifikat($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $sertifikat             = $this->data_sertifikat();
        $data['sertifikat']    = $sertifikat['result']['sert'];
        $data['edit']            = $this->get_edit_sert((array("id_sertifikat" => dec_data($id))))['result'];
         $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

         $data['tingkat'] = array(
            array('id_stat' => 'lokal', 'nama_stat' => 'Lokal'),
            array('id_stat' => 'provinsi', 'nama_stat' => 'Provinsi'),
            array('id_stat' => 'nasional', 'nama_stat' => 'Nasional'),
            array('id_stat' => 'internasional', 'nama_stat' => 'Internasional')
        );

          $data['jenis'] = array(
            array('id_jenis' => '4', 'nama_jenis' => 'Juara'),
            array('id_jenis' => '2', 'nama_jenis' => 'Kompetensi/Keahlian'),
            array('id_jenis' => '1', 'nama_jenis' => 'Panitia'),
            array('id_jenis' => '3', 'nama_jenis' => 'Peserta')            
        );

        $this->tema->backend('backend/kinerja/tendik_pegawai/edit_sertifikat', $data);
    }

    public function update_sertifikat()
    {
        $post = $this->input->post();
        $data['sertifikat']  = $this->data_sertifikat()['result'];
        $file                = $_FILES['file_sertif'];
        $namfol              = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
        }

         if (!empty($file['name'])) {
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'Sertifikat_'.$post['id_peg'].'_'.$post['jenis'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sertifikat/'.$namfol;
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if (!is_dir('file/uploads/sertifikat'))
                {
                    mkdir('./file/uploads/sertifikat', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sertifikat/' .$namfol))
                {
                    mkdir('./file/uploads/sertifikat/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);
            if ($this->upload->do_upload('file_sertif')) {
                 $file_lama = './file/uploads/sertifikat/'.$post['folder_lama'].'/'.$post['file_lama'];
             
                        if (is_file($file_lama)) {
                            chmod($file_lama, 0777);
                            unlink($file_lama);
                        }

                $nama_file = $upload_name;
                $nama_folder = $namfol;
                 
            } else {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
            }
        } else {
             $nama_file = $post['file_lama'];
             $nama_folder = $post['folder_lama'];
        }

                 $data_update = array(
                    'id_sertifikat' => $post['id_sertifikat'],
                    'no_sertifikat' => $post['nomor'],
                    'nama_sertifikat' => $post['judul'],
                    'tanggal_sertifikat' => $post['tgl_sert'],
                    'tanggal_berlaku' => $post['tgl_akhir'],
                    'penyelenggara' => $post['penyelenggara'],
                    'tingkat_sertifikat' => $post['tingkat'],
                    'jenis_sertifikat' => $post['jenis'],
                    'nm_file' => $nama_file,
                    'nm_folder' => $nama_folder,
                    'ket' => $post['keterangan']
                );

                $aksi = $this->_update_sertifikat($data_update);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
                }
         
        
    }

     public function hapus_sertifikat($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_ijazah/');
        }

        $edit         = $this->get_edit_sert((array("id_sertifikat" => dec_data($id))))['result'];

        $file_foto_lama      = './file/uploads/sertifikat/'.$edit['nm_folder'].'/'.$edit['nm_file'];

        if (is_file($file_foto_lama)) {
                            chmod($file_foto_lama, 0777);
                            unlink($file_foto_lama);
                        } 

        $delete         = $this->_hapus_sertifikat(array("id" => dec_data($id)));
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $delete['message']);
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
        } else{
             $this->session->set_flashdata('pesan', $delete['message']);
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sertifikat');
        }
    }

    //-------------------sk panitia
     public function tambah_sk()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $sk_peg = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
        $data['sk_pegawai']    = $sk_pan['result']['sk'];

        $this->tema->backend('backend/kinerja/tendik_pegawai/tambah_sk', $data);
    }

      public function simpan_sk()
    {
        $post = $this->input->post();
        $data = array(
            'jenis_sk' => $post['jenis'],
            'tanggal_sk' => $post['tgl'],
            'judul_sk' => $post['judul'],
            'no_sk' => $post['nomor'],
            'tmt_sk' => $post['tmt'],
            'exp_sk' => $post['exp'],
            'file_sk' => $post['filesk']
        );

        $aksi = $this->_simpan_sk($data);
        
        if ($aksi['result'] == false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk');
        }
        
    }

    public function tambah_sk_panitia()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
         
         $sk_pan = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
         $data['sk_panitia']    = $sk_pan['result']['sk'];
         $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
      
        $this->tema->backend('backend/kinerja/tendik_pegawai/form_sk_panitia', $data);
    }
  

    public function simpan_sk_panitia()
    {
        $post = $this->input->post();

        $file           = $_FILES['filesk'];

        $namfol = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
        }

            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'SK '.$post['jenis_sk'].'_'.$post['id_peg'].'_' .date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sk/'.$namfol.'/'.$post['jenis_sk'];
            $config['allowed_types']    = 'pdf';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

          if (!is_dir('file/uploads/sk'))
                {
                    mkdir('./file/uploads/sk', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sk/' .$namfol))
                {
                    mkdir('./file/uploads/sk/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }
                if (!is_dir('file/uploads/sk/' .$namfol.'/'.$post['jenis_sk']))
                {
                    mkdir('./file/uploads/sk/' .$namfol.'/'.$post['jenis_sk'], 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);

            if ($this->upload->do_upload('filesk')) {

                $data = array(
                    'jenis_sk' => $post['jenis_sk'],
                    'tanggal_sk' => $post['tgl'],
                    'judul_sk' => $post['judul'],
                    'no_sk' => $post['nomor'],
                    'tmt_sk' => $post['tmt'],
                    'exp_sk' => $post['exp'],
                    'file_sk' =>  $upload_name,
                    'folder_sk' => $namfol.'/'.$post['jenis_sk']
                );

                $aksi = $this->_simpan_sk($data);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
                }
         } else {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
            }
        
    }

    public function edit_sk_panitia($id_sk = '')
    {
        $data = array();
        $post = $this->input->post();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        // $data['sk']        = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $periode)))['result'];
         $sk_pan = $this->data_sk((array("jenis_sk" => 'panitia', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
         $data['sk_panitia']    = $sk_pan['result']['sk'];
        $data['edit']        = $this->edit_sk((array("id_sk" => dec_data($id_sk))))['result'];
        $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

        $this->tema->backend('backend/kinerja/tendik_pegawai/edit_sk_panitia', $data);
    }

    public function update_sk_panitia()
    {
        $post = $this->input->post();
       
        $file                = $_FILES['filesk'];
        $namfol              = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
        }

         if (!empty($file['name'])) {
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'SK '.$post['jenis_sk'].'_'.$post['id_peg'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sk/'.$namfol.'/'.$post['jenis_sk'];
            $config['allowed_types']    = 'pdf';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

             if (!is_dir('file/uploads/sk'))
                {
                    mkdir('./file/uploads/sk', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sk/' .$namfol))
                {
                    mkdir('./file/uploads/sk/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }
                if (!is_dir('file/uploads/sk/' .$namfol.'/'.$post['jenis_sk']))
                {
                    mkdir('./file/uploads/sk/' .$namfol.'/'.$post['jenis_sk'], 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);
            if ($this->upload->do_upload('filesk')) {
                 $file_lama = './file/uploads/sk/'.$post['folder_lama'].'/'.$post['file_lama'];
             
                        if (is_file($file_lama)) {
                            chmod($file_lama, 0777);
                            unlink($file_lama);
                        }

                $nama_file = $upload_name;
                $nama_folder = $namfol.'/'.$post['jenis_sk'];
                 
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
                }
            } else {
                 $nama_file = $post['file_lama'];
                 $nama_folder = $post['folder_lama'];
            }

                 $data_update = array(
                    'id_sk' => $post['id_sk'],
                    'jenis_sk' => $post['jenis_sk'],
                    'tanggal_sk' => $post['tgl'],
                    'judul_sk' => $post['judul'],
                    'no_sk' => $post['nomor'],
                    'tmt_sk' => $post['tmt'],
                    'exp_sk' => $post['exp'],
                    'file_sk' =>  $nama_file,
                    'folder_sk' => $nama_folder
                );

                $aksi = $this->_update_skpegawai($data_update);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
                }   
    }

    //------------------ sk pegawai

   public function tambah_sk_pegawai()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $sk_peg = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['sk']    = $sk_peg['result']['sk'];
         $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
        $data['units']        = $this->data_unit()['result'];

        $this->tema->backend('backend/kinerja/tendik_pegawai/form_sk_pegawai', $data);
    }

    public function simpan_sk_pegawai()
    {
        $post = $this->input->post();
        $file           = $_FILES['filesk'];
       
        $namfol = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
        }


            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'SK '.$post['jenis_sk'].'_'.$post['id_peg'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sk/'.$namfol.'/'.$post['jenis_sk'];
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if (!is_dir('file/uploads/sk'))
                {
                    mkdir('./file/uploads/sk', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sk/' .$namfol))
                {
                    mkdir('./file/uploads/sk/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }
                if (!is_dir('file/uploads/sk/' .$namfol.'/'.$post['jenis_sk']))
                {
                    mkdir('./file/uploads/sk/' .$namfol.'/'.$post['jenis_sk'], 0777, true);
                    $dir_exist = false; // dir not exist
                }


            $this->upload->initialize($config);

            if ($this->upload->do_upload('filesk')) {
                
                $data_insert = array(
                    'jenis_sk' => $post['jenis_sk'],
                    'tanggal_sk' => $post['tgl'],
                    'judul_sk' => $post['judul'],
                    'no_sk' => $post['nomor'],
                    'tmt_sk' => $post['tmt'],
                    'exp_sk' => $post['exp'],
                    'file_sk' => $upload_name,
                    'folder_sk' =>   $namfol.'/'.$post['jenis_sk'],
                    'flus' => $post['flus'],
                    'tmt_usm' => $post['tmt_usm'],
                     'status_kerja' => $post['stat_kerja'],
                     'jenis_pekerjaan' => $post['jenis_pekerjaan']
                );


                $aksi = $this->_simpan_sk_pegawi($data_insert);
                 
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
                }
            } else {

                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', $error());
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
            }
        
    }

    public function edit_sk_pegawai($id_sk = '')
    {
        $data = array();
        $post = $this->input->post();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        // $data['sk']        = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $periode)))['result'];
        $sk_peg = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
         $data['sk']    = $sk_peg['result']['sk'];
             $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
        $data['edit']        = $this->edit_sk((array("id_sk" => dec_data($id_sk))))['result'];
        $data['units']        = $this->data_unit()['result'];
        $data['stat_kerja'] = array(
            array('id_stat' => 'kontrak', 'nama_stat' => 'kontrak'),
            array('id_stat' => 'tetap', 'nama_stat' => 'tetap')
        );

          $data['jenis_pekerjaan'] = array(
            array('id_jenis' => 'tenaga kependidikan', 'nama_jenis' => 'tenaga kependidikan'),
            array('id_jenis' => 'tenaga penunjang', 'nama_jenis' => 'tenaga penunjang')            
        );
     
        $this->tema->backend('backend/kinerja/tendik_pegawai/edit_sk_pegawai', $data);
    }

    public function update_sk_pegawai()
    {
        $post = $this->input->post();
       
        $file                = $_FILES['filesk'];
        $namfol              = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
        }

         if (!empty($file['name'])) {
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'SK '.$post['jenis_sk'].'_'.$post['id_peg'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/sk/'.$namfol.'/'.$post['jenis_sk'];
            $config['allowed_types']    = 'pdf';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

             if (!is_dir('file/uploads/sk'))
                {
                    mkdir('./file/uploads/sk', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/sk/' .$namfol))
                {
                    mkdir('./file/uploads/sk/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }
                if (!is_dir('file/uploads/sk/' .$namfol.'/'.$post['jenis_sk']))
                {
                    mkdir('./file/uploads/sk/' .$namfol.'/'.$post['jenis_sk'], 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);
            if ($this->upload->do_upload('filesk')) {
                 $file_lama = './file/uploads/sk/'.$post['folder_lama'].'/'.$post['file_lama'];
             
                        if (is_file($file_lama)) {
                            chmod($file_lama, 0777);
                            unlink($file_lama);
                        }

                $nama_file = $upload_name;
                $nama_folder = $namfol.'/'.$post['jenis_sk'];
                 
                } else {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
                }
            } else {
                 $nama_file = $post['file_lama'];
                 $nama_folder = $post['folder_lama'];
            }

                 $data_update = array(
                    'id_sk' => $post['id_sk'],
                   'jenis_sk' => $post['jenis_sk'],
                    'tanggal_sk' => $post['tgl'],
                    'judul_sk' => $post['judul'],
                    'no_sk' => $post['nomor'],
                    'tmt_sk' => $post['tmt'],
                    'exp_sk' => $post['exp'],
                    'file_sk' =>$nama_file,
                    'folder_sk' =>   $nama_folder,
                    'flus' => $post['flus'],
                    'tmt_usm' => $post['tmt_usm'],
                     'status_kerja' => $post['stat_kerja'],
                     'jenis_pekerjaan' => $post['jenis_pekerjaan']
                );

                $aksi = $this->_update_skpegawai($data_update);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
                }   
    }

    public function hapus_sk($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai/');
        }

        $edit         = $this->edit_sk((array("id_sk" => dec_data($id))))['result'];
        
        $file_foto_lama      = './file/uploads/sk/'.$edit['folder_sk'].'/'. $edit['file_sk'];

         if (is_file($file_foto_lama)) {
                                chmod($file_foto_lama, 0777);
                                unlink($file_foto_lama);
                            } 

            $delete         = $this->_hapus_sk(array("id" => dec_data($id)));
            
        if($edit['jenis_sk'] == 'pegawai'){
            $this->session->set_flashdata('pesan', $delete['message']);
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_pegawai');
        } else if($edit['jenis_sk'] == 'panitia'){
            $this->session->set_flashdata('pesan', $delete['message']);
            redirect(base_url() . 'kinerja/tendik/pegawai/tambah_sk_panitia');
            } else {
                $pesan = 'Jenis SK tidak ditemukan';
                $this->session->set_flashdata('pesan', $pesan);
                redirect(base_url() . 'kinerja/tendik/pegawai/');
            }
    }

    //----------------------- event

     public function form_event($p_awal = '', $p_akhir='', $jenis='')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $event1 = $this->data_event((array("jenis_event" => $jenis, "periode" => $data['periode_dipilih'], "awal" => dec_data($p_awal), "akhir" => dec_data($p_akhir))));
      
        if($jenis == 1 ){
            $data['event']    = $event1['result']['event1'];
        } else {   
            $data['event']    = $event1['result']['event2'];
        }

        
         $data['foto'] = $event1['result']['event_usr'];

        $data['finger'] = 'rest finger/ db finger';

        $data['awal'] = $p_awal;
        $data['akhir'] = $p_akhir;
        $data['jenis_event'] = $jenis;
       
        $this->tema->backend('backend/kinerja/tendik_pegawai/form_event', $data);
    }

    public function form_event_integritas($p_awal = '', $p_akhir='', $jenis='')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

         $event1 = $this->data_event((array("jenis_event" => dec_data($jenis), "periode" => $data['periode_dipilih'], "awal" => dec_data($p_awal), "akhir" => dec_data($p_akhir))));
         $data['event_wajib']    = $event1['result']['event1'];
        
        $data['awal'] = $p_awal;
        $data['akhir'] = $p_akhir;
        $data['jenis_event'] = $jenis;
       
        $this->tema->backend('backend/kinerja/tendik_pegawai/form_event_wajib', $data);
    }

    public function edit_event()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['sk']        = $this->data_sk((array("jenis_sk" => 'pegawai', "periode" => $periode)))['result'];

        $data['jenis_sk'] = array(
            array('id_stat' => 'panitia', 'nama_stat' => 'Panitia Kegiatan'),
            array('id_stat' => 'pegawai', 'nama_stat' => 'Pengangkatan Pegawai'),
            array('id_stat' => '', 'nama_stat' => 'lainnya')
        );

        $this->tema->backend('backend/kinerja/tendik_pegawai/form_event', $data);
    }

    public function upload_foto()
    {
        $post = $this->input->post();
        $file           = $_FILES['file_foto'];
        $namfol              = date('Y');

            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'Foto_'.dec_data($post['idEvent']).'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/event/'.$namfol.'/';
            $config['allowed_types']    = 'pdf|jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

              if (!is_dir('file/uploads/event'))
                {
                    mkdir('./file/uploads/event', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/event/' .$namfol))
                {
                    mkdir('./file/uploads/event/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);

             if ($this->upload->do_upload('file_foto')) {
            $datainsert = array(
                'id_event' => dec_data($post['idEvent']),
                'nama_folder' => $namfol,
                'file_foto' => $upload_name
            );
            
            $aksi = $this->_simpan_foto($datainsert);
            
            if ($aksi['result'] === false) {
                $this->session->set_flashdata('error', $aksi['message']);
                 return redirect(base_url() . 'kinerja/tendik/pegawai/form_event/'.$post['awwal'].'/'.$post['akhhir'].'/'.$post['jenis']);
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                 return redirect(base_url() . 'kinerja/tendik/pegawai/form_event/'.$post['awwal'].'/'.$post['akhhir'].'/'.$post['jenis']);
            }
        } else {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/pegawai/form_event/'.$post['awwal'].'/'.$post['akhhir'].'/'.$post['jenis']);
            }
        
    }

    public function hapus_foto($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/pegawai');
        }
        
        $edit         = $this->get_kep((array("id" => dec_data($id))))['result'];

        $file_foto_lama      = './file/uploads/event/'. $edit['nama_folder'].'/'.$edit['file_foto'];

        if (is_file($file_foto_lama)) {
                            chmod($file_foto_lama, 0777);
                            unlink($file_foto_lama);
                        } 

        $delete         = $this->_hapus_foto(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/pegawai');
    }
    

    //----------------------------------
    public function kuesioner($id_periode='')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $data['personel']       = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];
        foreach($data['personel'] as $val){
            $jabatan = $val['jabatan'];
            $data['unit'] = $val['nama_unit'];
        }
      
        $data['list']       = $this->_list_dinilai((array("id_periode" => dec_data($id_periode))))['result'];

        $this->tema->backend('backend/kinerja/tendik_pegawai/form_kuesioner', $data);
    }

    public function detail_kuesioner()
    {

        $post           = $this->input->post();
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'r');
        $datapeg        = $this->sinkron_peg((array("user_key" => $post['dinilai'])))['result'];
   
        $periods = $this->get_periode();
        $data['periode'] = $periods['result']['periode_now'];

        $data['profil'] = $this->_cek_pegawai(array('user_key' => $post['dinilai']))['result'];

        $data['tanya']  = $this->_pernyataan(array('dinilai' => $post['dinilai'], 'jenis_peg' => $data['profil']['jenis_pegawai'], 'id_periode' => $data['periode']['id_periode'] ))['result']['data'];
        $data['button'] = $this->_pernyataan(array('dinilai' => $post['dinilai']))['result']['status'];

        $this->tema->backend('backend/kinerja/tendik_pegawai/detail_kuesioner', $data);
    }

    public function simpan_kuesioner()
    {
        $post       = $this->input->post();

        $response   = $this->_save_kuesioner($post);

        if ($response['result'] === true) {
            $this->session->set_flashdata('pesan', 'Data berhasil disimpan');
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('pesan', 'Data gagal disimpan.');
            $this->session->set_flashdata('type', 'alert-danger');
        }
        return redirect(base_url($this->link));
    }

    //-----------------------------
    public function simpan_ajuan()
    {
        $post = $this->input->post();

        if (empty($post)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'backend/kinerja/tendik/pegawai/');
        }

        $data['personel']   = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

        $data['edit'] = $this->get_skp_peg((array("jabatan" => $data['personel']['jenis_pegawai'])))['result'];

        foreach($data['edit'] as $skp){
                        
                $data = array(
                    'id_skp' => $skp['id_skp'],
                    'user_key' => $this->userkey,
                    'skor' => $post['skor'.$skp['urutan']],
                    'bobot' => $post['bobot'.$skp['urutan']],
                    'nilai' => $post['nilai'.$skp['urutan']],
                    'total_nilai' => $post['ttl_nilai'],
                    'periode_penilaian' => $post['id_periode']
                );

                $aksi = $this->_simpan_ajuan($data);
            }
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'backend/kinerja/tendik_pegawai/');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'backend/kinerja/tendik_pegawai/');
                }
        }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }

     public function get_skp_peg($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/skp_peg', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus', 'base_token', $data), true);
    }

    public function get_rincian($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/rincian', 'base_token', $data), true);
    }

    public function data_log_book($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_log_book', 'base_token', $data), true);
    }

    public function data_presensi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_wd', 'base_token', $data), true);
    }
    //--------------- sertifikat
    public function data_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_sertifikat', 'base_token', $data), true);
    }

      public function _simpan_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sertifikat', 'base_token', $data), true);
    }

     public function _update_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/update_sertifikat', 'base_token', $data), true);
    }

    public function _hapus_sertifikat($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus_sertifikat', 'base_token', $data), true);
    }

    //----------------------- SK

    public function data_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_sk', 'base_token', $data), true);
    }

    public function _simpan_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sk', 'base_token', $data), true);
    }

    public function _simpan_sk_pegawi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_sk_pegawai', 'base_token', $data), true);
    }

    public function _update_skpegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/update_sk_pegawai', 'base_token', $data), true);
    }

     public function _hapus_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus_sk', 'base_token', $data), true);
    }

   
    //------------------- ijazah

    public function data_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_ijazah', 'base_token', $data), true);
    }

    public function get_ijazah_edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/ijazah_edit', 'base_token', $data), true);
    }

    public function _simpan_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_ijazah', 'base_token', $data), true);
    }

     public function _update_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/update_ijazah', 'base_token', $data), true);
    }

    public function _hapus_ijazah($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus_ijazah', 'base_token', $data), true);
    }

        public function get_edit_sert($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/edit_sert', 'base_token', $data), true);
    }

    //-------------------------

    public function get_data_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/data_periode', 'base_token', $data), true);
    }

    public function data_unit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_unit', 'base_token', $data), true);
    }

    public function data_prodi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_prodi', 'base_token', $data), true);
    }

    public function edit_sk($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/edit_sk', 'base_token', $data), true);
    }

    //---------------------


    public function _cek_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_data_pegawai', 'base_token', $data), true);
    }

    public function _cek_jenis_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_jenis_pegawai', 'base_token', $data), true);
    }

    public function _hapus_foto($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/pegawai/hapus_foto', 'base_token', $data), true);
    }

        public function get_kep($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/edit_event_peserta', 'base_token', $data), true);
    }

     //--------------------- event

    public function data_event($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_event', 'base_token', $data), true);
    }

    public function _simpan_foto($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_foto', 'base_token', $data), true);
    }

    public function data_lpp($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_lpp', 'base_token', $data), true);
    }

    public function _list_dinilai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/pegawai/list_rekan', 'base_token', $data), true);
    }

    public function _pernyataan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/pegawai/list_pertanyaan', 'base_token', $data), true);
    }

    public function _save_kuesioner($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_kuesioner', 'base_token', $data), true);
    }

    public function nilai_kuesioner($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/nilai_kuesioner', 'base_token', $data), true);
    }

     public function _simpan_ajuan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pegawai/simpan_ajuan', 'base_token', $data), true);
    }

    public function cek_skp_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_skp_pegawai', 'base_token', $data), true);
    }



    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_kependidikan_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/kependidikan_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }



}
