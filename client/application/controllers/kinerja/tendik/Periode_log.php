<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Periode_log extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '6393a968-7af1-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->judul     = 'Periode Log';
        $this->link = 'kinerja/tendik/periode_log';

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $periode = $this->get_periode();
        $data['periode'] = $periode['result']['semua_periode'];

        $this->tema->backend('backend/kinerja/tendik_periodelog/index', $data);
    }

    public function data()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $input        = $this->input->post();

        $periode = $this->get_periode();
        $data['periode'] = $periode['result']['semua_periode'];
        $data['id_periode'] = $this->input->post('periode');
        $periodelog = $this->get_periode((array("id" => $input['periode'] )));
        $data['default'] = $periodelog['result']['periode_log'];


        $this->tema->backend('backend/kinerja/tendik_periodelog/data', $data);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
             'id_periode' => $post['id_periode'],
            'nama_periode' => $post['nama_periode'],
            'tgl_awal' => $post['tgl_awal'],
            'tgl_akhir' => $post['tgl_akhir'],
            'status' => $post['ket']
        );

        $aksi = $this->_simpan($data);

        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/periode_log/tambah');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/periode_log/index');
        }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $periode = $this->get_periode();
        $data['periode'] = $periode['result']['semua_periode'];
        
        $data['id_periode'] = dec_data($id);
    
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

        $periodelog = $this->get_periode((array("id" =>  $data['edit']['periode_kinerja'] )));
        $data['default'] = $periodelog['result']['periode_log'];

         $data['stats'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Aktif'),
            array('id_stat' => '0', 'nama_stat' => 'Tidak Aktif')
        );
        
        $this->tema->backend('backend/kinerja/tendik_periodelog/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'id' => dec_data($post['id_periode']),
            'nama_periode' => $post['nama_periode'],
            'tgl_awal' => $post['tgl_awal'],
            'tgl_akhir' => $post['tgl_akhir'],
            'ket' => $post['ket']
        );

        $aksi = $this->_update($data);
         
        if ($aksi['result'] === false) {
            $id_log = trim($post['id']);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/periode_log/edit/'. $id_log);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/periode_log/index');
        }
        
    }

     public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/periode_log/index');
        }

        $delete         = $this->_hapus(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/periode_log/index');
    }


    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/get_log_book', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/periode_log/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/periode_log/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/periode_log/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/periode_log/hapus', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/periode_log/default', 'base_token', $data), true);
    }

    
}
