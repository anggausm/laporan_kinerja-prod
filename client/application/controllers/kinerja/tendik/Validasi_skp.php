<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_skp extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = 'e28f2625-6456-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->isdm_url = "http://192.168.19.200/server/index.php/";
        $this->load->helper('download');
        $this->token    = $this->app->cek_token();
        $this->userkey = $this->token['result']['user_key'];
    }

    public function index()
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'r');
        $periode        = $this->get_periode();
        $data['periode']= $periode['result']['all_periode'];
 
        $this->tema->backend('backend/kinerja/tendik_validasi_skp/index', $data);
    }

     public function data()
    {
        $data             = array();
        $data['crud']     = $this->app->AksesMenu($this->appUrl, 'r');
        $post             = $this->input->post();
        $data['p_chosen'] = $post['periode'];   

        $periode        = $this->get_periode();
        $data['periode']  = $periode['result']['all_periode'];

        $data['list']     = $this->_list_dinilai((array("periode" => $post['periode'])))['result'];
    
        $data['p_aktif']  = $this->get_data_periode((array("id" => $post['periode'])))['result'];

        $this->tema->backend('backend/kinerja/tendik_validasi_skp/data', $data);

    }

    public function detail($userkey = '', $periode = '')
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $post['id_periode'] = dec_data($periode);
        $post['user_key'] = dec_data($userkey);
        
        $data['periode_dipilih'] = $post['periode'];
        $data['p_aktif'] = $this->get_data_periode((array("id" => $post['id_periode'])))['result'];

         $data['personel']   = $this->_cek_pegawai((array("user_key" => $post['user_key'])))['result'];
         $data_skp = $this->get_skp_pegawai((array("periode" => $post['id_periode'], "user_key" => $post['user_key'])));
         $data['skp_peg'] = $data_skp['result']['skp_peg'];
         $data['skp_valid'] = $data_skp['result']['skp_valid'];
         $data['rekap_nilai'] = $data_skp['result']['nilai'];

         //get dokumen prasyarat
         $serkom = $this->get_dokumen((array("user_key" => $post['user_key'], "periode" => $data['periode_dipilih'], "awal" => $data['p_aktif']['pengisian_mulai'], "akhir" => $data['p_aktif']['pengisian_selesai'])));
            $data['dok1']    = $serkom['result']['sk_1_2'];
            $data['dok2']    = $serkom['result']['ijazah_1_2'];
            $data['dok3']    = $serkom['result']['sertifikat_1_2'];
            $data['dok4']    = $serkom['result']['sertifikat_2_4'];
            $data['dok5']    = $serkom['result']['sertifikat_3_1'];
            $data['dok6']    = $serkom['result']['sk_3_2'];
            $data['foto']    = $serkom['result']['foto_event'];

        if($data['personel']['jenis_pegawai'] == 1){
            $this->tema->backend('backend/kinerja/tendik_validasi_skp/detail_le', $data); 
        } else if($data['personel']['jenis_pegawai'] == 2){ 
            $this->tema->backend('backend/kinerja/tendik_validasi_skp/detail_2', $data);
        } else if($data['personel']['jenis_pegawai'] == 3){
            $this->tema->backend('backend/kinerja/tendik_validasi_skp/detail_3', $data);
        }  else if(data['personel']['jenis_pegawai'] == 4){
            $this->tema->backend('backend/kinerja/tendik_validasi_skp/detail_3', $data);
        } else {
            $this->tema->backend('backend/kinerja/tendik_validasi_skp/detail', $data); 
        }     
    }

    //  public function validasi_pengajuan($userkey = '', $periode = '')
    // {
    //     $post = $this->input->post();
    //     $data = array(
    //         'periode_penilaian' => dec_data($periode),
    //         'user_key' => dec_data($userkey)
    //     );

    //     $aksi = $this->_update_validasi($data);
         
    //     if ($aksi['result'] === false) {
    //         $id_per = trim($periode);
    //          $id_user = trim($userkey);
    //         $this->session->set_flashdata('error', $aksi['message']);
    //         return redirect(base_url() . 'kinerja/tendik/validasi_skp/edit/'. $id_user.'/'.$id_per);
    //     } else {
    //         $this->session->set_flashdata('pesan', $aksi['message']);
    //         return redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
    //     }
        
    // }

    public function validasi($userkey = '', $periode = '')
    {
        $post = $this->input->post();
        $data = array(
            'periode_penilaian' => dec_data($periode),
            'user_key' => dec_data($userkey),
            'ket' => '1'
        );

        $aksi = $this->_update_validasi($data);
         
        if ($aksi['result'] === false) {
             $id_per = trim($periode);
             $id_user = trim($userkey);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_skp/detail/'. $id_user.'/'.$id_per);
        } else {

             $skp_valid = $this->get_skp_valid((array("periode" => dec_data($periode), "user_key" => dec_data($userkey))))['result'];
             $sekor = $skp_valid['total_nilai']*100;
             if($sekor >= 81){
                $nilai = 'Sangat Baik';
                $kategori = '100';
             } else if($sekor >= 60){
                $nilai = 'Baik';
                $kategori = '100';
             } else if($sekor >= 40){
                $nilai = 'Cukup';
                $kategori = '75';
             } else if($sekor >= 20){
                $nilai = 'Buruk';
                $kategori = '50';
            } else {
                $nilai = 'Sangat Buruk';
                $kategori = '25';
            }

             $datainsert = array(
                    'id_periode' => dec_data($periode),
                    'user_key' => dec_data($userkey),
                    'skor' => $skp_valid['total_nilai'],
                    'nilai' => $nilai,
                    'kategori' => $kategori
                );

             $aksi = $this->_insert_rekap($datainsert);

            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
        }
        
    }

     public function revisi($userkey = '', $periode = '')
    {
        $post = $this->input->post();

        $data = array(
            'id_periode' => dec_data($periode),
            'user_key' => dec_data($userkey),
            'ket' => 0
        );

        $aksi = $this->_update_revisi($data);
        
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
             return redirect(base_url() . 'kinerja/tendik/validasi_skp/detail/'. $id_user.'/'.$id_per);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
        }
        
    }

     public function unvalidasi($userkey = '', $periode = '')
    {
        $post = $this->input->post();

         $data = array(
            'id_periode' => dec_data($periode),
            'user_key' => dec_data($userkey),
            'ket' => NULL
        );

        $aksi = $this->_update_validasi($data);
         
        if ($aksi['result'] === false) {
             $id_per = trim($periode);
             $id_user = trim($userkey);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_skp/detail/'. $id_user.'/'.$id_per);
        } else {

            $aksi = $this->_unvalidasi((array("periode" => dec_data($periode), "userkey" => dec_data($userkey))))['result'];

            if ($aksi['result'] === false) {
                $this->session->set_flashdata('error', $aksi['message']);
                 return redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
            }
        }
    }

   

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
        }

        $delete         = $this->_batal(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/validasi_skp/index');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/

    public function get_skp_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/skp_pegawai', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/periode', 'base_token', $data), true);
    }

    public function _update_validasi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_skp/update_validasi', 'base_token', $data), true);
    }    

     public function _update_revisi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_skp/update_revisi', 'base_token', $data), true);
    }  

    public function _unvalidasi($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_skp/update_unvalidasi', 'base_token', $data), true);
    }    

    public function get_periode_skp($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/p_skp', 'base_token', $data), true);
    }

    public function get_skp_valid($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/get_skp_valid', 'base_token', $data), true);
    }

    public function _insert_rekap($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_skp/simpan', 'base_token', $data), true);
    }   

    public function get_data_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/data_periode', 'base_token', $data), true);
    }

         public function _list_dinilai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_skp/list', 'base_token', $data), true);
    }

        public function get_dokumen($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_skp/log_pegawai', 'base_token', $data), true);
    }

    //=====unused






    public function _batal($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_skp/hapus', 'base_token', $data), true);
    }

    public function _cek_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_data_pegawai', 'base_token', $data), true);
    }

     // REST API KEPEGAWAIAN
   public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_kependidikan_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/kependidikan_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }






}
