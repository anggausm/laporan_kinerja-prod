<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log_book extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '45268465-4fce-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');

    }

   public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');

        $periods = $this->get_periode();
            $data['p_all'] = $periods['result']['all_periode'];
            $data['p_aktif'] = $periods['result']['periode_aktif'];        

        if (!empty($this->input->post())) {
             $data['id_periode'] = $this->input->post('periode');
            $data['default'] = $this->get_default((array("id_periode" => $this->input->post('periode') )))['result'];
        } else {            
            $data['id_periode'] = $periods['result']['periode_aktif']['id_periode'];
            $data['default'] = $this->get_default((array("id_periode" => $data['id_periode'] )))['result'];
        }

        $this->tema->backend('backend/kinerja/log_book/index', $data);
    }

     public function tambah()
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $periods = $this->get_periode();
        $data['p_log'] = $periods['result']['periode_log'];  

        $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/log_book/tambah', $data);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'tanggal_log_book' => $post['tgl_log_book'],
            'tugas' => $post['tugas'],
            'status_pekerjaan' => $post['stat_kerja'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_simpan($data);
        
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/log_book/tambah');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/log_book/index');
        }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/log_book/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'id_log_book' => dec_data($post['id']),
            'tanggal_log_book' => $post['tgl_log_book'],
            'tugas' => $post['tugas'],
            'status_pekerjaan' => $post['stat_kerja'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_update($data);
         
        if ($aksi['result'] === false) {
            $id_log = trim($post['id']);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/log_book/edit/'. $id_log);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/log_book/index');
        }
        
    }

    public function detail($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/log_book/detail', $data);
    }

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/log_book/index');
        }

        $delete         = $this->_hapus(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/log_book/index');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/log_book/default', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/log_book/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/log_book/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/log_book/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/log_book/hapus', 'base_token', $data), true);
    }

    public function get_periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/log_book/p_log', 'base_token', $data), true);
    }
}
