<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lpp extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '91614dbd-5f5e-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->title = 'LPP';
        $this->token    = $this->app->cek_token();
        $this->userkey = $this->token['result']['user_key'];

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        if (!empty($this->input->post())) {
            $data['bulan'] = $this->input->post('bulan');
            $data['tahun'] = $this->input->post('tahun');
            $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        } else {
            $data['bulan'] = date('m');
            $data['tahun'] = date('Y');
            
            $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        }

        $data['pa'] =  $this->_periode_aktif()['result'];

        $data['tendik']     = $this->_cek_pegawai((array("user_key" => $this->userkey)))['result'];

         if($data['tendik']['jenis_pegawai'] == '2' ){
             $this->tema->backend('backend/kinerja/lpp/index', $data);
            }else {
                $this->tema->backend('backend/kinerja/lpp/index_ns', $data);
            }       
    }

    public function simpan()
    {
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'c');
        $post = $this->input->post();
        $file           = $_FILES['file_lpp'];
        $pa = $post['periode_aktiff'];
        $namfol = date('Y');

        if (empty($post) or empty($file)) {
            $this->session->set_flashdata('pesan', 'Maaf proses anda belum bisa kami lanjutkan..');
            return redirect(base_url() . 'kinerja/lpp/index');
        }

            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'LPP_'.$post['id_pegawai'].'_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/lpp/'.$namfol.'/'.$pa;
            $config['allowed_types']    = 'pdf';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if (!is_dir('file/uploads/lpp'))
                {
                    mkdir('./file/uploads/lpp', 0777, true);
                }
                $dir_exist = true; // flag for checking the directory exist or not
                if (!is_dir('file/uploads/lpp/' .$namfol))
                {
                    mkdir('./file/uploads/lpp/' .$namfol, 0777, true);
                    $dir_exist = false; // dir not exist
                }
                if (!is_dir('file/uploads/lpp/' .$namfol.'/'.$pa))
                {
                    mkdir('./file/uploads/lpp/' .$namfol.'/'.$pa, 0777, true);
                    $dir_exist = false; // dir not exist
                }

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file_lpp')) {

                 $datainsert = array(
                        'periode_kinerja' => $post['periode_aktiff'],
                        'periode_lpp' => $post['tahun'].'-'.$post['bulan'],
                        'tugas' => $post['tugas'],
                        'nama_folder' => $namfol.'/'.$pa,
                        'nama_file' => $upload_name
                    );
                 

                $aksi = $this->_simpan($datainsert);
                
                if ($aksi['result'] === false) {
                    $this->session->set_flashdata('error', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/lpp/index');
                } else {
                    $this->session->set_flashdata('pesan', $aksi['message']);
                    return redirect(base_url() . 'kinerja/tendik/lpp/index');
                }
               

            } else {

                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error', $this->upload->display_errors());
                return redirect(base_url() . 'kinerja/tendik/lpp/index');
            }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];
        $data['pa'] =  $this->_periode_aktif()['result'];
        if (!empty($this->input->post())) {
            $data['bulan'] = $this->input->post('bulan');
            $data['tahun'] = $this->input->post('tahun');
            $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        } else {
            $data['bulan'] = date('m');
            $data['tahun'] = date('Y');
            
            $data['default'] = $this->get_default((array("bulan" => $data['bulan'],"tahun" => $data['tahun'] )))['result'];
        }
        $data['pa'] =  $this->_periode_aktif()['result'];

        $this->tema->backend('backend/kinerja/lpp/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $file           = $_FILES['file_lpp'];
        $namfol = date('Y');
        $pa = $post['periode_aktiff'];

          if (!empty($file['name'])) {
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'LPP_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;

            $config['upload_path']      = './file/uploads/lpp/'.$namfol.'/'.$pa;
            $config['allowed_types']    = 'pdf';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $folder = $namfol.'/'.$pa;
            $filee = $upload_name;

            if ($this->upload->do_upload('file_lpp')) {
                        if(!empty($input['file_lpp'])){
                        $file_lama = './file/uploads/lpp/'. $post['folder_lama'].'/'.$post['file_lama'];
                    
                        if (is_file($file_lama)) {
                            chmod($file_lama, 0777);
                            unlink($file_lama);
                        }
                    }
                    }
                    else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    return redirect(base_url() . 'kinerja/tendik/lpp/edit/' . enc_data($input['id']));
                }
         } else {
            $folder = $post['folder_lama'];
            $filee = $post['file_lama'];
         }
    
            $data_update = array(
                'id_lpp' => dec_data($post['id']),
                'tugas' => $post['tugas'],
                'nama_folder' => $folder,
                'nama_file' => $filee
            );

            $aksi = $this->_update($data_update);
             
            if ($aksi['result'] === false) {
                $id_log = trim($post['id']);
                $this->session->set_flashdata('error', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/lpp/edit/'. $id_log);
            } else {
                $this->session->set_flashdata('pesan', $aksi['message']);
                return redirect(base_url() . 'kinerja/tendik/lpp/index');
            }
        
        
    }

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/lpp/index');
        }

        $delete         = $this->_hapus(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/lpp/index');
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function get_default($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/lpp/default', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/lpp/simpan', 'base_token', $data), true);
    }
    public function _edit($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/lpp/edit', 'base_token', $data), true);
    }
    public function _update($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/lpp/update', 'base_token', $data), true);
    }

    public function _hapus($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/lpp/hapus', 'base_token', $data), true);
    }

    public function _periode_aktif($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_aktif', 'base_token', $data), true);
    }

    public function _cek_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_data_pegawai', 'base_token', $data), true);
    }
}
