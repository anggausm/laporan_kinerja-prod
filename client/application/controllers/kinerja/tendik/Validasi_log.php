<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_log extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '59f09a3f-5043-11ef-9a0f-56cb879f2d55';
        $this->base_url = $this->app->get_server('base_server');
        $this->isdm_url = $this->app->isdm_server();
        $this->server_file = $this->app->server_file();
        $this->load->helper('download');
        $this->token    = $this->app->cek_token();
        $this->link = 'kinerja/tendik/validasi_log';
    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
            
           
            $user                   = $this->token['result'];
            $userkey                = $user['user_key'];

            $data['profil'] = $this->_isdm_profil(array('user_key' => $userkey))['result'];
             $data['pa'] =  $this->_periode_aktif()['result'];
             
            // $data['pegawai'] = $this->_isdm_kependidikan_list()['result'];
            $data['pegawai']     = $this->_list_dinilai((array("periode" => $data['pa']['id_periode'])))['result'];   
            // var_dump($data['pa']); die();
            $data['p_log'] = $this->get_periode_log((array("id_periode" => $data['pa']['id_periode'] )))['result'];
        
        $this->tema->backend('backend/kinerja/tendik_validasi_log/index', $data);
    }

     public function data()
    {
        $data = array();
        $post = $this->input->post();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['pa'] =  $this->_periode_aktif()['result'];
        $data['p_log'] = $this->get_periode_log((array("id_periode" => $data['pa']['id_periode'] )))['result'];
        $data['profil']         = $this->_cek_pegawai((array("user_key" => $post['pegawai'])))['result'];
        $data['jenis_peg'] = $data['profil']['jenis_pegawai'];
        
            $user                   = $this->token['result'];
            $userkey                = $user['user_key'];

            $data['profil'] = $this->_isdm_profil(array('user_key' => $user['user_key']))['result'];
             // $data['pegawai'] = $this->_isdm_kependidikan_list()['result'];
             $data['pegawai']     = $this->_list_dinilai((array("periode" => $data['pa']['id_periode'])))['result'];  
        
        $p_log = $this->get_data_pl((array("id_p_log" => $post['periode'] )))['result'];
 
        $logs = $this->get_log_pegawai((array("userkey" => $post['pegawai'], "id_periode" => $post['periode'], "periode_pen" => $data['pa']['id_periode'] )))['result'];
      
         $data['data_log']   = $logs['logp'];
         $data['valid_log']   = $logs['vlog'];

        $data['userkey_pegawai'] = $post['pegawai'];
        $data['log_mg_cosen'] = $post['periode'];
        $data['kinerja_periode'] = $data['pa']['id_periode'];

        $this->tema->backend('backend/kinerja/tendik_validasi_log/data', $data);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $data = array(
            'id_peg'=> $post['user_pegawai'],
            'p_log' => $post['periode_log'],
            'p_pen' => $post['periode_pen'],
            'indi1' => $post['ind_1'],
            'indi2' => $post['ind_2'],
            'indi3' => $post['ind_3'],
            'indi4' => $post['ind_4'],
            'indi5' => $post['ind_5'],
            'indi6' => $post['ind_6'],
            'nilai' => $post['ind_1'] + $post['ind_2'] + $post['ind_3'] + $post['ind_4'] + $post['ind_5'] + $post['ind_6']
        );

        $aksi = $this->_simpan($data);
        
        if ($aksi['result'] === false) {
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_log/tambah');
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_log/index');
        }
        
    }

     public function edit($id = '')
    {
        $data = array();

        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        $data['edit'] = $this->_edit((array("id" => dec_data($id))))['result'];

         $data['stat_kerja'] = array(
            array('id_stat' => '1', 'nama_stat' => 'Selesai'),
            array('id_stat' => '2', 'nama_stat' => 'Berlanjut'),
            array('id_stat' => '3', 'nama_stat' => 'Tidak Selesai')
        );
        $this->tema->backend('backend/kinerja/tendik_validasi_log/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = array(
            'id_validasi_log' => dec_data($post['id']),
            'tanggal_validasi_log' => $post['tgl_validasi_log'],
            'tugas' => $post['tugas'],
            'status_pekerjaan' => $post['stat_kerja'],
            'ket' => $post['keterangan']
        );

        $aksi = $this->_update($data);
         
        if ($aksi['result'] === false) {
            $id_log = trim($post['id']);
            $this->session->set_flashdata('error', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_log/edit/'. $id_log);
        } else {
            $this->session->set_flashdata('pesan', $aksi['message']);
            return redirect(base_url() . 'kinerja/tendik/validasi_log/index');
        }
        
    }

    public function hapus($id = '')
    {
        $data           = array();
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'd');
        if (empty($id)) {
            $this->session->set_flashdata('error', 'Maaf proses anda belum bisa kami lanjutkan..');
            redirect(base_url() . 'kinerja/tendik/validasi_log/index');
        }

        $delete         = $this->_batal(array("id" => dec_data($id)));
        $this->session->set_flashdata('pesan', $delete['message']);
        redirect(base_url() . 'kinerja/tendik/validasi_log/index');
    }



    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/


     public function _periode_aktif($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_aktif', 'base_token', $data), true);
    }

   public function get_periode_log($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/p_log', 'base_token', $data), true);
    }
    public function get_log_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/log_pegawai', 'base_token', $data), true);
    }

    public function get_data_pl($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/val_log/data_pl', 'base_token', $data), true);
    }

    public function _simpan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/val_log/simpan', 'base_token', $data), true);
    }

    public function _batal($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('DELETE', $this->base_url . 'api/kinerja/val_log/hapus', 'base_token', $data), true);
    }

    public function _cek_pegawai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pegawai/cek_data_pegawai', 'base_token', $data), true);
    }

        public function _list_dinilai($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/val_log/list', 'base_token', $data), true);
    }

     // REST API KEPEGAWAIAN
   public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_kependidikan_list($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url . 'connection/isdm/kependidikan_list', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }



    


}
