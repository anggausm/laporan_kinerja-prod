<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pak_dosen extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url  = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->menu_key = $this->lib_menu_key->kinerja_dosen_pak();
        $this->base_url = $this->app->get_server('base_server');
        $this->sia_url  = $this->app->get_server('sia');
        $this->kpta_url = $this->app->get_server('kpta');
        $this->isdm_url = "https://isdm.usm.ac.id/server/index.php/";
        $this->view     = 'backend/kinerja/dosen_pak/';
        $this->link     = 'kinerja/dosen/pak_dosen/';
        $this->sample   = '3b68908b-6f68-11ea-89e7-1cb72c27dd68'; // khoirudin  3b689547-6f68-11ea-89e7-1cb72c27dd68
    }

    public function index()
    {
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'r');
        $data['periode']= $this->_get_periode()['result'];
        
        $this->tema->backend($this->view . 'index', $data);
    }

    public function pak($id_periode)
    {
        $data['crud']   = $this->app->AksesMenu($this->appUrl, 'r');
        $id_periode     = dec_data_url($id_periode);
        $session        = $this->session->userdata();
        $user           = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }

        $pak            = $this->_get_pak(array('id_periode' => $id_periode))['result'];
        $data['periode']= $pak['periode'];
        if (!empty($pak['profil'])) {
            $data['profil'] = $pak['profil'];
        } else {
            $profil = $this->_isdm_profil(array('user_key' => $user['user_key']))['result'];
            $data['profil'] = array('id_doskar'         => $profil['id_doskar'],
                                    'user_key'          => $profil['user_key'],
                                    'nama_doskar'       => $profil['nama_doskar'],
                                    'nidn'              => $profil['nidn'],
                                    'nis'               => $profil['nis'],
                                    'fakultas'          => $profil['nama_unit1'],
                                    'program_studi'     => $profil['program_studi'],
                                    'status_dosen'      => $profil['status_kerja'],
                                    'jabatan_akademik'  => $profil['jafa'],
                                    'kum'               => $profil['kum'],
                                    'tmt_jab_akademik'  => $profil['tmt_jafa'],
                                    'status_keaktifan'  => $profil['status_keaktifan'] );
        }
        $data['unsur']      = $pak['unsur'];
        $data['histori']    = $pak['histori'];
        $data['kriteria']   = $pak['kriteria'];
        $data['kriteria_score']   = $pak['kriteria_score'];
        $data['button']     = $pak['button'];

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
        
        $this->tema->backend($this->view . 'pak', $data);
    }

    public function pak_kegiatan($periode, $unsur)
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $id_periode         = dec_data_url($periode);
        $id_unsur           = dec_data_url($unsur);
        $get_data           = $this->_get_pak_kegiatan(array('id_periode' => $id_periode, 'id_unsur' => $id_unsur))['result'];

        $data['periode']    = $get_data['periode'];
        $data['unsur']      = $get_data['unsur'];
        $data['kegiatan']   = $get_data['kegiatan'];
                
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
        
        $this->tema->backend($this->view . 'pak_kegiatan', $data);
    }

    public function pak_kegiatan_detail($periode, $kegiatan)
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $id_periode         = dec_data_url($periode);
        $id_kegiatan        = dec_data_url($kegiatan);
        $parameter          = array('id_periode' => $id_periode, 'id_kegiatan' => $id_kegiatan);
        $data               = $this->_get_pak_kegiatan_detail($parameter)['result']; //['result']

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
        
        $this->tema->backend($this->view . 'pak_kegiatan_detail', $data);
    }

    public function pak_kegiatan_import()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $post               = $this->input->post();
        $session            = $this->session->userdata();
        $user               = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }

        $jenis              = dec_data_url($post['jenis']);
        $id_periode         = dec_data_url($post['periode']);
        $id_semester        = dec_data_url($post['semester']);
        $id_kegiatan        = dec_data_url($post['kegiatan']);
        $dt_periode         = $this->_get_periode_pilih(array('id_periode' => $id_periode))['result'];

        if ($jenis == 'pendidikan_mengajar') {
            // ambil daftar mengajar di server sia
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester);
            $get_data       = $this->_sia_get_jadwal_dosen($data_send)['result'];
        } elseif ($jenis == 'pendidikan_bimbingankkn') {
            // ambil daftar membimbing KKN/MBKN DLL
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester, 'kategori' => 'Kerja Praktek');
            $get_data       = $this->_kpta_get_bimbingan($data_send)['result'];
        } elseif ($jenis == 'pendidikan_bimbinganskripsi') {
            // ambil daftar membimbing TA / TESIS
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester);
            $get_data       = $this->_kpta_get_bimbingan_ta($data_send)['result'];
        } elseif ($jenis == 'pendidikan_penguji') {
            // ambil daftar menguji TA / TESIS
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester);
            $get_data       = $this->_kpta_get_penguji_ta($data_send)['result'];
        } elseif ($jenis == 'pendidikan_jabakademik') {
            // ambil data jabatan akademik
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester);
            $get_data       = $this->_isdm_get_jabakademik($data_send)['result'];
        } elseif ($jenis == 'pendidikan_doktoral') {
            // ambil data doktoral
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester);
            $get_data       = $this->_isdm_get_doktor($data_send)['result'];
        } elseif ($jenis == 'corevalues_integritas') {
            // ambil data integritas
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester, 'id_periode' => $id_periode);
            $get_data       = $this->_server_get_integritas($data_send)['result'];
        } elseif ($jenis == 'corevalues_akuntabilitas') {
            // ambil data akuntabilitas
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester, 'id_periode' => $id_periode);
            $get_data       = $this->_sia_input_nilai_uas($data_send)['result'];
        } elseif ($jenis == 'tugas_tambahan') {
            // ambil data tugas tambahan
            $data_send      = array('user_key' => $user['user_key'], 'id_semester' => $id_semester, 'id_periode' => $id_periode);
            $get_data       = $this->_server_get_tugastambahan($data_send)['result'];
        } else {

            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', 'Kondisi belum tersedia');

            return redirect(base_url($this->link.'pak_kegiatan_detail/'.$post['periode'].'/'.$post['kegiatan']));
        }

        // jika data kosong tidak perlu proses simpan
        if (empty($get_data)) {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', 'Tidak ditemukan data.');

            return redirect(base_url($this->link.'pak_kegiatan_detail/'.$post['periode'].'/'.$post['kegiatan']));
        }

        $simpan         = array('id_periode'    => $id_periode,
                                'id_semester'   => $id_semester,
                                'id_kegiatan'   => $id_kegiatan,
                                'data'          => $get_data);
        $response       = $this->_send_kegiatan_import($simpan);

        // echo "<pre>";
        // print(json_encode($simpan));
        // echo "</pre>";
        // die();
        
        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        return redirect(base_url($this->link.'pak_kegiatan_detail/'.$post['periode'].'/'.$post['kegiatan']));
    }

    public function pak_kegiatan_lihat($periode, $values)
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'r');
        $id_periode         = dec_data_url($periode);
        $id_values          = dec_data_url($values);
        $parameter          = array('id_periode' => $id_periode, 'id_values' => $id_values);
        $data               = $this->_get_pak_kegiatan_lihat($parameter)['result']; //['result']

        // echo "<pre>";
        // print_r ($data);
        // echo "</pre>";
        // die();

        $this->tema->backend($this->view . 'pak_kegiatan_lihat', $data);
    }

    public function pak_kegiatan_hapus($periode, $values)
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'd');
        $id_periode         = dec_data_url($periode);
        $id_values          = dec_data_url($values);
        $parameter          = array('id_periode' => $id_periode, 'id_values' => $id_values); 
        
        // cek data yang akan dihapus
        $cek_data = $this->_get_pak_kegiatan_lihat($parameter)['result'];

        // hapus gambar
        if ($cek_data['detail']['file_type'] != 'pdf') {
            $file_foto= './'.$cek_data['detail']['link_berkas'].'';
            if (is_file($file_foto)) {
                chmod($file_foto, 0777);
                unlink($file_foto);
            } 
        }
              
        // echo "<pre>";
        // print_r ($cek_data);
        // echo "</pre>";
        // die();

        $response           = $this->_value_hapus($parameter);

        if ($response['result']['status'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        // id_periode dan id_kegiatan
        return redirect(base_url($this->link.'pak_kegiatan_detail/'.$periode.'/'.enc_data_url($response['result']['id_kegiatan'])));
    }

    public function pak_url_simpan()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'c');
        $post               = $this->input->post();
        $id_values          = dec_data_url($post['id_values']);
        $parameter          = array('id_values' => $id_values, 'url' => $post['url']);
        $response           = $this->_simpan_url($parameter); //['result']

        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', 'URL berhasil disimpan');
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', 'URL gagal disimpan');
        }
        
        return redirect(base_url($this->link.'pak_kegiatan_lihat/'.$post['id_periode'].'/'.$post['id_values']));
    }

    public function pak_pesan_value()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'c');
        $post               = $this->input->post();
        $id_values          = dec_data_url($post['id_values']);
        $parameter          = array('id_values' => $id_values, 'pesan' => $post['pesan'], 'kategori' => dec_data_url($post['kategori']));
        $response           = $this->_simpan_value_pesan($parameter); //['result']

        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        return redirect(base_url($this->link.'pak_kegiatan_lihat/'.$post['id_periode'].'/'.$post['id_values']));
    }

    public function pak_pengajuan_pkd()
    {
        $data['crud']           = $this->app->AksesMenu($this->appUrl, 'c');
        $post                   = $this->input->post();
        $session                = $this->session->userdata();
        $user                   = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }
        
        $id_periode             = dec_data_url($post['id_periode']);
        $parameter['histori']   = array('id_periode' => $id_periode, 'pesan' => $post['pesan'], 'kategori' => dec_data_url($post['status']));
        $parameter['profil']    = $this->_isdm_profil(array('user_key' => $user['user_key']))['result'];
        $response               = $this->_ajukan_pkd($parameter); //['result']

        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        return redirect(base_url($this->link.'pak/'.$post['id_periode']));
    }

    public function pak_kegiatan_form()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'c');
        $post               = $this->input->post();
        $session            = $this->session->userdata();
        $user               = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }

        $jenis          = dec_data_url($post['jenis']);
        $id_periode     = dec_data_url($post['periode']);
        $id_kegiatan    = dec_data_url($post['kegiatan']);
        $attribute      = array('id_kegiatan' => $id_kegiatan, 'id_periode' => $id_periode, 'id' => dec_data_url($post['id']));
        $data           = $this->_kegiatan_form($attribute)['result'];
        $data['id_values'] .= dec_data_url($post['id_values']);

        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();

        $this->tema->backend(''.$this->view . 'pak_kegiatan_form', $data);
    }

    public function pak_kegiatan_add()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'c');
        $post               = $this->input->post();
        $session            = $this->session->userdata();
        $user               = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }

        $id_periode         = dec_data_url($post['periode']);
        $id_kegiatan        = dec_data_url($post['kegiatan']);
        $id_subunsur        = dec_data_url($post['subunsur']);

        if ($id_subunsur == '22') {
            $tahun          = date('Y');
            $file           = $_FILES['file_foto'];
            $path           = $file['name'];
            $ext            = pathinfo($path, PATHINFO_EXTENSION);
            $nama_file      = explode('.' . $ext, $path)[0];
            $name_upload    = 'harmoni_'.date('Ymd_His');
            $upload_name    = preg_replace("/[^a-zA-Z0-9_]/", '_', $name_upload). '.' . $ext;
            $nama_folder    = './file/uploads/event_dosen/'.$tahun.'/';

            $config['upload_path']      = $nama_folder;
            $config['allowed_types']    = 'jpg|jpeg';
            $config['max_size']         = '2048';
            $config['file_name']        = $upload_name;
            $config['overwrite']        = true;
            $this->load->library('upload', $config);

            if (!is_dir('file/uploads/event_dosen'))
            {
                mkdir('./file/uploads/event_dosen', 0777, true);
            }
            $dir_exist = true; // flag for checking the directory exist or not

            if (!is_dir($nama_folder))
            {
                mkdir($nama_folder, 0777, true);
                $dir_exist = false; // dir not exist
            }

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file_foto')) {
                $post['link_berkas'] = 'file/uploads/event_dosen/'.$tahun.'/'.$upload_name.'';
            } else {
                $this->session->set_flashdata('type', 'alert-danger');
                $this->session->set_flashdata('pesan', $this->upload->display_errors());

                return redirect(base_url($this->link.'pak_kegiatan_detail/'.enc_data_url($id_periode).'/'.enc_data_url($id_kegiatan)));
            }
        }
        
        unset($post['periode']);
        unset($post['kegiatan']);
        unset($post['subunsur']);

        $post['id_periode']     = $id_periode;
        $post['id_kegiatan']    = $id_kegiatan;
        $response               = $this->_kegiatan_simpan($post);

        // echo "<pre>";
        // print_r (json_encode($post));
        // echo "</pre>";
        // die();

        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        return redirect(base_url($this->link.'pak_kegiatan_detail/'.enc_data_url($id_periode).'/'.enc_data_url($id_kegiatan)));
    }

    public function pak_kegiatan_edit()
    {
        $data['crud']       = $this->app->AksesMenu($this->appUrl, 'u');
        $post               = $this->input->post();
        $session            = $this->session->userdata();
        $user               = $this->cek_user($session['base_token'])['result'];
        if ($user['username'] == 'a_rifai') {
            $user['user_key'] = $this->sample;
        }

        $id_periode     = dec_data_url($post['periode']);
        $id_kegiatan    = dec_data_url($post['kegiatan']);
        $id_values      = dec_data_url($post['id_values']);
        $id             = dec_data_url($post['id']);
        
        unset($post['periode']);
        unset($post['kegiatan']);
        unset($post['id']);

        $post['id_periode'] = $id_periode;
        $post['id_kegiatan']= $id_kegiatan;
        $post['id_values']  = $id_values;
        $post['id']         = $id;
        $response           = $this->_kegiatan_edit($post);

        // echo "<pre>";
        // print(json_encode($post));
        // echo "</pre>";
        // die();

        if ($response['result'] === true) {
            $this->session->set_flashdata('type', 'alert-success');
            $this->session->set_flashdata('pesan', $response['message']);
        } else {
            $this->session->set_flashdata('type', 'alert-danger');
            $this->session->set_flashdata('pesan', $response['message']);
        }
        
        return redirect(base_url($this->link.'pak_kegiatan_lihat/'.enc_data_url($id_periode).'/'.enc_data_url($id_values)));
    }







    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_get_jabakademik($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/dosen_jafa', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _isdm_get_doktor($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/dosen_doktor', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    // public function _isdm_get_struktural($array = array())
    // {
    //     $data = json_encode($array);
    //     return json_decode($this->curl->request_manual_tkn('GET', $this->isdm_url.'connection/isdm/dosen_pejabat', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    // }
        
    // ================================================================================= 

    // REST API SIA
    public function _sia_get_jadwal_dosen($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->sia_url . 'api/kinerja/get_jadwal_dosen', 'sia_token', $data), true);
    }

    public function _sia_input_nilai_uas($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->sia_url . 'api/kinerja/get_input_nilai', 'sia_token', $data), true);
    }

    // =================================================================================

    // REST API KP-TA
    public function _kpta_get_bimbingan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->kpta_url . 'api/kinerja/get_bimbingan', 'kpta_token', $data), true);
    }

    public function _kpta_get_bimbingan_ta($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->kpta_url . 'api/kinerja/get_bimbingan_ta', 'kpta_token', $data), true);
    }

    public function _kpta_get_penguji_ta($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->kpta_url . 'api/kinerja/get_penguji_ta', 'kpta_token', $data), true);
    }


    // =================================================================================

    // REST API SERVER
    public function cek_user($base_token = '')
    {
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }

    public function _get_periode($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_periode', 'base_token', $data), true);
    }

    public function _get_periode_pilih($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_periode_pilih', 'base_token', $data), true);
    }

    public function _get_pak($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_pak', 'base_token', $data), true);
    }

    public function _get_pak_kegiatan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_pak_keg', 'base_token', $data), true);
    }

    public function _get_pak_kegiatan_detail($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_pak_detail', 'base_token', $data), true);
    }

    public function _send_kegiatan_import($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/send_pak_import', 'base_token', $data), true);
    }

    public function _get_pak_kegiatan_lihat($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/get_pak_lihat', 'base_token', $data), true);
    }

    public function _value_hapus($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/value_hapus', 'base_token', $data), true);
    }

    public function _simpan_url($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/update_url', 'base_token', $data), true);
    }

    public function _simpan_value_pesan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/pesan_value', 'base_token', $data), true);
    }

    public function _ajukan_pkd($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/ajukan_pkd', 'base_token', $data), true);
    }

    public function _kegiatan_form($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/kegiatan_form', 'base_token', $data), true);
    }

    public function _kegiatan_simpan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/kegiatan_simpan', 'base_token', $data), true);
    }

    public function _kegiatan_edit($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PATCH', $this->base_url . 'api/kinerja/pak_dosen/kegiatan_edit', 'base_token', $data), true);
    }

    public function _server_get_integritas($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/corevalues_integritas', 'base_token', $data), true);
    }

    public function _server_get_tugastambahan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/pak_dosen/tugas_tambahan', 'base_token', $data), true);
    }
}
