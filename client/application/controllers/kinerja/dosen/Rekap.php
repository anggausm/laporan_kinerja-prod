<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->base_url = $this->app->get_server('base_server');
        // $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->isdm_url = "https://isdm.usm.ac.id/server/index.php/";
        $this->view = 'backend/kinerja/kependidikan/penilaian_diri/';
        $this->link = 'kinerja/kependidikan/penilaian_diri/';
    }

    public function index()
    {
        // $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        echo "<pre>";
        print_r('asd');
        echo "</pre>";
        die();
        $session    = $this->session->userdata();
        $user       = $this->_cek_token($session['base_token'])['result'];
        $data['profil'] = $this->_isdm_profil(array('user_key' => '3b6892bf-6f68-11ea-89e7-1cb72c27dd68'))['result']; //$user['user_key']
        $data['tanya']  = $this->_pernyataan()['result'];
        $this->tema->backend($this->view . 'index', $data);
    }

    public function simpan()
    {
        $data = array();
        $post         = $this->input->post();

        foreach ($post['nilai'] as $key => $value) {
            $jawaban[] = array('id_pertanyaan' => $key, 'nilai' => $value['nilai']);
        }
        $response  = $this->_save($post);

        if ($response['result'] === true) {
            $this->session->set_flashdata('pesan', 'Data berhasil disimpan');
            $this->session->set_flashdata('type', 'alert-success');
        } else {
            $this->session->set_flashdata('pesan', 'Data gagal disimpan.');
            $this->session->set_flashdata('type', 'alert-danger');
        }
        return redirect(base_url($this->link));
    }

    public function _cek_token($base_token = '')
    {
        // return print $this->app->base_server() . 'api/cek_token';
        return json_decode($this->curl->request_manual_tkn('PATCH', $this->app->base_server() . 'api/cek_token', $base_token, ""), true);
    }

    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', 'https://isdm.usm.ac.id/server/index.php/connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _pernyataan($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/kependidikan/pertanyaan', 'base_token', $data), true);
    }

    public function _periode($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('GET', $this->base_url . 'api/kinerja/kependidikan/pertanyaan/periode', 'base_token', $data), true);
    }

    public function _save($array = '')
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/kependidikan/pertanyaan/simpan', 'base_token', $data), true);
    }
}
->base_server() . 'api/cek_token', $base_token, ""), true);
    }

    // REST API KEPEGAWAIAN
    public function _isdm_profil($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request_manual_tkn('GET', 'https://isdm.usm.ac.id/server/index.php/connection/isdm/lapker_doskar_detail', '4c78fad26291ebcda3376aaadf350ca2', $data), true);
    }

    public function _get_pertanyaan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/dosen/rekap/get_pertanyaan', 'base_token', $data), true);
    }

    public function _get_periode($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('PUT', $this->base_url . 'api/kinerja/dosen/rekap/get_periode', 'base_token', $data), true);
    }

    public function _save($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/dosen/rekap/save', 'base_token', $data), true);
    }
    public function _ajukan($array = array())
    {
        $data = json_encode($array);
        return json_decode($this->curl->request('POST', $this->base_url . 'api/kinerja/dosen/rekap/ajukan', 'base_token', $data), true);
    }
}
