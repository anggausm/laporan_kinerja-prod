<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Test extends CI_Controller
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        //==== ALLOWING CORS
        $this->sso_url = $this->app->sso_app();
        $this->app->cekTokenAkses();
        $this->appUrl = '04de8cf2-5561-11ea-8ebb-1cb72c27dd68';
        $this->base_url = $this->app->get_server('base_aplikasi');

    }

    public function index()
    {
        $data = array();
        $data['crud'] = $this->app->AksesMenu($this->appUrl, 'r');
        return print_r($data);

        $this->tema->backend('backend/acl/aplikasi/index', $data);
    }

    /*=============================================================
    ========================= Get Data  ===========================
    =============================================================*/
    public function bidang($array = '')
    {
        $data = "";
        return json_decode($this->curl->request('PUT', $this->sso_url . 'api/aplikasi/bidang', 'sso_token', $data), true);
    }

}
