<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class App extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token = $this->app->cek_token();
        $this->url_sso = $this->app->sso_app();
        $this->server_file = $this->app->server_file();
        $this->base_url = $this->app->get_server('base_server');

    }

    public function index()
    {
        // $this->session->unset_userdata('nm_aplikasi');
        // $this->session->unset_userdata('nm_level');
        // $this->session->unset_userdata('menu');
        // $this->session->unset_userdata('server');
        // $this->session->unset_userdata('base_server');
        // $this->session->unset_userdata('base_token');
        // $token_clear = $this->clear_server_token();
        // foreach ($token_clear['result'] as $value) {
        //     $this->session->unset_userdata($value['token_name']);
        // }

        // $data = array();
        // $app_list = $this->app_list();
        // if ($app_list['status'] == '201') {
        //     $this->session->set_flashdata('pesan', $app_list['message']);
        //     redirect(base_url());
        // }
        // $data['app_list'] = $app_list['result'];
        // $this->tema->app('backend/app/index', $data);
        // $this->session->sess_destroy();
        // $this->ddk->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
        redirect($this->app->server_portal() . '/app');
    }

    public function profil($value = '')
    {
        $data = array();
        $profil = $this->data_profil();
        $data['profil'] = $profil['result'];
        $this->tema->app('backend/app/profil', $data);
    }
    public function update_password($value = '')
    {
        $password_lama = $this->input->post('password_lama');
        if (empty($password_lama)) {
            redirect(base_url() . 'app/');
        }
        $password_baru = $this->input->post('password_baru');

        $update = $this->update_pass($password_lama, $password_baru);
        $this->session->set_flashdata('pesan', $update['message']);
        redirect(base_url() . 'app/profil');

    }
    public function upload_foto($value = '')
    {
        $file = $_FILES['foto'];
        if ($file['size'] < 1000) {
            redirect(base_url() . 'app');
        };

        $upload = json_decode($this->curl->upload($this->server_file . 'upload/profile', $file, 'sso_token'), 'TRUE');
        if ($upload['status'] == '201') {
            $this->session->set_flashdata('pesan', $upload['message']);
            return redirect(base_url() . 'app/profil');
        }
        $update_foto = $this->upload_file($upload['nm_file']);
        $this->session->set_flashdata('pesan', $update_foto['message']);
        return redirect(base_url() . 'app/profil');
    }

    /*
    ===============================================================================================
    ========================================== R O U T E ==========================================
    ===============================================================================================
     */

    public function routes($id_aplikasi = '', $level_key = '')
    {

        if (empty($id_aplikasi) or empty($level_key)) {
            $this->session->sess_destroy();
            $this->ddk->session->set_flashdata('pesan', 'Silahkan Login terlebih dahulu');
            redirect($this->app->server_portal());
        }
        $token = $this->session->userdata('sso_token');
        $rs = $this->app_menu($id_aplikasi, $level_key);
        foreach ($rs['result']['server'] as $vs) {
            $this->session->set_userdata("$vs[nm]", $vs['url_server']);
        }

        foreach ($rs['result']['token'] as $value) {
            $this->session->set_userdata("$value[token_name]", $value['token']);

        }

        $session = array(
            'nm_aplikasi' => $rs['result']['nm_aplikasi'],
            'nm_level' => $rs['result']['nm_level'],
            'menu' => $rs['result']['menu'],
            'sso_token' => $rs['result']['sso_token'],
            'munu_crud' => $rs['result']['munu_crud'],
        );
        $this->session->set_userdata($session);
        return redirect(base_url() . 'dashboard');

    }

    /*
    ===============================================================================================
    ========================================== D A T A  ===========================================
    ===============================================================================================
     */

    // public function cek_app($array = '')
    // {
    //     $data = json_encode($array);
    //     return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/app/st_app', 'sso_token', $data), 'TRUE');
    // }
    public function data_profil()
    {
        return json_decode($this->curl->request('PUT', $this->url_sso . 'api/profil', 'sso_token', ''), 'TRUE');
    }
    public function update_pass($password_lama = '', $password_baru = '')
    {
        $data = json_encode(array('password_lama' => $password_lama, 'password_baru' => $password_baru));
        return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/profil/password', 'sso_token', $data), 'TRUE');
    }
    public function upload_file($nm_file = '')
    {
        $data = json_encode(array('nm_file' => $nm_file));
        return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/profil/update_foto', 'sso_token', $data), 'TRUE');
    }

    // public function app_list()
    // {
    //     $data = ""; //json_encode(array('nm_file' => $nm_file));
    //     return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/app', 'sso_token', $data), 'TRUE');
    // }

    public function app_menu($id_aplikasi = '', $level_key = '')
    {
        $data = json_encode(array('id_aplikasi' => $id_aplikasi, 'level_key' => $level_key));
        return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/app/menu', 'sso_token', $data), 'TRUE');
    }

    // public function clear_server_token($value = '')
    // {
    //     # code...api/token/clear
    //     $data = ""; // json_encode(array('id_aplikasi' => $id_aplikasi, 'level_key' => $level_key));
    //     return json_decode($this->curl->request('PATCH', $this->url_sso . 'api/token/clear', 'sso_token', $data), 'TRUE');
    // }

}
