<?php
class Lib_menu_key
{
    protected $CI;
    /*development OR production*/
    protected $mode = 'production';

    public function __construct()
    {
        $this->CI = &get_instance();
        // $this->mode = ENVIRONMENT;
    }

    /*LIST MENU KEPEGAWAIAN - FINGER*/
    public function kepegawaian_mesin()
    {
        if ($this->mode == 'production') {
            return '85575dd8-9a54-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_anggota()
    {
        if ($this->mode == 'production') {
            return '96719611-9a54-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_log()
    {
        if ($this->mode == 'production') {
            return '07c89b08-9a55-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_ijin()
    {
        if ($this->mode == 'production') {
            return '0d51a57e-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_tubel()
    {
        if ($this->mode == 'production') {
            return '3b08f0d4-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_harikhusus()
    {
        if ($this->mode == 'production') {
            return '4ff66338-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_presensikhusus()
    {
        if ($this->mode == 'production') {
            return '5f4df47e-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_olahtransport()
    {
        if ($this->mode == 'production') {
            return '10164a12-af8b-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_periodepresensi()
    {
        if ($this->mode == 'production') {
            return '748dfd88-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_presensianggota()
    {
        if ($this->mode == 'production') {
            return '8e7b8e94-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_rekappresensi()
    {
        if ($this->mode == 'production') {
            return '9baa036d-b424-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kepegawaian_online_koordinat()
    {
        if ($this->mode == 'production') {
            return '9fba235b-fbba-11ee-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    /*END LIST MENU KEPEGAWAIAN */


    // laporan kinerja
    public function kinerja_kependidikan_diri()
    {
        if ($this->mode == 'production') {
            return '76ad5fba-1ce7-11ef-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kinerja_kependidikan_rekan()
    {
        if ($this->mode == 'production') {
            return '8537efe7-1ce7-11ef-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }

    public function kinerja_dosen_pak()
    {
        if ($this->mode == 'production') {
            return '12edf0e7-4252-11ef-9a0f-56cb879f2d55';
        }
        return "#"; //dev
    }
    // end laporan kinerja




    /*
    Return true jika punya akses (show)
    return false jika tidak ada akses (hide)
     */
    public function showAksesMenu($menu_key = '', $akses = '')
    {
        $session = $this->CI->session->userdata('munu_crud');
        $proses = array_search($menu_key, array_column($session, 'menu_key'));
        if ($proses === false) {
            return false;
        }
        $rs_data = $session[$proses];
        if ($rs_data[$akses] == '1') {
            return true;
        }
        return false;
    }
}
