<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>biodata/tendik">Biodata Diri</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
	if (!empty($this->session->flashdata('pesan'))) {
	?>
		<div class="alert alert-success" role="alert">
			<?php print $this->session->flashdata('pesan') ?>
		</div>
	<?php
	}
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-user"></i> <b>Biodata Diri</b>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<table class="table table-responsive table-bordered">
						<tbody>
							<tr>
								<td width="30%">NIS</td>
								<td width="80%">
									<?php if (!empty($rs_data['result']['rs']['nis'])) : ?>
										<div class="font-bold"><?php echo $rs_data['result']['rs']['nis']; ?></div>
									<?php else : ?>
										<span class="text-warning font-bold"><?php echo "Anda belum memiliki NIS"; ?></span>
									<?php endif ?>
								</td>
							</tr>
							<tr>
								<td width="30%">Nama Lengkap</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['nama_doskar']; ?></td>
							</tr>
							<tr>
								<td width="30%">NIDN</td>
								<td width="80%">
									<?php if (!empty($rs_data['result']['rs']['nidn'])) : ?>

										<div class="font-bold"><?php echo $rs_data['result']['rs']['nidn']; ?></div>
									<?php else : ?>
										<span class="text-warning font-bold"><?php echo "Anda belum memiliki NIDN"; ?></span>
									<?php endif ?>
								</td>
							</tr>
							<tr>
								<td width="30%">Nomor HP</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['handphone']; ?></td>
							</tr>
							<tr>
								<td width="30%">E-mail</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['email']; ?></td>
							</tr>
							<tr>
								<td width="30%">Gol. Darah</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['gol_darah']; ?></td>
							</tr>
							<tr>
								<td width="30%">Alamat</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['alamat']; ?></td>
							</tr>
							<tr>
								<td width="30%">Kab/ Kota</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['kota']; ?></td>
							</tr>
							<tr>
								<td width="30%">Kode Pos</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['kode_pos']; ?></td>
							</tr>
							<tr>
								<td width="30%">NIK</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['no_ktp']; ?></td>
							</tr>
							<tr>
								<td width="30%">Tanggal Lahir</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['tgl_lhr_format']; ?></td>
							</tr>
							<tr>
								<td width="30%">Tempat Lahir</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['tmpt_lhr']; ?></td>
							</tr>
							<tr>
								<td width="30%">Jenis Kelamin</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['jns_kelamin']; ?></td>
							</tr>
							<tr>
								<td width="30%">Agama</td>
								<td width="80%"><?php echo $rs_data['result']['rs']['agama']; ?></td>
							</tr>
						</tbody>
					</table>
					<div class="row">
						<div class="col-sm-12 m-r m-b m-t">
							<a href="<?php print base_url() ?>biodata/tendik/edit"><button class="btn btn-info pull-right">Edit Biodata</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>