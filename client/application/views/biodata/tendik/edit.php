<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>biodata/tendik">Biodata Diri</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>biodata/tendik/edit">Edit Biodata Diri</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-user"></i> <b>Form Update Biodata Diri</b>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>biodata/tendik/update" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">NIS </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  value="<?php print $rs_data['result']['rs']['nis'];?>" disabled="">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nama Lengkap </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="nama_doskar" value="<?php print $rs_data['result']['rs']['nama_doskar'];?>" required="">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">NIDN </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="nidn" value="<?php print $rs_data['result']['rs']['nidn'];?>">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nomor HP </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="handphone" value="<?php print $rs_data['result']['rs']['handphone'];?>">	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">E-mail </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="email" value="<?php print $rs_data['result']['rs']['email'];?>">	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Golongan Darah </label>
									<div class="col-sm-9">
										<select class="form-control" name="gol_darah" value="<?=$rs_data['result']['rs']['gol_darah'];?>">
		                                    <option value="A">A</option>
		                                    <option value="B">B</option>
		                                    <option value="AB">AB</option>
		                                    <option value="O">O</option>
		                                    <option selected="" value="<?=$rs_data['result']['rs']['gol_darah'];?>"><?=$rs_data['result']['rs']['gol_darah'];?></option> 
			                            </select>
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Alamat </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="alamat" value="<?php print $rs_data['result']['rs']['alamat'];?>">	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Kabupaten/ Kota </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="kota" value="<?php print $rs_data['result']['rs']['kota'];?>">	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Kode Pos </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="kode_pos" value="<?php print $rs_data['result']['rs']['kode_pos'];?>">	
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nomor KTP </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="no_ktp" value="<?php print $rs_data['result']['rs']['no_ktp'];?>">	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Tanggal Lahir </label>
									<div class="col-sm-9">
										<div class="input-group date" >
			                                <div class="input-group-addon">
			                                    <i class="fa fa-calendar"></i>
			                                </div>
			                                <input type="date" class="form-control pull-right" id="tgl_lhr" name="tgl_lhr" value="<?php echo $rs_data['result']['rs']['tgl_lhr']; ?>">
			                            </div>	
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Tempat Lahir </label>
									<div class="col-sm-9">
										<input type="text" class="form-control"  name ="tmpt_lhr" value="<?php print $rs_data['result']['rs']['tmpt_lhr'];?>">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Jenis Kelamin </label>
									<div class="col-sm-9">
										<div class="col-sm-4">
				                            <div class="radio">
				                                <label class="ui-checks">
				                                    <input name="jns_kelamin" value="L" class="has-value" type="radio"  <?php if($rs_data['result']['rs']['jns_kelamin'] == 'L'){ print 'checked=""='; } ?>>
				                                    <i></i>
				                                    Laki-laki
				                                </label>
				                            </div>
				                        </div>
				                        <div class="col-sm-4">
				                            <div class="radio">
				                                <label class="ui-checks">
				                                    <input name="jns_kelamin" value="P" class="has-value" type="radio"  <?php if($rs_data['result']['rs']['jns_kelamin'] == 'P'){ print 'checked=""='; } ?>>
				                                    <i></i>
				                                    Perempuan
				                                </label>
				                            </div>
				                        </div>
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Agama </label>
									<div class="col-sm-9">
										<select class="form-control" name="agama" value="<?=$rs_data['result']['rs']['agama'];?>">
		                                    <option value="Islam">Islam</option>
		                                    <option value="Katholik">Katholik</option>
		                                    <option value="Kristen">Kristen</option>
		                                    <option value="Hindu">Hindu</option>
		                                    <option value="Budha">Budha</option>
		                                    <option value="Tionghoa">Konghuchu</option>
		                                    <option selected="" value="<?=$rs_data['result']['rs']['agama'];?>"><?=$rs_data['result']['rs']['agama'];?></option> 
			                            </select>
									</div>
								</div>
							</div>
						</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-info pull-right" style="margin-top: 10px; margin-bottom: 10px">Update Biodata</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>