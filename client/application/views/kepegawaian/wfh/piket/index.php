<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/periode">Jadwal WFH </a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="panel panel-default">
		<div class="panel-heading bg-white">
			<i class="fa  fa-folder-open"></i>  Periode WFH
			<small class="text-muted"> </small>
		</div>
		<div class="panel-body">
			<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kepegawaian/wfh/piket/unit" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<label class="col-sm-4 control-label text-right">Periode WFH </label>
							<div class="col-sm-8">
								<select class="form-control select2" name="id_piket_wfh" >
									<option>-</option>
									<?php foreach ($periode['result'] as $value) {?>
										 <option value="<?=$value['id_piket_wfh']?>"><?=$value['periode_wfh']?></option>
									<?php }?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group ">
							<label class="col-sm-4 control-label text-right">Unit Bagian </label>
							<div class="col-sm-8">
								<select class="form-control select2" name="key" >
									<option>-</option>
									<?php foreach ($level['result'] as $value) {?>
										 <option value="<?=$value['key']?>"><?=$value['level']?></option>
									<?php }?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-sm-2">
						<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>