<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/piket/set_jadwal ">Jadwal Piket </a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/piket/set_jadwal ">Set Jadwal Piket</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i> Set Jadwal Piket
					<span style="float: right;"><a href="<?php print base_url() . 'kepegawaian/wfh/piket/set_jadwal/' . enc_data($id_struktural) . '/' . enc_data($key) . '/' . enc_data($id_piket_wfh)?>">
					<button class="btn btn-info btn-sm"><< Kembali</button></a>
				</span>
			</div>
			<div class="panel-body">
				<form  class="form-horizontal p-h-xs">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group ">
								<label class="col-sm-4 control-label text-right">Periode WFH </label>
								<label class="col-sm-8 control-label text-right">: <?=$jadwal_piket['result']['detail_doskar']['nama_doskar']?></label>
							</div>
							<div class="form-group ">
								<label class="col-sm-4 control-label text-right">NIS</label>
								<label class="col-sm-8 control-label text-right">: <?=$jadwal_piket['result']['detail_doskar']['nis']?></label>
							</div>
							<div class="form-group ">
								<label class="col-sm-4 control-label text-right">Jabatan Struktural</label>
								<label class="col-sm-8 control-label text-right">: <?=$jadwal_piket['result']['detail_doskar']['nama_sub_struktural']?></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="col-sm-4 control-label text-right">Periode</label>
								<label class="col-sm-8 control-label text-right">: <?=$jadwal_piket['result']['wfh_periode']['periode_wfh']?></label>
							</div>
							<div class="form-group ">
								<label class="col-sm-4 control-label text-right">Tanggal Mulai / Selesai</label>
								<label class="col-sm-4 control-label text-right">: <?=$jadwal_piket['result']['wfh_periode']['mulai']?></label>
								<label class="col-sm-4 control-label text-right">: <?=$jadwal_piket['result']['wfh_periode']['selesai']?></label>
							</div>
						</div>
					</div>
				</form>
				<!-- //user_key id_piket_wfh -->
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
								<thead>
									<tr>
										<th style="width:5%" class="text-center">No</th>
										<th style="width:15%" >Hari</th>
										<th style="width:25%" >Tanggal</th>
										<th style="width:15%" class="text-center">Set Piket</th>
										<th style="width:15%" ></th>
									</tr>
								</thead>
								<tbody>
									<?php
//Array ( [tanggal] => 2020-05-19 [hari] => Selasa [tgl] => 19 May 2020 [id] => [id_piket_wfh] => [user_key] => )
$no = 1;
foreach ($jadwal_piket['result']['jadwal_piket'] as $value) {
    // print var_dump($value);
    ?>
									<!--   -->
									<tr>
										<td class="text-center">
											<?=$no?>
										</td>
										<td ><?php print $value['hari'];?></td>
										<td class="text-center"><?php print $value['tgl'];?></td>

										<td class="text-center">
											<?php if (!empty($value['id_piket_wfh'])) {
        print '<button class="btn btn-info btn-xs">
												<i class="fa fa-bell-o "></i> Masuk / Piket
												</button>';
    } else {
        print '<button class="btn btn-warning btn-xs">
												<i class="fa fa-tags "></i> Kerja Di Rumah
												</button>';
    }?>
										</td>
										<td >
											<?php
if (empty($value['id_piket_wfh'])) {?>
											<a href="<?=base_url() . 'kepegawaian/wfh/piket/simpan_jadwal_piket/' . enc_data($id_struktural) . '/' . enc_data($key) . '/' . enc_data($id_piket_wfh) . '/' . enc_data($user_key) . '/' . enc_data($id_sub_struktural) . '/' . enc_data($value['tanggal'])?>"><button class="btn btn-info btn-xs">
												<i class="fa fa-bell-o "></i> Set Piket
												</button>
											</a>
											<?php } else {?>
											<a href="<?=base_url() . 'kepegawaian/wfh/piket/delete_jadwal_piket/' . enc_data($id_struktural) . '/' . enc_data($key) . '/' . enc_data($id_piket_wfh) . '/' . enc_data($user_key) . '/' . enc_data($id_sub_struktural) . '/' . enc_data($value['tanggal'])?>">
												<button class="btn btn-danger btn-xs">
												<i class="fa fa-bell-slash "></i> Batalkan Piket
												</button>
											</a>
											<?php }
    ?>


										</td>
									</tr>
									<?php
$no++;
}
?>
								</tbody>
							</table>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>