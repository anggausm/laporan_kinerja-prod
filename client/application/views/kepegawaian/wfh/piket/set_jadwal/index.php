<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/piket/set_jadwal ">Jadwal Piket </a></li>

				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i> Daftar Angota Unit <?=$list_anggota['result']['unit_bagian']['unit_bagian'] . ', Periode ' . $list_anggota['result']['periode_wfh']['periode_wfh']?>
					<span style="float: right;"><a href="<?php print base_url() . 'kepegawaian/wfh/piket/unit/' . enc_data($id_piket_wfh) . '/' . enc_data($key)?>">
						<button class="btn btn-info btn-sm"><< Kembali</button></a>
					</span>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
									<thead>
										<tr>
											<th style="width:5%" class="text-center">No</th>
											<th style="width:25%" >Jabatan</th>
											<th style="width:15%" >NIDN</th>
											<th style="width:25%" >Nama</th>
											<th style="width:25%" >Jadwal Piket</th>
											<th style="width:15%" class="text-center">Detail</th>
										</tr>
									</thead>
									<tbody>
										<?php
$no = 1;
foreach ($list_anggota['result']['rs'] as $value) {
    // print var_dump($value);
    ?>
										<!--   -->
										<tr>
											<td class="text-center">
												<?=$no?>
											</td>
											<td ><?php print $value['nama_sub_struktural'];?></td>
											<td ><?php print $value['nis'];?></td>
											<td ><?php print $value['nama_doskar'];?></td>
											<td>
													<ul>
													<?php foreach ($value['piket'] as $val) {?>
														<li><?=$val['hari'] . ' ' . $val['piket']?></li>
													<?php }?>
													</ul>
												</td>

											<td class="text-center">
												<a href="<?php print base_url()?>kepegawaian/wfh/piket/set_jadwal_piket/<?=enc_data($id_struktural) . '/' . enc_data($key) . '/' . enc_data($id_piket_wfh) . '/' . enc_data($value['user_key']) . '/' . enc_data($value['id_sub_struktural'])?>">
													<button class="btn btn-info btn-xs"><i class="fa fa-list"></i> Set Jadwal Piket</button>
												</a>
											</td>
										</tr>
										<?php
$no++;
}
?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>