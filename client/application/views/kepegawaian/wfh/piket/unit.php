<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/piket">Jadwal WFH </a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/wfh/piket">Piket WFH </a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="panel panel-default">
		<div class="panel-heading bg-white">
			<i class="fa  fa-folder-open"></i>  Seting Jadwal Piket
			<small class="text-muted"> </small>
		</div>
		<div class="panel-body">
			<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kepegawaian/wfh/piket/unit" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group ">
							<label class="col-sm-4 control-label text-right">Periode WFH </label>
							<div class="col-sm-8">
								<select class="form-control select2" name="id_piket_wfh" >
									<option>-</option>
									<?php foreach ($periode['result'] as $value) {?>
										 <option value="<?=$value['id_piket_wfh']?>" <?php if ($value['id_piket_wfh'] == $id_piket_wfh) {print "selected=''";}?> ><?=$value['periode_wfh']?></option>
									<?php }?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group ">
							<label class="col-sm-4 control-label text-right">Unit Bagian </label>
							<div class="col-sm-8">
								<select class="form-control select2" name="key" >
									<option>-</option>
									<?php foreach ($level['result'] as $value) {?>
										 <option value="<?=$value['key']?>" <?php if ($value['key'] == $key) {print "selected=''";}?>><?=$value['level']?></option>
									<?php }?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-sm-2">
						<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
					</div>
				</div>
			</form>
			<hr>
			<h5><i class="fa fa-list"></i> Unit Bagian </h5>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
									<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:35%" >Unit / Lembaga </th>
												<th style="width:15%" class="text-center">Detail</th>
											</tr>
										</thead>
										<tbody>
											<?php
$no = 1;
foreach ($level_list['result'] as $value) {
    // print var_dump($value);
    ?>
											<!--   -->
											<tr>
												<td class="text-center">
													<?=$no?>
												</td>
												<td ><?php print $value['unit_bagian'];?></td>
												<td class="text-center">
													<a href="<?php print base_url()?>kepegawaian/wfh/piket/set_jadwal/<?=enc_data($value['id_struktural']) . '/' . enc_data($key) . '/' . enc_data($id_piket_wfh)?>">
														 <button class="btn btn-info btn-xs"><i class="fa fa-sitemap"></i> Anggota</button>
													</a>
												</td>
											</tr>
											<?php
$no++;
}
?>
										</tbody>
									</table>
								</div>

				</div>
			</div>
		</div>

	</div>
</div>