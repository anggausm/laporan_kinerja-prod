<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/shift">Manajemen Shift</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/shift/edit">Edit Shift</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-user"></i> <b>Form Update Shift Kerja</b>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kepegawaian/shift/update" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
								<input type="hidden" name="user_key" value="<?=$rs_data['result']['rs']['user_key'];?>">
									<label class="col-sm-3 control-label text-right">NIS </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: 
										<?php if ($rs_data['result']['rs']['nis'] == null): ?>
											<span class="text-danger"><?php echo "Anda belum memiliki NIS"; ?></span>
										<?php else: ?>
											<?=$rs_data['result']['rs']['nis'];?>
										<?php endif ?>
									</label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Nama Lengkap </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['nama_doskar'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Jabatan </label>
									<label class="col-sm-9 control-label text-right" style="font-weight: bold;">: <?=$rs_data['result']['rs']['keterangan'];?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Shift Kerja</label>
									<div class="col-sm-9">
										<div class="col-sm-3">
				                            <div class="radio">
				                                <label class="ui-checks">
				                                    <input name="shift_finjer" value="Pagi" class="has-value" type="radio"  <?php if($rs_data['result']['rs']['shift_finjer'] == 'Pagi'){ print 'checked=""='; } ?>>
				                                    <i></i>
				                                    Pagi
				                                </label>
				                            </div>
				                        </div>
				                        <div class="col-sm-3">
				                            <div class="radio">
				                                <label class="ui-checks">
				                                    <input name="shift_finjer" value="Sore" class="has-value" type="radio"  <?php if($rs_data['result']['rs']['shift_finjer'] == 'Sore'){ print 'checked=""='; } ?>>
				                                    <i></i>
				                                    Sore
				                                </label>
				                            </div>
				                        </div>
									</div>
								</div>
							</div>
						</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-info pull-right" style="margin-top: 10px; margin-bottom: 10px">Update Shift</button>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>