<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/shift">Manajemen Shift</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-user"></i> <b>Manajemen Shift Kerja</b>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
								<div class="table-responsive">
									<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:45%" >Nama </th>
												<th style="width:15%" class="text-center">NIS</th>
												<th style="width:15%" class="text-center">Shift</th>
												<th style="width:15%" class="text-center">Jabatan</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$no = 1;
												// var_dump ($shift);
												foreach ($shift['result']['rs_data'] as $value) {
												    // print var_dump($value);
												    ?>
											<tr>
												<td class="text-center">
													<?=$no?>
												</td>
												<td ><?php print $value['nama_doskar'];?></td>
												<td ><?php print $value['nis'];?></td>
												<td ><?php print $value['shift_finjer'];?></td>
												<td ><?php print $value['keterangan'];?></td>
												<td class="text-center">
													<a href="<?php print base_url()?>kepegawaian/shift/edit/<?=enc_data($value['user_key'])?>">
														 <button class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit Shift</button>
													</a>
												</td>
											</tr>

											<?php $no++; } ?>

										</tbody>
									</table>
								</div>

							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>