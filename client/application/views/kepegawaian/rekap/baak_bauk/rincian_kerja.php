<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kepegawaian/rekap/laporan_baak_bauk ">Laporan Pekerjaan </a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i>  Rekap Laporan Pekerjaan Unit dan Lembaga <?=$pekerjaan['result']['unit_bagian']['unit_bagian']?>
					<small class="text-muted"> </small>
					<span style="float: right;"><a href="<?php print base_url() . 'kepegawaian/rekap/laporan_baak_bauk/anggota/' . enc_data($id_struktural)?>">
						<button class="btn btn-info btn-sm"><< Kembali</button></a>
					</span>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs"  >
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Jabatan Struktural </label>
									<label class="col-sm-8 control-label text-right"> : <?=$pekerjaan['result']['profil']['nama_sub_struktural']?> </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">NIS </label>
									<label class="col-sm-8 control-label text-right"> : <?=$pekerjaan['result']['profil']['nis']?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Nama  </label>
									<label class="col-sm-8 control-label text-right"> : <?=$pekerjaan['result']['profil']['nama_doskar']?> </label>
								</div>
							</div>
						</div>
					</form>
					<hr>
					<h5><i class="fa fa-list"></i> Riwayat Pekrjaan </h5>
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kepegawaian/rekap/laporan_baak_bauk/bulan" enctype="multipart/form-data">
						<input type="hidden" name="id_struktural" value="<?=$id_struktural?>">
						<input type="hidden" name="user_key" value="<?=$pekerjaan['result']['profil']['user_key']?>">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Tahun </label>
									<div class="col-sm-8">
										<select class="form-control" name="tahun">
											<?php for ($i=2020; $i < date('Y')+1 ; $i++) { ?>
												<option value="<?php echo $i ?>" <?php if ($tahun == $i) {print 'selected=""';}?> ><?php echo $i ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Bulan </label>
									<div class="col-sm-8">
										<select class="form-control" name="bulan">
											<option value="01" <?php if ($bulan == '01') {print 'selected=""';}?>>Januari</option>
											<option value="02" <?php if ($bulan == '02') {print 'selected=""';}?> >Februari</option>
											<option value="03" <?php if ($bulan == '03') {print 'selected=""';}?> >Maret</option>
											<option value="04" <?php if ($bulan == '04') {print 'selected=""';}?> >April</option>
											<option value="05" <?php if ($bulan == '05') {print 'selected=""';}?> >Mei</option>
											<option value="06" <?php if ($bulan == '06') {print 'selected=""';}?> >Juni</option>
											<option value="07" <?php if ($bulan == '07') {print 'selected=""';}?> >Juli</option>
											<option value="08" <?php if ($bulan == '08') {print 'selected=""';}?> >Agustus</option>
											<option value="09" <?php if ($bulan == '09') {print 'selected=""';}?> >September</option>
											<option value="10" <?php if ($bulan == '10') {print 'selected=""';}?> >Oktober</option>
											<option value="11" <?php if ($bulan == '11') {print 'selected=""';}?> >November</option>
											<option value="12" <?php if ($bulan == '12') {print 'selected=""';}?> >Desember</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<button class="btn btn-info btn-sm" type="submit"><i class="fa fa-search"></i> Riwayat Pekerjaan</button>
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table ui-jp="dataTable"  class="table table-striped b-t b-b table-bordered">
									<thead>
										<tr>
											<th style="width:5%" class="text-center">No</th>
											<th style="width:10%" >Hri</th>
											<th style="width:15%" >Tanggal</th>
											<th style="width:15%" >Pres. Masuk</th>
											<th style="width:15%" >Pres. Pulang</th>
											<th style="width:55%" >Rencaan Pekerjaan</th>
										</tr>
									</thead>
									<tbody>
										<?php
$no = 1;
foreach ($pekerjaan['result']['presensi'] as $value) {
    // print var_dump($value);
    // ] => Array ( [tanggal] => 2020-05-03 [keterangan] => [tgl] => 03 May 2020 [hari] => Minggu [id_absen] => [jam_masuk] => [jam_pulang] => [kordinat_masuk] => [kordinat_pulang] => [task] => Array ( ) ) [3] => Array ( [tanggal] => 2020-05-04 [keterangan] => [tgl] => 04 May 2020 [hari] => Senin [id_absen] => b5f28fa0-8da2-11ea-a0e9-0e02d85c309a [jam_masuk] => 08:00:56 [jam_pulang] => 14:30:17 [kordinat_masuk] => -7.0312308,110.4727991 [kordinat_pulang] => -7.0312148,110.4728265 [task] => Array ( [0] => Array ( [id_task] => 31b952cd-8da4-11ea-a0e9-0e02d85c309a [judul_kerjaan] => Memantau dosen mengajar kelas pagi sesuai dg jadwal kuliah kelas pagi( kuliah mulai jam 08.00 WIB) bukti mengajar adalah screenshoot. [keterangan] => Dosen mengirimkan screenshoot ke WA group mengajar online FH sebagai bukti Dosen ybs telah menyelenggarakan perkuliahan online. [dokumen] => 05360104052020_USM_JAYA.jpg [link] => [jam_task] => 08:11:33 [jam_riport] => 08:16:36 ) [1] => Array ( [id_task] => c81a7d16-8db8-11ea-a0e9-0e02d85c309a [judul_kerjaan] => Melanjutkan proses pengajuan praktikum untuk kelas pagi dan sore pada fakultas hukum( bukti terlampir) [keterangan] => Lampiran untuk pengajuan praktikum , data harus komplit, antara lain: kode mata kuliah, nama mata kuliah, kelas paralel, sks, semester, status ( pagi/ sore), jumlah mahasiswa, ruang kelas dan kategori. [dokumen] => [link] => [jam_task] => 10:38:55 [jam_riport] => 10:44:14 ) )
    ?>
										<!--   -->
										<tr>
											<td class="text-center">
												<?=$no?>
											</td>
											<td ><?php print $value['hari'];?></td>
											<td ><?php print $value['tgl'];?></td>
											<td ><?php print $value['jam_masuk'];?></td>
											<td ><?php print $value['jam_pulang'];?></td>
											<td >
												<ul>
													<?php foreach ($value['task'] as $val) {
        ?>
													<li><?=$val['judul_kerjaan']?>
														<?php if (!empty($val['dokumen'])) {?>
														<a href="<?=base_url() . 'kepegawaian/rekap/laporan_baak_bauk/download_files/' . $val['id_task']?>" taget="_blank">
															<button class="btn btn-warning btn-xs"><i class="fa fa-download"></i></button>
														</a>
														<?php }
        if (!empty($val['link'])) {?>
														<a href="<?=$val['link']?>" taget="_blank"><button class="btn btn-info btn-xs"><i class="fa fa-link"></i></button></a>
														<?php }
        ?>
													</li>
													<?php }?>
												</ul>
											</td>
										</tr>
										<?php
$no++;
}
?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>