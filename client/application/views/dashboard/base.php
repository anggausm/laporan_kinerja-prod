<div class="row">
  <div class="col-sm-12">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"> Sistem Kepegawaian</a></li>
      </ol>
    </nav>
  </div>
</div>
<?php echo "<pre>";
print_r ($sudah_mengisi_kuesioner_bpm);
echo "</pre>"; ?>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading bg-white">
        Sistem Kepegawaian .<br>
        <small class="text-muted"> </small>
      </div>
      <div class="panel-body">
        <div class="btn-groups">
          <div class="row" ui-view="navbar@">
            <?php
            foreach ($menu as $value) {
              if ($value['sub_menu'] == array()) {

                $exp = explode(' ', $value['nm_menu']);
                $jm = count($exp);
            ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                  <a href="<?= base_url() . '' . $value['url']; ?>" class="text-sm btn btn-lg btn-rounded btn-stroke btn-success m-r waves-effect" style="width: 100%">
                    <i class="fa <?= $value['icon'] ?> fa-3x pull-left"></i>
                    <?php if ($jm <= 1) {
                    ?>
                      <span class="block clear text-left m-v-xs"> <b class="text-sm block font-bold"><?= $exp['0'] ?><br>&nbsp;</b>
                      </span>
                    <?php } else { ?>
                      <span class="block clear text-left m-v-xs"><?= $exp['0'] ?> <b class="text-sm block font-bold"><?= $exp['1'] . '' . $exp['2'] ?></b>
                      </span>
                    <?php } ?>
                  </a>
                </div>
            <?php
              }
            }

            ?>
          </div>
        </div>
        <hr>

        <div class="md-whiteframe-z0 bg-white">
          <ul class="nav nav-md nav-tabs nav-lines b-info">
            <li class="active">
              <a href="" data-toggle="tab" data-target="#tab_1" aria-expanded="false"><i class="fa fa-file"></i> Download Panduan</a>
            </li>
            <!-- <li class="">
            <a href="" data-toggle="tab" data-target="#tab_2" aria-expanded="false"><i class="fa fa-file"></i> Panduan Mengatasi Error</a>
          </li> -->
          </ul>
          <div class="tab-content p m-b-md b-t b-t-2x">
            <div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_1">
              <a href="https://apps.usm.ac.id/kpg/file/Panduan_presensi_WFH.pdf" download="" target="_blank">
                <button class="btn btn-info"><i class="fa fa-download"></i> Download Panduan Presensi WFH</button>
              </a>
              <a href="https://apps.usm.ac.id/kpg/file/PANDUAN_MENGATASI_ERROR.pdf" download="" target="_blank">
                <button class="btn btn-info"><i class="fa fa-download"></i> Download Panduan Mengatasi Error</button>
              </a>
              <a href="https://apps.usm.ac.id/kpg/file/PANDUAN_MENGATASI_ERROR_LOKASI_MOBILE.pdf" download="" target="_blank">
                <button class="btn btn-info"><i class="fa fa-download"></i> Download Panduan Mengatasi Error Di HP</button>
              </a>


              <!-- <iframe src="https://apps.usm.ac.id/kpg/file/Panduan_presensi_WFH.pdf" width="100%" height="500px">
            </iframe> -->
            </div>
            <div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_2">
              <!--  <iframe src="https://apps.usm.ac.id/kpg/file/PANDUAN_MENGATASI_ERROR.pdf" width="100%" height="500px">
           </iframe> -->
              <hr>
              <div class="widget-item">
                <h4>VISI MISI UNIVERSITAS SEMARANG</h4>
                <h4>Visi</h4>
                <p>
                  Menjadi Universitas yang menghasilkan sumber daya insani yang profesional, beradab serta berkelndonesiaan, dan ipteks yang berdaya guna dan berhasil guna
                </p>
                <h4>Misi</h4>
                <ul>
                  <li> Melaksanakan pendidikan akademik, vokasi, dan profesi; </li>
                  <li>Menyelenggarakan penelitian dan pengabdian kepada masyarakat yang menghasilkan ipteks yang berwawasan lingkungan dan berkelanjutan; dan </li>
                  <li>Menyebar luaskan ipteks untuk meningkatkan kesejahteraan masyarakat.</li>
                </ul>
                <h4>Tujuan</h4>
                <ul>
                  <li>Menghasilkan lulusan berkemampuan akademik, vokasi, dan profesi yang berkualitas dalam berbagai bidang ilmu, beriman dan bertaqwa kepada Tuhan Yang Maha Esa, bermoral Pancasila, berwawasan kebangsaan, berbudi luhur, dan mampu bersaing dalam skala nasional maupun global; </li>
                  <li>Mengembangkan dan menghasilkan ipteks dalam berbagai bidang ilmu sesuai dengan kebutuhan dan perkembangan masyarakat; dan </li>
                  <li>Menyebarluaskan ipteks dalam berbagai bidang ilmu untuk meningkatkan kesejahteraan masyarakat.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>