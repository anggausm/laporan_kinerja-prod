<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i></a></li>
                <li class="breadcrumb-item">BPM</li>
                <li class="breadcrumb-item"><a href="<?= base_url() . $this->link . 'sudah_mengisi' ?>">Kuesioner Tendik</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-angle-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert alert-<?php echo $this->session->flashdata('type'); ?>" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="card p r-3x box-shadow-md">
    <div class="card-body no-padding">
        <h3 class="font-bold text-success"><i class="fa fa-check-square"></i> Kuesioner Tendik</h3>
        <span class="text-muted font-normal">Terima kasih atas partisipasi anda dalam pengisian kuesioner kepuasan tendik</span>
    </div>
</div>