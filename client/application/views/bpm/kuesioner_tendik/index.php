<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i></a></li>
                <li class="breadcrumb-item">BPM</li>
                <li class="breadcrumb-item"><a href="<?= base_url() . $this->link ?>">Kuesioner Tendik</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-angle-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert alert-<?php echo $this->session->flashdata('type'); ?>" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="card p r-3x box-shadow-md">
    <div class="card-body no-padding">
        <h3 class="font-bold"><i class="fa fa-edit"></i> Kuesioner Tendik</h3>
        <span class="text-muted font-normal">Silakan isi kuesioner kepuasan dibawah sesuai pengalaman anda</span>
    </div>
</div>

<script type="text/css">
    .radio-container input[type="radio"] {
        display: inline-block;
    }
</script>

<div class="card p r-3x box-shadow-md">
    <div class="card-body m-b">
        <form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">
            <div class="row">
                <div class="col-sm-12">
                    <?php $no_dimensi = 1; ?>
                    <?php foreach ($dimensi_indikator_item as $key => $dimensi) : ?>
                        <span class="font-bold text-md m-t"><?php echo $no_dimensi . '. ' . $dimensi['nama_dimensi'] ?></span>
                        <?php $no_indikator = 1; ?>
                        <?php foreach ($dimensi['indikator'] as $key2 => $indikator) : ?>
                            <ul style="list-style-type: none"><span class="text-sm"><?php echo $no_indikator . '. ' . $indikator['nama_indikator'] ?></span></ul>
                            <?php $no_item = 1; ?>
                            <?php foreach ($indikator['item'] as $key3 => $item) : ?>
                                <input type="hidden" class="" name="<?php echo "data[$key$key2$key3][id_dimensi]" ?>" value="<?php echo $dimensi['id_dimensi'] ?>">
                                <input type="hidden" class="" name="<?php echo "data[$key$key2$key3][id_indikator]" ?>" value="<?php echo $indikator['id_indikator'] ?>">
                                <input type="hidden" class="" name="<?php echo "data[$key$key2$key3][id_item]" ?>" value="<?php echo $item['id_item'] ?>">
                                <ol style="list-style-type: none">
                                    <ul style="list-style-type: none">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <span class="font-normal text-muted"><?php echo $no_item . '. ' . $item['nama_item'] ?></span>
                                            </div>
                                            <div class="col-sm-7">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="radio-container">
                                                            <label class="md-check m-r">
                                                                <input type="radio" class="" name="<?php echo "data[$key$key2$key3][skor]" ?>" value="1" required><i class="green"></i> Kurang Relevan
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="radio-container">
                                                            <label class="md-check m-r">
                                                                <input type="radio" class="" name="<?php echo "data[$key$key2$key3][skor]" ?>" value="2" required><i class="green"></i> Cukup Relevan
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="radio-container">
                                                            <label class="md-check m-r">
                                                                <input type="radio" class="" name="<?php echo "data[$key$key2$key3][skor]" ?>" value="3" required><i class="green"></i> Relevan
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="radio-container">
                                                            <label class="md-check m-r">
                                                                <input type="radio" class="" name="<?php echo "data[$key$key2$key3][skor]" ?>" value="4" required><i class="green"></i> Sangat Relevan
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </ol>
                                <?php $no_item++; ?>
                            <?php endforeach ?>
                            <?php $no_indikator++; ?>
                        <?php endforeach ?>
                        <?php $no_dimensi++; ?>
                    <?php endforeach ?>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-sm btn-primary pull-right r-2x m-v-sm"><id class="fa fa-save"></id> Simpan</button> 
        </form>
    </div>
</div>