<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>ijin/ijin "> Ijin </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-<?php print $this->session->flashdata('type')?> text-center" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}

?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<?php
				if ($crud['c'] == '1') 
				{	?>
				<div class="col-md-12">
					<div class="panel-heading bg-white">
						<form method="POST" action="<?php print base_url() . 'ijin/ijin/tambah'?>">
							<button class="btn btn-info pull-left">
								<i class="fa fa-add-square-o"></i> Tambah
							</button>
						</form>
					</div>
				</div>
				<?php
				} ?>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:25%" class="text-center"> Nama </th>
												<th style="width:10%" class="text-center"> Jenis Ijin </th>
												<th style="width:15%" class="text-center"> Tanggal Awal </th>
												<th style="width:15%" class="text-center"> Tanggal Akhir </th>
												<th style="width:25%" class="text-center"> Keterangan </th>
												<th style="width:5%" class="text-center"> Aksi </th>
											</tr>
										</thead>

										<tbody>
											<?php
											$no = 1;
											foreach ($get_default['result'] as $value) 
											{
											    // print var_dump($value);
											    ?>
												<tr>
													<td class="text-center"> <?=$no?> </td>
													<td> <?=$value['nama_doskar']?></td>
													<td class="text-center"> <?=$value['jenis_absensi']?> </td>
													<td class="text-center"> <?=$value['tanggal_awal']?> </td>
													<td class="text-center"> <?=$value['tanggal_akhir']?> </td>
													<td> <?=$value['keterangan']?> </td>
													<td  class="text-center">
														<form method="POST" 
															action="<?php print base_url() . 'ijin/ijin/edit'?>">
															<input type="hidden" name="id" value="<?php print enc_data($value['id'])?>">
															<button class="btn btn-warning btn-xs"><i class="fa fa-check-square-o"></i> Edit </button>
														</form>
													</td>
												</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>