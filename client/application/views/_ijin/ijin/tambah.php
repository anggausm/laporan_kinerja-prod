<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>ijin/ijin">Manajemen Ijin</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>ijin/ijin/tambah">Tambah Ijin</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<div class="row">
		<?php
		if ($crud['c'] == '1') 
		{	?>
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Tambah Ijin
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>ijin/ijin/simpan">
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Nama Dosen / Karyawan </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="doskar_usm" required="">
										<option value="">Pilih Doskar</option>
										<?php foreach ($doskar_usm as $value): ?>
										<option value="<?php echo $value['user_key'] ?>"><?php echo $value['nama_doskar'] ?></option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Mulai</label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl_mulai" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required/>
	                            </div>
	                            <label for="" class="col-sm-1 control-label"> Sampai </label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker2' name="tgl_selesai" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenis Ijin </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenis_ijin" required>
										<option value="">Pilih Jenis Ijin</option>
										<?php foreach ($jenis_ijin as $ijin): ?>
										<option value="<?php echo $ijin['id_jenis'] ?>">
											<?php echo $ijin['jenis_absensi'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan </label>
								<div class="col-sm-9">
									<textarea name="keterangan" placeholder="Catatan" class="form-control" rows="5" required=""></textarea>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<?php 
		} ?>
	</div>
</div>