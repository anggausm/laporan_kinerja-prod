<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kinerja/tendik/validasi_skp">Validasi Kinerja Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Penilaian Pegawai
                    </div>
                </div>
                <div class="col-sm-9">

                    <form action="<?php print base_url()?>kinerja/tendik/validasi_skp/data" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($p_chosen == $vl['id_periode']){ print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Kinerja Pegawai
				
				</div>
				<div class="panel-body">
					<b>Periode Pengisian : <?= date('d M Y', strtotime($p_aktif['pengisian_mulai'])) ?> sampai <?= date('d M Y', strtotime($p_aktif['pengisian_selesai'])) ?></b>
					<br><br>
					<div class="table-responsive">
							<table class="table table-striped b-t b-b table-bordered">
								<thead>
									<tr class="text-center">
										<th class="text-center">NO</th>
										<th class="text-center">NAMA</th>
										<th class="text-center">UNIT</th>
										<th class="text-center">STATUS PENGAJUAN KINERJA</th>
										<th class="text-center">AKSI</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($list as $value) {
									?>
										<tr>
											<td class="text-center"><?= $no ?></td>
											<td> <?= $value['dinilai_nama']; ?> </td>
											<td> <?= $value['dinilai_fak_unit']; ?> </td>
											<td class="text-center"> 
													<?= $value['status']; ?> </td>
											<td class="text-center">
												<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/detail/<?php echo enc_data($value['dinilai_user_key']) ?>/<?= enc_data($p_aktif['id_periode']); ?>"><button class="btn btn-sm btn-info">Lihat  </button></a>
													<!-- <form method="POST" action="<?= base_url('kinerja/tendik/validasi_skp/detail') ?>">
														<input type="hidden" name="user_key" value="<?= $value['dinilai_user_key']; ?>">
														<input type="hidden" name="id_periode" value="<?= $p_aktif['id_periode']; ?>">
														<button type="submit" class="btn btn-sm btn-info"> Lihat </button>
													</form> -->

											</td>
										</tr>
									<?php
										$no++;
									}
									?>
								</tbody>
							</table>
						</div>
 					<br>
 				</div>
				</div>
			</div>
		</div>
	</div>
</div>