<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kinerja/tendik/validasi_skp">Validasi Penilaian Kinerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Kinerja Pegawai
				
				</div>
				<div class="panel-body">
					<?php 
					foreach($p_aktif as $val){
						$p_mulai = $val['pengisian_mulai'];
						$p_akhir = $val['pengisian_selesai'];
					} 

					?>
					<div class="alert alert-info font-bold" role="alert">
						Periode Pengisian : <?= date('d M Y', strtotime($p_aktif['pengisian_mulai'])) ?> sampai <?= date('d M Y', strtotime($p_aktif['pengisian_selesai'])) ?>
					</div>
					<form class="form-horizontal p-h-xs">
						<div class="row">
                            <div class="col-md-12">
                                <h4 class="text-center font-bold">REKAP PENILAIAN KINERJA</h4>
                                <h4 class="text-center font-bold"><?=$personel['nama_jenis_pegawai'];?></h4>
                                 <br>
                            </div> 
							<div class="col-md-6">
								<div>
									<label class="col-sm-2 control-label text-right">NIS </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nis'];?></label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">Nama </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nama_doskar'];?> </label>
								</div>
                                <div>
									<label class="col-sm-2 control-label text-right">Unit </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nama_unit'];?>  </label>
								</div>
							</div>
							<div class="col-md-6">
                                <div>
									<label class="col-sm-2 control-label text-right">Pekerjaan</label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['pekerjaan'];?>  </label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">TMT USM</label>
									<label class="col-sm-10 control-label text-right">: <?= date('d M Y', strtotime($personel['tmt_usm']));?>  </label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">Status Pengajuan</label>
									<label class="col-sm-10 control-label text-right">: <?php
									foreach($skp_peg as $vals){
										$validasi = $vals['ket'];
									}
									
											if(empty($skp_peg)){
												echo 'Belum ada pengajuan';
											} else if($validasi == 1){
												echo 'Pengajuan sudah divalidasi';
											} else if($validasi == 0){
												echo 'Pengajuan belum divalidasi';
											}
											?>  </label>
								</div>
							</div>
						</div>
						<br>
					</form>

					<?php if(!empty($skp_peg)){ ?>
					<div class="table-responsive">
						<table  class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr class="text-center">
									<th class="text-center" >No</th>
									<th class="text-center" >Indikator</th>
									<th class="text-center"> Skor</th>
									<th class="text-center"> Bobot </th>
									<th class="text-center"> Nilai </th>
								</tr>
							</thead>
							<tbody>
				                <form  class="form-horizontal p-h-xs" action="" method ="POST">
                                <input type="hidden" name="user_key" value="<?= $personel['user_key'] ?>">
                                <input type="hidden" name="periode" value="<?= $p_aktif['id_periode'] ?>">
								<?php

                                $no = 1;
                                foreach ($skp_peg as $value) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td>
                                            <?=$value['rincian_indikator'];?>
                                           
                                        </td>
                                        <td class="text-center">
                                          	<?=$value['skor'];?>
                                        </td>
                                        <td class="text-center">
                                           	<?=$value['bobot'];?>
                                        </td>
                                        <td class="text-center">
                                          	<?=$value['nilai'];?>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                                <tr>
                                    <td class="text-center" colspan="4">
                                        Total Nilai</td>
                                        <td>
                                        	<?=$value['total_nilai'];?>
                                    </td>
                                </tr>
                                </form>
							</tbody>
						</table>
							<?php if(empty($value['validasi_atasan'])){ ?>
						<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/validasi/<?php echo enc_data($personel['user_key']) ?>/<?php echo enc_data($p_aktif['id_periode']) ?>"><button class="btn btn-md btn-success pull-right" onclick="return confirm('Apakah Anda yakin untuk memvalidasi data ini?');"><i class="fa fa-check"></i> Validasi Penilaian </button></a> &nbsp;
						<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/revisi/<?php echo enc_data($personel['user_key']) ?>/<?php echo enc_data($p_aktif['id_periode']) ?>"><button class="btn btn-md btn-warning pull-right" onclick="return confirm('Apakah Anda yakin untuk mengembalikan data penilaian ini?');"><i class="fa fa-undo"></i> Kembalikan Pengajuan</button></a> &nbsp;
					<?php } ?>
						
					
						
					</div>
				<?php }
				else {
					echo '<h3>Belum ada penilaian yang diajukan ...';
				} ?>

 					<br>
 				</div>
				</div>
			</div>
		</div>
	</div>
</div>