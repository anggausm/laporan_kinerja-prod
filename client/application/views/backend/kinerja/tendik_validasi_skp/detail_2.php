<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kinerja/tendik/validasi_skp">Validasi Penilaian Kinerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Kinerja Pegawai
				
				</div>
				<div class="panel-body">

					<?php 
					foreach($p_aktif as $val){
						$p_mulai = $val['pengisian_mulai'];
						$p_akhir = $val['pengisian_selesai'];
					} 

					?>
					<form class="form-horizontal p-h-xs">
						<div class="row">
                            <div class="col-md-12">
                                <h4 class="text-center font-bold">REKAP PENILAIAN KINERJA</h4>
                                <h4 class="text-center font-bold"><?=$personel['nama_jenis_pegawai'];?></h4>
                                 <br>
                            </div> 
							<div class="col-md-6">
								<div>
									<label class="col-sm-2 control-label text-right">NIS </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nis'];?></label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">Nama </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nama_doskar'];?> </label>
								</div>
                                <div>
									<label class="col-sm-2 control-label text-right">Unit </label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['nama_unit'];?>  </label>
								</div>
							</div>
							<div class="col-md-6">
                                <div>
									<label class="col-sm-2 control-label text-right">Pekerjaan</label>
									<label class="col-sm-10 control-label text-right">: <?=$personel['pekerjaan'];?>  </label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">TMT USM</label>
									<label class="col-sm-10 control-label text-right">: <?= date('d M Y', strtotime($personel['tmt_usm']));?>  </label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">Status Pengajuan</label>
									<label class="col-sm-10 control-label text-right">: <?php
									foreach($skp_peg as $vals){
										$validasi = $vals['ket'];
									}
									
											if(empty($skp_peg)){
												echo 'Belum ada pengajuan';
											
											} else if($rekap_nilai['status'] == 1){
												echo 'Pengajuan sudah divalidasi';
											} else if($rekap_nilai['status'] == 0){
												echo 'Pengajuan belum divalidasi';
											}
											?>  </label>
								</div>
							</div>
						</div>
						<br>
					</form>
					
					<?php 
							if(empty($skp_peg)){
												echo 'Belum ada pengajuan';
											} else if($validasi == 0){
					 ?>

								<div class="table-responsive">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>I. </h4>
												</td>
												<td style="width:40%" class="text-left"><h4>Pelaksanaan Tugas  Utama </h>
																								</td>
												<td style="width:12.5%" class="text-right">
													Aksi
												</td>
												<td style="width:12.5%" class="text-right">
													Skor
												</td>
												<td style="width:12.5%" class="text-right">
													Bobot
												</td>
												<td style="width:12.5%" class="text-right">
													Nilai
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-left">
												</td>
												<td style="width:40%" class="text-left">
													a. Penilaian Individu
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-left">
												</td>
												<td style="width:40%" class="text-left">
													1. Kesesuaian pekerjaan:
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>

											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%" class="text-left">   a. Menyelesaikan pekerjaan dengan benar</td>
												<td style="width:12.5%" class="text-right">
													<a href="<?php echo base_url() ?>kinerja/tendik/validasi_log/index"><button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button></a>
													</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[0]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[0]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[0]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   b. Menyelesaikan pekerjaan tepat waktu</td>
												<td style="width:12.5%" class="text-right">
											
													<a href="<?php echo base_url() ?>kinerja/tendik/validasi_log/index"><button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[1]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[1]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[1]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   c. Bersedia menjalankan tugas tambahan yang relevan sesuai dengan pekerjaan</td>
												<td style="width:12.5%" class="text-right">
													
														<a href="<?php echo base_url() ?>kinerja/tendik/validasi_log/index"><button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[2]['skor'] ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[2]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[2]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 2. Kesesuaian spesifikasi pekerjaan</td>
												<td style="width:12.5%" class="text-right">		
												
	
													 <a id="detail_surat1" class="btn btn-default btn-md" data-toggle="modal" data-target="#modal-detail1" data-namfol1="<?php echo $dok1['folder_sk']; ?>" data-namfil1="<?php echo $dok1['file_sk']; ?>"
													 	data-namfil2="<?php echo $dok2['file']; ?>" data-namfil3="<?php echo $dok3['nm_file']; ?>" data-namfil3="<?php echo $dok3['nm_folder']; ?>"><i class="fa fa-eye"></i></a>							
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[3]['skor'] ?></button>		
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[3]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[3]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">b. Penilaian Kepuasan Layanan</td>
												<td style="width:12.5%" class="text-right">												
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kuesioner dari Dosen atau Mahasiswa atau atasan langsung </td>
												<td style="width:12.5%" class="text-right">
															<a href="<?php echo base_url() ?>kinerja/kependidikan/penilaian_pimpinan">
																<button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button>
															</a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[4]['skor'] ?></button>
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[4]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[4]['nilai'] ?> </button>
													
												</td>

											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>II. </h4>
												</td>
												<td style="width:40%"class="text-left"><h4>Core Values </h4></td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Harmoni: Mengikuti kegiatan di Universitas/ Fakultas </td>
												<td style="width:50%" class="text-right">
													
													<?php 
													if(!empty($foto)){
														?>
														<a id="detail_foto" class="btn btn-default btn-md" data-toggle="modal" data-target="#modal-foto" data-namfolto="<?php echo $foto['nama_folder']; ?>" data-namfilto="<?php echo $foto['file_foto']; ?>"><i class="fa fa-eye"></i></a>
													 	<?php
													} else {
														?>
														<button class="btn btn-md btn-default"> <i class="fa fa-remove"></i></button>
														<?php
													}

													?>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[5]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[5]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[5]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 2. Integritas : Presensi fingerprint dan keberadaan di tempat tugasnya sesuai jam kerja, kehadiran pada acara universitas/ fakultas  </td>
												<td style="width:12.5%" class="text-right">
													
													<?php 
													if(!empty($event_wajib)){
														$skor_7 = '1';
													} else {
														$skor_7 = '0';
													}

														$bobot_7 = "0.06";
														$nil7 = $skor_7*$bobot_7; 

													if($skor_7 >= 1){
													?>
												<button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button>
												<?php } else { ?>
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button>
												<?php } ?>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[6]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[6]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[6]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 3. Akuntabilitas : Pelayanan terhadap mahasiswa/dosen terkait surat menyurat atau pekerjaan lainnya diselesaikan tepat waktu</td>
												<td style="width:12.5%" class="text-right">
													<a href="<?php echo base_url() ?>kinerja/tendik/validasi_log/index"><button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[7]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[7]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[7]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 4. Prestasi (lolos Sertifikasi dengan uji kompetensi dari lembaga yang berwenang, juara tingkat lokal, nasional, regional )</td>
												<td style="width:12.5%" class="text-right">

													  <?php if(!empty($dok4)){ ?>
													<a id="detail_surat4" class="btn btn-default btn-md" data-toggle="modal" data-target="#modal-detail4" data-namfol4="<?php echo $dok4['nm_folder']; ?>" data-namfil4="<?php echo $dok4['nm_file']; ?>"
													 	><i class="fa fa-eye"></i></a>
													 <?php } else { ?>
													 	<button class="btn btn-md btn-default"> <i class="fa fa-remove"></i></button>
													 	<?php
													 } ?>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[8]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[8]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[8]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 5. Kepemimpinan : membuat laporan kegiatan yang sudah dilaksanakan dalam periode penilaian</td>
												<td style="width:12.5%" class="text-right">													
													<a href="<?php echo base_url() ?>kinerja/tendik/validasi_log/index"><button class="btn btn-md btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_10; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_10; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil10,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>III. </h4>
												</td>
												<td style="width:40%"class="text-left"> <h4>Pelaksanaan Kegiatan Penunjang  (Tambahan) </h4></td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> a. Pengembangan diri Sesuai dengan Pekerjaan</td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Sertifikat/ surat tugas mengikuti pelatihan/ seminar/ workshop/Studi Banding</td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($dok5)){ ?>
														 <a id="detail_surat5" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail5" data-namfol5="<?php echo $dok5['nm_folder']; ?>" data-namfil5="<?php echo $dok5['nm_file']; ?>"><i class="fa fa-eye"></i></a>
														<?php } else { ?>
															<button class="btn btn-md btn-default"> <i class="fa fa-remove"></i></button>
														<?php } ?>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[9]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[9]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[9]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> b. Keterlibatan dalam kepanitiaan</td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. SK kepanitiaan</td>
												<td style="width:12.5%" class="text-right">
													
												
														<?php if(!empty($dok6)){ ?>
														 <a id="detail_surat6" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail6" data-namfol6="<?php echo $dok6['folder_sk']; ?>" data-namfil6="<?php echo $dok6['file_sk']; ?>"><i class="fa fa-eye"></i></a>
														<?php } else { ?>
															<button class="btn btn-md btn-default"> <i class="fa fa-remove"></i></button>
														<?php } ?>
														
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[10]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[10]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[10]['nilai'] ?> </button>
													
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>IV. </h4>
												</td>
												<td style="width:40%"class="text-left"> <h4>Penilaian Kehadiran </h4></td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> a. Kehadiran Fingerprint</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button>
													<?php 
														$jp = $presensi['persentes']; 
													if($jp <= '100'){
														$skor_12 = '1';

													} else if($jp >= '75'){
														$skor_12 = '0.5';
													} else {
														$skor_12 = '0';
													}
													 $bobot_12 = "0.10";
														$nil12 = $skor_12*$bobot_12; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													
													<button class="btn btn-md btn-default"><?php echo $skp_peg[11]['skor'] ?></button>
													
												
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skp_peg[11]['bobot'] ?></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $skp_peg[11]['nilai'] ?> </button>
													
												</td>
											</tr>
											</tr>
												<td style="width:10%" class="text-center"> </td>
												<td style="width:40%"class="text-left">Total Nilai </td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
													<b><?php echo $skp_peg[11]['total_nilai'] ?></b>
												</td>
											<tr>
										</tbody>
									</table>
									
					<?php if(empty($value['ket'])){ ?>
						<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/validasi/<?php echo enc_data($personel['user_key']) ?>/<?php echo enc_data($p_aktif['id_periode']) ?>"><button class="btn btn-md btn-success pull-right" onclick="return confirm('Apakah Anda yakin untuk memvalidasi data ini?');"><i class="fa fa-check"></i> Validasi Penilaian </button></a> &nbsp;
						<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/revisi/<?php echo enc_data($personel['user_key']) ?>/<?php echo enc_data($p_aktif['id_periode']) ?>"><button class="btn btn-md btn-warning pull-right" onclick="return confirm('Apakah Anda yakin untuk mengembalikan data penilaian ini?');"><i class="fa fa-undo"></i> Kembalikan Pengajuan</button></a> &nbsp;
					<?php } ?>					
					
						
					</div>
				<?php } else if($validasi == 1){
					?>
					<div class="table-responsive">
						<table  class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr class="text-center">
									<th class="text-center" >No</th>
									<th class="text-center" >Indikator</th>
									<th class="text-center"> Skor</th>
									<th class="text-center"> Bobot </th>
									<th class="text-center"> Nilai </th>
								</tr>
							</thead>
							<tbody>
				                <form  class="form-horizontal p-h-xs" action="" method ="POST">
                                <input type="hidden" name="user_key" value="<?= $personel['user_key'] ?>">
                                <input type="hidden" name="periode" value="<?= $p_aktif['id_periode'] ?>">
								<?php

                                $no = 1;
                                foreach ($skp_peg as $value) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td>
                                            <?=$value['rincian_indikator'];?>
                                           
                                        </td>
                                        <td class="text-center">
                                          	<?=$value['skor'];?>
                                        </td>
                                        <td class="text-center">
                                           	<?=$value['bobot'];?>
                                        </td>
                                        <td class="text-center">
                                          	<?=$value['nilai'];?>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                                <tr>
                                    <td class="text-center" colspan="4">
                                        Total Nilai</td>
                                        <td>
                                        	<?=$value['total_nilai'];?>
                                    </td>
                                </tr>
                                </form>
							</tbody>
						</table>
						<?php if($rekap_nilai['validai_univ'] == NULL ){?>
						<a href="<?php echo base_url() ?>kinerja/tendik/validasi_skp/unvalidasi/<?php echo enc_data($personel['user_key']) ?>/<?php echo enc_data($p_aktif['id_periode']) ?>"><button class="btn btn-md btn-warning pull-right" onclick="return confirm('Apakah Anda yakin untuk mengembalikan data penilaian ini?');"><i class="fa fa-undo"></i> Unvalidasi</button></a>
											
											<?php	
										}
											} ?>

 					<br>
 				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-detail1" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <?php if(!empty($dok1)){ ?>
              
              <div class="modal-body" id="body_detail_surat1"> 
              	<p align="center">SK Pengangkatan Pegawai :</p>
              </div>  
          		<?php } else {
          			echo 'Tidak ada file SK';
          		}
          		?>

          		<?php if(!empty($dok2)){ ?>
          			<br>
             
              <div class="modal-body" id="body_detail_surat2"> 
              	 <p align="center">Ijazah :</p>
              </div> 
          <?php } else {
          			echo 'Tidak ada file Ijazah'; 
          		}?>
          		
          <?php if(!empty($dok3)){ ?>
              
              <div class="modal-body" id="body_detail_surat3"> 
              	<p align="center">Sertifikat kompetensi :</p>
              </div>  
          <?php } else {
          		echo 'Tidak ada sertifikat kompetensi';
          } ?>
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div>

<div id="modal-detail4" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
            
              <div class="modal-body" id="body_detail_surat4"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 
<div id="modal-detail5" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
            
              <div class="modal-body" id="body_detail_surat5"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 
<div id="modal-detail6" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
            
              <div class="modal-body" id="body_detail_surat6"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 
<div id="modal-foto" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View Foto</h4>  
              </div>  
            
              <div class="modal-body" id="body_detail_foto"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat4', function(){
				var nm_file4  = $(this).data('namfil4');
				var nm_folder4  = $(this).data('namfol4');
				$("#body_detail_surat4").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sertifikat/'+nm_folder4+'/'+nm_file4+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
			$(document).on('click','#detail_surat1', function(){
				var nm_file1  = $(this).data('namfil1');
				var nm_folder1  = $(this).data('namfol1');
				var nm_file2  = $(this).data('namfil2');
				var nm_file3  = $(this).data('namfil3');
				var nm_folder3  = $(this).data('namfol3');
				$("#body_detail_surat1").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sk/'+nm_folder1+'/'+nm_file1+'" width="100%" height="600px" allowfullscreen></iframe>' );
				$("#body_detail_surat2").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/ijazah/'+nm_file2+'" width="100%" height="600px" allowfullscreen></iframe>' );
				$("#body_detail_surat3").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sertifikat/'+nm_folder3+'/'+nm_file3+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
			$(document).on('click','#detail_surat5', function(){
				var nm_file5  = $(this).data('namfil5');
				var nm_folder5  = $(this).data('namfol5');
				$("#body_detail_surat5").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sertifikat/'+nm_folder5+'/'+nm_file5+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
			$(document).on('click','#detail_surat6', function(){
				var nm_file6  = $(this).data('namfil6');
				var nm_folder6  = $(this).data('namfol6');
				$("#body_detail_surat6").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sk/'+nm_folder6+'/'+nm_file6+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
			$(document).on('click','#detail_foto', function(){
				var nm_foto  = $(this).data('namfilto');
				var nm_foldto  = $(this).data('namfolto');
				$("#body_detail_foto").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/event/'+nm_foldto+'/'+nm_foto+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>