<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/lpp"><?php print $this->title; ?></a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
		<strong>Penilaian Laporan Pertanggungjawaban Pekerjaan untuk Tenaga Kependidikan Struktural meliputi poin-poin indikator di bawah :  </strong>
		<ul>
			<li>Menyelesaikan pekerjaan dengan benar</li>
			<li>Menyelesaikan pekerjaan tepat waktu</li>
			<li>Bersedia menjalankan tugas tambahan yang relevan sesuai dengan pekerjaan</li>
			<li>Kesesuaian spesifikasi pekerjaan</li>
			<li>Akuntabilitas : Pelayanan terhadap mahasiswa/dosen terkait surat menyurat atau pekerjaan lainnya diselesaikan tepat waktu</li>
			<li>Kepemimpinan : mengkoordinasi dan mengarahkan bawahan untuk mengerjakan tugas sesuai yang telah ditentukan.</li>
		</ul>
		<strong>Nilai akan masuk ke dashboard penilaian jika sudah divalidasi oleh pimpinan/atasan.</strong>
	</div>

	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <?php print $this->title; ?>
					<a href="<?= base_url()?>file/PANDUAN_MENU_LPP.pdf" class="btn btn-success btn-md pull-right" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian LPP</a><br>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/lpp/" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Tahun </label>
									<div class="col-sm-8">
										<select class="form-control" name="tahun">
											<option value="2024" <?php if ($tahun == '2024') {print 'selected=""';}?> >2024</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
							</div>
						</div>
					</form>
					<br>
					<h5><i class="fa fa-calendar"></i> Laporan Pertanggungjawaban Pekerjaan <b>Tahun <?php print $tahun; ?></b></h5>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Periode</th>
												<th style="width:30%" class="text-center">Keterangan</th>
												<th style="width:15%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($default as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-F-Y",strtotime($value['periode_lpp'])); ?></td>
												<td style="text-align: left;"><?php echo $value['tugas'];?></td>
												<td class="text-center">
													
													<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfol="<?php echo $value['nama_folder']; ?>" data-namfil="<?php echo $value['nama_file']; ?>"><i class="fa fa-eye"></i></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/lpp/edit/<?php echo enc_data($value['id_lpp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/lpp/hapus/<?php echo enc_data($value['id_lpp']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Tambah <?php print $this->title; ?>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/lpp/simpan" enctype="multipart/form-data">
							<input type="hidden" name="periode_aktiff" value="<?php echo $pa['id_periode']; ?>">
							<input type="hidden" name="id_pegawai" value="<?php echo $tendik['id_peg_isdm']; ?>">
							<div class="form-group">
			                            <label for="" class="col-sm-3 control-label">Tahun</label>
			                            <div class="col-sm-4">
			                              		<select class="form-control" name="tahun">
												<option value="2024" <?php if ($tahun == '2024') {print 'selected=""';}?> >2024</option>
												<option value="2024" <?php if ($tahun == '2025') {print 'selected=""';}?> >2025</option>
											</select>
			                            </div>
			                        </div>
							<div class="form-group">
								<input type="hidden" name="periode_aktiff" value="<?php echo $pa['id_periode']; ?>">
			                            <label for="" class="col-sm-3 control-label">Bulan</label>
			                            <div class="col-sm-4">
			                               <select class="form-control" name="bulan">
													<option value="01" <?php if ($bulan == '01') {print 'selected=""';}?>>Januari</option>
													<option value="02" <?php if ($bulan == '02') {print 'selected=""';}?> >Februari</option>
													<option value="03" <?php if ($bulan == '03') {print 'selected=""';}?> >Maret</option>
													<option value="04" <?php if ($bulan == '04') {print 'selected=""';}?> >April</option>
													<option value="05" <?php if ($bulan == '05') {print 'selected=""';}?> >Mei</option>
													<option value="06" <?php if ($bulan == '06') {print 'selected=""';}?> >Juni</option>
													<option value="07" <?php if ($bulan == '07') {print 'selected=""';}?> >Juli</option>
													<option value="08" <?php if ($bulan == '08') {print 'selected=""';}?> >Agustus</option>
													<option value="09" <?php if ($bulan == '09') {print 'selected=""';}?> >September</option>
													<option value="10" <?php if ($bulan == '10') {print 'selected=""';}?> >Oktober</option>
													<option value="11" <?php if ($bulan == '11') {print 'selected=""';}?> >November</option>
													<option value="12" <?php if ($bulan == '12') {print 'selected=""';}?> >Desember</option>
												</select>
			                            </div>
	                            </div>
	                        
		                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan </label>
								<div class="col-sm-9">
									<input type="text" name="tugas" placeholder="Isikan Keterangan Laporan" class="form-control" required>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File Laporan</label>
								<div class="col-sm-9">
									<span  class="label red">Format file PDF maks. 2 MB</span>
									<input type="file" name="file_lpp" class="form-control" required>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
		</div>
	</div>
</div>

<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_folder  = $(this).data('namfol');
				var nm_file  = $(this).data('namfil');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/lpp/'+nm_folder+'/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>