<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak/'.enc_data_url($periode['id_periode'])) ?>">Rekap</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak_kegiatan/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($kegiatan['id_unsur'])) ?>"> <?= $kegiatan['nama_unsur'] ?> </a></li>
                <li class="breadcrumb-item"><a href="#">Detail</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PENILAIAN KINERJA DOSEN PERIODE <?= strtoupper($periode['nama_periode']) ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<?php if (!empty($ketentuan) && !empty($berkas)) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <div class="row">
                <div class="col-md-4">
                    <div class="alert alert-info" role="alert">
                        <h4 class="text-capitalize no-margin"> <span class="font-bold"> Ketentuan : </span></h4>  <hr>
                        <table class="table text-justify" style="border: 1px solid #e7eaec">
                            <?php 
                            $no = 1;
                            foreach ($ketentuan as $keten) : ?>
                            <tr>
                                <td style="width:5%"> <?= $no++.'.' ?> </td>
                                <td style="width:95%"> <?= $keten['keterangan'] ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </table>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="alert alert-info" role="alert">
                        <h4 class="text-capitalize no-margin"> <span class="font-bold"> Berkas : </span></h4> <hr>
                        <table class="table text-justify" style="border: 1px solid #e7eaec">
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach ($berkas as $ber) : ?>
                            <tr>
                                <td style="width:5%"> <?= $no++.'.' ?> </td>
                                <td style="width:95%"> <?= $ber['keterangan'] ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="alert alert-info" role="alert">
                        <h4 class="text-capitalize no-margin"> <span class="font-bold"> Penilaian : </span></h4> <hr>
                        <table class="table text-justify" style="border: 1px solid #e7eaec">
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach ($penilaian as $pen) : ?>
                            <tr>
                                <td style="width:5%"> <?= $no++.'.' ?> </td>
                                <td style="width:95%"> <?= $pen['keterangan'] ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <div class="row">
                <div class="col-sm-10">
                    <h4 class="text-capitalize no-margin">daftar kegiatan <span class="font-bold"> <?= $kegiatan['nama_unsur'] ?> </span></h4>
                    <p class="no-margin m-b"> <?= $kegiatan['nomor'].'. '.$kegiatan['nama_kegiatan'] ?> </p>
                </div>

                <div class="col-sm-2"> 
                <?php if ($button['status_button'] == 'Enable') : ?>
                    <?php if($kegiatan['jenis_insert'] == 'import') : ?>
                        <form action="<?= base_url($this->link.'pak_kegiatan_import') ?>" class="form-inline text-right" method='post' 
                            onclick="return confirm('Melakukan synchronize sata akan menghapus kegiatan pada periode ini yang sudah di synchronize sebelumnya.');">
                            <input type="hidden" name="jenis" value="<?= enc_data_url($kegiatan['link']) ?>">
                            <input type="hidden" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                            <input type="hidden" name="semester" value="<?= enc_data_url($periode['id_semester']) ?>">
                            <input type="hidden" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
                            <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-refresh"></i> Synchronize </button>
                        </form>
                    <?php else : ?>
                        <form action="<?= base_url($this->link.'pak_kegiatan_form') ?>" class="form-inline text-right" method='post'>
                            <input type="hidden" name="jenis" value="<?= enc_data_url($kegiatan['link']) ?>">
                            <input type="hidden" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                            <input type="hidden" name="semester" value="<?= enc_data_url($periode['id_semester']) ?>">
                            <input type="hidden" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
                            <input type="hidden" name="id" value="<?= enc_data_url('') ?>">
                            <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tambah </button>
                        </form>
                    <?php endif ?>
                <?php endif ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th class="v-m text-center">No.</th> <!--  style="width:3%"  -->
                            <th class="v-m">Nama Kegiatan</th>
                            <th class="v-m">Rincian Kegiatan</th>
                            <th class="v-m text-center">Nilai</th>
                            <th class="v-m text-center">Jumlah / SKS</th>
                            <th class="v-m text-center">Nilai SKP</th>
                            <th class="v-m text-center">Status</th>
                            <th class="v-m text-center" style="width:10%"> Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $no     = 1;
                        $j_skor = 0;
                        $j_skp  = 0;
                        foreach ($sub_keg as $keg) : ?>
                        <tr <?php if ($keg['link_berkas'] == '' || $keg['link_berkas'] == null) : ?> class="red-100" <?php endif ?> >
                            <td class="text-center"> <?= $no++ ?> </td>
                            <td> <?= $keg['nama'] ?>  </td>
                            <td> <?= $keg['rincian'] ?> </td>
                            <td class="text-center"> <?= $keg['nilai'] ?> </td>
                            <td class="text-center"> <?= $keg['skor'] ?> </td>
                            <td class="text-center"> <?= $keg['nilai_skp'] ?> </td>
                            <td class="text-center"> <?= $keg['status'] ?> </td>
                            <td class="text-center" style="width: 10%;">
                                <a href="<?= base_url($this->link.'pak_kegiatan_lihat/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($keg['id_values'])) ?>" 
                                    class="btn btn-stroke btn-success r-2x btn-sm" alt="Lihat"><i class="fa fa-eye"></i> Lihat
                                </a>
                                <?php if ($button['status_button'] == 'Enable') : ?>
                                <!-- <a href="#" class="btn btn-stroke btn-info r-2x btn-sm">Edit</a> -->
                                <a href="<?= base_url($this->link.'pak_kegiatan_hapus/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($keg['id_values'])) ?>" 
                                    class="btn btn-stroke btn-danger r-2x btn-sm" alt="Hapus" 
                                    onclick="return confirm('Apakah anda yakin untuk menghapus data ini ?');"><i class="fa fa-trash"></i>
                                </a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php 
                        $j_skor = $j_skor + $keg['skor'];
                        $j_skp  = $j_skp + $keg['nilai_skp'];
                        endforeach ?>
                        <tr>
                            <td colspan="4" class="text-right font-bold"> Jumlah Skor </td>
                            <td class="text-center font-bold"><?= $j_skor ?></td>
                            <td class="text-center font-bold"><?= $j_skp ?></td>
                            <td colspan="2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php /*
<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="no-margin m-b">Perhitungan KUM</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th rowspan="2" style="width:3%" class="v-m">No.</th>
                            <th rowspan="2" style="width:17%" class="v-m text-center">Semester</th>
                            <th colspan="2" style="width:40%" class="v-m text-center">Usulan Dosen</th>
                            <th colspan="2" style="width:40%" class="v-m text-center">Penilaian Asesor</th>
                        </tr>
                        <tr>
                            <th class="v-m text-center">SKS Total</th>
                            <th class="v-m text-center">Nilai KUM</th>
                            <th class="v-m text-center">SKS Total</th>
                            <th class="v-m text-center">Nilai KUM</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td class="text-center">20231</td>
                            <td class="text-center">6</td>
                            <td class="text-center">3</td>
                            <td class="text-center">6</td>
                            <td class="text-center">3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">tambah kegiatan <span class="font-bold">Pendidikan</span></h4>
            <p class="no-margin m-b">Sub Komponen 1. Melaksanakan perkuliahan</p>

            <form role="form" method="POST" class="form-horizontal" action="#" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-2">Tanggal Terbit Dokumen</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control r-2x" name="" placeholder="dd/mm/yyyy" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Semester</label>
                    <div class="col-sm-10">
                        <select name="" class="form-control r-2x select2" style="width: 100%;" required>
                            <option value="">Pilih Semester</option>
                            <option value="">2023/2024 Genap</option>
                            <option value="">2023/2024 Gasal</option>
                            <option value="">2022/2023 Genap</option>
                            <option value="">2022/2023 Gasal</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Nama Mata Kuliah</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Nama Mata Kuliah" required>
                        <p class="text-muted no-margin">Inputkan hanya 1 mata kuliah</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">SKS</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah SKS" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Jumlah Kelas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah Kelas" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2">Jumlah Dosen</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control r-2x" name="" placeholder="Inputkan Jumlah Dosen" required>
                        <p class="text-muted no-margin">Cukup ditulis angka</p>
                    </div>
                </div>
                <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> Simpan</button>
            </form>
        </div>
    </div>
</div>
*/ ?>