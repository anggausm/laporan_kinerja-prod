<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link . 'pak/' . enc_data_url($periode['id_periode'])) ?>">Rekap</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link . 'pak_kegiatan/' . enc_data_url($periode['id_periode']) . '/' . enc_data_url($kegiatan['id_unsur'])) ?>"> <?= $kegiatan['nama_unsur'] ?> </a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link . 'pak_kegiatan_detail/' . enc_data_url($periode['id_periode']) . '/' . enc_data_url($kegiatan['id_kegiatan'])) ?>">Detail</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Lihat</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PENILAIAN KINERJA DOSEN PERIODE <?= strtoupper($periode['nama_periode']) ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <div class="row">
                <div class="col-md-10">
                    <div class="font-bold text-u-c">
                        <h4 class="font-bold text-capitalize no-margin m-b">Detail Kegiatan</h4>
                    </div>
                </div>
                <div class="col-md-2">
                <?php if ($kegiatan['id_subunsur'] != '22') : ?>
                    <?php if ($button['status_button'] == 'Enable') : ?>
                        <?php if($kegiatan['jenis_insert'] != 'import') : ?>
                        <form action="<?= base_url($this->link.'pak_kegiatan_form') ?>" class="form-inline text-right" method='post'>
                            <input type="hidden" name="jenis" value="<?= enc_data_url($kegiatan['link']) ?>">
                            <input type="hidden" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                            <input type="hidden" name="semester" value="<?= enc_data_url($periode['id_semester']) ?>">
                            <input type="hidden" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
                            <input type="hidden" name="id" value="<?= enc_data_url($detail['id']) ?>">
                            <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                            <button type="submit" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Edit </button>
                        </form>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
                </div>
            </div>

            
            
    
            <?php            
            $cek_file = file_exists(FCPATH . "application/views/$this->view/lihat/$kegiatan[link].php");
            if ($cek_file):
                include "lihat/$kegiatan[link].php"
            ?>
            <?php else: ?>
                <div class="alert alert-danger text-center" role="alert">
                    <i class="fa fa-exclamation-circle"></i> View tidak ditemukan.
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<?php if (!empty($pesan) && $cek_file === true) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card p r-3x box-shadow-md">
                <h4 class="font-bold text-capitalize no-margin">Catatan</h4>
                <hr>
                <?php if ($button['status'] == 'Revisi') : ?>
                    <div class="col-sm-12">
                        <form method="POST" class="form-validation" action="<?= base_url($this->link . 'pak_pesan_value') ?>">
                            <div class="panel-body">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" name="pesan" placeholder="Type your message"></textarea>
                                    <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                                    <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                                    <input type="hidden" name="kategori" value="<?= enc_data_url('Dosen') ?>">
                                </div>
                                <footer class="text-right">
                                    <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-send"></i> Send </button>
                                    <br><br>
                                </footer>
                            </div>
                        </form>
                    </div>
                <?php endif ?>

                <?php foreach ($pesan as $psn) : ?>
                    <div class="col-sm-12">
                        <div class="list-group md-whiteframe-z0">
                            <div class="list-group-item b-<?= $psn['style'] ?>"> <!-- tambahkan b-l-info  -->
                                <div>
                                    <h4 class="font-bold"> <?= $psn['kategori'] ?> </h4>
                                    <small><?= tanggal_indonesia($psn['tanggal']) ?> <?= $psn['jam'] ?> </small>
                                </div>
                                <hr>
                                <p class="text-justify"> <?= $psn['pesan'] ?> </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
                <br>
            </div>
        </div>
    </div>
<?php endif ?>


<?php 
if (empty($detail['link_berkas']) || $detail['link_berkas'] == '-') {
    $link_file  = '';
} else {
    $link_file  = $detail['link_berkas'];
}

if (!empty($link_file) && $cek_file === true) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card p r-3x box-shadow-md">
                <h4 class="font-bold text-capitalize no-margin m-b">Berkas</h4>
                <hr>
                <?php if ($detail['file_type'] == 'pdf') : ?>
                <embed src="<?= $detail['link_berkas'] ?>" style="width:100%; height:1000px;">
                <?php else : ?>
                <div class="text-center">
                <img src="<?= base_url().'/'.$detail['link_berkas'] ?>" class="img-fluid">
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>