<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td style="width:30%">Komponen</td>
                <td style="width:70%"> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Sub Komponen</td>
                <td style="width:70%"> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Kategori</td>
                <td style="width:70%"> : <?= $detail['kategori'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Mata Kuliah</td>
                <td style="width:70%"> : <?= $detail['nama_makul'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Fakultas</td>
                <td style="width:70%"> : <?= ucwords(strtolower($detail['fakultas'])) ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Program Studi</td>
                <td style="width:70%"> : <?= $detail['program_studi'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Nim</td>
                <td style="width:70%"> : <?= $detail['nim'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Nama Mahasiswa</td>
                <td style="width:70%"> : <?= $detail['nama_mahasiswa'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Judul</td>
                <td style="width:70%"> : <?= $detail['judul'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Penguji Ke</td>
                <td style="width:70%"> : <?= $detail['penguji_ke'] ?> dari <?= $detail['banyak_penguji'] ?> Penguji </td>
            </tr>
            <tr>
                <td style="width:30%">Link Berkas</td>
                <td style="width:70%">
                    <?php if ($button['status_button'] == 'Enable') : ?>
                    <form method="POST" action="<?= base_url($this->link.'pak_url_simpan') ?>">
                        <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                        <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                        <input style="width:70%" type="text" name="url" value="<?= $detail['link_berkas'] ?>"> &nbsp;
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-save"></i> Simpan </button>
                    </form>
                    <?php else : ?>
                    : <a href="<?= $detail['link_berkas'] ?>" target="_blank" class="text-info-lt m-r-xs"> <?= $detail['link_berkas'] ?> </a>
                    <?php endif ?>
                    <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>