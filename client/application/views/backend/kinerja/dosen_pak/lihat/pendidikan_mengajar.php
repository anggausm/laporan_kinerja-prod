<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td style="width:30%">Komponen</td>
                <td style="width:70%"> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Sub Komponen</td>
                <td style="width:70%"> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Fakultas</td>
                <td style="width:70%"> : <?= ucwords(strtolower($detail['fakultas'])) ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Program Studi</td>
                <td style="width:70%"> : <?= $detail['program_studi'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Nama MK</td>
                <td style="width:70%"> : <?= $detail['kode_mk']. ' - ' . $detail['nm_makul'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Rincian</td>
                <td style="width:70%"> : <?= $detail['kelas'] ?>, <?= $detail['hari'] ?>, <?= $detail['jam'] ?>, <?= $detail['ruang'] ?>, <?= $detail['kelompok'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">SKS</td>
                <td style="width:70%"> : <?= $detail['sks'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Banyak Dosen</td>
                <td style="width:70%"> : <?= $detail['banyak_dosen'] ?> Sebagai Dosen Ke <?= $detail['dosen_ke'] ?> </td>
            </tr>
            <tr>
                <td style="width:30%">Banyak Mengajar</td>
                <td style="width:70%"> : <?= $detail['pertemuan_total'] ?> Pertemuan (Offline : <?= $detail['pertemuan_offline'] ?>, Online : <?= $detail['pertemuan_online'] ?>, UTS : <?= $detail['pertemuan_uts'] ?>, UAS : <?= $detail['pertemuan_uas'] ?>) </td>
            </tr>
            <tr>
                <td style="width:30%">Banyak Perserta</td>
                <td style="width:70%"> : <?= $detail['peserta'] ?> Mahasiswa </td>
            </tr>
            <tr>
                <td style="width:30%">Link Berkas</td>
                <td style="width:70%">
                    <?php if ($button['status_button'] == 'Enable') : ?>
                    <form method="POST" action="<?= base_url($this->link.'pak_url_simpan') ?>">
                        <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                        <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                        <input style="width:70%" type="text" name="url" value="<?= $detail['link_berkas'] ?>"> &nbsp;
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-save"></i> Simpan </button>
                    </form>
                    <?php else : ?>
                    : <a href="<?= $detail['link_berkas'] ?>" target="_blank" class="text-info-lt m-r-xs"> <?= $detail['link_berkas'] ?> </a>
                    <?php endif ?>
                    <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>