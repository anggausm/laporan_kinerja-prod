<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td>Komponen</td>
                <td> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td>Sub Komponen</td>
                <td> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Jabatan Akademik</td>
                <td> : <?= $detail['jabatan_akademik'] ?> </td>
            </tr>
            <tr>
                <td>KUM</td>
                <td> : <?= $detail['kum_akademik'] ?> </td>
            </tr>
            <tr>
                <td>No. SK</td>
                <td> : <?= $detail['sk_dikti'] ?> </td>
            </tr>
            <tr>
                <td>Tanggal SK</td>
                <td> : <?= tanggal_indonesia($detail['tgl_dikti']) ?> </td>
            </tr>
            <?php /*
            <tr>
                <td>Link Berkas</td>
                <td>
                    <?php if ($button['status_button'] == 'Enable') : ?>
                    <form method="POST" action="<?= base_url($this->link.'pak_url_simpan') ?>">
                        <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                        <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                        <input style="width:70%" type="text" name="url" value="<?= $detail['link_berkas'] ?>"> &nbsp;
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-save"></i> Simpan </button>
                    </form>
                    <?php else : ?>
                    :   <a href="<?= $detail['link_berkas'] ?>" class="btn btn-xs btn-info" target="_blank"> 
                            Lihat File <div class="md-ripple-container"></div>
                        </a>
                    <?php endif ?>
                    <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
                </td>
            </tr>
            */ ?>
        </tbody>
    </table>
    <p class="font-bold">
        NB : Jika ada data yang tidak sesuai agar dapat dikoordinasikan dengan Bagian Kepegawaian, setelah itu dapat dilakukan sinkron ulang.
    </p>
</div>