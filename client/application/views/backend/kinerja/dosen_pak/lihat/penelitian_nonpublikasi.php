<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td>Komponen</td>
                <td> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td>Sub Komponen</td>
                <td> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Jenis</td>
                <td> : <?= $detail['nama_jenis'] ?> </td>
            </tr>
            <tr>
                <td>Judul</td>
                <td> : <?= $detail['judul'] ?> </td>
            </tr>
            <tr>
                <td>Tanggal Kegiatan</td>
                <td> : <?= tanggal_indonesia($detail['tanggal_terbit']) ?> </td>
            </tr>
            <tr>
                <td>Sebagai</td>
                <td> : <?= $detail['sebagai'] ?> </td>
            </tr>
            <tr>
                <td>Link Berkas</td>
                <td>
                    <?php if ($button['status_button'] == 'Enable') : ?>
                    <form method="POST" action="<?= base_url($this->link.'pak_url_simpan') ?>">
                        <input type="hidden" name="id_values" value="<?= enc_data_url($value['id_values']) ?>">
                        <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                        <input style="width:70%" type="text" name="url" value="<?= $detail['link_berkas'] ?>"> &nbsp;
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-save"></i> Simpan </button>
                    </form>
                    <?php else : ?>
                    :   <a href="<?= $detail['link_berkas'] ?>" class="btn btn-xs btn-info" target="_blank"> 
                            Lihat File <div class="md-ripple-container"></div>
                        </a>
                    <?php endif ?>
                    <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>