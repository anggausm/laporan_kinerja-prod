<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td>Komponen</td>
                <td> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td>Sub Komponen</td>
                <td> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Nama Kegiatan</td>
                <td> : <?= $detail['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td> : <?= $detail['tempat'] ?> </td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td>
                <td> : <?= tanggal_indonesia($detail['tanggal_mulai']) ?> </td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td> : <?= tanggal_indonesia($detail['tanggal_selesai']) ?> </td>
            </tr>
            <tr>
                <td>Jam Mulai</td>
                <td> : <?= $detail['jam_mulai'] ?> </td>
            </tr>
            <tr>
                <td>Jam Selesai</td>
                <td> : <?= $detail['jam_selesai'] ?> </td>
            </tr>
        </tbody>
    </table>
</div>