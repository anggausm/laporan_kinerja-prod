<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td>Komponen</td>
                <td> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td>Sub Komponen</td>
                <td> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Kategori</td>
                <td> : <?= $detail['kategori'] ?> </td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td> : <?= $detail['jabatan'] ?> </td>
            </tr>
            <tr>
                <td>Penempatan</td>
                <td> : <?= $detail['penempatan'] ?> </td>
            </tr>
            <?php /*
            <tr>
                <td>SK Rektor</td>
                <td> : <?= $detail['sk_rektor'] ?> </td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td>
                <td> : <?= tanggal_indonesia($detail['tgl_mulai']) ?> </td>
            </tr>
            <tr>
                <td>Tanggal Selesai</td>
                <td> : <?= tanggal_indonesia($detail['tgl_selesai']) ?> </td>
            </tr>
            */ ?>
        </tbody>
    </table>
</div>