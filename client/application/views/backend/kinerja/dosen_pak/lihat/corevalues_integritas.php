<div class="table-responsive">
    <table class="table table-hover" style="border: 1px solid #e7eaec">
        <tbody>
            <tr>
                <td>Komponen</td>
                <td> : <?= $kegiatan['nama_unsur'] ?> </td>
            </tr>
            <tr>
                <td>Sub Komponen</td>
                <td> : <?= $kegiatan['nomor'] ?>. <?= $kegiatan['nama_kegiatan'] ?> </td>
            </tr>
            <tr>
                <td>Nama Kegiatan</td>
                <td> : <?= $detail['nama'] ?> </td>
            </tr>
            <tr>
                <td>Rincian</td>
                <td> <?= $detail['rincian'] ?> </td>
            </tr>
        </tbody>
    </table>
</div>