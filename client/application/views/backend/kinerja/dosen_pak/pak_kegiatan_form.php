<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak/'.enc_data_url($periode['id_periode'])) ?>">Rekap</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak_kegiatan/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($kegiatan['id_unsur'])) ?>"> <?= $kegiatan['nama_unsur'] ?> </a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak_kegiatan_detail/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($kegiatan['id_kegiatan'])) ?>"> Kegiatan </a></li>
                <li class="breadcrumb-item"><a href="#">Form</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PENILAIAN KINERJA DOSEN PERIODE <?= strtoupper($periode['nama_periode']) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="text-capitalize no-margin">Form kegiatan <span class="font-bold"><?= $kegiatan['nama_unsur'] ?></span></h4>
            <p class="no-margin m-b"> <?= $kegiatan['nomor'].'. '.$kegiatan['nama_kegiatan'] ?> </p>

            <?php if (!empty($dt['id'])) : 
                $link   = base_url($this->link.'pak_kegiatan_edit');
                $button = 'Update';
            else :
                $link   = base_url($this->link.'pak_kegiatan_add');
                $button = 'Simpan';
            endif ?>
            

            <?php            
            $cek_file = file_exists(FCPATH . "application/views/$this->view/form/$kegiatan[link].php");
            if ($cek_file):
                include "form/$kegiatan[link].php"
            ?>
            <?php else: ?>
                <div class="alert alert-danger text-center" role="alert">
                    <i class="fa fa-exclamation-circle"></i> Form tidak ditemukan.
                </div>
            <?php endif ?>



        </div>
    </div>
</div>