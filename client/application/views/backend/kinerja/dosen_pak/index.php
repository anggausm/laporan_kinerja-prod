<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>
<!-- 
<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PERIODE PAK DOSEN
            </div>
        </div>
    </div>
</div> -->

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <!-- <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="alert alert-info" role="alert">
                    INFO : 
                </div>
            </div>
        </div>
    </div> -->

    <?php  
    $no = 1;
    foreach ($periode as $key => $value) : ?>

    <div class="col-md-4 col-xs-12">
        <div class="card r-2x">
            <div class="card-heading p" style="padding-bottom: 0;">
                <div class="font-bold text-u-c">
                    <span class="text-success ">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <?= strtoupper('KINERJA '.$value['nama_periode']) ?>
                </div>
            </div> <hr>

            <div class="card-body p">
                <div class="row m-t-sm">
                    <div class="col-sm-12">
                        <div class="font-bold">Periode Pengisian</div>
                    </div>
                </div>
                <div class="row p-v-xs">
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-c-l text-muted">Mulai</div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-muted text-right"> <?= tanggal_indonesia($value['pengisian_mulai']) ?> </div>
                    </div>
                </div>
                <div class="row p-v-xs">
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-c-l text-muted">Selesai</div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-muted text-right"> <?= tanggal_indonesia($value['pengisian_selesai']) ?> </div>
                    </div>
                </div>
                <div class="row m-t-sm">
                    <div class="col-sm-12">
                        <div class="font-bold">Periode Penilaian</div>
                    </div>
                </div>
                <div class="row p-v-xs">
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-c-l text-muted">Mulai</div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-muted text-right"> <?= tanggal_indonesia($value['penilaian_mulai']) ?> </div>
                    </div>
                </div>
                <div class="row p-v-xs">
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-c-l text-muted">Selesai</div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-muted text-right"> <?= tanggal_indonesia($value['penilaian_selesai']) ?> </div>
                    </div>
                </div>
                <div class="row m-t-sm">
                    <div class="col-sm-12">
                        <div class="font-bold">Semester</div>
                    </div>
                </div>
                <div class="row p-v-xs">
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-c-l text-muted">Mengajar dan Membimbing</div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="text-muted text-right"> <?= $value['nama_semester'] ?> </div>
                    </div>
                </div>
            </div>

            <!-- <form action="<?= base_url($this->link.'pak') ?>" method='post'>
                <input type="hidden" name="id_periode" value="<?= enc_data($value['id_periode']) ?>">
                <input type="hidden" name="versi" value="<?= enc_data($value['versi']) ?>">
                <button type="submit" class="btn btn-block btn-info" style="border-radius: 0 0 3px 3px;"> <i class="fa fa-book"></i> Lihat Laporan Kinerja </button>
            </form> -->
            <a href="<?= base_url($this->link.'pak/'.enc_data_url($value['id_periode'])) ?>" class="btn btn-block btn-info" style="border-radius: 0 0 3px 3px;"> <i class="fa fa-book"></i> Lihat Laporan Kinerja </a>
        </div>
    </div>

    <?php endforeach ?>
</div>