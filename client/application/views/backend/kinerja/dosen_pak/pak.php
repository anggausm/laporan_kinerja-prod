<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <li class="breadcrumb-item"><a href="#">Rekap</a></li>
                <span style="float: right;">
                    <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PENILAIAN KINERJA DOSEN PERIODE <?= strtoupper($periode['nama_periode']) ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row row-flex">
    
    <div class="col-md-6">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">Data Ajuan</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th class="v-m text-center">No.</th>
                            <th class="v-m text-center">Jenis Kinerja</th>
                            <th class="v-m text-center">Syarat</th>
                            <th class="v-m text-center">Usulan Dosen</th>
                            <th class="v-m text-center">Nilai Verifikator</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $no             = 1;
                        $usulan_dosen   = 0;
                        $nilai_asesor   = 0;
                        foreach ($unsur as $un) : ?>
                        <tr>
                            <td class="text-center"> <?= $no++ ?> </td>
                            <td> <?= $un['nama_unsur'] ?> </td>
                            <td> <?= $un['syarat'] ?> </td>
                            <td class="text-center"> <?= $un['usulan_dosen'] ?> </td>
                            <td class="text-center"> <?= $un['nilai_asesor'] ?> </td>
                        </tr>
                        <?php 
                        $usulan_dosen   = $usulan_dosen + $un['usulan_dosen'];
                        $nilai_asesor   = $nilai_asesor + $un['nilai_asesor'];
                        endforeach ?>
                        <tr>
                            <td colspan="3" class="bg-light lt text-black text-right font-bold">Score PKD</td>
                            <td class="bg-light lt text-black text-center font-bold"> <?= $usulan_dosen ?> </td>
                            <td class="bg-light lt text-black text-center font-bold"> <?= $nilai_asesor ?> </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="bg-light lt text-black text-right font-bold">
                                Nilai 
                                <a href="#" class="text-info" data-toggle="modal" data-target="#kriteria-pkd"> (Tabel Kriteria PKD) </a>
                            </td>
                            <?php if ($kriteria_score['hitung_dosen'] == 'Ya') : ?>
                                <!-- GRADE USULAN DOSEN -->
                                <?php if ($usulan_dosen >= 85) : ?>
                                    <td class="p green-900 text-center font-bold">Sangat Baik</td>
                                <?php elseif ($usulan_dosen >= 40) : ?>
                                    <td class="bg-success text-center font-bold">Baik</td>
                                <?php elseif ($usulan_dosen >= 30) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Cukup</td>
                                <?php elseif ($usulan_dosen >= 20) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Kurang</td>
                                <?php elseif ($usulan_dosen >= 0) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Sangat Kurang</td>
                                <?php else : ?>
                                    <td class="bg-danger text-center font-bold">Buruk</td>
                                <?php endif ?>
                            <?php elseif ($kriteria_score['hitung_dosen'] == 'Tidak') : ?>
                                <td class="bg-warning text-center text-black font-bold">Sangat Kurang</td>
                            <?php else : ?>
                                <td class="bg-danger text-center font-bold">Buruk</td>
                            <?php endif ?>

                            <?php if ($kriteria_score['hitung_verifikator'] == 'Ya') : ?>
                                <!-- GRADE PENILAIAN VERIFIKATOR -->
                                <?php if ($nilai_asesor >= 85) : ?>
                                    <td class="p green-900 text-center font-bold">Sangat Baik</td>
                                <?php elseif ($nilai_asesor >= 40) : ?>
                                    <td class="bg-success text-center font-bold">Baik</td>
                                <?php elseif ($nilai_asesor >= 30) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Cukup</td>
                                <?php elseif ($nilai_asesor >= 20) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Kurang</td>
                                <?php elseif ($nilai_asesor >= 0) : ?>
                                    <td class="bg-warning text-center text-black font-bold">Sangat Kurang</td>
                                <?php else : ?>
                                    <td class="bg-danger text-center font-bold">Buruk</td>
                                <?php endif ?>
                            <?php elseif ($kriteria_score['hitung_verifikator'] == 'Tidak') : ?>
                                <td class="bg-warning text-center text-black font-bold">Sangat Kurang</td>
                            <?php else : ?>
                                <td class="bg-danger text-center font-bold">Buruk</td>
                            <?php endif ?>

                            <!-- <td class="bg-success text-center font-bold">Baik</td> -->
                            <!-- <td class="bg-success text-center font-bold">Baik</td> -->
                            <!-- <td class="bg-warning text-black font-bold">Cukup</td>
                            <td class="bg-warning text-black font-bold">Cukup</td> -->
                            <!-- <td class="bg-danger font-bold text-center">Sangat Kurang</td>
                            <td class="bg-danger font-bold text-center">Sangat Kurang</td> -->
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr class="m-b">
            
            <?php if ($button['status_button'] == 'Enable') : ?>
                <a href="#" class="btn btn-info r-2x p-h-md" data-toggle="modal" data-target="#ajukan-pkd"> <i class="fa fa-send-o"></i> Ajukan </a>

                <div class="modal fade" id="ajukan-pkd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="POST" class="form-validation" action="<?= base_url($this->link.'pak_pengajuan_pkd') ?>">
                                <div class="modal-header">
                                    <h4 class="modal-title font-bold" id="exampleModalLongTitle">Pengajuan PKD</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        Perhatian :
                                        <ul>
                                            <li>Pastikan inputan kegiatan sudah sesuai dengan kinerja yang dijalani</li>
                                            <li>Setelah dilakukan klik Ajukan, kegiatan tidak dapat dirubah.</li>
                                        </ul>
                                    </p>
                                    <div class="form-group">
                                        <input type="hidden" name="id_periode" value="<?= enc_data_url($periode['id_periode']) ?>">
                                        <input type="hidden" name="status" value="<?= enc_data_url('Diajukan') ?>">
                                        <textarea class="form-control" rows="5" name="pesan" placeholder="Type your message"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-send-o"></i> Ajukan </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endif ?>

        </div>
    </div>

    <div class="col-md-6">
        <div class="card p r-3x box-shadow-md">
            <div class="row">
                <div class="col-sm-8">
                    <div class="font-bold text-u-c">
                        <h4 class="font-bold text-capitalize no-margin m-b">Data Dosen</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <?php /*
                    <form action="<?= base_url($this->link.'pak_profil_update') ?>" class="form-inline text-right" method='post'>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-refresh"></i> </button>
                    </form>
                    */ ?>
                </div>
            </div>
            
            <!-- <form action="<?= base_url($this->link.'pak_profil') ?>" method='post'> -->
            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
                        <tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%"> <?= $profil['nama_doskar'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">NIDN</td>
                            <td style="width:70%"> <?= $profil['nidn'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">NIS</td>
                            <td style="width:70%"> <?= $profil['nis'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">Fakultas</td>
                            <td style="width:70%"> <?= $profil['fakultas'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">Program Studi</td>
                            <td style="width:70%"> <?= ucwords(strtolower($profil['program_studi'])) ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Dosen</td>
                            <td style="width:70%"> <?= $profil['status_dosen'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan Akademik / KUM</td>
                            <td style="width:70%"> <?= $profil['jabatan_akademik'] ?> / <?= $profil['kum'] ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">TMT Jabatan Akademik</td>
                            <td style="width:70%"> <?= tanggal_indonesia($profil['tmt_jab_akademik']) ?> </td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Keaktifan</td>
                            <td style="width:70%"> <?= $profil['status_keaktifan'] ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr class="m-b">
            <?php /*
            <a href="<?= base_url($this->link) ?>biodata" class="btn btn-stroke btn-info r-2x p-h-md">Detail Data</a>
            
            </form> */ ?>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <?php foreach ($unsur as $uns) : ?>
            <div class="col-sm-2 col-xs-6">
                <a href="<?= base_url($this->link.'pak_kegiatan/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($uns['id_unsur'])) ?>" 
                    class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                    <div class="text-center">
                        <i class="fa <?= $uns['class_fa'] ?> fa-3x text-primary"></i>
                        <p class="no-margin m-t font-bold"> <?= $uns['nama_unsur'] ?> </p>
                    </div>
                </a>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b">hisori ajuan</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Status Ajuan</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $no = 1;
                        foreach ($histori as $his) : ?>
                        <tr>
                            <td> <?= $no++ ?> </td>
                            <td> <?= tanggal_indonesia($his['tgl']) ?> </td>
                            <td><span class="label <?= $his['label'] ?>"> <?= $his['status'] ?> </span></td>
                            <td> <?= $his['keterangan'] ?> </td>
                        </tr>
                        <?php 
                        endforeach ?>

                        <!-- <tr>
                            <td>Ajuan Kinerja</td>
                            <td>13 Juli 2024</td>
                            <td>02 Agustus 2024</td>
                            <td><span class="label orange">Ditangguhkan</span></td>
                            <td><a href="#" class="btn btn-info r-2x btn-sm"><i class="fa fa-info-circle"></i></a></td>
                        </tr>
                        <tr>
                            <td>Ajuan Kinerja</td>
                            <td>14 Juli 2024</td>
                            <td>03 Agustus 2024</td>
                            <td><span class="label teal">Disetuji</span></td>
                            <td><a href="#" class="btn btn-info r-2x btn-sm"><i class="fa fa-info-circle"></i></a></td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="kriteria-pkd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-bold" id="exampleModalLongTitle">Tabel Kriteria PKD</h4>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <div class="alert alert-info" role="alert">
                    <p class="text-justify">
                        Jika terdapat nilai <span class="text-danger m-r-xs"> 0 (nol) </span> pada salah satu jenis kinerja dengan syarat <span class="text-danger m-r-xs"> Tidak Boleh Kosong </span> 
                        maka nilai otomatis <span class="text-danger m-r-xs"> Sangat Kurang </span>
                    </p>
                </div>
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th class="text-center">NO</th>
                            <th class="text-center">NILAI</th>
                            <th class="text-center">SCORE</th>
                            <th class="text-center">KATEGORI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $no = 1;
                        foreach ($kriteria as $kri) : ?>
                        <tr>
                            <td class="text-center"> <?= $no++ ?> </td>
                            <td> <?= $kri['nilai'] ?> </td>
                            <td class="text-center"> <?= $kri['score'] ?> </span></td>
                            <td class="text-center"> <?= $kri['kategori'] ?> </td>
                        </tr>
                        <?php 
                        endforeach ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>