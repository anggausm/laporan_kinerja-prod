<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2">Jenis <span class="text-danger-dk"> * </span> </label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Jurnal</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>      
    
    <div class="form-group">
        <label class="col-sm-2">Nama Kegiatan * </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="nama_kegiatan" value="<?= $dt['nama_kegiatan'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Instansi * </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="instansi" value="<?= $dt['instansi'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tingkat Kegiatan * </label>
        <div class="col-sm-10">
            <select name="tingkat" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Tingkat Pertemuan</option>
                <option value="Lokal" <?= $dt['tingkat'] == 'Lokal' ? 'selected' : ''; ?>> Lokal </option>
                <option value="Daerah" <?= $dt['tingkat'] == 'Daerah' ? 'selected' : ''; ?>> Daerah </option>
                <option value="Nasional" <?= $dt['tingkat'] == 'Nasional' ? 'selected' : ''; ?>> Nasional </option>
                <option value="Lain-lain" <?= $dt['tingkat'] == 'Lain-lain' ? 'selected' : ''; ?>> Lain-lain </option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"> Nomor SK Penugasan * </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="sk_penugasan" value="<?= $dt['sk_penugasan'] ?>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Mulai SK *</label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_mulai"  value="<?= $dt['tanggal_mulai'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Selesai SK *</label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_selesai"  value="<?= $dt['tanggal_selesai'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Link Berkas *</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>