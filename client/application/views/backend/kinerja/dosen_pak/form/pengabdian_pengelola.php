<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2"><span>Jenis <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Jurnal</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>      
    
    <div class="form-group">
        <label class="col-sm-2"><span>Media Publikasi <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="media_publikasi" value="<?= $dt['media_publikasi'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Peran <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="peran" value="<?= $dt['peran'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Nomor SK <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="sk_penugasan" value="<?= $dt['sk_penugasan'] ?>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tanggal Mulai SK <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_mulai"  value="<?= $dt['tanggal_mulai'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tanggal Selesai SK <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_selesai"  value="<?= $dt['tanggal_selesai'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Status <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <span>
            <label class="radio-inline"> <input type="radio" name="status" id="Aktif" value="Aktif" <?= $dt['status'] == 'Aktif' ? 'checked' : ''; ?> > Aktif </label>
            <label class="radio-inline"> <input type="radio" name="status" id="Tidak" value="Tidak" <?= $dt['status'] == 'Tidak' ? 'checked' : ''; ?> > Tidak </label>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Link Berkas <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>