<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
    <input type="hidden" class="form-control r-2x" name="subunsur" value="<?= enc_data_url($kegiatan['id_subunsur']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2"><span>Jenis <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Kegiatan</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>
    
    <div class="form-group">
        <label class="col-sm-2"><span>Nama Kegiatan <span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="nama_kegiatan" value="<?= $dt['nama_kegiatan'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tempat <span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="tempat" value="<?= $dt['tempat'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tanggal Mulai<span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_mulai" value="<?= $dt['tanggal_mulai'] ?>" required> 
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Selesai</label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_selesai" value="<?= $dt['tanggal_selesai'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Jam Mulai<span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="time" class="form-control r-2x" name="jam_mulai" value="<?= $dt['jam_mulai'] ?>" required> 
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Jam Selesai</label>
        <div class="col-sm-10">
            <input type="time" class="form-control r-2x" name="jam_selesai" value="<?= $dt['jam_selesai'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Upload Foto <span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="file" class="form-control r-2x" name="file_foto" required>
            <p class="text-muted no-margin">file dengan type JPG, JPEG, dan ukuran maksimal 2 MB, gunakan WA untuk kompres gambar</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>