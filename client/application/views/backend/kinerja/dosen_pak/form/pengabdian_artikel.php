<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2">Jenis <span class="text-danger-dk"> * </span> </label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Artikel / Buku</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>                
    
    <div class="form-group">
        <label class="col-sm-2">Judul * </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="judul" value="<?= $dt['judul'] ?>" required>
        </div>
    </div>    
    <div class="form-group">
        <label class="col-sm-2">Nama Penerbit/Jurnal * </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="nama_penerbit" value="<?= $dt['nama_penerbit'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">ISBN</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="isbn" value="<?= $dt['isbn'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Volume</label>
        <div class="col-sm-10">
            <input type="number" class="form-control r-2x" name="volume" value="<?= $dt['volume'] ?>">
            <p class="text-muted no-margin">hanya angka, contoh Volume 1 maka cukup diisi 1</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Nomor</label>
        <div class="col-sm-10">
            <input type="number" class="form-control r-2x" name="nomor" value="<?= $dt['nomor'] ?>">
            <p class="text-muted no-margin">hanya angka, contoh Nomor 1 maka cukup diisi 1</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Halaman</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="halaman"  value="<?= $dt['halaman'] ?>" placeholder="1-12">
            <p class="text-muted no-margin">untuk buku dapat diisi dengan jumlah halaman saja, untuk jurnal dapat diisi halaman format awal-akhir contoh 1-12</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Sinta</label>
        <div class="col-sm-10">
            <input type="number" class="form-control r-2x" name="sinta" value="<?= $dt['sinta'] ?>">
            <p class="text-muted no-margin">hanya angka, contoh jurnal sinta 2 maka cukup diisi 2</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Terbit *</label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_terbit"  value="<?= $dt['tanggal_terbit'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Link Artikel/DOI *</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_artikel" value="<?= $dt['link_artikel'] ?>" required>
            <p class="text-muted no-margin">link yang mengarah pada abstrak artikel pada jurnal.</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Link Berkas *</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>