<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
    <input type="hidden" class="form-control r-2x" name="subunsur" value="<?= enc_data_url($kegiatan['id_subunsur']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2"><span> Nama Event <span class="text-danger-dk"> * </span> </label>
        <div class="col-sm-10">
            <select name="id_non_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Event</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>      
    
    <div class="form-group">
        <label class="col-sm-2"><span>Upload Foto <span class="text-danger-dk"> * </span></label>
        <div class="col-sm-10">
            <input type="file" class="form-control r-2x" name="file_foto" required>
            <p class="text-muted no-margin">file dengan type JPG, JPEG, dan ukuran maksimal 2 MB, gunakan WA untuk kompres gambar</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>