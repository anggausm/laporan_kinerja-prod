<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    
    <div class="form-group">
        <label class="col-sm-2"><span>Kategori <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <select name="kategori" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Kategori</option>
                <option value="Sertifikasi Kompetensi" <?= $dt['kategori'] == 'Sertifikasi Kompetensi' ? 'selected' : ''; ?> >Sertifikasi Kompetensi</option>
                <option value="Sertifikasi Profesi" <?= $dt['kategori'] == 'Sertifikasi Profesi' ? 'selected' : ''; ?>>Sertifikasi Profesi</option>
            </select>
        </div>
    </div>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2"><span>Jenis <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>
    
    <div class="form-group">
        <label class="col-sm-2"><span>Bidang Studi <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="bidang_studi" value="<?= $dt['bidang_studi'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Lembaga Sertifikasi <span class="text-danger-dk"> * </span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="lembaga_sertifikasi" value="<?= $dt['lembaga_sertifikasi'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Nomor Registrasi Pendidik</label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="no_registrasi_pendidik" value="<?= $dt['no_registrasi_pendidik'] ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Nomor SK Sertifikasi<span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="no_sk_sertifikasi" value="<?= $dt['no_sk_sertifikasi'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Mulai </label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_mulai"  value="<?= $dt['tanggal_mulai'] ?>" placeholder="dd/mm/yyyy">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal Selesai </label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_selesai"  value="<?= $dt['tanggal_selesai'] ?>" placeholder="dd/mm/yyyy">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Link Berkas <span class="text-danger-dk"> * </span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>