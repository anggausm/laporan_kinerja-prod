<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">

    <!-- digunakan saat form edit  -->
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2">Jenis <span class="text-danger-dk"> * </span> </label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Kegiatan</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>      
    
    <div class="form-group">
        <label class="col-sm-2">Kategori * </label>
        <div class="col-sm-10">
            <select name="kategori" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Kategori</option>
                <option value="Pembicara Pada Pertemuan Ilmiah" <?= $dt['kategori'] == 'Pembicara Pada Pertemuan Ilmiah' ? 'selected' : ''; ?>> Pembicara Pada Pertemuan Ilmiah </option>
                <option value="Pembicara Kunci" <?= $dt['kategori'] == 'Pembicara Kunci' ? 'selected' : ''; ?>> Pembicara Kunci </option>
                <option value="Pembicara / Narasumber pada Pelatihan / Penyuluhan / Ceramah" <?= $dt['kategori'] == 'Pembicara / Narasumber pada Pelatihan / Penyuluhan / Ceramah' ? 'selected' : ''; ?>>
                    Pembicara / Narasumber pada Pelatihan / Penyuluhan / Ceramah
                </option>
            </select>
        </div>
    </div> 
    <div class="form-group">
        <label class="col-sm-2"><span>Judul <span class="text-danger-dk">*</span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="judul" value="<?= $dt['judul'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Nama Pertemuan <span class="text-danger-dk">*</span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="nama_pertemuan" value="<?= $dt['nama_pertemuan'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tingkat Pertemuan <span class="text-danger-dk">*</span></span> </label>
        <div class="col-sm-10">
            <select name="tingkat_pertemuan" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Tingkat Pertemuan</option>
                <option value="Lokal" <?= $dt['tingkat_pertemuan'] == 'Lokal' ? 'selected' : ''; ?>> Lokal </option>
                <option value="Daerah" <?= $dt['tingkat_pertemuan'] == 'Daerah' ? 'selected' : ''; ?>> Daerah </option>
                <option value="Nasional" <?= $dt['tingkat_pertemuan'] == 'Nasional' ? 'selected' : ''; ?>> Nasional </option>
                <option value="Internasional" <?= $dt['tingkat_pertemuan'] == 'Internasional' ? 'selected' : ''; ?>> Internasional </option>
                <option value="Lain-lain" <?= $dt['tingkat_pertemuan'] == 'Lain-lain' ? 'selected' : ''; ?>> Lain-lain </option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"> <span>Penyelenggara <span class="text-danger-dk">*</span></span> </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="penyelenggara" value="<?= $dt['penyelenggara'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tanggal Pelaksanaan <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_pelaksanaan"  value="<?= $dt['tanggal_pelaksanaan'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"> Nomor SK </label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="sk_penugasan" value="<?= $dt['sk_penugasan'] ?>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Tanggal SK</label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_sk"  value="<?= $dt['tanggal_sk'] ?>" placeholder="dd/mm/yyyy">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Link Berkas <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>