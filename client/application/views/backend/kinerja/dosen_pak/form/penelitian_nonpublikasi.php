<form role="form" method="POST" class="form-horizontal" action="<?= $link ?>" enctype="multipart/form-data">
    <input type="hidden" class="form-control r-2x" name="periode" value="<?= enc_data_url($periode['id_periode']) ?>">
    <input type="hidden" class="form-control r-2x" name="kegiatan" value="<?= enc_data_url($kegiatan['id_kegiatan']) ?>">
    <?php if (!empty($dt['id'])) : ?>
    <input type="hidden" class="form-control r-2x" name="id" value="<?= enc_data_url($dt['id']) ?>">
    <input type="hidden" class="form-control r-2x" name="id_values" value="<?= enc_data_url($id_values) ?>">
    <?php endif ?>

    <?php if (!empty($jenis)) : ?>
    <div class="form-group">
        <label class="col-sm-2"><span>Jenis <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <select name="id_jenis" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Jenis Artikel / Buku</option>
                <?php foreach ($jenis as $jns): ?>
                    <option value="<?= $jns['id_jenis'] ?>" <?= $jns['id_jenis'] == $dt['id_jenis'] ? 'selected' : ''; ?>> 
                        <?= $jns['nama_jenis'] ?> 
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <?php endif ?>                
    
    <div class="form-group">
        <label class="col-sm-2"><span>Judul <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="judul" value="<?= $dt['judul'] ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Tanggal Kegiatan <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <input type="date" class="form-control r-2x" name="tanggal_terbit"  value="<?= $dt['tanggal_terbit'] ?>" placeholder="dd/mm/yyyy" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Sebagai <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <select name="sebagai" class="form-control r-2x select2" style="width: 100%;" required>
                <option value="">Pilih Posisi Penulis</option>
                <option value="Ketua" <?= $dt['sebagai'] == 'Ketua' ? 'selected' : ''; ?> >Ketua</option>
                <option value="Anggota" <?= $dt['sebagai'] == 'Anggota' ? 'selected' : ''; ?>>Anggota</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"><span>Link Berkas <span class="text-danger-dk">*</span></span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control r-2x" name="link_berkas" value="<?= $dt['link_berkas'] ?>" required>
            <p class="text-muted no-margin">disarankan menggunakan repository usm pada menu dokumen</p>
        </div>
    </div>

    <button type="submit" class="btn btn-success r-2x" id="btnSubmit"> <?= $button ?> </button>
</form>