<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb r-3x light-blue-900">
                <li class="breadcrumb-item"><a href="<?php print base_url() ?>dashboard/"><i class="fa fa-dashboard "></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Penilaian Kinerja Dosen</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($this->link.'pak/'.enc_data_url($periode['id_periode'])) ?>">Rekap</a></li>
                <li class="breadcrumb-item"><a href="#"> <?= $unsur['nama_unsur'] ?> </a></li>
                <span style="float: right;">
                <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih"></i> &nbsp;Kembali</a>
                </span>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> PENILAIAN KINERJA DOSEN PERIODE <?= strtoupper($periode['nama_periode']) ?>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) { ?>
    <div class="alert alert-warning" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-12">
        <div class="card p r-3x box-shadow-md">
            <h4 class="font-bold text-capitalize no-margin m-b"> <?= $unsur['nama_unsur'] ?> </h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <thead class="light-blue-100">
                        <tr>
                            <th style="width:5%" class="v-m">No.</th>
                            <th style="width:40%" class="v-m">Sub Komponen</th>
                            <th style="width:10%" class="v-m text-center">Aksi</th>
                            <th style="width:15%" class="v-m text-center">Satuan</th>
                            <th style="width:15%" class="v-m text-center">Usulan Dosen</th>
                            <th style="width:15%" class="v-m text-center">Nilai Verifikator</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $usulan_dosen  = 0;
                        $nilai_asesor = 0;

                        foreach ($kegiatan as $keg) : ?>
                        <tr <?php if ($keg['value'] == 0) : ?> class="grey-200" <?php endif ?> >
                            <td> <?= $keg['nomor'] ?> </td>
                            <td> <?= $keg['nama_kegiatan'] ?> </td>
                            <?php if ($keg['value'] == 1) : ?> 
                                <td class="text-center"> 
                                    <a href="<?= base_url($this->link.'pak_kegiatan_detail/'.enc_data_url($periode['id_periode']).'/'.enc_data_url($keg['id_kegiatan'])) ?>" 
                                    class="btn btn-info r-2x btn-sm">Detail
                                    </a> 
                                </td>
                                <td class="text-center"> <?= $keg['satuan'] ?> </td>
                                <td class="text-center"> <?= $keg['j_skor_diajukan'] ?> </td>
                                <td class="text-center"> <?= $keg['j_skor_verifikator'] ?> </td>
                            <?php else : ?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            <?php endif ?>
                        </tr>
                        <?php 
                        $usulan_dosen   = $usulan_dosen + $keg['j_skor_diajukan'];
                        $nilai_asesor   = $nilai_asesor + $keg['j_skor_verifikator'];
                        endforeach ?>
                        <tr>
                            <td colspan="4" class="bg-light lt text-black text-right font-bold"> Jumlah </td>
                            <td class="bg-light lt text-black text-center font-bold"> <?= $usulan_dosen ?> </td>
                            <td class="bg-light lt text-black text-center font-bold"> <?= $nilai_asesor ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>