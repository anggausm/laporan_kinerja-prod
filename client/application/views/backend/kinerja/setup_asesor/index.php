<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url($this->link) ?>">Setup Asesor </a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="card r-2x p">
        <form id="formSearch" role="form" method="POST" action="#">
            <div class="row">
                <div class="col-md-2">
                    <select name="bulan_tahun" class="form-control" required>
                        <option value="">Pilih Bulan Tahun</option>
                        <?php foreach ($periode as $key => $value): ?>
                            <option value="<?php echo $value['bulan_tahun']; ?>" <?php echo $key == 0 ? 'selected' : ''; ?>><?php echo $value['nama_periode']; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-sm btn-success" id="btnSearch">Tampilkan</button>
                </div>
            </div>
        </form>
    </div>
    <div id="transAlert"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-white">
                    <i class="fa fa-folder-open"></i> Setup Asesor
                    <small class="text-muted"> </small>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-groups">
                                <div class="table-responsive">
                                    <table id="ajaxTable" class="table dataTableDraw table-striped b-t b-b table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center" width="2%">No</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-center">Asesor Rekan Kerja</th>
                                                <th class="text-center">Asesor Pimpinan</th>
                                                <th class="text-center">Asesor Universitas</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTransaction">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Asesor untuk : <span class="modal_nama_doskar"></span></h4>
            </div>
            <form id="formTransaction" method="POST">
                <div class="modal-body">
                    <div id="modalAlert"></div>
                    <div class="text-center hideCreate">
                        <button id="prevModalTransaction" type="button" class="btn btn-default btn-prev">Prev</button>
                        <button id="nextModalTransaction" type="button" class="btn btn-default btn-next">Next</button>
                    </div>
                    <table id="ajaxTableTransaksi" class="table dataTableDraw table-striped b-t b-b table-bordered">
                        <thead>
                            <tr>
                                <th width="10%" class="text-center">No</th>
                                <th width="40%" class="text-center">Nama</th>
                                <th width="40%" class="text-center">Unit</th>
                                <th width="10%" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input type="hidden" name="dinilai_id_doskar">
                    <input type="hidden" name="dinilai_user_key">
                    <input type="hidden" name="dinilai_nama">
                    <input type="hidden" name="dinilai_fak_unit">
                    <input type="hidden" name="dinilai_nama_unit">
                    <input type="hidden" name="dinilai_jenis_pekerjaan">
                    <input type="hidden" name="dinilai_status_kerja">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    var current_link = '<?php echo base_url($this->link) ?>';

    $(document).ready(function() {
        var dataTableDoskar = $("#ajaxTable").DataTable({
            ajax: {
                "url": `${current_link}json`,
                "type": "POST",
                "data": function(data) {
                    data.bulan_tahun = $('#formSearch select[name="bulan_tahun"]').val();
                }
            },
            columns: [{
                "className": "text-center",
                "data": "no",
            }, {
                "data": "nama_doskar",
            }, {
                "data": "nama_unit",
                "render": function(data, type, row) {
                    return `${data}<br><small>${row.jenis_pekerjaan}</small>`;
                }
            }, {
                "data": "search_asesor_teman",
                "render": function(data, type, row) {
                    var html = '';
                    data.forEach(item => {
                        html += `<button type="button" class="btn btn-xs btn-danger btn_delete" data-id="${item.id_asesor}"><i class="fa fa-trash"></i></button>${item.penilai_nama} <br>`
                    })
                    return html;
                }
            }, {
                "data": "search_asesor_pimpinan",
                "render": function(data, type, row) {
                    var html = '';
                    data.forEach(item => {
                        html += `<button type="button" class="btn btn-xs btn-danger btn_delete" data-id="${item.id_asesor}"><i class="fa fa-trash"></i></button>${item.penilai_nama} <br>`
                    })
                    return html;
                }
            }, {
                "data": "search_asesor_universitas",
                "render": function(data, type, row) {
                    var html = '';
                    data.forEach(item => {
                        html += `<button type="button" class="btn btn-xs btn-danger btn_delete" data-id="${item.id_asesor}"><i class="fa fa-trash"></i></button>${item.penilai_nama} <br>`
                    })
                    return html;
                }
            }, {
                "className": "text-center",
                "data": null,
                "render": function(data, type, row) {
                    var html = ``;
                    if (data.created_by != '') {
                        html += `<button class="btn btn-xs btn-primary btn_create"><i class="fa fa-users"></i></button>`;
                    }
                    return html;
                }
            }, ],
            order: [],
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "scrollX": true,
            processing: true,
        });
        $('#btnSearch').click(function(e) {
            e.preventDefault();
            dataTableDoskar.ajax.reload(null, false);
        });
        // var dataTableDoskarCurrentPage = 0; // Initialize with 0 initially

        // $('#ajaxTable').on('page.dt', function() {
        //     dataTableDoskarCurrentPage = dataTableDoskar.page();
        // });
        dataTableDoskar.on('click', '.btn_create', function(e) {
            var idRow = dataTableDoskar.row(e.target.closest('tr')).index();
            if (idRow == undefined) {
                idRow = dataTableDoskar.row(this).index();
            }
            if (idRow == undefined) {
                alert('Data not properly passed')
                return false;
            }
            fillingModalTransaction(dataTableDoskar, idRow);
            $('#modalTransaction').modal('show');
        });
        dataTableDoskar.on('click', '.btn_delete', function(e) {
            var id = $(this).data('id');
            var confirmation = confirm('Are you sure you want to delete this item?');

            if (confirmation) {
                $.ajax({
                        url: `${current_link}ajax_delete/${id}`,
                        type: 'GET',
                        dataType: 'JSON',
                    })
                    .done(function(response) {
                        $('#transAlert').html('');
                        setTimeout(() => {
                            $('#transAlert').html(`<div class="alert ${response.type} alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  ${response.message}
				</div>`);
                        }, 500);
                        dataTableDoskar.ajax.reload(null, false);
                    })
                    .fail(function() {
                        console.log("error");
                        dataTableDoskar.ajax.reload(null, false);
                    })
            }
        });

        var dataTableTransaksi = $("#ajaxTableTransaksi").DataTable({
            ajax: {
                "url": `${current_link}json`,
                "type": "POST",
                "data": function(data) {
                    data.dinilai_id_doskar = $('#formTransaction input[name="dinilai_id_doskar"]').val();
                    data.table = 'modal_tambah';
                }
            },
            columns: [{
                "className": "text-center",
                "data": "no",
            }, {
                "data": "nama_doskar",
            }, {
                "data": "nama_unit",
            }, {
                "className": "text-center",
                "data": null,
                "render": function(data, type, row) {
                    var html = ``;
                    if (data.allow_tambah == '1') {
                        html += `<button class="btn btn-xs btn-primary btn_tambah" data-kategori="teman" type="button"><i class="fa fa-plus"></i> Tambah Asesor</button>`;
                    } else {
                        html += `<button class="btn btn-xs btn-primary" disabled type="button"><i class="fa fa-plus"></i> Tambah Asesor</button>`;
                    }
                    if (data.allow_tambah == '1') {
                        html += `<button class="btn btn-xs btn-primary btn_tambah" data-kategori="pimpinan" type="button"><i class="fa fa-plus"></i> Asesor Pimpinan</button>`;
                    } else {
                        html += ` <button class="btn btn-xs btn-primary" disabled type="button"><i class="fa fa-plus"></i> Asesor Pimpinan</button>`;
                    }
                    html += `<br>`;
                    if (data.allow_tambah == '1') {
                        html += ` <button class="btn btn-xs btn-primary btn_tambah" data-kategori="universitas" type="button"><i class="fa fa-plus"></i> Asesor Universitas</button>`;
                    } else {
                        html += ` <button class="btn btn-xs btn-primary" disabled type="button"><i class="fa fa-plus"></i> Asesor Universitas</button>`;
                    }
                    return html;
                }
            }, ],
            order: [],
            "responsive": true,
            "lengthChange": true,
            "autoWidth": true,
        });
        dataTableTransaksi.on('click', '.btn_tambah', function(e) {
            var idRow = dataTableTransaksi.row(e.target.closest('tr')).index();
            if (idRow == undefined) {
                idRow = dataTableTransaksi.row(this).index();
            }
            if (idRow == undefined) {
                alert('Data not properly passed')
                return false;
            }
            var formData = $('#formTransaction').serializeArray();
            var dataPenilai = dataTableTransaksi.row(idRow).data();
            formData.push({
                'name': 'penilai_id_doskar',
                'value': dataPenilai.id_doskar,
            });
            formData.push({
                'name': 'penilai_user_key',
                'value': dataPenilai.user_key,
            });
            formData.push({
                'name': 'penilai_nama',
                'value': dataPenilai.nama_doskar,
            });
            formData.push({
                'name': 'penilai_fak_unit',
                'value': dataPenilai.fak_unit,
            });
            formData.push({
                'name': 'penilai_nama_unit',
                'value': dataPenilai.nama_unit,
            });
            formData.push({
                'name': 'penilai_jenis_pekerjaan',
                'value': dataPenilai.jenis_pekerjaan,
            });
            formData.push({
                'name': 'penilai_status_kerja',
                'value': dataPenilai.status_kerja,
            });
            let kategori = $(this).data('kategori');
            formData.push({
                'name': 'kategori',
                'value': kategori,
            });
            let bulan_tahun = $('#formSearch select[name="bulan_tahun"]').val();
            formData.push({
                'name': 'bulan_tahun',
                'value': bulan_tahun,
            });
            $('#formTransaction').trigger("reset");
            $.ajax({
                    url: `${current_link}ajax_update`,
                    type: 'POST',
                    dataType: 'JSON',
                    data: formData
                })
                .done(function(response) {
                    $('#modalAlert').html('');
                    setTimeout(() => {
                        $('#modalAlert').html(`<div class="alert ${response.type} alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  ${response.message}
				</div>`);
                    }, 500);
                    dataTableDoskar.ajax.reload(null, false);
                    dataTableTransaksi.ajax.reload(null, false);
                    // $('#modalTransaction').modal('hide');
                })
                .fail(function() {
                    console.log("error");
                    dataTableDoskar.ajax.reload(null, false);
                })
        });

        /*TOMBOL NAVIGASI*/
        $('#prevModalTransaction').click(function(event) {
            var idRow = $(this).data('id');
            fillingModalTransaction(dataTableDoskar, idRow);
        });
        $('#nextModalTransaction').click(function(event) {
            var idRow = $(this).data('id');
            fillingModalTransaction(dataTableDoskar, idRow);
        });
        /*END*/
        function fillingModalTransaction(var_table, idRow) {
            var data = var_table.row(idRow).data();
            var prevRow = var_table.row(idRow).index() - 1;
            var nextRow = var_table.row(idRow).index() + 1;
            var lastRow = var_table.row(':last').index();
            if (prevRow < 0) {
                $('#prevModalTransaction').attr('disabled', 'true');
                $('#prevModalTransaction').removeData('id');
            } else {
                $('#prevModalTransaction').removeAttr('disabled');
                $('#prevModalTransaction').data('id', prevRow);
            }
            if (nextRow > lastRow) {
                $('#nextModalTransaction').attr('disabled', 'true');
                $('#nextModalTransaction').removeData('id');
            } else {
                $('#nextModalTransaction').removeAttr('disabled');
                $('#nextModalTransaction').data('id', nextRow);
            }
            $('#formTransaction input[name="dinilai_id_doskar"]').val(data.id_doskar);
            $('#formTransaction input[name="dinilai_user_key"]').val(data.user_key);
            $('#formTransaction input[name="dinilai_nama"]').val(data.nama_doskar);
            $('#formTransaction input[name="dinilai_fak_unit"]').val(data.fak_unit);
            $('#formTransaction input[name="dinilai_nama_unit"]').val(data.nama_unit);
            $('#formTransaction input[name="dinilai_jenis_pekerjaan"]').val(data.jenis_pekerjaan);
            $('#formTransaction input[name="dinilai_status_kerja"]').val(data.status_kerja);

            $('#modalTransaction .modal_nama_doskar').text(data.nama_doskar);
            dataTableTransaksi.ajax.reload(null, false);
        }
    });
</script>