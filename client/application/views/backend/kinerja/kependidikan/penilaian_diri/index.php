<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kpg/anggota/dosen">Penilaian Diri</a></li>
				</ol>
			</nav>
		</div>
	</div>

	<?php if (!empty($this->session->flashdata('pesan'))) : ?>
		<div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
			<?php print $this->session->flashdata('pesan') ?>
		</div>
	<?php endif ?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Penilaian Diri</b>
				</div>
				<div class="panel-body">
					<div class="alert alert-info font-bold" role="alert">
						Periode Pengisian : <?= tanggal_indonesia($periode['pengisian_mulai']) ?> sampai <?= tanggal_indonesia($periode['pengisian_selesai']) ?>
					</div>

					<a href="https://drive.google.com/file/d/1APJnx0k_t78Y8cQNpUXcEqkFRaj-lU_H/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian</a>
					<a href="https://drive.google.com/file/d/1RBopoMwo27ll_KjOE0e4ZOvGR_bjI04J/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Edaran dan Peraturan Rektor Penilaian Kinerja </a>
					<form class="form-horizontal p-h-xs">
						<div class="row">
							<div class="col-md-12">
								<h4 class="text-center font-bold">FORM PENILAIAN KINERJA DARI DIRI SENDIRI</h4>
								<h4 class="text-center font-bold">TENAGA KEPENDIDIKAN / TENAGA PENUNJANG</h4>
								<h5 class="text-center font-bold">PERIODE 1 DESEMBER 2023 s.d 30 MEI 2024</h5> <br>
							</div>
							<div class="col-md-6">
								<div class="">
									<label class="col-sm-2 control-label text-right">NIS </label>
									<label class="col-sm-10 control-label text-right">: <?= $profil['nis']; ?></label>
								</div>
								<div class="">
									<label class="col-sm-2 control-label text-right">Nama </label>
									<label class="col-sm-10 control-label text-right">: <?= $profil['nama_doskar']; ?> </label>
								</div>
								<div class="">
									<label class="col-sm-2 control-label text-right">Unit </label>
									<label class="col-sm-10 control-label text-right">: <?= $profil['nama_unit1']; ?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="">
									<label class="col-sm-2 control-label text-right">Pekerjaan</label>
									<label class="col-sm-10 control-label text-right">: <?= $profil['nama_jabatan']; ?> </label>
								</div>
								<div class="">
									<label class="col-sm-2 control-label text-right">Status</label>
									<label class="col-sm-10 control-label text-right">: <?= $profil['status_kerja']; ?> </label>
								</div>
								<div class="">
									<label class="col-sm-2 control-label text-right">TMT USM</label>
									<label class="col-sm-10 control-label text-right">: <?= tanggal_indonesia($profil['tmt_usm']); ?> </label>
								</div>
							</div>
						</div>
					</form><br>

					<?php if (date('Y-m-d') >= $periode['pengisian_mulai'] && date('Y-m-d') <= $periode['pengisian_selesai']) { ?>

						<div class="table-responsive">
							<table class="table table-striped b-t b-b table-bordered">
								<thead>
									<tr class="text-center">
										<th class="text-center" rowspan="2">NO</th>
										<th class="text-center" rowspan="2">PERNYATAAN</th>
										<th class="text-center"> SANGAT TIDAK SETUJU </th>
										<th class="text-center"> TIDAK SETUJU </th>
										<th class="text-center"> SETUJU </th>
										<th class="text-center"> SANGAT SETUJU </th>
									</tr>
									<tr>
										<th class="text-center">1</th>
										<th class="text-center">2</th>
										<th class="text-center">3</th>
										<th class="text-center">4</th>
									</tr>
								</thead>
								<tbody>
									<form class="form-horizontal p-h-xs" action="<?= base_url($this->link . 'simpan') ?>" method="POST">
										<?php
										$no = 1;
										foreach ($tanya as $value) {
										?>
											<tr>
												<td class="text-center"><?= $no ?></td>
												<td>
													<?= $value['pertanyaan']; ?>
													<input type="hidden" name="id_pertanyaan[]" value="<?= $value['id_pertanyaan'] ?>">
												</td>
												<td class="text-center">
													<input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="1" <?= $value['nilai'] == 1 ? 'checked' : ''; ?> required <?= $button == 1 ? 'disabled' : ''; ?>>
												</td>
												<td class="text-center">
													<input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="2" <?= $value['nilai'] == 2 ? 'checked' : ''; ?> required <?= $button == 1 ? 'disabled' : ''; ?>>
												</td>
												<td class="text-center">
													<input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="3" <?= $value['nilai'] == 3 ? 'checked' : ''; ?> required <?= $button == 1 ? 'disabled' : ''; ?>>
												</td>
												<td class="text-center">
													<input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="4" <?= $value['nilai'] == 4 ? 'checked' : ''; ?> required <?= $button == 1 ? 'disabled' : ''; ?>>
												</td>
											</tr>
										<?php
											$no++;
										}
										?>
										<tr>
											<td class="text-center" colspan="6">
												<?php
												if ($button == 0) {
												?>
													<div class="alert alert-info font-bold" role="alert">
														PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. <br>
														SETELAH MELAKUKAN SIMPAN ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA.
													</div>
													<button type="submit" class="btn btn-sm btn-info" onclick="return confirm('PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. \n \nSETELAH MELAKUKAN SIMPAN ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA. \n \nApakah Anda yakin akan menyimpan penilaian kinerja ini ???');"> <i class="fa fa-save"></i> Simpan</button>
												<?php
												} else {
												?>
													<div class="alert alert-info font-bold" role="alert">
														Anda sudah melakukan penilaian kinerja
													</div>
												<?php
												}
												?>
											</td>
										</tr>
									</form>
								</tbody>
							</table>
						</div>
					<?php
					} else {
					?>
						<div class="alert alert-danger font-bold" role="alert">
							<h1 class="text-center">
								MASA PENGISIAN PENILAIAN KINERJA DIBUKA <br><br>
								TANGGAL <?= strtoupper(tanggal_indonesia($periode['pengisian_mulai'])) ?>
								SAMPAI <?= strtoupper(tanggal_indonesia($periode['pengisian_selesai'])) ?>
							</h1>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>