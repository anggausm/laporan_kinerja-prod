<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url() ?>">Penilaian Univeristas</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <?php if (!empty($this->session->flashdata('pesan'))) : ?>
        <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
            <?php print $this->session->flashdata('pesan') ?>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-white">
                    <i class="fa fa-list"></i> <b>Penilaian Univeristas</b>
                </div>
                <div class="panel-body">
                    <a href="https://drive.google.com/file/d/1APJnx0k_t78Y8cQNpUXcEqkFRaj-lU_H/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian</a>
                    <a href="https://drive.google.com/file/d/1RBopoMwo27ll_KjOE0e4ZOvGR_bjI04J/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Edaran dan Peraturan Rektor Penilaian Kinerja </a>
                    <div class="table-responsive">
                        <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th class="text-center">NO</th>
                                    <th class="text-center">NAMA</th>
                                    <th class="text-center">UNIT</th>
                                    <th class="text-center">KATEGORI</th>
                                    <th class="text-center">STATUS PENILAIAN</th>
                                    <th class="text-center">AKSI</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($data as $key => $value) : ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td> <?= $value['dinilai_nama']; ?> </td>
                                        <td> <?= $value['dinilai_nama_unit']; ?> </td>
                                        <td> <?= $value['dinilai_jenis_pekerjaan']; ?> </td>
                                        <td>
                                            <?php echo $value['status_nilai']; ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?php echo base_url($this->link . 'detail/' . enc_data_url($value['dinilai_user_key']) . '/' . enc_data_url($value['dinilai_jenis_pekerjaan'])); ?>" type="submit" class="btn btn-xs <?php echo $value['disabled'] == '0' ? 'btn-danger' : 'btn-info'; ?>"><?php echo $value['disabled'] == '1' ? 'Lihat' : '<i class="fa fa-edit"></i> Nilai' ?> </a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>