<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kpg/anggota/dosen">Penilaian Rekan</a></li>
				</ol>
			</nav>
		</div>
	</div>

	<?php if (!empty($this->session->flashdata('pesan'))) : ?>
		<div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
			<?php print $this->session->flashdata('pesan') ?>
		</div>
	<?php endif ?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Penilaian Rekan Kerja</b>
				</div>
				<div class="panel-body">
					<div class="alert alert-info font-bold" role="alert">
						Periode Pengisian : <?= tanggal_indonesia($periode['pengisian_mulai']) ?> sampai <?= tanggal_indonesia($periode['pengisian_selesai']) ?>
					</div>
					<a href="https://drive.google.com/file/d/1APJnx0k_t78Y8cQNpUXcEqkFRaj-lU_H/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian</a>
					<a href="https://drive.google.com/file/d/1RBopoMwo27ll_KjOE0e4ZOvGR_bjI04J/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Edaran dan Peraturan Rektor Penilaian Kinerja </a>

					<?php if (date('Y-m-d') >= $periode['pengisian_mulai'] && date('Y-m-d') <= $periode['pengisian_selesai']) { ?>

						<div class="table-responsive">
							<table class="table table-striped b-t b-b table-bordered">
								<thead>
									<tr class="text-center">
										<th class="text-center">NO</th>
										<th class="text-center">NAMA</th>
										<th class="text-center">UNIT</th>
										<th class="text-center">STATUS PENILAIAN</th>
										<th class="text-center">AKSI</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($list as $value) {
									?>
										<tr>
											<td class="text-center"><?= $no ?></td>
											<td> <?= $value['dinilai_nama']; ?> </td>
											<td> <?= $value['dinilai_fak_unit']; ?> </td>
											<td class="text-center"> <?= $value['status']; ?> </td>
											<td class="text-center">
												<?php
												if ($value['status'] == 'Sudah Dinilai') {
												?>
													<form method="POST" action="<?= base_url($this->link . 'detail') ?>">
														<input type="hidden" name="dinilai" value="<?= $value['dinilai_user_key']; ?>">
														<button type="submit" class="btn btn-sm btn-info"> Lihat </button>
													</form>
												<?php
												} else {
												?>
													<form method="POST" action="<?= base_url($this->link . 'detail') ?>">
														<input type="hidden" name="dinilai" value="<?= $value['dinilai_user_key']; ?>">
														<button type="submit" class="btn btn-sm btn-danger"> Nilai </button>
													</form>
												<?php
												}
												?>

											</td>
										</tr>
									<?php
										$no++;
									}
									?>
								</tbody>
							</table>
						</div>

					<?php
					} else {
					?>
						<br><br><br>
						<div class="alert alert-danger font-bold" role="alert">
							<h1 class="text-center">
								MASA PENGISIAN PENILAIAN KINERJA DIBUKA <br><br>
								TANGGAL <?= strtoupper(tanggal_indonesia($periode['pengisian_mulai'])) ?>
								SAMPAI <?= strtoupper(tanggal_indonesia($periode['pengisian_selesai'])) ?>
							</h1>
						</div>
					<?php } ?>

				</div>
			</div>
		</div>
	</div>