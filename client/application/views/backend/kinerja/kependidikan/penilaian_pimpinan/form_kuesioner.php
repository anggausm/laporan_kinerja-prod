<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                     <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url() ?>">Penilaian Pimpinan</a></li>
                    <span  style="float: right;">
                        <a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
                    </span>
                </ol>
            </nav>
        </div>
    </div>
    <?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
        <?php elseif ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger text-center" role="alert">
                <?php print $this->session->flashdata('error')?> 
            </div>
    <?php endif ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-white">
                    <i class="fa  fa-list"></i>  <b>Kuesioner Penilaian Kinerja Pegawai</b>
                
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                            <table class="table table-striped b-t b-b table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th class="text-center">NO</th>
                                        <th class="text-center">NAMA</th>
                                        <!-- <th class="text-center">KATEGORI</th> -->
                                        <th class="text-center">STATUS PENILAIAN</th>
                                        <th class="text-center">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($list as $value) {
                                    ?>
                                        <tr>
                                            <td class="text-center"><?= $no ?></td>
                                            <td> <?= $value['dinilai_nama']; ?> </td>
                                            <!-- <td> <?= $value['kategori']; ?> </td> -->
                                            <td class="text-center"> <?= $value['status']; ?> </td>
                                            <td class="text-center">
                                                <?php
                                                if ($value['status'] == 'Sudah Dinilai') {
                                                ?>
                                                    <form method="POST" action="<?= base_url($this->link . 'detail_kuesioner') ?>">
                                                        <input type="hidden" name="dinilai" value="<?= enc_data_url($value['dinilai_user_key']); ?>">
                                                        <button type="submit" class="btn btn-sm btn-info"> Lihat </button>
                                                    </form>
                                                <?php
                                                } else {
                                                ?>
                                                    <form method="POST" action="<?= base_url($this->link . 'detail_kuesioner') ?>">
                                                        <input type="hidden" name="dinilai" value="<?= enc_data_url($value['dinilai_user_key']); ?>">
                                                        <button type="submit" class="btn btn-sm btn-danger"> Nilai </button>
                                                    </form>
                                                <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                    <?php
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <br>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>