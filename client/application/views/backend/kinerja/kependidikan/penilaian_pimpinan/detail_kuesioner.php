<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					 <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url() ?>">Penilaian Pimpinan</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Kinerja Pegawai
				
				</div>
				<div class="panel-body">
					<?php 
					
						$p_mulai = $periode['pengisian_mulai'];
						$p_akhir = $periode['pengisian_selesai'];
					
					?>
					<div class="alert alert-info font-bold" role="alert">
						Periode Pengisian : <?= date('d M Y', strtotime($p_mulai)) ?> sampai <?= date('d M Y', strtotime($p_akhir)) ?>
					</div>
					<form class="form-horizontal p-h-xs">
						<div class="row">
                            <div class="col-md-12">
                                <h4 class="text-center font-bold">FORM PENILAIAN KINERJA</h4>
                                <!-- <h4 class="text-center font-bold">TENAGA KEPENDIDIKAN / TENAGA PENUNJANG</h4> -->
                                <h4 class="text-center font-bold"><?php 
                                	foreach($tanya as $values){
			                                $jenis_kep = $values['jenis'];
			                                } 
			                                echo $jenis_kep; ?></h4>
                                 <br>
                            </div> 
							<div class="col-md-6">
								<div>
									<label class="col-sm-2 control-label text-right">NIS </label>
									<label class="col-sm-10 control-label text-right">: <?=$profil['nis'];?></label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">Nama </label>
									<label class="col-sm-10 control-label text-right">: <?=$profil['nama_doskar'];?> </label>
								</div>
                                <div>
									<label class="col-sm-2 control-label text-right">Unit </label>
									<label class="col-sm-10 control-label text-right">: <?=$profil['nama_unit'];?>  </label>
								</div>
							</div>
							<div class="col-md-6">
                                <div>
									<label class="col-sm-2 control-label text-right">Pekerjaan</label>
									<label class="col-sm-10 control-label text-right">: <?=$profil['pekerjaan'];?>  </label>
								</div>
								<div>
									<label class="col-sm-2 control-label text-right">TMT USM</label>
									<label class="col-sm-10 control-label text-right">: <?= date('d M Y', strtotime($profil['tmt_usm']));?>  </label>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table  class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr class="text-center">
									<th class="text-center" rowspan="2">NO</th>
									<th class="text-center" rowspan="2">PERNYATAAN</th>
									<th class="text-center"> SANGAT TIDAK PUAS </th>
									<th class="text-center"> TIDAK PUAS </th>
									<th class="text-center"> CUKUP </th>
									<th class="text-center"> PUAS </th>
									<th class="text-center"> SANGAT PUAS </th>
								</tr>
								<tr>
									<th class="text-center">1</th>
									<th class="text-center">2</th>
									<th class="text-center">3</th>
									<th class="text-center">4</th>
									<th class="text-center">5</th>
								</tr>
							</thead>
							<tbody>
				                <form  class="form-horizontal p-h-xs" action="<?= base_url($this->link.'simpan_kuesioner') ?>" method ="POST">
                                <input type="hidden" name="dinilai" value="<?= $profil['user_key'] ?>">
								<?php

                                $no = 1;
                                foreach ($tanya as $value) {
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=$no?></td>
                                        <td>
                                            <?=$value['pertanyaan'];?>
                                            <input type="hidden" name="id_pertanyaan[]" value="<?= $value['id_pertanyaan'] ?>">
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="1" <?= $value['nilai'] == 1 ? 'checked' : ''; ?> required <?= $button == 'close' ? 'disabled' : ''; ?> >
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="2" <?= $value['nilai'] == 2 ? 'checked' : ''; ?> required <?= $button == 'close' ? 'disabled' : ''; ?> >
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="3" <?= $value['nilai'] == 3 ? 'checked' : ''; ?> required <?= $button == 'close' ? 'disabled' : ''; ?> >
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="4" <?= $value['nilai'] == 4 ? 'checked' : ''; ?> required <?= $button == 'close' ? 'disabled' : ''; ?> >
                                        </td>
                                        <td class="text-center">
                                            <input type="radio" name="nilai[<?= $value['id_pertanyaan'] ?>]" value="5" <?= $value['nilai'] == 5 ? 'checked' : ''; ?> required <?= $button == 'close' ? 'disabled' : ''; ?> >
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                                <tr>
                                    <td class="text-center" colspan="7">
                                        <?php 
                                        if ($button == 'open') {
                                            ?>
                                            <div class="alert alert-info font-bold" role="alert">
                                                PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. <br>
                                                SETELAH MELAKUKAN SIMPAN ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA.
                                            </div>
                                            <button type="submit" class="btn btn-sm btn-info" onclick="return confirm('PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. \n \nSETELAH MELAKUKAN SIMPAN ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA. \n \nApakah Anda yakin akan menyimpan penilaian kinerja ini ???');"> <i class="fa fa-save"></i> Simpan</button>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="alert alert-info font-bold" role="alert">
                                            Anda sudah melakukan penilaian kinerja
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                </form>
							</tbody>
						</table>
					</div>

 					<br>
 				</div>
				</div>
			</div>
		</div>
	</div>
</div>