<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/setup/mst_pegawai">Master Pegawai</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/setup/event"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Master Pegawai</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:10%" class="text-center">Nama Pegawai</th>
												<th style="width:10%" class="text-center">Pekerjaan</th>
												<th style="width:25%" class="text-center">Jenis Pegawai</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($pegawai as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo $value['nama_doskar'];?></td>
												<td><?php echo $value['pekerjaan'];?></td>
												<td style="text-align: left;"><?php echo $value['nama_jenis_pegawai'];?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/setup/mst_pegawai/edit/<?php echo enc_data($value['user_key']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<!-- <a href="<?php echo base_url() ?>kinerja/setup/mst_pegawai/hapus/<?php echo enc_data($value['id_jabatan']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a> -->
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> <b>Edit Pegawai</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/setup/mst_pegawai/update" enctype="multipart/form-data">
							
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nama Pegawai</label>
								<div class="col-sm-9">
									<input type="hidden" name="user_key" value="<?php echo enc_data($edit['user_key']); ?>" class="form-control">
									<input type="text" name="nama_pegawai" value="<?php echo $edit['nama_doskar']; ?>" class="form-control" readonly>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Pekerjaan</label>
								<div class="col-sm-9">
									<input type="text" name="ket" value="<?php echo $edit['pekerjaan']; ?>" class="form-control" readonly>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Jabatan Struktural</label>
								<div class="col-sm-9">
									<input type="text" name="ket" value="<?php echo $edit['jabatan']; ?>" class="form-control" readonly>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenis Pegawai </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenis_peg" required>
										<option value="">Pilih Jenis Pegawai</option>
										<?php foreach ($jenis as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>" <?php if($edit['jenis_pegawai'] == $stat['id_stat']) { echo 'selected'; } ?>>
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>