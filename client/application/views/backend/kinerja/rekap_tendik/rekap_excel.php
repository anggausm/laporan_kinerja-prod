<?php
$this->load->helper('excel');
$this->load->library('phpexcel');
$filepath       = "file/kinerja/rekap_penilaian_tendik.xlsx";
$objReader      = PHPExcel_IOFactory::createReader('Excel2007');
$this->phpexcel = $objReader->load($filepath);
$objWorksheet   = $this->phpexcel->setActiveSheetIndex(0);

$objWorksheet->setCellValue('A3', 'PERIODE ' .$rekap[0]['nama_periode']);
// $objWorksheet->mergeCells('A3:F3');
// $objWorksheet->getStyle("A3:F3")->getFont()->setBold(true);
$letter = 1;
// $objWorksheet->getColumnDimension(generateExcelColumnLetters($letter++))->setAutoSize(true);

$no     = 1;
$line   = 4;
foreach ($rekap as $value) {
    $letter = 1;
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $no);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nama_doskar']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nama_unit']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['skor']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nilai']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['kategori']);
    // $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=P' . $line . ' * Q' . $line . '');

    $line++;
    $no++;
}


$nm_file = 'rekap_penilaian_' . date('YmdHis');
$nm_file = strtolower($nm_file);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename=' . $nm_file . '.xlsx');
header('Cache-Control: max-age=0');
// output
$obj_writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
$obj_writer->save('php://output');
?>