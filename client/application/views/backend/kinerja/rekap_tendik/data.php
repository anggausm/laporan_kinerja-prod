<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kinerja/rekap_tendik">Rekap Penilaian Kerja Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Rekap Penilaian Kerja Pegawai
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/setup/rekap_tendik/data" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $id_periode) { echo 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i> <b> Rekap List Penilaian Kerja Pegawai</b>
					<?php if(!empty($list)){ ?>
					 <a href="<?php print base_url() ?>kinerja/setup/rekap_tendik/export_excel/<?php print enc_data($id_periode) ?>" class="btn btn-md btn-success pull-right">
                            <i class="fa fa-download"></i> Export Excel 
                        </a> 
                        <?php } ?>
				</div>
				<div class="panel-body">
					<br>
					<div class="table-responsive">
							<table class="table table-striped b-t b-b table-bordered">
								<thead>
									<tr class="text-center">
										<th class="text-center">No</th>
										<th class="text-center">Nama</th>
										<th class="text-center">Unit</th>
										<th class="text-center">Skor</th>
										<th class="text-center">Nilai</th>
										<th class="text-center">Kategori <br>(%)</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($list as $value) {
									?>
										<tr>
											<td class="text-center"><?= $no ?></td>
											<td> <?= $value['nama_doskar']; ?> </td>
											<td> <?= $value['nama_unit']; ?> </td>
											<td class="text-center"> <?= $value['skor']; ?> </td>
											<td class="text-center"> <?= $value['nilai']; ?> </td>
											<td class="text-center"> <?= $value['kategori']; ?> </td>
										</tr>
									<?php
										$no++;
									}
									?>
								</tbody>
							</table>
						</div>
 					<br>
 				</div>
				</div>
			</div>
		</div>
	</div>
</div>