<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url() ?>kpg/anggota/dosen">Daftar Angota Dosen</a></li>
				</ol>
			</nav>
		</div>
	</div>

	<?php if (!empty($this->session->flashdata('pesan'))) : ?>
		<div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
			<?php print $this->session->flashdata('pesan') ?>
		</div>
	<?php endif ?>
	<?php $date_now =  date('Y-m-d'); ?>
	<!-- <?php if ($date_now < $periode['pengisian_mulai']) : ?>
		<div class="alert alert-danger font-bold" role="alert">
			<h1 class="text-center"> MASA PENGISIAN FORM PENILAIAN KINERJA TELAH SELESAI. </h1>
		</div>
	<?php elseif ($date_now > $periode['pengisian_selesai']) : ?>
		<div class="alert alert-danger font-bold" role="alert">
			<h1 class="text-center"> MASA PENGISIAN FORM PENILAIAN KINERJA TELAH SELESAI. </h1>
		</div>
	<?php endif ?> -->

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Penilaian Diri</b>
				</div>
				<div class="panel-body" style="padding: 5px !important;">
					<div class="alert alert-info font-bold" role="alert">
						Periode Pengisian : <?= date('d M Y', strtotime($periode['pengisian_mulai'])) ?> sampai <?= date('d M Y', strtotime($periode['pengisian_selesai'])) ?> <br>
						Periode Penilaian : <?= date('d M Y', strtotime($periode['penilaian_mulai'])) ?> sampai <?= date('d M Y', strtotime($periode['penilaian_selesai'])) ?>
					</div>
					<a href="https://drive.google.com/file/d/1APJnx0k_t78Y8cQNpUXcEqkFRaj-lU_H/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian</a>
					<a href="https://drive.google.com/file/d/1RBopoMwo27ll_KjOE0e4ZOvGR_bjI04J/view?usp=sharing" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-file-pdf-o"></i> Edaran dan Peraturan Rektor Penilaian Kinerja </a>
					<?php if ($kinerja_ajuan['status_ajuan'] == '1') : ?>
						<div class="alert alert-warning text-center" role="alert">
							Telah mengajukan form penilaian, tidak dapat mengisi kembali
						</div>
					<?php endif ?>
					<?php if ($disabled == '1') : ?>
						<a class="btn btn-primary btn-sm pull-right" disabled><i class="fa fa-check"></i> Ajukan</a>
					<?php else : ?>
						<a onclick="return confirm('Apakah anda akan mengajukan data ini.');" href="<?php echo base_url($this->link . 'ajukan'); ?>" class="btn btn-primary btn-sm pull-right"><i class="fa fa-check"></i> Ajukan</a>
					<?php endif ?>
					<form class="form-horizontal p-h-xs">
						<div class="row">
							<div class="col-md-12">
								<h4 class="text-center font-bold">FORM PENILAIAN KINERJA DARI DIRI SENDIRI</h4>
								<h4 class="text-center font-bold">DOSEN</h4>
								<h5 class="text-center font-bold">PERIODE 1 DESEMBER 2023 s.d 30 MEI 2024</h5>
							</div>
							<div class="col-md-6">
								<div class="">
									<label class="col-sm-4 control-label text-right">NIDN </label>
									<label class="col-sm-8 control-label text-right">: <?= $profil['nidn']; ?></label>
								</div>
								<div class="">
									<label class="col-sm-4 control-label text-right">Nama </label>
									<label class="col-sm-8 control-label text-right">: <?= $profil['nama_doskar']; ?> </label>
								</div>
								<div class="">
									<label class="col-sm-4 control-label text-right">Fakultas </label>
									<label class="col-sm-8 control-label text-right">: <?= $profil['nama_unit1']; ?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="">
									<label class="col-sm-4 control-label text-right">Pekerjaan</label>
									<label class="col-sm-8 control-label text-right">: <?= $profil['nama_jabatan']; ?> </label>
								</div>
								<div class="">
									<label class="col-sm-4 control-label text-right">TMT USM</label>
									<label class="col-sm-8 control-label text-right">: <?= tanggal_indonesia($profil['tmt_usm']); ?> </label>
								</div>
								<div class="">
									<label class="col-sm-4 control-label text-right">POIN</label>
									<label class="col-sm-8 control-label text-right">: <span class="total_poin"></span></label>
								</div>
								<div class="">
									<label class="col-sm-4 control-label text-right">KRITERIA PENILAIAN DOSEN</label>
									<label class="col-sm-8 control-label text-right">
										<table width="100%" class="table-bordered">
											<tr>
												<td class="text-center">NILAI</td>
												<td class="text-center">SCORE</td>
												<td class="text-center">KATEGORI</td>
											</tr>
											<tr>
												<td class="text-center">A</td>
												<td class="text-center">> 35</td>
												<td class="text-center">100%</td>
											</tr>
											<tr>
												<td class="text-center">B</td>
												<td class="text-center">26 - 35</td>
												<td class="text-center">75%</td>
											</tr>
											<tr>
												<td class="text-center">C</td>
												<td class="text-center">15 - 25</td>
												<td class="text-center">50%</td>
											</tr>
											<tr>
												<td class="text-center">D</td>
												<td class="text-center">
													< 15 </td>
												<td class="text-center">25%</td>
											</tr>
										</table>
									</label>
								</div>
							</div>
						</div>
					</form>
					<br>
					<form id="myForm" class="form-horizontal p-h-xs" action="<?= base_url($this->link . 'simpan') ?>" method="POST">
						<div class="table-responsive">
							<table class="table b-t b-b table-bordered">
								<thead>
									<tr>
										<th width="5%" class="text-center">NO</th>
										<th width="30%" class="text-center">KOMPONEN PENILAIAN</th>
										<th width="10%" class="text-center">SKOR</th>
										<th width="20%" class="text-center">JUMLAH</th>
										<th width="10%" class="text-center">NILAI (SKOR x JUMLAH)</th>
										<th width="" class="text-center">
											BUKTI LINK DOKUMEN
											<br><small class="text-danger font-bold">Tidak boleh menggunakan link dari PORTAL USM</small>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($tanya as $key => $value) : ?>
										<tr>
											<td class="text-center"><?php echo $value['urutan']; ?></td>
											<td <?php echo $value['value'] == '0' ? 'colspan="5"' : ''; ?>>
												<?php if ($value['value'] == '0') : ?>
													<strong><?= $value['pertanyaan']; ?></strong>
												<?php else : ?>
													<?= $value['pertanyaan']; ?>
												<?php endif ?>
											</td>
											<?php if ($value['value'] == '1') : ?>
												<td class="text-left">
													<input type="hidden" name="<?php echo "kinerja_nilai[$key][id_pertanyaan]"; ?>" value="<?= enc_data($value['id_pertanyaan']) ?>">
													<input type="hidden" name="<?php echo "kinerja_nilai[$key][skor]"; ?>" value="<?= enc_data($value['skor']) ?>">
													<?php echo $value['skor']; ?>
													<input type="hidden" id="<?php echo "skor_" . $value['id_pertanyaan']; ?>" value="<?= $value['skor'] ?>">
												</td>
												<td class="text-left">
													<input style="width: 100px;" <?php echo $disabled == '1' ? 'disabled' : ''; ?> type="text" class="jumlah" name="<?php echo "kinerja_nilai[$key][jumlah]"; ?>" data-id="<?php echo $value['id_pertanyaan']; ?>" value="<?= $value['jumlah'] ?>">
												</td>
												<td class="text-left">
													<span class="nilai" id="<?php echo "nilai_" . $value['id_pertanyaan']; ?>"><?php echo $value['nilai']; ?></span>
												</td>
												<td>
													<textarea <?php echo $disabled == '1' ? 'disabled' : ''; ?> name="<?php echo "kinerja_nilai[$key][link_dokumen]"; ?>" data-id="<?php echo $value['id_pertanyaan']; ?>" class="link_dokumen form-control" placeholder="Masukan link seperti https://repository.usm.ac.id"><?php echo $value['link_dokumen']; ?></textarea>
													<small class="small" class="urlFeedback" id="<?php echo "urlFeedback_" . $value['id_pertanyaan']; ?>"></small>
												</td>
											<?php endif ?>
										</tr>
									<?php endforeach ?>
									<tr>
										<td class="text-right" colspan="4"><strong>POIN</strong></td>
										<td class="text-center"><strong class="total_poin"></strong></td>
										<td class="text-center"></td>
									</tr>
									<tr>
										<td class="text-center" colspan="6">
											<?php if ($disabled == '1') : ?>
												<button type="button" disabled class="btn btn-sm btn-info pull-right"> <i class="fa fa-save"></i> Simpan</button>
											<?php else : ?>
												<button type="submit" class="btn btn-sm btn-info pull-right"> <i class="fa fa-save"></i> Simpan</button>
											<?php endif ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('.jumlah').on('input', function(e) {
			let input = $(this).val();
			let sanitizedInput = input.replace(/[^0-9.-]/g, ''); // Remove invalid characters

			// Parse the sanitized input as a float
			let parsedValue = parseFloat(sanitizedInput);
			let displayValue = sanitizedInput;

			// Regular expression to match valid floating point numbers
			let floatRegex = /^-?\d*\.?\d*$/;

			if (!floatRegex.test(sanitizedInput) || isNaN(parsedValue)) {
				displayValue = ''; // If the sanitized input is not valid, reset display value
			}

			$(this).val(displayValue);
			let id = $(this).data('id');
			let skor = $('#skor_' + id).val();
			let nilai = input * skor;
			$('#nilai_' + id).text(nilai);
			hitung_total_poin();
		});
		$('.link_dokumenaaa').on('input', function() {
			let input = $(this).val();

			// Regular expression to match valid URL characters
			let urlRegex = /^[a-zA-Z0-9-._~:/?#[@\]!$&'()*+,;=]*$/;

			// Remove invalid characters
			let sanitizedInput = input.split('').filter(char => urlRegex.test(char)).join('');

			// Regular expression to validate the full URL
			let validUrlRegex = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([\/\w .-]*)*\/?$/;

			// Check if the sanitized input is a valid URL
			let isValidUrl = validUrlRegex.test(sanitizedInput);

			// Update the textarea with the sanitized input
			$(this).val(sanitizedInput);

			// Update feedback element with the validity of the URL
			let feedbackElementId = $(this).data('id');
			$('#urlFeedback_' + feedbackElementId).text('');
			if (!isValidUrl && input != '') {
				// $('#urlFeedback_' + feedbackElementId).text('Invalid URL').css('color', 'red');
			}
		});
		$('.link_dokumenaaa').keypress(function(event) {
			if (event.which === 13) {
				event.preventDefault();
			}
		});

		// Sanitize input to remove new lines
		$('.link_dokumenaaa').on('input', function() {
			var sanitizedText = $(this).val().replace(/[\r\n]+/g, ' ');
			$(this).val(sanitizedText);
		});
		hitung_total_poin();

		function hitung_total_poin() {
			var nilaiElements = $('.nilai');
			var sum = 0;
			nilaiElements.each(function() {
				sum += parseFloat($(this).text()) || 0;
			});
			$('.total_poin').text(sum);
		}

	});
</script>