<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/validasi_log ">Validasi Log Book</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Validasi Log Book
					<?php 

					$mulai =  date("d-F-Y",strtotime($pa['pengisian_mulai']));
					$selesai =  date("d-F-Y",strtotime($pa['pengisian_selesai']));
					?>
					<button class="btn btn-sm btn-primary pull-right"><i class="fa fa-clock-o"></i> Periode Aktif : <?php echo $mulai.' s.d. '.$selesai ?> </button>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/validasi_log/data" enctype="multipart/form-data">
						<div class="row">
							<a href="<?= base_url()?>file/PANDUAN_VALIDASI_LOGBOOK.pdf" class="btn btn-success btn-md" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Validasi Log Book</a><br><br>

							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Nama Pegawai </label>
									<div class="col-sm-8">
										<select class="form-control select2" name="pegawai" required>
											<option value="">Pilih Pegawai</option>
										<?php
										foreach ($pegawai  as $vl) {
											?>
											<option value="<?= $vl['dinilai_user_key'] ?>" ><?= $vl['dinilai_nama'] ?></option>
											<!-- <option value="<?= $vl['user_key'] ?>" ><?= $vl['nama_doskar'] ?></option> -->
										<?php 
										}
									?>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-5">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Periode Log </label>
									<div class="col-sm-8">
										<select class="form-control select2" name="periode" required>
											<option value="">Pilih Periode Log</option>
										<?php
										foreach ($p_log  as $val) {?>
											<option value="<?= $val['id'] ?>">Minggu <?= $val['nama_log'] ?> (<?= date("d-M-Y",strtotime($val['tgl_awal'])) ?> s.d. <?= date("d-M-Y",strtotime($val['tgl_akhir'])) ?>)</option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-1">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>