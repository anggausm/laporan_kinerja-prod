<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/log_book">Log Book</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/log_book"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-edit "></i> <b>Edit Log</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/log_book/update" enctype="multipart/form-data">
							<input type="hidden" name="id" value=" <?php echo enc_data($edit['id_log_book']) ?>">
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Log Book</label>
	                            <div class="col-sm-4">
	                                <input type="text" class="form-control" name="tgl_log_book" value="<?php echo date("d-M-Y",strtotime($edit['tgl_awal'])).' s.d '.date("d-M-Y",strtotime($edit['tgl_akhir'])); ?>" readonly/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tugas / Pekerjaan </label>
								<div class="col-sm-9">
									<textarea name="tugas" value="<?php echo $edit['ket']; ?>" rows="5" ><?php echo $edit['tugas']; ?></textarea>
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Status Pekerjaan </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="stat_kerja" required>
										<option value="">Pilih Status Pekerjaan</option>
										<?php foreach ($stat_kerja as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>" <?php if($stat['id_stat'] == $edit['status_pekerjaan']){ echo 'selected'; }?>>
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan / Tugas Tambahan</label>
								<div class="col-sm-9">
									<textarea name="keterangan" rows="5" ><?php echo $edit['ket']; ?></textarea>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Update 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>