<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/log_book ">Log Book</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
		<strong>Poin Penilaian Log Book meliputi poin-poin indikator di bawah :  </strong>
		<ul>
			<li>Menyelesaikan pekerjaan dengan benar</li>
			<li>Menyelesaikan pekerjaan tepat waktu</li>
			<li>Bersedia menjalankan tugas tambahan yang relevan sesuai dengan pekerjaan</li>
			<li>Kesesuaian spesifikasi pekerjaan</li>
			<li>Pelayanan terhadap mahasiswa/dosen terkait surat menyurat atau pekerjaan lainnya diselesaikan tepat waktu</li>
		</ul>
		<strong>Nilai akan masuk ke dashboard penilaian jika sudah divalidasi oleh pimpinan/atasan.</strong>
	</div>

	<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <!-- <i class="icon mdi-action-view-list i-20"></i> <b>Log Book</b> -->
                        <a href="<?= base_url()?>file/PANDUAN_PENGISIAN_LOGBOOK.pdf" class="btn btn-success btn-md" target="_blank"><i class="fa fa-file-pdf-o"></i> Panduan Pengisian Log Book</a><br>
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/tendik/log_book/" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($p_all  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $id_periode) { print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i> <b> Data Log Book Periode <?php print $default[0]['nama_periode']; ?></b>
					<a class="pull-right" href="<?php echo base_url() ?>kinerja/tendik/log_book/tambah"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
				</div>
				<div class="panel-body">

					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">

								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Periode</th>
												<th style="width:25%" class="text-center">Isi Log Book</th>
												<th style="width:15%" class="text-center">Status Pekerjaan</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($default as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-M-Y",strtotime($value['tgl_awal'])); ?> s.d. 
												<?php echo date("d-M-Y",strtotime($value['tgl_akhir'])); ?></td>
												<td style="text-align: left;"><?php echo $value['tugas'];?></td>
												<td class="text-center">
													<?php if($value['status_pekerjaan'] == '1') {
														echo 'Selesai';
													} else if($value['status_pekerjaan'] == '2') {
														echo 'Berlanjut';
													} else { 
														echo 'Tidak Selesai';
													}
        											?>
												</td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/tendik/log_book/detail/<?php echo enc_data($value['id_log_book']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/log_book/hapus/<?php echo enc_data($value['id_log_book']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus log ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>