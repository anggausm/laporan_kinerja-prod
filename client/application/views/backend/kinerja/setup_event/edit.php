<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai">Setup Event</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/setup/event"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Kegiatan</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:10%" class="text-center">Tanggal</th>
												<th style="width:25%" class="text-center">Nama Event</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($event as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-F-Y",strtotime($value['tanggal_mulai'])); ?> s.d. <?php echo date("d-F-Y",strtotime($value['tanggal_selesai'])); ?></td>
												<td style="text-align: left;"><?php echo $value['nama_event'];?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/setup/event/edit/<?php echo enc_data($value['id_event']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/setup/event/hapus/<?php echo enc_data($value['id_event']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> <b>Edit Kegiatan</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/setup/event/update" enctype="multipart/form-data">
							<input type="hidden" name="id_event" value="<?php echo enc_data($edit['id_event']); ?>" class="form-control">
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nama Kegiatan</label>
								<div class="col-sm-9">
									<input type="text" name="nama_event" value="<?php echo $edit['nama_event']; ?>" class="form-control">
								</div>
							</div>
						<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Mulai</label>
	                            <div class="col-sm-4">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl_awal" data-date-format="YYYY-MM-DD" value="<?php echo $edit['tanggal_mulai']; ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Selesai</label>
	                            <div class="col-sm-4">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl_akhir" data-date-format="YYYY-MM-DD" value="<?php echo $edit['tanggal_selesai']; ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Jam Mulai</label>
	                            <div class="col-sm-4">
	                                <input type='time' class="form-control" id='datetimepicker1' name="jam_awal" data-time-format="HH:ii:ss"  value="<?php echo $edit['jam_mulai']; ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Jam Selesai</label>
	                            <div class="col-sm-4">
	                                <input type='time' class="form-control" id='datetimepicker1' name="jam_akhir" data-date-format="HH:ii:ss" value="<?php echo $edit['jam_selesai']; ?>" required/>
	                            </div>
	                        </div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Lokasi Kegiatan </label>
								<div class="col-sm-9">
									<input type="text" name="lokasi" value="<?php echo $edit['lokasi_event']; ?>" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenis Kegiatan </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ket" required>
										<option value="">Pilih Jenis Kegiatan</option>
										<?php foreach ($jenis as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>" <?php if($stat['id_stat'] == $edit['sub_unsur']) { echo 'selected'; } ?>>
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Update 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>