<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/tambah_ijazah">Tambah Ijazah</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
    		<ul>Dokumen Ijazah digunakan untuk penilaian poin di bawah : 
    		
	            <li><strong>Kesesuaian spesifikasi pekerjaan</strong></li>
	            Ketentuan berkas yang diunggah :  Ijazah terakhir yang sesuai dengan pendidikan/keahlian/pengalaman pekerjaan.
	            
	     </ul>

    </div>

	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Ijazah Terakhir</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:25%" class="text-center">Rincian ijazah</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($ijazah as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td style="text-align: left;">Ijazah <?php echo $value['jenjang_pendidikan'];?> di <?php echo $value['nama_instansi'];?>
													<br>Nomor <?php echo $value['no_ijazah'];?>
												</td>
												<td class="text-center">
													<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file']; ?>"><i class="fa fa-eye"></i></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_ijazah/<?php echo enc_data($value['id_ijazah']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_ijazah/<?php echo enc_data($value['id_ijazah']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> <b>Tambah ijazah</b><br>
						<small class="text-muted"> Submit Ijazah Terakhir saat anda bekerja di Universitas Semarang</small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/simpan_ijazah" enctype="multipart/form-data">
							<input type='hidden' name="id_peg" value="<?php echo $personel['id_peg_isdm']; ?>" class="form-control">
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenjang Pendidikan </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenjang" required>
										<option value="">Pilih Jenis ijazah</option>
										<?php foreach ($jenis_ijazah as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>">
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal ijazah</label>
	                            <div class="col-sm-4">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl_jazah" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nama Sekolah / Universitas </label>
								<div class="col-sm-9">
									<input type='text' name="nama_sekolah" placeholder="Ex. : Universitas Semarang" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor ijazah </label>
								<div class="col-sm-9">
									<input type='text' name="nomor" placeholder="Ex. : 03/USM/DN.51/2024" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tahun Masuk </label>
								<div class="col-sm-9">
									<input type='text' name="t_masuk" placeholder="2024" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tahun Lulus </label>
								<div class="col-sm-9">
									<input type='text' name="t_lulus" placeholder="2024" class="form-control">
								</div>
							</div>
							 <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Jurusan </label>
								<div class="col-sm-9">
									<input type='text' name="jurusan" placeholder="Ex. : Teknik Sipil" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tempat Belajar </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="tempat_belajar" required>
										<option value="">Pilih Tempat Belajar</option>
										
										<option value="Dalam Negeri">Dalam Negeri</option>
										<option value="Luar Negeri">Luar Negeri</option>
									</select>
								</div>
							</div>
	                        
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File Ijazah
									</label>
								<div class="col-sm-9">
									<span  class="label red">Format file PDF,JPG,JPEG maks. 2 MB</span>
									<input type='file' name="file_ijazah" class="form-control">

								</div>

							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>

<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_file  = $(this).data('namfil');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/ijazah/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>