<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'dashboard'?>"> Home</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <!-- <i class="icon mdi-action-view-list i-20"></i>  -->
                        <a href="<?= base_url()?>file/PANDUAN_DASHBOARD_PENILAIAN.pdf" class="btn btn-success btn-md" target="_blank"><i class="fa fa-file-pdf-o"></i> PANDUAN MENU DASHBOARD PENILAIAN</a><br>
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/tendik/pegawai/dashboard" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>"><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php 

                    $mulai =  date("d-F-Y",strtotime($p_aktif['pengisian_mulai']));
                    $selesai =  date("d-F-Y",strtotime($p_aktif['pengisian_selesai']));
                    ?>
<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <b>Master Dokumen Pegawai</b>
							<button class="btn btn-sm btn-primary pull-right"><i class="fa fa-clock-o"></i> Periode Aktif : <?php echo $mulai.' s.d. '.$selesai ?> </button>
				</div>
				<div class="panel-body">
					<div class="row">
                        <?php if($tendik['jenis_pegawai'] == 2) { ?>
                            <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url($this->link) ?>tambah_ijazah" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-mortar-board fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold">Pendidikan</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url($this->link) ?>tambah_sertifikat" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-certificate fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold">Sertifikat</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url($this->link) ?>tambah_sk_pegawai" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-file-text-o fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold text-ellipsis">SK Pegawai</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url($this->link) ?>tambah_sk_panitia" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-file-text-o fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold text-ellipsis">SK Panitia</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url() ?>kinerja/tendik/log_book" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-list-alt fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold text-ellipsis">Log Book</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <a href="<?= base_url() ?>kinerja/tendik/lpp" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-list-ul fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold text-ellipsis">LPP</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                        
                            <a href="<?= base_url($this->link) ?>kuesioner/<?php echo enc_data($p_aktif['id_periode']); ?>" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                <div class="text-center">
                                    <i class="fa fa-check-circle-o fa-3x text-primary"></i>
                                    <p class="no-margin m-t font-bold">Kuesioner</p>
                                </div>
                            </a>
                        </div> 
                    <?php } else if($tendik['jenis_pegawai'] == 1) { ?>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url($this->link) ?>tambah_ijazah" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-mortar-board fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold">Pendidikan</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url($this->link) ?>tambah_sertifikat" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-certificate fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold">Sertifikat</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url($this->link) ?>tambah_sk_pegawai" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-file-text-o fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold text-ellipsis">SK Pegawai</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url($this->link) ?>tambah_sk_panitia" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-file-text-o fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold text-ellipsis">SK Panitia</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url() ?>kinerja/tendik/log_book" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-list-alt fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold text-ellipsis">Log Book</p>
                                    </div>
                                </a>
                            </div>
                             <div class="col-sm-3 col-xs-6">

                                <a href="<?= base_url($this->link) ?>kuesioner/<?php echo enc_data($p_aktif['id_periode']); ?>" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-check-circle-o fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold">Kuesioner</p>
                                    </div>
                                </a>
                            </div>
                        <?php } else {?>
                            <div class="col-sm-3 col-xs-6">
                                <a href="<?= base_url() ?>kinerja/tendik/log_book" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                    <div class="text-center">
                                        <i class="fa fa-list-alt fa-3x text-primary"></i>
                                        <p class="no-margin m-t font-bold text-ellipsis">Log Book</p>
                                    </div>
                                </a>
                            </div>
                                <div class="col-sm-3 col-xs-6">
                                    <a href="<?= base_url($this->link) ?>tambah_ijazah" class="btn btn-thumbnail card r-2x p animated fadeIn p b-b-4x b-b-primary inline text-u-c w-full">
                                        <div class="text-center">
                                            <i class="fa fa-mortar-board fa-3x text-primary"></i>
                                            <p class="no-margin m-t font-bold">Pendidikan</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        
                    <?php } ?>
                    </div>
            				</div>
            			</div>
            			
            		</div>
		<!-- <div class="col-md-4">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <b>Biodata Pegawai</b>
							
				</div>
				<div class="panel-body">
					<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                     <tbody>
                    	<tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%"><?php echo $profil['nama_doskar']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">NIS</td>
                            <td style="width:70%"><?php echo $profil['nis']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan</td>
                            <td style="width:70%"><?php echo $profil['nama_jabatan']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Unit</td>
                            <td style="width:70%"><?php echo $profil['nama_unit1']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jenis Pegawai</td>
                            <td style="width:70%"><?php echo $profil['jenis_pekerjaan']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Kerja</td>
                            <td style="width:70%"><?php echo $profil['status_kerja']; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">TMT </td>
                            <td style="width:70%"><?php echo $profil['tmt_usm']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
							</form>
				</div>
			</div>
		</div> -->
			
	</div>