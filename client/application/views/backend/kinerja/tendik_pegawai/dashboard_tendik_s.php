<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'dashboard'?>"> Home</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-default text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Penilaian Pegawai
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?php print base_url()?>kinerja/tendik/pegawai/dashboard" class="form-inline text-right" method='post'>
                       <select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $p_aktif['id_periode']) { print 'selected'; }?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>	
	
	<?php
        $hariini = date('Y-m-d'); 
	   $mulai = tanggal_indonesia($p_aktif['pengisian_mulai']);
						$selesai =tanggal_indonesia($p_aktif['pengisian_selesai']);

		foreach($rekap_log as $rlog){
							$log1 = $rlog['p1'];
							$log2 = $rlog['p2'];
							$log3 = $rlog['p3'];
							$log4 = $rlog['p4'];
							$log5 = $rlog['p5'];
							$log6 = $rlog['p6'];
						}

						$jml_log = count($periode_log);
						$jml_lpp = count($lpp);
						$p_lpp = '6';

    ?>
	
	<div class="row">
		<div class="col-md-8">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
							
				</div>
				<div class="panel-body">
					<?php if (date('Y-m-d') >= $mulai && date('Y-m-d') <= $selesai) { ?>
					<h4>Periode Pengisian : <?php echo $mulai; ?> s.d. <?php echo $selesai; ?></h4>

					<div class="table-responsive">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>I. </h4>
												</td>
												<td style="width:40%" class="text-left">
													<h4>Pelaksanaan Tugas Utama </h4>
												</td>
												<td style="width:12.5%" class="text-right">
													Aksi
												</td>
												<td style="width:12.5%" class="text-right">
													Skor
												</td>
												<td style="width:12.5%" class="text-right">
													Bobot
												</td>
												<td style="width:12.5%" class="text-right">
													Nilai
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-left">
												</td>
												<td style="width:40%" class="text-left">
													1. Kesesuaian pekerjaan:
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   a. Menyelesaikan pekerjaan dengan benar</td>
												<td style="width:12.5%" class="text-right">
											
													
													<?php 
														$skorr_1  = $log1/$jml_log;

														if($skorr_1 >= 1){
														$skor_1_1 = '0.5';
														} else {
															$skor_1_1 = '0';
														}

														$skorr2  = $jml_lpp/$p_lpp;

														if($skorr2 >=1 ){
														$skor_1_2 = '0.5';
														} else {
															$skor_1_2 = '0';
														}

														$skor_1 = $skor_1_1 + $skor_1_2;
													if($skor_1 >= '1' ){
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}
														$bobot_1 = "0.10";
														$nil1 = ($skor_1_1 + $skor_1_2) *$bobot_1; 
													?>
												</td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $skor_1; ?></button></td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $bobot_1;?></button> </td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo number_format($nil1,2); ?></button> </td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   b. Menyelesaikan pekerjaan tepat waktu</td>
												<td style="width:12.5%" class="text-right">
											
													
													<?php 
														$skorr_1  = $log2/$jml_log;

														if($skorr_1 >= 1){
														$skor_2_1 = '0.5';
														} else {
															$skor_2_1 = '0';
														}

														$skorr2  = $jml_lpp/$p_lpp;

														if($skorr2 >= 1){
														$skor_2_2 = '0.5';
														} else {
															$skor_2_2 = '0';
														}

														$skor_2 = $skor_2_2 + $skor_2_2;
													if($skor_2 >= '1' ){
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}
														$bobot_2 = "0.10";
														$nil2 = ($skor_2_2 + $skor_2_2)*$bobot_2; 
													?>
												</td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $skor_2; ?></button></td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $bobot_2;?></button> </td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo number_format($nil2,2); ?></button> </td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   c. Bersedia menjalankan tugas tambahan yang relevan</td>
												<td style="width:12.5%" class="text-right">
													<?php 
														$skorr_1  = $log3/$jml_log;

														if($skorr_1 >= 1){
														$skor_3_1 = '0.5';
														} else {
															$skor_3_1 = '0';
														}

														$skorr2  = $jml_lpp/$p_lpp;

														if($skorr2 >= 2){
														$skor_3_2 = '0.5';
														} else {
															$skor_3_2 = '0';
														}

														$skor_3 = $skor_3_1 + $skor_3_2;
													if($skor_3 >= '1' ){
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}
														$bobot_3 = "0.10";
														$nil3 = ($skor_3_1 + $skor_3_2)*$bobot_3; 
													?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_3; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $bobot_3; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil3,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 2. Kesesuaian spesifikasi pekerjaan</td>
												<td style="width:12.5%" class="text-right">		
													<?php 

													$skor_log4 = $log4/$jml_log;
														if($skor_log4 >= 0.75){
														$skor_4_1 = '0.25';
														} else {
															$skor_4_1 = '0';
														}


														if($sk_pegawai['status_kerja'] = 'kontrak'){
															if($sk_pegawai['exp_sk'] >= $p_aktif['pengisian_selesai']){
								                                    $skor_4_2 = '0.25';
								                                    } else {
								                                    $skor_4_2 = '0';
								                                    }

														} else if ($sk_pegawai['status_kerja'] = 'tetap'){
															$skor_4_2 = '0.25';
														} else {
															$skor_4_2 = '0';
														}

														if(!empty($ijazah)){
														$skor_4_3 = '0.25';
														}else {
															$skor_4_3 = '0';
														}

														$rekap_lpp = $jml_lpp/$p_lpp;
														if($rekap_lpp >= '1'){
														$skor_4_4 = '0.25';
														} else {
															$skor_4_4 = '0';
														}														

														$skor_4 = $skor_4_1 + $skor_4_2 + $skor_4_3 + $skor_4_4 ;

														$bobot_4 = "0.10";
														$nil4 = $skor_4*$bobot_4; 

														if($skor_4 >='1' ){
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/form_spek_peg/<?php echo enc_data($p_aktif['id_periode']);?>"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
														<?php
														} else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/form_spek_peg/<?php echo enc_data($p_aktif['id_periode']);?>"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
															}
														?>										
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil4,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">b. Penilaian Kepuasan Layanan</td>
												<td style="width:12.5%" class="text-right">												
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>

											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kuesioner dari Dosen atau Mahasiswa atau atasan langsung </td>
												<td style="width:12.5%" class="text-right">

													<?php 
													$n_kuesioner = $kuesioner['rerata_akhir']/7; 

													if($n_kuesioner >= 1){
														$skor_5 = '1';
														?>
														<!-- <a href="<?php echo base_url() ?>kinerja/tendik/pegawai/kuesioner/<?php echo enc_data($p_aktif['id_periode']); ?>"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a> -->
														<button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button>
														<?php 
													} else {
														$skor_5 ='0';
														?>
														<!-- <a href="<?php echo base_url() ?>kinerja/tendik/pegawai/kuesioner/<?php echo enc_data($p_aktif['id_periode']); ?>"><button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button></a> -->
														<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button>
														<?php
														
													}

													 $bobot_5 = "0.20";
														$nil5 = $skor_5*$bobot_5; 
													?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil5,2); ?></button> 
												</td>

											</tr>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>II. </h4>
												</td>
												<td style="width:40%"class="text-left"><h4>Core Values </h4></td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Harmoni: Gathering, Mengikuti kegiatan di Universitas/ Fakultas </td>
												<td style="width:50%" class="text-right">
													
													<?php 

													if(!empty($foto1)){
														$skor_6 = '1';
													} else {
														$skor_6 = '0';
													}

														$bobot_6 = "0.05";
														$nil6 = $skor_6*$bobot_6; 

														if($skor_6 == 1){ ?> <a href="<?php echo base_url() ?>kinerja/tendik/pegawai/form_event/<?php echo enc_data($p_aktif['pengisian_mulai']); ?>/<?php echo enc_data($p_aktif['pengisian_selesai']); ?>/<?php echo enc_data(2); ?>"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
													<?php } else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/form_event/<?php echo enc_data($p_aktif['pengisian_mulai']); ?>/<?php echo enc_data($p_aktif['pengisian_selesai']); ?>/<?php echo enc_data(2); ?>"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}
													?>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil6,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 2. Integritas : Presensi fingerprint dan keberadaan ditempat tugasnya sesuai jam kerja, kehadiran pada acara universitas/ fakultas  </td>
												<td style="width:12.5%" class="text-right">
													
													<?php 
													if(!empty($event_wajib)){
														$skor_7 = '1';
													} else {
														$skor_7 = '0';
													}

														$bobot_7 = "0.04";
														$nil7 = $skor_7*$bobot_7; 
														if($skor_7 >= 1){
													?>
												<button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button>
												<?php } else { ?>
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button>
												<?php } ?>

												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_7; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_7; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil7,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 3. Akuntabilitas : Pelayanan terhadap mahasiswa/dosen terkait surat menyurat diselesaikan maksimal 2 hari</td>
												<td style="width:12.5%" class="text-right">
													
													<?php 

														$skors_1  = $log5/$jml_log;

														if($skors_1 >= 1){
														$skor_8_1 = '0.5';
														} else {
															$skor_8_1 = '0';
														}

														$skors_2  = $jml_lpp/$p_lpp;

														if($skors_2 >= 1){
														$skor_8_2 = '0.5';
														} else {
															$skor_8_2 = '0';
														}

														$skor_8  = $skor_8_1 + $skor_8_2;
														$bobot_8 = "0.05";
														$nil8 = ($skor_8_1 + $skor_8_2)*$bobot_8; 

													if($skor_8 >= 1){
														?> <a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/log_book/index"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}
														$bobot_8 = "0.05";
														$nil8 = $skor_8*$bobot_8; 
													 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_8; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_8; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil8,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 4. Prestasi (lolos Sertifikasi dengan uji kompetensi dari lembaga yang berwenang, juara tingkat lokal, nasional, regional )</td>
												<td style="width:12.5%" class="text-right">
													
													<?php if(!empty($sert_1)){
														$skor_9 = '1';
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sertifikat"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														$skor_9 = '0';
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sertifikat"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}

													$bobot_9 = "0.05";
														$nil9 = $skor_9*$bobot_9; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_9; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_9; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil9,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 5. Kepemimpinan : membuat laporan kegiatan yang sudah dilaksanakan dalam periode penilaian</td>
												<td style="width:12.5%" class="text-right">
													
													<?php 
														$rekap_lpp = $jml_lpp/$p_lpp;

													if($rekap_lpp >= 1){
														$skor_10 = '1';
														?><a href="<?php echo base_url() ?>kinerja/lpp"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														$skor_10 = '0';
														?>
														<a href="<?php echo base_url() ?>kinerja/lpp"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
													}

													$bobot_10 = "0.05";
													$nil10= $skor_10*$bobot_10; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_10; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_10; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil10,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>III. </h4>
												</td>
												<td style="width:40%"class="text-left"> <h4>Pelaksanaan Kegiatan Penunjang  (Tambahan) </h4></td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> a. Pengembangan diri Sesuai dengan Pekerjaan</td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Sertifikat/ surat tugas mengikuti pelatihan/ seminar/ workshop/Studi Banding</td>
												<td style="width:12.5%" class="text-right">
													
													<?php if(!empty($sert_event)){
														$skor_11 = '1'; ?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sertifikat"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														$skor_11 = '0';
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sertifikat"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
														
													} 
													$bobot_11 = "0.03";
														$nil11= $skor_11*$bobot_11; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_11; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_11; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil11,2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> b. Keterlibatan dalam kepanitiaan</td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. SK kepanitiaan</td>
												<td style="width:12.5%" class="text-right">
													
													<?php if(!empty($sk_panitia)){
														$skor_12 = '1';
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sk_panitia"><button class="btn btn-md btn-default"><i class="fa fa-check-square-o"></i> </button></a>
														<?php
													} else {
														$skor_12 = '0';
														?>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/tambah_sk_panitia"><button class="btn btn-md btn-warning"><i class="fa fa-edit"></i> </button></a>
														<?php
														
													} $bobot_12 = "0.03";
														$nil12 = $skor_12*$bobot_12; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_12; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_12; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil12, 2); ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													<h4>IV. </h4>
												</td>
												<td style="width:40%"class="text-left"> <h4>Penilaian Kehadiran </h4></td>
												<td style="width:12.5%" class="text-right">
													
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
												</td>
												<td style="width:40%"class="text-left"> a. Kehadiran Fingerprint</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i> </button>
													<?php 
													$jp = $presensi['persentes']; 
													if($jp <= '100'){
														$skor_13 = '1';

													} else if($jp >= '75'){
														$skor_13 = '0.5';
													} else {
														$skor_13 = '0';
													}
													$bobot_13 = "0.10";
														$nil13 = $skor_13*$bobot_13; 
														 ?>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_13; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_13; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo number_format($nil13, 2); ?></button> 
												</td>
											</tr>
											</tr>
												<td style="width:10%" class="text-center"> </td>
												<td style="width:40%"class="text-left">Total Nilai </td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
													<b><?php $tot = $nil1 + $nil2 + $nil3 + $nil4 + $nil5 + $nil6 + $nil7 + $nil8 + $nil9 + $nil10 + $nil11 + $nil12 + $nil13; 
													echo number_format($tot, 3); ?></b>
												</td>
											<tr> 
										
											</tbody>

											</table>
									
								</div>
								<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/simpan_ajuan" enctype="multipart/form-data">
										
										<input type="hidden" name="skor1" value="<?php echo number_format($skor_1, 2); ?>" >
										<input type="hidden" name="bobot1" value="<?php echo $bobot_1;?>" >
										<input type="hidden" name="nilai1" value="<?php echo number_format($nil1, 2); ?>" >

									
										<input type="hidden" name="skor2" value="<?php echo number_format($skor_2,2); ?>" >
										<input type="hidden" name="bobot2" value="<?php echo $bobot_2;?>" >
										<input type="hidden" name="nilai2" value="<?php echo number_format($nil2,2); ?>" >

										
										<input type="hidden" name="skor3" value="<?php echo number_format($skor_3,2); ?>" >
										<input type="hidden" name="bobot3" value=" <?php echo $bobot_3; ?>" >
										<input type="hidden" name="nilai3" value="<?php echo number_format($nil3,2); ?>" >

									
										<input type="hidden" name="skor4" value="<?php echo number_format($skor_4,2); ?>" >
										<input type="hidden" name="bobot4" value=" <?php echo $bobot_4; ?>" >
										<input type="hidden" name="nilai4" value="<?php echo number_format($nil4,2); ?>" >

									
										<input type="hidden" name="skor5" value="<?php echo number_format($skor_5,2); ?>" >
										<input type="hidden" name="bobot5" value="<?php echo $bobot_5; ?>" >
										<input type="hidden" name="nilai5" value="<?php echo number_format($nil5,2); ?>" >

									
										<input type="hidden" name="skor6" value="<?php echo $skor_6; ?>" >
										<input type="hidden" name="bobot6" value="<?php echo $bobot_6; ?>" >
										<input type="hidden" name="nilai6" value="<?php echo number_format($nil6,2); ?>" >

									
										<input type="hidden" name="skor7" value="<?php echo $skor_7; ?>" >
										<input type="hidden" name="bobot7" value="<?php echo $bobot_7; ?>" >
										<input type="hidden" name="nilai7" value="<?php echo number_format($nil7,2); ?>" >

										
										<input type="hidden" name="skor8" value="<?php echo $skor_8; ?>" >
										<input type="hidden" name="bobot8" value="<?php echo $bobot_8; ?>" >
										<input type="hidden" name="nilai8" value="<?php echo number_format($nil8,2); ?>" >
										
										
										<input type="hidden" name="skor9" value="<?php echo $skor_9; ?>" >
										<input type="hidden" name="bobot9" value="<?php echo $bobot_9; ?>" >
										<input type="hidden" name="nilai9" value="<?php echo number_format($nil9,2); ?>" >

										
										<input type="hidden" name="skor10" value="<?php echo $skor_10; ?>" >
										<input type="hidden" name="bobot10" value="<?php echo $bobot_10; ?>" >
										<input type="hidden" name="nilai10" value="<?php echo number_format($nil10,2); ?>" >

										
										<input type="hidden" name="skor11" value="<?php echo $skor_11; ?>" >
										<input type="hidden" name="bobot11" value="<?php echo $bobot_11; ?>" >
										<input type="hidden" name="nilai11" value="<?php echo number_format($nil11,2); ?>" >

										
										<input type="hidden" name="skor12" value="<?php echo $skor_12; ?>" >
										<input type="hidden" name="bobot11" value="<?php echo $bobot_12; ?>" >
										<input type="hidden" name="nilai12" value="<?php echo number_format($nil12,2); ?>" >

										<input type="hidden" name="skor13" value="<?php echo $skor_13; ?>" >
										<input type="hidden" name="bobot13" value="<?php echo $bobot_13; ?>" >
										<input type="hidden" name="nilai13" value="<?php echo number_format($nil13,2); ?>" >

										<input type="hidden" name="ttl_nilai" value="<?php echo number_format($tot, 3); ?>" >
										 <input type="hidden" name="id_periode" value="<?php echo $p_aktif['id_periode']; ?>" > 
								<div class="panel-footer bg-white">

									<?php if(empty($cek_skp_peg)){ ?>
									<?php if($tot < 0.6){ ?>
									<span  class="label red">Syarat penilaian belum terpenuhi, silahkan cek Kembali berkas yang harus di ajukan!</span>	
									<button class="btn btn-md btn-success pull-right"  onclick="return confirm('PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. \n \nSETELAH MELAKUKAN SUBMIT ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA. \n \nApakah Anda yakin akan menyimpan penilaian kinerja ini ???');" disabled><i class="fa fa-paper-plane"></i> Submit Pengajuan</button>
								<?php } else { ?>
										<span  class="label green">Syarat penilaian sudah terpenuhi, silahkan submit pengajuan </span>
										<button class="btn btn-md btn-success pull-right"  onclick="return confirm('PASTIKAN ANDA MELAKUKAN PENGISIAN PENILAIAN KINERJA DENGAN BENAR. \n \nSETELAH MELAKUKAN SUBMIT ANDA TIDAK DAPAT MELAKUKAN PERUBAHAN PENGISIAN KINERJA. \n \nApakah Anda yakin akan menyimpan penilaian kinerja ini ???');"><i class="fa fa-paper-plane"></i> Submit Pengajuan</button>
									<?php
								} ?>
									
										<?php } else { echo 'Penilaian Kinerja sudah diajukan...'; } ?>	
								</div>
							</form>
							<?php } else { echo '<h2>Periode Pengisian Penilaian telah berakhir</h2>'; } ?>
					</div>

			</div>
			
		</div>
		<div class="col-md-4">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Biodata Pegawai
							
				</div>
				<div class="panel-body">
					<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">
					
								<h4 class="font-bold text-capitalize no-margin m-b">Data Pegawai</h4>

            <div class="table-responsive">
                 <table class="table table-hover" style="border: 1px solid #e7eaec">
			                    <tbody>
			                    	<tr>
			                            <td style="width:30%">Nama</td>
			                            <td style="width:70%"><?php echo $personel['nama_doskar']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">NIS</td>
			                            <td style="width:70%"><?php echo $personel['nis']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Pekerjaan</td>
			                            <td style="width:70%"><?php echo $personel['pekerjaan']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Unit</td>
			                            <td style="width:70%"><?php echo $personel['nama_unit']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">TMT </td>
			                            <td style="width:70%"><?php echo $personel['tmt_usm']; ?></td>
			                        </tr>
			                        <tr>
			                            <td style="width:30%">Status Kerja</td>
			                            <td style="width:70%"><?php echo $personel['status_kerja']; ?></td>
			                        </tr>
			                    </tbody>
			                </table>
            </div>
							</form>
				</div>
			</div>
		</div>
			
	</div>

 
