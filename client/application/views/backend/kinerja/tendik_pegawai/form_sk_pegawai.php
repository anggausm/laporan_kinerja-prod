<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/tambah_sk_pegawai">Tambah SK Pegawai</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
		<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
		<h4>SK Pengangkatan Pegawai digunakan untuk penilaian poin di bawah :</h4>
    		<ul> 
    		
	            <li><strong>Kesesuaian spesifikasi pekerjaan</strong></li>
	            Ketentuan berkas yang diunggah :  <br>
	            - SK Pengangkatan Pegawai terbaru untuk karyawan kontrak atau<br>
	            - SK Pengangkatan Pegawai Tetap yang terakhir untuk karyawan tetap	 <br>           
	     </ul>

    </div>

	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data SK Pengangkatan Pegawai</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
					<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:25%" class="text-center">Rincian SK</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($sk as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td style="text-align: left;"><?php echo $value['judul_sk'];?> No. <?php echo $value['no_sk'];?></td>
												<td class="text-center">
													<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file_sk']; ?>" data-namfol="<?php echo $value['folder_sk']; ?>"><i class="fa fa-eye"></i></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_sk_pegawai/<?php echo enc_data($value['id_sk']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_sk/<?php echo enc_data($value['id_sk']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i><b> Tambah Data SK Pegawai</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/simpan_sk_pegawai" enctype="multipart/form-data">
							<input type="hidden" name="jenis_sk" value="pegawai" class="form-control">
							<input type="hidden" name="id_peg" value="<?php echo $personel['id_peg_isdm']; ?>" class="form-control">
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal SK</label>
	                            <div class="col-sm-9">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Judul SK</label>
								<div class="col-sm-9">
									<input type="text" name="judul" placeholder="Judul SK" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor SK</label>
								<div class="col-sm-9">
									<input type="text" name="nomor" placeholder="Tuliskan Nomor SK" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tanggal Mulai Berlaku </label>
								<div class="col-sm-9">
									<input type='date' class="form-control" id='datetimepicker1' name="tmt" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required/>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Akhir Berlaku</label>
	                            <div class="col-sm-9">
	                                <input type='date' class="form-control" id='datetimepicker1' name="exp" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD"/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Fakultas/Unit</label>
								<div class="col-sm-9">
									<select class="form-control select2" name="flus" required>
										<option value="">Pilih Fakultas/Unit/Lembaga</option>
										<?php foreach ($units as $val): ?>
										<option value="<?php echo $val['id_unit'] ?>">
											<?php echo $val['nama_unit'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal mulai bekerja</label>
	                            <div class="col-sm-9">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tmt_usm" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Status Kerja</label>
								<div class="col-sm-9">
									<select class="form-control select2" name="stat_kerja" required>
										<option value="">Pilih Status</option>
										<option value="kontrak">Kontrak</option>
										<option value="tetap">Tetap</option>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Jenis Pekerjaan</label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenis_pekerjaan" required>
										<option value="">Pilih Jenis Pekerjaan</option>
										<option value="tenaga kependidikan">Tenaga Kependidikan</option>
										<option value="tenaga penunjang">Tenaga Penunjang</option>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File SK</label>
								<div class="col-sm-9">
									<span  class="label red">Format file PDF,JPG,JPEG maks. 2 MB</span>
									<input type="file" name="filesk" class="form-control" required>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>

<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_file  = $(this).data('namfil');
				var nm_folder  = $(this).data('namfol');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sk/'+nm_folder+'/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script> 
