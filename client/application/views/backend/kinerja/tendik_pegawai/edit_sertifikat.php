<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/tambah_sertifikat">Tambah Sertifikat</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b> Data Sertifikat </b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:10%" class="text-center">Tanggal Sertifikat</th>
												<th style="width:25%" class="text-center">Rincian Sertifikat</th>
												<th style="width:15%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($sertifikat as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-F-Y",strtotime($value['tanggal_sertifikat'])); ?></td>
												<td style="text-align: left;"><?php echo $value['nama_sertifikat'];?></td>
												<td class="text-center">
													<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['nm_file']; ?>" data-namfol="<?php echo $value['nm_folder']; ?>"><i class="fa fa-eye"></i></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_sertifikat/<?php echo enc_data($value['id_sertifikat']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_sertifikat/<?php echo enc_data($value['id_sertifikat']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-edit "></i> <b>Edit Sertifikat</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/update_sertifikat" enctype="multipart/form-data">
							<input type="hidden" name="id_peg" value="<?php echo $personel['id_peg_isdm']; ?>" class="form-control">
							<input type="hidden" name="id_sertifikat" value="<?php echo $edit['id_sertifikat']; ?>" class="form-control">
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenis Sertifikat </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenis" required>
										<option value="">Pilih Jenis Sertifikat</option>
										<?php foreach ($jenis as $val): ?>
										<option value="<?php echo $val['id_jenis'] ?>" <?php if($edit['jenis_sertifikat'] == $val['id_jenis']) { echo 'selected'; } ?>>
											<?php echo $val['nama_jenis'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor Sertifikat </label>
								<div class="col-sm-9">
									<input type="text" name="nomor" value="<?php echo $edit['no_sertifikat']; ?>" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Judul Sertifikat </label>
								<div class="col-sm-9">
									<input type="text" name="judul" value="<?php echo $edit['nama_sertifikat']; ?>" class="form-control">
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Sertifikat</label>
	                            <div class="col-sm-3">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl_sert" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo $edit['tanggal_sertifikat']; ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal akhir berlaku</label>
	                            <div class="col-sm-3">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl_akhir" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo $edit['tanggal_berlaku']; ?>" required/>
	                            </div>
	                        </div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Penyelenggara </label>
								<div class="col-sm-9">
									<input type="text" name="penyelenggara" value="<?php echo $edit['penyelenggara']; ?>" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Tingkat Sertifikat </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="tingkat" required>
										<option value="">Pilih Tingkat Sertifikat</option>
										<?php foreach ($tingkat as $value): ?>
										<option value="<?php echo $value['id_stat'] ?>" <?php if($edit['tingkat_sertifikat'] == $value['id_stat']) { echo 'selected'; } ?>>
											<?php echo $value['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
	                        
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan</label>
								<div class="col-sm-9">
									<input type="text" name="keterangan" value="<?php echo $edit['ket']; ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File Sertifikat</label>
								<div class="col-sm-9">
									<input type="file" name="file_sertif" class="form-control">
									<input type="hidden" name="file_lama" value="<?php echo $edit['nm_file']; ?>" class="form-control">
									<input type="hidden" name="folder_lama"  value="<?php echo $edit['nm_folder']; ?>" class="form-control">
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Update 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>

<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_file  = $(this).data('namfil');
				var nm_folder  = $(this).data('namfol');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sertifikat/'+nm_folder+'/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>