<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/form_event">Tambah Bukti Kegiatan</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
		<h4>Harmoni: Mengikuti kegiatan di Universitas/ Fakultas / Pascasarjana/ Unit lainnya</h4>

    		<strong>Ketentuan :</strong> 
    		
	            <li>Mengikuti kegiatan kebersamaan yang diadakan oleh Universitas /Fakultas/Pascasarjana/unit lainnya (capacity building/dies natalis /halal bi halal/kegiatan olahraga/senam)</li>
 				<li>Kegiatan tidak boleh kosong</li>
	            
	     	<strong>Berkas yang diunggah:</strong> Dokumentasi Foto Kegiatan

    </div>

	<div class="row">
		<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Kegiatan</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Rincian Event</th>
												<th style="width:15%" class="text-center">Foto</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$no = 1;
										foreach	($event as $value){
											?>
										
										<tr>
												<td class="text-center"><?=$no?></td>
												
												<td style="text-align: left;"><?php echo $value['nama_event'];?><br> 
													<?php echo date("d-F-Y",strtotime($value['tanggal_mulai'])); ?> s.d. <?php echo date("d-F-Y",strtotime($value['tanggal_selesai'])); ?> </td>
												<td style="text-align: center;">
													<?php if($value['file_foto'] == '' || $value['file_foto'] == NULL){
														?>
														<a href="#my_modal" data-toggle="modal" data-namakeg="<?php echo $awal; ?>" data-namaunit="<?php echo $akhir; ?>" data-idkeg="<?php echo enc_data($value['id_event']); ?>"  data-jns="<?php echo $jenis_event; ?>"><button class="btn btn-success btn-xs"> <i class="fa fa-picture-o"></i> Upload Foto</button></a>
														<?php
													} else {?>
														<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file_foto']; ?>" data-namfol="<?php echo $value['nama_folder']; ?>"><i class="fa fa-eye"></i> Lihat Foto</a>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_foto/<?php echo enc_data($value['id']); ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> Hapus </button></a>
													<?php } ?>
												</td>
										</tr>
													<?php $no++;
												}
													?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>

	</div>
</div>
	<!-- <div class="row">
		<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Kegiatan</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<?php if($jenis_event == '2') {?>
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Rincian Event</th>
												<th style="width:15%" class="text-center">Foto</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$no = 1;
										foreach	($event as $value){
											?>
										
										<tr>
												<td class="text-center"><?=$no?></td>
												
												<td style="text-align: left;"><?php echo $value['nama_event'];?><br> 
													<?php echo date("d-F-Y",strtotime($value['tanggal_mulai'])); ?> s.d. <?php echo date("d-F-Y",strtotime($value['tanggal_selesai'])); ?> </td>
												<td style="text-align: center;">
													<?php if($value['file_foto'] == '' || $value['file_foto'] == NULL){
														?>
														<a href="#my_modal" data-toggle="modal" data-namakeg="<?php echo $awal; ?>" data-namaunit="<?php echo $akhir; ?>" data-idkeg="<?php echo enc_data($value['id_event']); ?>"  data-jns="<?php echo $jenis_event; ?>"><button class="btn btn-success btn-xs"> <i class="fa fa-picture-o"></i> Upload Foto</button></a>
														<?php
													} else {?>
														<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file_foto']; ?>" data-namfol="<?php echo $value['nama_folder']; ?>"><i class="fa fa-eye"></i> Lihat Foto</a>
														<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_foto/<?php echo enc_data($value['id']); ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
													<?php } ?>
												</td>
										</tr>
													<?php $no++;
												}
													?>
										</tbody>
									</table>
								</div>
							<?php } else {
								?>
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Rincian Event</th>
												<th style="width:15%" class="text-center">Fingerprint</th>
												<th style="width:15%" class="text-center">Foto</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$no = 1;
										foreach	($event as $value){
										?>
										
										<tr>
												<td class="text-center"><?=$no?></td>
												
												<td style="text-align: left;"><?php echo $value['nama_event'];?><br> 
													<?php echo date("d-F-Y",strtotime($value['tanggal_mulai'])); ?> </td>

												<td style="text-align: center;"><?php if(empty($finger)){ ?>
													<button class="btn btn-sm btn-default"><i class="fa fa-remove"></i> </button>
												<?php } else {
													?>
													<button class="btn btn-sm btn-default"><i class="fa fa-check-square-o"></i> </button>
													<?php
												} ?>
													</td>
													<td style="text-align: center;">
														<?php if($value['file_foto'] == '' || $value['file_foto'] == NULL){
														?>
														<a href="#my_modal" data-toggle="modal" data-namakeg="<?php echo $awal; ?>" data-namaunit="<?php echo $akhir; ?>" data-idkeg="<?php echo enc_data($value['id_event']); ?>"  data-jns="<?php echo $jenis_event; ?>"><button class="btn btn-success btn-xs"> <i class="fa fa-picture-o"></i> Upload Foto</button></a>
														<?php
													} else {?>
														<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file_foto']; ?>" data-namfol="<?php echo $value['nama_folder']; ?>"><i class="fa fa-eye"></i> Lihat Foto</a>
													<?php } ?>
														</td>
													</tr>
													<?php $no++;
												}
													?>
										</tbody>
									</table>
								</div>
								<?php
							} ?>
					</div>
				</div>
			</div>

	</div>
</div> -->

<div class="modal" id="my_modal">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
          <h4 class="modal-title">Upload File</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/upload_foto" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">
					 <div class="col-md-12">
					 	
					 	<div class="form-group ">
							<div class="col-md-12">
								<label class="control-label text-right">Upload File/Foto
									<br><span class="label red">Format file pdf/jpg/jpeg dan ukuran maksimal 2 Mb.</span></label>
								<input type="hidden"  class="form-control" name="idEvent" value=""/>
								<input type="hidden"  class="form-control" name="awwal" value=""/ readonly>
								<input type="hidden"  class="form-control" name="akhhir" value=""/ readonly>
								<input type="hidden"  class="form-control" name="jenis" value=""/ readonly>
								<input type="file"  class="form-control" name="file_foto">
							</div>
						</div> 
						
					</div>
        	</div>
        </div>
        <div class="modal-footer">
        	<button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Tutup</button>
        </div>
        </form>
      </div>
    </div>
 </div>   

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

 <script type="text/javascript">
	$(document).ready(function() {
  //  Input nominal
    $('.nominal').keyup(function(event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;

        // format number
        $(this).val(function(index, value) {
            return format_number(value);
        });
    });

    function format_number(value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
 //   END Input nominal
	 // start modal
	 $('#my_modal').on('show.bs.modal', function(e) {
	    var awwal = $(e.relatedTarget).data('namakeg');
	    var idEvent = $(e.relatedTarget).data('idkeg');
	    var akhhir = $(e.relatedTarget).data('namaunit');
	    var jenis = $(e.relatedTarget).data('jns');

	    $(e.currentTarget).find('input[name="awwal"]').val(awwal);
	    $(e.currentTarget).find('input[name="idEvent"]').val(idEvent);
	    $(e.currentTarget).find('input[name="akhhir"]').val(akhhir);
	    $(e.currentTarget).find('input[name="jenis"]').val(jenis);
	});

});
</script>

<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 


<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_file  = $(this).data('namfil');
				var nm_folder  = $(this).data('namfol');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/event/'+nm_folder+'/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>