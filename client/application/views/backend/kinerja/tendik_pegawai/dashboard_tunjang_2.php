<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'dashboard'?>"> Home</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Pegawai
							
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/dashboard" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $p_aktif['id_periode']) { print 'selected'; }?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Lihat</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>
	</div>	
	
	<?php
        $hariini = date('Y-m-d'); 
      
						$mulai = tanggal_indonesia($p_aktif['pengisian_mulai']);
						$selesai =tanggal_indonesia($p_aktif['pengisian_selesai']);

						foreach($rekap_log as $rlog){
							$log1 = $rlog['p1'];
							$log2 = $rlog['p2'];
							$log3 = $rlog['p3'];
							$log4 = $rlog['p4'];
						}

						$jml_log = count($periode_log);
    ?>
	
	<div class="row">
		<div class="col-md-8">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
							
				</div>
				<div class="panel-body">
					<?php if (date('Y-m-d') >= $mulai && date('Y-m-d') <= $selesai) { ?>
					<h4>Periode Pengisian : <?php echo $mulai; ?> s.d. <?php echo $selesai; ?></h4>
								<div class="table-responsive">
									<table class="table table-hover">
										<!-- <thead>
											<tr>
												<th style="width:5%" class="text-center">Indikator</th>
												<th style="width:40%" class="text-center">Rincian SKP</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead> -->
										<tbody>
											<?php
											// $no = 1;
											// foreach ($edit as $skp) {
											//     ?>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>I. </h4>
												</td>
												<td style="width:40%" class="text-left"><h4>Pelaksanaan Tugas  Utama </h>
																								</td>
												<td style="width:12.5%" class="text-right">
													Aksi
												</td>
												<td style="width:12.5%" class="text-right">
													Skor
												</td>
												<td style="width:12.5%" class="text-right">
													Bobot
												</td>
												<td style="width:12.5%" class="text-right">
													Nilai
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-left">
												</td>
												<td style="width:40%" class="text-left">
													1. Kesesuaian pekerjaan:
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%" class="text-left">   a. Menyelesaikan pekerjaan dengan benar</td>
												<td style="width:12.5%" class="text-right">
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
													</td>
												<td style="width:12.5%" class="text-right"><?php 
													$jml_log = count($log_book);
													$hari_kerja = $hk['jumlah_hari_kerja'];
													$skor_1 = floor($jml_log/$hari_kerja);
													if(!empty($log_book)){
														$skor_1 = '1';
													} else {
														$skor_1 = '0';
													}
													$bobot_1 = "0.16";
														$nil1 = $skor_1*$bobot_1; 
													?>
													<button class="btn btn-md btn-default"><?php echo $skor_1; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_1;?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $nil1; ?> </button>
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   b. Menyelesaikan pekerjaan tepat waktu</td>
												<td style="width:12.5%" class="text-right">
											
													<?php 
														$jml_log = count($log_book);
													if(!empty($jml_log)){
														$skor_2 = '1';
													} else {
														$skor_2 = '0';
													}

													$bobot_2 = "0.16";
														$nil2 = $skor_1*$bobot_2; 
													?>
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $skor_1; ?></button></td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $bobot_2;?></button> </td>
												<td style="width:12.5%" class="text-right"><button class="btn btn-md btn-default"><?php echo $nil2; ?></button> </td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">   c. Bersedia menjalankan tugas tambahan yang relevan</td>
												<td style="width:12.5%" class="text-right">
													<?php 
														$jml_log = count($log_book);
													if(!empty($jml_log)){
														$skor_3 = '1';
													} else {
														$skor_3 = '0';
													}

													$bobot_3 = "0.14";
														$nil3 = $skor_1*$bobot_3; 
													?>
													<a href="<?php echo base_url() ?>kinerja/log_book/index"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_1; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"> <?php echo $bobot_3; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil3; ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">b. Penilaian Kepuasan Layanan</td>
												<td style="width:12.5%" class="text-right">												
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kuesioner dari atasan langsung dan teman sejawat </td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($kuesioner)){
														$skor_4 = '1';
													} else {
														$skor_4 ='0';
														
													}
													$bobot_4 = "0.20";
														$nil4 = $skor_4*$bobot_4; 
													?>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/kuesioner"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_4; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil4; ?></button> 
												</td>

											</tr>
											<tr>
												<td style="width:10%" class="text-center">
													<h4>II. </h4>
												</td>
												<td style="width:40%"class="text-left"><h4>Core Values </h4></td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left"> 1. Kehadiran Fingerprint </td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($finger)){
														$skor_5 = '1';
													} else {
														$skor_5 ='0';
														
													} 
													$bobot_5 = "0.17";
														$nil5 = $skor_5*$bobot_5; 
													?>
													<button class="btn btn-md btn-default"><i class="mdi mdi-device-access-time"></i></button>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_5; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil5; ?></button> 
												</td>
											</tr>
											<tr>
												<td style="width:10%" class="text-right">
													
												</td>
												<td style="width:40%"class="text-left">2. Menyelesaikan Pekerjaan dengan Tuntas memiliki karakter HATI</td>
												<td style="width:12.5%" class="text-right">
													<?php if(!empty($log_book)){
														$skor_6 = '1';
													} else {
														$skor_6 ='0';
														
													} 
													$bobot_6 = "0.17";
														$nil6 = $skor_6*$bobot_6; 
													?>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/kuesioner"><button class="btn btn-md btn-default"><i class="fa fa-edit"></i> </button></a>
													
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $skor_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $bobot_6; ?></button>
												</td>
												<td style="width:12.5%" class="text-right">
													<button class="btn btn-md btn-default"><?php echo $nil6; ?></button> 
												</td>
											</tr>
											</tr>
												<td style="width:10%" class="text-center"> </td>
												<td style="width:40%"class="text-left">Total Nilai </td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
												</td>
												<td style="width:12.5%" class="text-right">
													<b><?php $tot = $nil1 + $nil2 + $nil3 + $nil4 + $nil5 + $nil6; 
													echo $tot; ?></b>
												</td>
											<tr>
										</tbody>
									</table>
									
								</div>

								<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/dashboard" enctype="multipart/form-data">
								<div class="panel-footer bg-white">
									<?php if($tot < 0.6){ ?>
									<span  class="label red">Syarat penilaian belum terpenuhi, silahkan cek Kembali berkas yang harus di ajukan!</span>	
								<?php } else { ?>
										<span  class="label green">Syarat penilaian sudah terpenuhi, silahkan submit pengajuan </span>
									<?php
								} ?>
									<a href="<?php echo base_url() ?>kinerja/skp/detail/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-md btn-success pull-right"><i class="fa fa-paper-plane"></i> Submit Pengajuan</button></a>
											
								</div>
							</form>
							<?php } else { echo '<h2>Periode Pengisian Penilaian telah berakhir</h2>'; } ?>
				</div>
			</div>
			
		</div>
		<div class="col-md-4">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Biodata Pegawai
							
				</div>
				<div class="panel-body">
					<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">
					
								<h4 class="font-bold text-capitalize no-margin m-b">Data Pegawai</h4>

            <div class="table-responsive">
                <table class="table table-hover" style="border: 1px solid #e7eaec">
                    <tbody>
                    	<tr>
                            <td style="width:30%">Nama</td>
                            <td style="width:70%"><?php echo $nama_doskar; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">NIS</td>
                            <td style="width:70%"><?php echo $nis; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Jabatan</td>
                            <td style="width:70%"><?php echo $jabatan; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Unit</td>
                            <td style="width:70%"><?php echo $nama_unit; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Golongan</td>
                            <td style="width:70%"><?php echo $gol_baru; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">TMT </td>
                            <td style="width:70%"><?php echo $tmt_usm; ?></td>
                        </tr>
                        <tr>
                            <td style="width:30%">Status Kerja</td>
                            <td style="width:70%"><?php echo $status_kerja; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
							</form>
				</div>
			</div>
		</div>
			
	</div>

 
