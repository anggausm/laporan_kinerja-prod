<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/tambah_sk_panitia">Tambah SK Panitia</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> <b>Tambah SK Panitia</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:15%" class="text-center">No</th>
												<th style="width:60%" class="text-center">Rincian SK</th>
												<th style="width:20%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($sk_panitia as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td style="text-align: left;"><?php echo $value['judul_sk'];?> No. <?php echo $value['no_sk'];?>
													</td>
												<td class="text-center">
													<a id="detail_surat" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal-detail" data-namfil="<?php echo $value['file_sk']; ?>" data-namfol="<?php echo $value['folder_sk']; ?>"><i class="fa fa-eye"></i></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_sk_panitia/<?php echo enc_data($value['id_sk']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/hapus_sk/<?php echo enc_data($value['id_sk']) ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus data ini?');"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-edit "></i> <b>Edit SK Panitia</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/update_sk_panitia" enctype="multipart/form-data">
							<input type="hidden" name="jenis_sk" value="panitia" class="form-control">
							<input type="hidden" name="id_peg" value="<?php echo $personel['id_peg_isdm']; ?>" class="form-control">
							<input type="hidden" name="id_sk" value="<?php echo $edit['id_sk']; ?>" class="form-control">
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal SK</label>
	                            <div class="col-sm-9">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD"value="<?php echo $edit['tanggal_sk']; ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Judul SK </label>
								<div class="col-sm-9">
									<input type="text" name="judul" value="<?php echo $edit['judul_sk']; ?>" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor SK </label>
								<div class="col-sm-9">
									<input type="text" name="nomor" value="<?php echo $edit['no_sk']; ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tanggal awal berlaku </label>
								<div class="col-sm-9">
									<input type='date' class="form-control" id='datetimepicker1' name="tmt" data-date-format="YYYY-MM-DD" value="<?php echo $edit['tmt_sk']; ?>" required/>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal akhir berlaku</label>
	                            <div class="col-sm-9">
	                                <input type='date' class="form-control" id='datetimepicker1' name="exp" data-date-format="YYYY-MM-DD" value="<?php echo $edit['exp_sk']; ?>" required/>
	                            </div>
	                        </div>
	                        
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan</label>
								<div class="col-sm-9">
									<input type="text" name="ket" value="<?php echo $edit['ket']; ?>" class="form-control">
								</div>
							</div>

							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Upload File</label>
								<div class="col-sm-9">
									<input type="file" name="filesk" class="form-control">
									<input type="hidden" name="file_lama" class="form-control" value="<?php echo $edit['file_sk']; ?>">
									<input type="hidden" name="folder_lama" class="form-control" value="<?php echo $edit['folder_sk']; ?>">
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Update 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>
<div id="modal-detail" class="modal fade">  
    <div class="modal-dialog modal-lg">  
         <div class="modal-content">  
              <div class="modal-header">  
                   <h4 class="modal-title">View File</h4>  
              </div>  
              <div class="modal-body" id="body_detail_surat"> 
              	
              </div>  
              <div class="modal-footer">  
                   <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>  
              </div>  
         </div>  
    </div>  
</div> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
		$(document).ready(function(){
			$(document).on('click','#detail_surat', function(){
				var nm_file  = $(this).data('namfil');
				var nm_folder  = $(this).data('namfol');
				$("#body_detail_surat").html(
					'<iframe src="<?php echo base_url() ?>file/uploads/sk/'+nm_folder+'/'+nm_file+'" width="100%" height="600px" allowfullscreen></iframe>' );
			})
		})
		</script>