<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/form_event">Tambah Kegiatan</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<div class="alert alert-info" role="alert">
		<h4>Integritas: Presensi fingerprint dan keberadaan ditempat tugasnya sesuai jam kerja, kehadiran pada acara universitas/fakultas</h4>

    		<strong>Ketentuan :</strong> 
    		
	            <li>Tenaga kependidikan mematuhi peraturan tentang presensi fingerprint, menghadiri undangan setiap acara yang diselenggarakan Universitas/Fakultas, termasuk menghadiri Upacara Bendera.</li>
 				<li>Kegiatan ini tidak boleh kosong</li>
	            
	     	<strong>Berkas yang diunggah:</strong> Data ditarik dari sistem

    </div>

	<div class="row">
		<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-list "></i> <b>Data Kegiatan</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:20%" class="text-center">Rincian Event</th>
												<th style="width:15%" class="text-center">Status</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$no = 1;
										foreach	($event_wajib as $value){
											?>
										
										<tr>
												<td class="text-center"><?=$no?></td>
												
												<td style="text-align: left;"><?php echo $value['nama_event'];?><br> 
													<?php echo date("d-F-Y",strtotime($value['tanggal_mulai'])); ?> </td>
												<td>
													<?php if(empty($value['jam_finger'])) {?>
													Tidak ada data presensi fingerprint
												<?php } else { ?>

													Presensi fingerprint pada pukul : <?php echo $value['jam_finger']; ?>
													<?php
												} ?>
												</td>
										</tr>
													<?php $no++;
												}
													?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>

	</div>
</div>