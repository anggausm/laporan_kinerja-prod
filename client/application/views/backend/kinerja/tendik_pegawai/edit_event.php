<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'dashboard'?>"> Home</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/form_event">Tambah Bukti Kegiatan</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Data Kegiatan
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:10%" class="text-center">Tanggal</th>
												<th style="width:25%" class="text-center">Nama Event</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($event as $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td><?php echo date("d-F-Y",strtotime($value['tanggal_sertifikat'])); ?></td>
												<td style="text-align: left;"><?php echo $value['nama_sertifikat'];?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_event/<?php echo enc_data($value['id_event']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/detail/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Unggah bukti foto kegiatan
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/simpan_sertifikat" enctype="multipart/form-data">
							
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor Sertifikat </label>
								<div class="col-sm-9">
									<input type="text" name="nomor" placeholder="Isi Nomor Sertifikat" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Judul Sertifikat </label>
								<div class="col-sm-9">
									<input type="text" name="judul" placeholder="Isi Judul Sertifikat" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Sertifikat</label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl_sert" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal akhir berlaku</label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl_akhir" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Penyelenggara </label>
								<div class="col-sm-9">
									<input type="text" name="penyelenggara" placeholder="Penyelenggara Kegiatan" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Tingkat Sertifikat </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="tingkat" required>
										<option value="">Pilih Tingkat Sertifikat</option>
										<?php foreach ($tingkat as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>">
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
	                        
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan</label>
								<div class="col-sm-9">
									<input type="text" name="keterangan" placeholder="Tuliskan Keterangan" class="form-control">
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>