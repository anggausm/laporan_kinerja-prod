<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/pegawai ">Penilaian Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Penilaian Pegawai
							
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/pegawai/dashboard" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($edit['periode'] == $vl['id_periode']){ print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Lihat</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>
	</div>	
	
	
<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
							
				</div>
				<div class="panel-body">
	<div class="row">
                <div class="col-sm-12">
                	<form class="form" method="POST" action="<?php echo base_url() . $this->link . 'simpan' ?>">
                    <?php 
                    $no_dimensi = 1; ?>
                    <?php foreach ($rincian as $key => $dimensi) { ?>
                        <span class="font-bold text-md m-t"><?php echo $no_dimensi . '. ' . $dimensi['indikator_utama'] ?></span>
                        <?php $no_indikator = a; ?>
                        <?php foreach ($dimensi['id_sub_indikator'] as $key2 => $indikator) { 
                        	?>
                            <ul style="list-style-type: none"><span class="text-sm"><?php echo $no_indikator . '. ' . $indikator['nama_sub_indikator'] ?></span></ul>
                            <?php $no_item = 1; ?>
                            <?php foreach ($indikator['id_ind_penilaian'] as $key3 => $item) { ?>
                                <ol style="list-style-type: none">
                                    <ul style="list-style-type: none">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="font-normal text-muted"><?php echo $no_item . '. ' . $item['rincian_indikator'] ?></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-36">
                                                        <div class="radio-container">
                                                            <label class="md-check m-r">
                                                                <a href="<?php echo base_url() ?>kinerja/skp/edit/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/skp/detail/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </ol>
                                <?php $no_item++; } ?>
                            <?php $no_indikator++; } ?>
                        <?php $no_dimensi++; }?>
                </form>
                </div>
            </div>
				</div>
			</div>
			
		</div>
	</div>			

<!--  <div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
							
				</div>
				<div class="panel-body">
					<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">Indikator</th>
												<th style="width:10%" class="text-center">Rincian SKP</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($edit as $skp) {
											    ?>
											<tr>
												<td class="text-center"><?php print $skp['indikator_utama']; ?></td>
												<td><?php print $skp['rincian_indikator']; ?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/skp/edit/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/skp/detail/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
				</div>
			</div>
			
		</div>
	</div> -->
