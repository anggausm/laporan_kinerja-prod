<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai">Dashboard Penilaian Kinerja</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/pegawai/tambah_sk">Tambah SK</a></li>
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/pegawai"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Tambah SK
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:25%" class="text-center">Jenis SK</th>
												<th style="width:25%" class="text-center">Rincian SK</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($sk as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td style="text-align: left;"><?php echo $value['jenis_sk'];?></td>
												<td style="text-align: left;">SK NO . <?php echo $value['nomor_sk'];?> tentang <?php echo $value['judul_sk'];?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/tendik/pegawai/edit_sk/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<!-- <a href="<?php echo base_url() ?>kinerja/tendik/pegawai/detail/<?php echo enc_data($value['id_pegawai']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-list"></i> </button></a> -->
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> Tambah sk
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/pegawai/simpan_sk" enctype="multipart/form-data">
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Jenis SK</label>
								<div class="col-sm-9">
									<select class="form-control select2" name="jenis" required>
										<option value="">Pilih Jenis SK</option>
										<?php foreach ($jenis_sk as $stat): ?>
										<option value="<?php echo $stat['id_stat'] ?>">
											<?php echo $stat['nama_stat'] ?>
										</option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal sk</label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker1' name="tgl" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Judul sk </label>
								<div class="col-sm-9">
									<input type="text" name="judul" placeholder="Judul SK" class="form-control">
								</div>
							</div>
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor sk </label>
								<div class="col-sm-9">
									<input type="text" name="nomor" placeholder="Tuliskan Nomor SK" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Tanggal berlaku </label>
								<div class="col-sm-9">
									<input type='text' class="form-control" id='datetimepicker1' name="tmt" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
								</div>
							</div>
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal sk</label>
	                            <div class="col-sm-2">
	                                <input type='text' class="form-control" id='datetimepicker1' name="exp" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
	                        
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">File SK</label>
								<div class="col-sm-9">
									<input type="text" name="filesk" placeholder="Tuliskan Keterangan atau Tugas Tambahan" class="form-control">
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>