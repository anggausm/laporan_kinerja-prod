<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/skp"><?php print $this->judul;?></a></li>
				
					<span  style="float: right;">
						<a href="<?php print base_url()?>kinerja/tendik/skp"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-edit "></i> Edit <?php print $this->judul;?>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/skp/update" enctype="multipart/form-data">
							<input type= "hidden" name="id_skp" value="<?= $edit['id_skp'] ?>" class="form-control">
							<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Periode</label>
	                            <div class="col-sm-4">
	                                <select class="form-control select2" name="periode" required>
											<option value="">Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($edit['periode'] == $vl['id_periode']) { print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Jenis Pegawai</label>
	                            <div class="col-sm-4">
	                                <select class="form-control select2" name="id_jp" required>
											<option value="">Pilih Jenis Pegawai</option>
										<?php
										foreach ($jenis_peg  as $vl) {?>
											<option value="<?= $vl['id_jenis_pegawai'] ?>" <?php if($edit['jenis_pegawai'] == $vl['id_jenis_pegawai']) { print 'selected'; } ?>><?= $vl['nama_jenis_pegawai'] ?> (<?= $vl['deskripsi'] ?>) </option>
										<?php }?>
										</select>
	                            </div>
	                        </div>
	                        
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Indikator Utama</label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ind_utama" required>
											<option value="">Pilih Indikator</option>
										<?php
										foreach ($iu  as $vl) {?>
											<option value="<?= $vl['id_indikator_utama'] ?>" <?php if($edit['id_indikator_utama'] == $vl['id_indikator_utama']) { print 'selected'; } ?>><?= $vl['indikator_utama'] ?></option>
										<?php }?>
										</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Sub Indikator </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="sub_ind">
											<option value="">Pilih Sub Indikator</option>
										<?php
										foreach ($sip  as $vl) {?>
											<option value="<?= $vl['id_sub_indikator'] ?>" <?php if($edit['id_sub_indikator'] == $vl['id_sub_indikator']) { print 'selected'; } ?>><?= $vl['nama_sub_indikator'] ?></option>
										<?php }?>
										</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Indikator Penilaian </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ind_penilaian">
											<option value="">Pilih Penilaian</option>
										<?php
										foreach ($ip  as $vl) {?>
											<option value="<?= $vl['id'] ?>" <?php if($edit['id_ind_penilaian'] == $vl['id']) { print 'selected'; } ?>><?= $vl['nama_ind_penilaian'] ?></option>
										<?php }?>
										</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Rincian Indikator </label>
								<div class="col-sm-9">
									<input type= "text" name="rincian_ind" value="<?= $edit['rincian_indikator'] ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Urutan</label>
								<div class="col-sm-9">
									<input type= "text" name="urutan" placeholder="Ex. : 10" value="<?= $edit['urutan'] ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nomor Urut </label>
								<div class="col-sm-9">
									<input type= "text" name="no_urut" placeholder="Ex. : 1.2.13" value="<?= $edit['no_urut'] ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Syarat Berkas </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="prasyarat[]">
											<option value="">Pilih Berkas</option>
										<?php
										foreach ($berkas  as $vl) {?>
											<option value="<?= $vl['id'] ?>"><?= $vl['nama_ind_penilaian'] ?></option>
										<?php }?>
										</select>
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Bobot </label>
								<div class="col-sm-9">
									<input type= "text" name="bobot" value="<?= $edit['bobot'] ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Skor </label>
								<div class="col-sm-9">
									<input type= "text" name="skor" value="<?= $edit['skor'] ?>" class="form-control">
								</div>
							</div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right">Keterangan</label>
								<div class="col-sm-9">
									<textarea name="keterangan" value="<?= $edit['ket'] ?>" class="form-control" rows="5" ></textarea>
								</div>
							</div>
							<hr>
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</div>