<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/skp ">Satuan Kinerja Pegawai</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<?php 
	foreach ($datas as $key => $dt) {
		$period = $dt['periode'];
		$jns_peg = $dt['jenis_pegawai'];
	}
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  Satuan Kinerja Pegawai
					<a class="pull-right" href="<?php echo base_url() ?>kinerja/tendik/skp/tambah"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/skp/data" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $period) { print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Jenis Pegawai </label>
									<div class="col-md-6">
										<select class="form-control select2" name="jenis_peg" required>
											<option>Pilih Jenis Pegawai</option>
										<?php
										foreach ($jenis_peg  as $val) {?>
											<option value="<?= $val['id_jenis_pegawai'] ?>" <?php if($val['id_jenis_pegawai'] == $jns_peg) { print 'selected'; } ?>><?= $val['nama_jenis_pegawai'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari </button>
							</div>
						</div>
					</form>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:15%" class="text-center">Indikator utama</th>
												<th style="width:10%" class="text-center">Sub Indikator</th>
												<th style="width:15%" class="text-center">Penilaian Ind</th>
												<th style="width:25%" class="text-center">Rincian</th>
												<th style="width:10%" class="text-center">No Urut</th>
												<th style="width:10%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$no = 1;
											foreach ($datas as $key => $value) {
											    ?>
											<tr>
												<td class="text-center"><?=$no?></td>
												<td style="text-align: left;"><?php echo $value['indikator_utama'];?></td>
												<td><?php echo $value['nama_sub_indikator'];?></td>
												<td style="text-align: left;"><?php echo $value['nama_ind_penilaian'];?></td>
												<td class="text-left"><?php echo $value['rincian_indikator']; ?></td>
												<td class="text-left"><?php echo $value['no_urut']; ?></td>
												<td class="text-center">
													<a href="<?php echo base_url() ?>kinerja/tendik/skp/edit/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
													<a href="<?php echo base_url() ?>kinerja/tendik/skp/hapus/<?php echo enc_data($value['id_skp']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
												</td>
											</tr>
											<?php
											$no++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>