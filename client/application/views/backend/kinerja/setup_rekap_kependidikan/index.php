<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url() ?>">Rekap Kependidikan</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <?php if (!empty($this->session->flashdata('pesan'))) : ?>
        <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
            <?php print $this->session->flashdata('pesan') ?>
        </div>
    <?php endif ?>

    <?php if (
        !empty($kinerja_akses['id_akses'])
        || $is_akses_univ === true
    ) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-white">
                        <i class="fa fa-list"></i> <b>Rekap Kependidikan</b>
                    </div>
                    <div class="panel-body">
                        <form action="<?= base_url($this->link) ?>" class="form-inline" method="post">
                            <select class="form-control select2" name="fak_unit" style="width: 20%;">
                                <option value="">Pilih Unit</option>
                                <?php foreach ($unit as $key => $value) : ?>
                                    <option value="<?php echo $value['fak_unit']; ?>" <?php echo $value['fak_unit'] == $fak_unit ? 'selected' : ''; ?>><?php echo $value['nama_unit']; ?></option>
                                <?php endforeach ?>
                            </select>
                            <input type="hidden" name="nama_unit" value="<?php echo $nama_unit; ?>">
                            <button type="submit" name="submit" value="" class="btn btn-sm btn-primary">Tampilkan</button>
                            <?php if ($is_akses_univ === true) : ?>
                                <button type="submit" name="submit" value="excel" class="btn btn-sm btn-success">Download Excel</button>
                            <?php endif ?>
                            <button type="submit" name="submit" value="pdf" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i> Download PDF</button>
                        </form>
                        <div class="table-responsive">
                            <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='25'>
                                <thead>
                                    <tr class="text-center">
                                        <th rowspan="2" class="text-center">NO</th>
                                        <th rowspan="2" class="text-center">NAMA</th>
                                        <th colspan="5" class="text-center">PENILAIAN</th>
                                        <th rowspan="2" class="text-center">NILAI</th>
                                        <th rowspan="2" class="text-center">GRADE</th>
                                        <th rowspan="2" class="text-center">KINERJA (%)</th>
                                    </tr>
                                    <tr class="text-center">
                                        <th class="text-center">DIRI SENDIRI</th>
                                        <th class="text-center">REKAN 1</th>
                                        <th class="text-center">REKAN 2</th>
                                        <th class="text-center">REKAN 3</th>
                                        <th class="text-center">PIMPINAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($filter_doskar as $key => $value) : ?>
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td>
                                                <strong><?= $value['nama_doskar']; ?></strong>
                                                <br>
                                                <small><?= $value['nama_unit']; ?></small>
                                            </td>
                                            <td class="text-center"> <?= $value['nilai_diri']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_rekan_1']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_rekan_2']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_rekan_3']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan']; ?> </td>
                                            <td class="text-center"> <?= $value['total_nilai']; ?> </td>
                                            <td class="text-center"> <?= $value['grade']; ?> </td>
                                            <td class="text-center"> <?= $value['kinerja']; ?> </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
<script>
    $(document).ready(function() {
        $('.select2').change(function() {
            var text = $('.select2').find(':selected').text();
            $('input[name="nama_unit"]').val(text);
        });
    });
</script>