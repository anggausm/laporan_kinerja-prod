<html>

<head>
    <style>
        @page {
            margin-top: 1cm;
            margin-left: 1cm;
            margin-right: 1cm;
            footer: html_MyFooter1;
        }

        body {
            font-family: ctimes;
            font-size: 10pt;
        }

        .table-80 {
            width: 80%;
        }

        .table {
            width: 100%;
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border: 1px solid black;
            border-collapse: collapse;
            overflow: wrap;
        }

        .font-weight-bold {
            font-weight: bold;
        }

        .text-left {
            text-align: left;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .m-0 {
            margin: 0;
        }

        .float-left {
            float: left;
        }

        .float-right {
            float: right;
        }

        .text-underline {
            text-decoration: underline;
        }
    </style>
</head>

<body>
    <htmlpagefooter name="MyFooter1">
        <table width="100%" style="vertical-align: bottom; font-size: 10pt;">
            <tr>
                <td width="33%">
                    <!-- <span>{DATE j-m-Y}</span> -->
                </td>
                <td width="33%" align="center">
                    <!-- {PAGENO}/{nbpg} -->
                </td>
                <td width="33%" class="text-right"></td>
            </tr>
        </table>
    </htmlpagefooter>
    <p class="font-weight-bold">
        REKAP PENILAIAN KINERJA
        <br>TENAGA KEPENDIDIKAN DAN TENAGA PENUNJANG
        <br><?php echo strtoupper($kinerja_akses['nama_unit']); ?>
        <br>PERIODE 1 DESEMBER 2023 SAMPAI 31 MEI 2024
    </p>
    <table class="table table-bordered">
        <tr>
            <th rowspan="2" width="5%" class="text-center">NO</th>
            <th rowspan="2" width="30%" class="text-center">NAMA</th>
            <th colspan="5" class="text-center">PENILAIAN</th>
            <th rowspan="2" class="text-center">NILAI</th>
            <th rowspan="2" class="text-center">GRADE</th>
            <th rowspan="2" class="text-center">KINERJA</th>
        </tr>
        <tr>
            <th text-rotate="90" class="text-center">DIRI SENDIRI</th>
            <th text-rotate="90" class="text-center">REKAN KERJA 1</th>
            <th text-rotate="90" class="text-center">REKAN KERJA 2</th>
            <th text-rotate="90" class="text-center">REKAN KERJA 3</th>
            <th text-rotate="90" class="text-center">PIMPINAN</th>
        </tr>
        <?php $no = 1; ?>
        <?php foreach ($filter_doskar as $key => $value) : ?>
            <tr>
                <td class="text-center"><?= $no++ ?></td>
                <td>
                    <span><?= $value['nama_doskar']; ?></span>
                </td>
                <td class="text-center"> <?= $value['nilai_diri']; ?> </td>
                <td class="text-center"> <?= $value['nilai_rekan_1']; ?> </td>
                <td class="text-center"> <?= $value['nilai_rekan_2']; ?> </td>
                <td class="text-center"> <?= $value['nilai_rekan_3']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan']; ?> </td>
                <td class="text-center"> <?= $value['total_nilai']; ?> </td>
                <td class="text-center"> <?= $value['grade']; ?> </td>
                <td class="text-center"> <?= $value['kinerja']; ?> </td>
            </tr>
        <?php endforeach ?>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="50%">
                <?php if (!empty($kinerja_akses['nama_mengetahui'])) : ?>
                    <p style="margin-top: 10px;">
                        <br>
                        Mengetahui, <br>
                        <?php echo $kinerja_akses['jabatan_mengetahui']; ?>
                        <br><br><br><br><br>
                        <?php echo $kinerja_akses['nama_mengetahui']; ?>
                        <br><?php echo $kinerja_akses['nis_mengetahui']; ?>
                    </p>
                <?php endif ?>
            </td>
            <td width="50%">
                <p style="margin-top: 10px;">
                    Semarang, <?= tanggal_indonesia(date('Y-m-d')); ?> <br>
                    Menyetujui, <br>
                    <?php echo $kinerja_akses['jabatan_menyetujui']; ?>
                    <br><br><br><br><br>
                    <?php echo $kinerja_akses['nama_menyetujui']; ?>
                    <br><?php echo $kinerja_akses['nis_menyetujui']; ?>
                </p>
            </td>
        </tr>
    </table>
</body>

</html>