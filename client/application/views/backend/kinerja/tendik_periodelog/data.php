<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/periode_log "><?php echo $this->judul; ?></a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>

	<?php 
	foreach ($datas as $key => $dt) {
		$period = $dt['periode'];
		$jns_peg = $dt['jenis_pegawai'];
	}
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <?php echo $this->judul; ?>
					
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/periode_log/data" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option>Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>" <?php if($vl['id_periode'] == $id_periode) { print 'selected'; } ?>><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari </button>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
					<div class="row">
						<div class="col-md-8">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="btn-groups">
										<div class="table-responsive">
											<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
												<thead>
													<tr>
														<th style="width:5%" class="text-center">No</th>
														<th style="width:10%" class="text-center">Nama Log</th>
														<th style="width:10%" class="text-center">Tanggal Awal</th>
														<th style="width:10%" class="text-center">Tanggal Akhir</th>
														<th style="width:5%" class="text-center">Status</th>
														<th style="width:5%" class="text-center">Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php
													$no = 1;
													foreach ($default as $value) {
													    ?>
													<tr>
														<td class="text-center"><?=$no?></td>
														<td style="text-align: left;">Minggu ke-<?php echo $value['nama_log'];?></td>
														<td style="text-align: center;"><?php echo date("d-M-Y",strtotime($value['tgl_awal']));?></td>
														<td style="text-align: center;"><?php echo date("d-M-Y",strtotime($value['tgl_akhir']));?></td>
														<td class="text-center"><?php 

																if($value['status'] == 1){ echo 'Aktif';} else {
																	echo 'Tidak Aktif';
																} ?></td>
														<td class="text-center">
															<a href="<?php echo base_url() ?>kinerja/tendik/periode_log/edit/<?php echo enc_data($value['periode_kinerja']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-edit"></i> </button></a>
															<a href="<?php echo base_url() ?>kinerja/tendik/periode_log/hapus/<?php echo enc_data($value['periode_kinerja']) ?>"><button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> </button></a>
														</td>
													</tr>
													<?php
													$no++;
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel panel-default">
					<div class="panel-heading bg-white">
						<i class="fa  fa-plus "></i> <b>Tambah Periode Log</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/periode_log/simpan" enctype="multipart/form-data">
							
	                        <div class="form-group ">
								<label class="col-sm-3 control-label text-right">Nama Periode</label>
								<div class="col-sm-9">
									<input type="hidden" name="id_periode" value="<?php echo $id_periode; ?>" class="form-control"></textarea>
									<input type="text" name="nama_periode" placeholder="Isikan angka. Cth: 1" class="form-control"></textarea>
								</div>
							</div>
						<div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Mulai</label>
	                            <div class="col-sm-4">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl_awal" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label for="" class="col-sm-3 control-label">Tanggal Selesai</label>
	                            <div class="col-sm-4">
	                                <input type='date' class="form-control" id='datetimepicker1' name="tgl_akhir" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo date('Y-m-d'); ?>" required/>
	                            </div>
	                        </div>
							<div class="form-group ">
								<label class="col-sm-3 control-label text-right"> Status </label>
								<div class="col-sm-9">
									<select class="form-control select2" name="ket" required>
										<option value="">Pilih Status</option>
										<option value="1">Aktif</option>
										<option value="0">Tidak Aktif</option>
									</select>
								</div>
							</div>
							<hr>
							<div class="col-sm-4"></div> 
							<div class="col-sm-4">
								<button class="btn btn-info btn-sm">
									<i class="fa fa-save"></i> Simpan 
								</button>
							</div>
						</form>
					</div>
				</div>
						</div>
					</div>
</div>