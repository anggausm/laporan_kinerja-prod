<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kinerja/tendik/periode_log "><?php echo $this->judul; ?></a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if ($this->session->flashdata('pesan')): ?>
    <div class="alert alert-success text-center" role="alert">
        <?php print $this->session->flashdata('pesan')?>
    </div>
	    <?php elseif ($this->session->flashdata('error')): ?>
	        <div class="alert alert-danger text-center" role="alert">
	            <?php print $this->session->flashdata('error')?> 
	        </div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-list"></i>  <?php echo $this->judul; ?>
					<a class="pull-right" href="<?php echo base_url() ?>kinerja/tendik/periode_log/tambah"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</button></a>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>kinerja/tendik/periode_log/data" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-6 control-label text-right">Periode </label>
									<div class="col-md-6">
										<select class="form-control select2" name="periode" required>
											<option value="">Pilih Periode</option>
										<?php
										foreach ($periode  as $vl) {?>
											<option value="<?= $vl['id_periode'] ?>"><?= $vl['nama_periode'] ?></option>
										<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari </button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>