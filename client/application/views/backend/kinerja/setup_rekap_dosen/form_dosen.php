<style>
    .custom-table,
    .custom-table th,
    .custom-table td {
        padding: 2px !important;
        font-size: 12px;
    }
</style>
<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url($this->link) ?>">Penilaian PIMPINAN</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <?php if (!empty($this->session->flashdata('pesan'))) : ?>
        <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
            <?php print $this->session->flashdata('pesan') ?>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-white">
                    <i class="fa fa-list"></i> <b>Penilaian PIMPINAN</b>
                </div>
                <div class="panel-body" style="padding: 5px !important;">
                    <?php if (in_array($cek_token['username'], array('angga'))) : ?>
                        <a onclick="return confirm('Apakah anda akan mengajukan data ini.');" href="<?php echo base_url($this->link . 'ajukan/' . enc_data_url($profil['user_key'])); ?>" class="btn btn-primary btn-sm pull-right"><i class="fa fa-check"></i> Ajukan</a>
                    <?php endif ?>
                    <form class="form-horizontal p-h-xs">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="text-center font-bold">FORM PENILAIAN KINERJA DARI PIMPINAN</h4>
                                <h4 class="text-center font-bold">UNTUK DOSEN</h4>
                                <h5 class="text-center font-bold">PERIODE 1 DESEMBER 2023 s.d 30 MEI 2024</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">NIDN </label>
                                    <label class="col-sm-8 control-label text-right">: <?= $profil['nidn']; ?></label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">Nama </label>
                                    <label class="col-sm-8 control-label text-right">: <?= $profil['nama_doskar']; ?> </label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">Fakultas </label>
                                    <label class="col-sm-8 control-label text-right">: <?= $profil['nama_unit1']; ?> </label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">KRITERIA PENILAIAN DOSEN</label>
                                    <label class="col-sm-8 control-label text-right">
                                        <table width="100%" class="table-bordered">
                                            <tr>
                                                <td class="text-center">NILAI</td>
                                                <td class="text-center">SCORE</td>
                                                <td class="text-center">KATEGORI</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">A</td>
                                                <td class="text-center">> 35</td>
                                                <td class="text-center">100%</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">B</td>
                                                <td class="text-center">26 - 35</td>
                                                <td class="text-center">75%</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">C</td>
                                                <td class="text-center">15 - 25</td>
                                                <td class="text-center">50%</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">D</td>
                                                <td class="text-center">
                                                    < 15 </td>
                                                <td class="text-center">25%</td>
                                            </tr>
                                        </table>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">Pekerjaan</label>
                                    <label class="col-sm-8 control-label text-right">: <?= $profil['nama_jabatan']; ?> </label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">TMT USM</label>
                                    <label class="col-sm-8 control-label text-right">: <?= tanggal_indonesia($profil['tmt_usm']); ?> </label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">POIN DOSEN</label>
                                    <label class="col-sm-8 control-label text-right">: <span class="total_poin"></span></label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">POIN FAKULTAS</label>
                                    <label class="col-sm-8 control-label text-right">: <span class="total_poin_pimpinan"></span></label>
                                </div>
                                <div class="">
                                    <label class="col-sm-4 control-label text-right">POIN UNIVERSITAS</label>
                                    <label class="col-sm-8 control-label text-right">: <span class="total_poin_univ"></span></label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="table-responsive">
                        <form id="myForm" class="form-horizontal p-h-xs" action="<?= base_url($this->link . 'simpan') ?>" method="POST">
                            <input type="hidden" name="jenis_pekerjaan" value="<?php echo enc_data_url($jenis_pekerjaan); ?>">
                            <input type="hidden" name="dinilai_user_key" value="<?php echo enc_data_url($user_key); ?>">
                            <table class="table b-t b-b table-bordered custom-table">
                                <thead>
                                    <tr>
                                        <th width="3%" rowspan="2" class="text-center">NO</th>
                                        <th width="15%" rowspan="2" class="text-center">KOMPONEN PENILAIAN</th>
                                        <th width="3%" rowspan="2" class="text-center">SKOR</th>
                                        <th width="5%" colspan="2">DOSEN</th>
                                        <th width="5%" colspan="2">FAKULTAS</th>
                                        <th width="5%" colspan="2">UNIVERSITAS</th>
                                        <th width="10%" class="text-center">BUKTI LINK DOKUMEN & Keterangan</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center" style="font-size: 11px;">JUMLAH</th>
                                        <th class="text-center" style="font-size: 11px;">NILAI</th>
                                        <th class="text-center" style="font-size: 11px;">JUMLAH</th>
                                        <th class="text-center" style="font-size: 11px;">NILAI</th>
                                        <th class="text-center" style="font-size: 11px;">JUMLAH</th>
                                        <th class="text-center" style="font-size: 11px;">NILAI</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tanya as $key => $value) : ?>
                                        <tr>
                                            <td class="text-center"><?php echo $value['urutan']; ?></td>
                                            <td <?php echo $value['value'] == '0' ? 'colspan="9"' : ''; ?>>
                                                <?php if ($value['value'] == '0') : ?>
                                                    <strong><?= $value['pertanyaan']; ?></strong>
                                                <?php else : ?>
                                                    <?= $value['pertanyaan']; ?>
                                                <?php endif ?>
                                            </td>
                                            <?php if ($value['value'] == '1') : ?>
                                                <td class="text-left">
                                                    <input type="hidden" name="<?php echo "kinerja_nilai[$key][id_nilai]"; ?>" value="<?= enc_data($value['id_nilai']) ?>">
                                                    <input type="hidden" name="<?php echo "kinerja_nilai[$key][id_pertanyaan]"; ?>" value="<?= enc_data($value['id_pertanyaan']) ?>">
                                                    <input type="hidden" name="<?php echo "kinerja_nilai[$key][skor]"; ?>" value="<?= enc_data($value['skor']) ?>">
                                                    <?php echo $value['skor']; ?>
                                                    <input type="hidden" id="<?php echo "skor_" . $value['id_pertanyaan']; ?>" value="<?= $value['skor'] ?>">
                                                </td>
                                                <td class="text-left"><?= $value['jumlah'] ?></td>
                                                <td class="text-left"><span class="nilai"><?php echo $value['nilai']; ?></span></td>
                                                <td class="text-left">
                                                    <?= $value['jumlah_pimpinan'] ?>
                                                </td>
                                                <td class="text-left">
                                                    <span class="nilai_pimpinan" id="<?php echo "nilai_pimpinan_" . $value['id_pertanyaan']; ?>"><?php echo $value['nilai_pimpinan']; ?></span>
                                                </td>
                                                <td class="text-left">
                                                    <?= $value['jumlah_univ'] ?>
                                                </td>
                                                <td class="text-left">
                                                    <span class="nilai_univ" id="<?php echo "nilai_univ_" . $value['id_pertanyaan']; ?>"><?php echo $value['nilai_univ']; ?></span>
                                                </td>
                                                <td>
                                                    Link :
                                                    <?php if (strpos($value['link_dokumen'], 'http') !== false) : ?>
                                                        <?php
                                                        $pattern = '/(?=http)/'; // Lookahead assertion for 'http' or 'https'
                                                        $value['link_dokumen'] = str_replace(' ', '', $value['link_dokumen']);
                                                        $parts = preg_split($pattern, $value['link_dokumen'], -1, PREG_SPLIT_NO_EMPTY);

                                                        // Add 'http' or 'https' back to the start of each part
                                                        foreach ($parts as &$part) {
                                                            if (substr($part, 0, 4) === 'http') {
                                                                $part =  $part;
                                                            }
                                                        };
                                                        ?>
                                                        <?php foreach ($parts as $key2 => $val2) : ?>
                                                            <a href="<?php echo $val2; ?>" target="_blank" class="text-primary small" style="text-decoration: underline; word-break: break-all;"><?php echo $val2; ?></a>
                                                        <?php endforeach ?>
                                                    <?php else : ?>
                                                        <?php echo $value['link_dokumen']; ?>
                                                    <?php endif ?>
                                                    <br>Keterangan : <?php echo $value['keterangan']; ?>
                                                </td>
                                            <?php endif ?>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        hitung_total_poin_univ();
        hitung_total_poin_pimpinan();
        hitung_total_poin();

        function hitung_total_poin_pimpinan() {
            var nilaiElements = $('.nilai_pimpinan');
            var sum = 0;
            nilaiElements.each(function() {
                sum += parseFloat($(this).text()) || 0;
            });
            $('.total_poin_pimpinan').text(sum);
        }

        function hitung_total_poin_univ() {
            var nilaiElements = $('.nilai_univ');
            var sum = 0;
            nilaiElements.each(function() {
                sum += parseFloat($(this).text()) || 0;
            });
            $('.total_poin_univ').text(sum);
        }

        function hitung_total_poin() {
            var nilaiElements = $('.nilai');
            var sum = 0;
            nilaiElements.each(function() {
                sum += parseFloat($(this).text()) || 0;
            });
            $('.total_poin').text(sum);
        }

    });
</script>