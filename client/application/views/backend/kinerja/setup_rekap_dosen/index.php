<style>
    th.rotate {
        height: 100px;
        /* Adjust based on your design */
        white-space: nowrap;
    }

    th.rotate>div {
        transform: translate(0px, 9px) rotate(270deg);
        width: 1px;
        /* Adjust based on your design */
    }

    th.rotate>div>span {
        padding: 10px 5px;
        font-weight: bold;
    }
</style>
<div id="app">
    <div class="row">
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app' ?>"> Portal</a></li>
                    <li class="breadcrumb-item"><a href="<?php print base_url() ?>">Rekap Dosen</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <?php if (!empty($this->session->flashdata('pesan'))) : ?>
        <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
            <?php print $this->session->flashdata('pesan') ?>
        </div>
    <?php endif ?>

    <?php if (
        !empty($kinerja_akses['id_akses'])
        || $is_akses_univ === true
    ) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-white">
                        <i class="fa fa-list"></i> <b>Rekap Dosen</b>
                    </div>
                    <div class="panel-body">
                        <form action="<?= base_url($this->link) ?>" class="form-inline" method="post">
                            <select class="form-control select2" name="id_unit" style="width: 20%;">
                                <option value="">Pilih Unit</option>
                                <?php foreach ($unit as $key => $value) : ?>
                                    <option value="<?php echo $value['id_unit']; ?>" <?php echo $value['id_unit'] == $id_unit ? 'selected' : ''; ?>><?php echo $value['nama_unit']; ?> </option>
                                <?php endforeach ?>
                            </select>
                            <input type="hidden" name="nama_unit" value="<?php echo $nama_unit; ?>">
                            <button type="submit" name="submit" value="" class="btn btn-sm btn-primary">Tampilkan</button>
                            <?php if ($is_akses_univ === true) : ?>
                                <button type="submit" name="submit" value="excel" class="btn btn-sm btn-success">Download Excel</button>
                            <?php endif ?>
                            <button type="submit" name="submit" value="pdf" class="btn btn-sm btn-danger"><i class="fa fa-file-pdf-o"></i> Download PDF</button>
                            <?php if (in_array($cek_token['username'], array('angga'))) : ?>
                                <a href="<?php echo base_url($this->link . 'cek_link'); ?>" class="btn btn-sm btn-primary" target="_blank">Cek Link</a>
                            <?php endif ?>
                        </form>
                        <div class="table-responsive">
                            <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{ ordering:false  }" data-page-length='25'>
                                <thead>
                                    <tr>
                                        <th rowspan="2" width="3%" class="text-center">NO</th>
                                        <th rowspan="2" width="15%" class="text-center">NAMA</th>
                                        <th colspan="8" class="text-center">FAKULTAS</th>
                                        <th rowspan="2" class="text-center">KINERJA</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center rotate">
                                            <div><span>AKADEMIK</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>PENDIDIKAN</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>PENELITIAN</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>PKM</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>PENUNJANNG</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>CORE VALUE</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>NILAI</span></div>
                                        </th>
                                        <th class="text-center rotate">
                                            <div><span>GRADE</span></div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($filter_doskar as $key => $value) : ?>
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td>
                                                <?php if ($is_akses_univ) : ?>
                                                    <a style="text-decoration: underline;" href="<?php echo base_url($this->link . 'detail_pertanyaan/' . enc_data_url($value['user_key'])); ?>"><span class="font-bold"><?= $value['nama_doskar']; ?></span></a>
                                                <?php else : ?>
                                                    <span class="font-bold"><?= $value['nama_doskar']; ?></span>
                                                <?php endif ?>
                                                <br><small><?= $value['nama_prodi']; ?> </small>
                                            </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_akademik']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_pendidikan']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_penelitian']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_pkm']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_penunjang']; ?> </td>
                                            <td class="text-center"> <?= $value['nilai_pimpinan_core_value']; ?> </td>
                                            <td class="text-center"> <?= $value['total_pimpinan_nilai']; ?> </td>
                                            <td class="text-center"> <?= $value['grade_pimpinan']; ?> </td>
                                            <td class="text-center"> <?= $value['kinerja_pimpinan']; ?> </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
<script>
    $(document).ready(function() {
        $('.select2').change(function() {
            var text = $('.select2').find(':selected').text();
            $('input[name="nama_unit"]').val(text);
        });
    });
</script>