<html>

<head>
    <style>
        @page {
            margin-top: 1cm;
            margin-left: 1cm;
            margin-right: 1cm;
            footer: html_MyFooter1;
        }

        body {
            font-family: ctimes;
            font-size: 10pt;
        }

        .table-80 {
            width: 80%;
        }

        .table {
            width: 100%;
        }

        .table-bordered,
        .table-bordered th,
        .table-bordered td {
            border: 1px solid black;
            border-collapse: collapse;
            overflow: wrap;
        }

        .font-weight-bold {
            font-weight: bold;
        }

        .text-left {
            text-align: left;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .m-0 {
            margin: 0;
        }

        .float-left {
            float: left;
        }

        .float-right {
            float: right;
        }

        .text-underline {
            text-decoration: underline;
        }
    </style>
</head>

<body>
    <htmlpagefooter name="MyFooter1">
        <table width="100%" style="vertical-align: bottom; font-size: 10pt;">
            <tr>
                <td width="33%">
                    <!-- <span>{DATE j-m-Y}</span> -->
                </td>
                <td width="33%" align="center">
                    <!-- {PAGENO}/{nbpg} -->
                </td>
                <td width="33%" class="text-right"></td>
            </tr>
        </table>
    </htmlpagefooter>
    <p class="font-weight-bold">
        REKAP PENILAIAN KINERJA
        <br>DOSEN
        <br><?php echo strtoupper($kinerja_akses['nama_unit']); ?>
        <br>PERIODE 1 DESEMBER 2023 SAMPAI 31 MEI 2024
    </p>
    <table class="table table-bordered">
        <tr>
            <th width="5%" class="text-center">NO</th>
            <th width="30%" class="text-center">NAMA</th>
            <th text-rotate="90" class="text-center">AKADEMIK</th>
            <th text-rotate="90" class="text-center">PENDIDIKAN</th>
            <th text-rotate="90" class="text-center">PENELITIAN</th>
            <th text-rotate="90" class="text-center">PKM</th>
            <th text-rotate="90" class="text-center">PENUNJANNG</th>
            <th text-rotate="90" class="text-center">CORE VALUE</th>
            <th class="text-center">NILAI</th>
            <th class="text-center">GRADE</th>
            <th class="text-center">KINERJA</th>
        </tr>
        <?php $no = 1; ?>
        <?php foreach ($filter_doskar as $key => $value) : ?>
            <tr>
                <td class="text-center"><?= $no++ ?></td>
                <td>
                    <span><?= $value['nama_doskar']; ?></span>
                </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_akademik']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_pendidikan']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_penelitian']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_pkm']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_penunjang']; ?> </td>
                <td class="text-center"> <?= $value['nilai_pimpinan_core_value']; ?> </td>
                <td class="text-center"> <?= $value['total_pimpinan_nilai']; ?> </td>
                <td class="text-center"> <?= $value['grade_pimpinan']; ?> </td>
                <td class="text-center"> <?= $value['kinerja_pimpinan']; ?> </td>
            </tr>
        <?php endforeach ?>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="50%">
                <?php if (!empty($kinerja_akses['nama_mengetahui'])) : ?>
                    <p style="margin-top: 10px;">
                        Mengetahui, <br>
                        <?php echo $kinerja_akses['jabatan_mengetahui']; ?>
                        <br><br><br><br><br>
                        <?php echo $kinerja_akses['nama_mengetahui']; ?>
                        <br><?php echo $kinerja_akses['nis_mengetahui']; ?>
                    </p>
                <?php endif ?>
            </td>
            <td width="50%">
                <p style="margin-top: 10px;">
                    Menyetujui, <br>
                    <?php echo $kinerja_akses['jabatan_menyetujui']; ?>
                    <br><br><br><br><br>
                    <?php echo $kinerja_akses['nama_menyetujui']; ?>
                    <br><?php echo $kinerja_akses['nis_menyetujui']; ?>
                </p>
            </td>
        </tr>
    </table>
</body>

</html>