<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Tugas Belajar</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-6">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Tugas Belajar
                    </div>
                </div>
                <div class="col-sm-6">
                    <!-- <form action="<?= base_url($this->link) ?>tambah" class="form-inline text-right"  method='post'>
                        <button type="submit" class="btn btn-sm btn-info" name="button">Tambah Mesin</button>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-8">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>NAMA DOSEN / KARYAWAN</th>
                                <th>KATEGORI</th>
                                <th>STATUS</th>
                                <th>TOOLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            foreach ($dt as $key => $value) : ?>
                            <tr <?= $value['status'] == 'Tidak' ? 'class = "alert alert-danger"' : ''; ?> >
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= $value['nama_doskar'] ?> </td>
                                <td> <?= $value['kategori'] ?> </td>
                                <td> <?= $value['status'] ?> </td>
                                <td class="text-center">
                                    <a href="<?= base_url($this->link."index/".enc_data($value['id'])) ?>" >
                                        <button class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i> </button>
                                    </a> 
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php if (empty($idne)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Tambah Tugas Belajar
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>insert">
                    <div class="form-group">
                        <label for="" class="control-label">Nama Doskar</label>
                        <select name="key" class="form-control select2" required>
                            <option value="">Pilih Dosen / Karyawan</option>
                            <?php foreach ($doskar as $key => $dk) : ?>
                                <option value="<?= $dk['user_key'] ?>"> <?= $dk['nama_doskar'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" name="kategori" id="inlineRadio1" value="Dalam Kota" checked> Dalam Kota
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="kategori" id="inlineRadio2" value="Luar Kota"> Luar Kota
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> SIMPAN 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>

    <?php if (!empty($idne)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Edit Tugas Belajar
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>update">
                    <input type='hidden' class="form-control" name="id_belajar" value="<?= enc_data($idne) ?>" required/>
                    <div class="form-group">
                        <label for="" class="control-label">Nama Doskar</label>
                        <select name="key" class="form-control select2" required>
                            <option value="">Pilih Dosen / Karyawan</option>
                            <?php foreach ($doskar as $key => $dk) : ?>
                                <option value="<?= $dk['user_key'] ?>" <?= $edit['user_key'] == $dk['user_key'] ? 'selected' : ''; ?> > <?= $dk['nama_doskar'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Kategori</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" name="kategori" id="inlineRadio1" value="Dalam Kota" <?= $edit['kategori'] == 'Dalam Kota' ? 'checked' : ''; ?> > Dalam Kota
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="kategori" id="inlineRadio2" value="Luar Kota" <?= $edit['kategori'] == 'Luar Kota' ? 'checked' : ''; ?> > Luar Kota
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-9">
                            <label class="radio-inline">
                                <input type="radio" name="status" id="inlineRadio1" value="Aktif" <?= $edit['status'] == 'Aktif' ? 'checked' : ''; ?> > Aktif
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="status" id="inlineRadio2" value="Tidak" <?= $edit['status'] == 'Tidak' ? 'checked' : ''; ?> > Tidak
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> UPDATE 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>