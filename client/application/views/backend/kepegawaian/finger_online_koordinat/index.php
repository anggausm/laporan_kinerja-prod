<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Logs Finger Online</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="font-bold text-u-c">
                <i class="icon mdi-action-view-list i-20"></i> Logs Finger Online
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <form action="<?= base_url($this->link) ?>" class="form-inline"  method='post'>
                <div class="form-group form-group-sm m-b-xs">
                    <select name="user_key" class="form-control select2" required>
                        <option value="">Pilih Dosen/Karyawan </option>
                        <?php foreach ($dt['peserta'] as $key => $doskar) : ?>
                            <option value="<?= $doskar['user_key'] ?>"> <?= $doskar['nama_doskar'] ?> </option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group form-group-sm m-b-xs">
                    <input type='date' class="form-control" name="awal" required />
                </div>
                sampai
                <div class="form-group form-group-sm m-b-xs">
                    <input type='date' class="form-control" name="akhir"/>
                </div>

                <button type="submit" class="btn btn-sm btn-info"> Tampilkan </button>
            </form>
        </div>
    </div>
</div>

<?php if (!empty($dt['doskar']['user_key'])) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-heading p">
                <table class="font-bold">
                    <tr>
                        <td>Nama </td>
                        <td>: <?= $dt['doskar']['nama_doskar'] ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal </td>
                        <td>: <?= tanggal_indonesia($awal) ?> s/d <?= tanggal_indonesia($akhir) ?></td>
                    </tr>
                </table>
            </div>
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='25' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th class="text-center">NO</th>
                                <th class="text-center">HARI</th>
                                <th class="text-center">TANGGAL</th>
                                <th class="text-center">JAM MASUK</th>
                                <th class="text-center">JAM PULANG</th>
                                <th class="text-center">LOKASI MASUK</th>
                                <th class="text-center">LOKASI PULANG</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            
                            foreach ($dt['logs'] as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td class="text-center"> <?= $value['hari'] ?> </td>
                                <td class="text-center"> <?= $value['tanggal'] ?> </td>
                                <td class="text-center"> <?= $value['jam_masuk'] ?> </td>
                                <td class="text-center"> <?= $value['jam_pulang'] ?> </td>
                                <td class="text-center"> <?= $value['kordinat_masuk'] ?> </td>
                                <td class="text-center"> <?= $value['kordinat_pulang'] ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>