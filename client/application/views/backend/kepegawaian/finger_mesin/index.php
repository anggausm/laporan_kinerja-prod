<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Mesin Finger</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-6">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Mesin Finger
                    </div>
                </div>
                <div class="col-sm-6">
                    <!-- <form action="<?= base_url($this->link) ?>tambah" class="form-inline text-right"  method='post'>
                        <button type="submit" class="btn btn-sm btn-info" name="button">Tambah Mesin</button>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-8">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>MESIN FINGER</th>
                                <th>JENIS / KETERANGAN</th>
                                <!-- <th>KETERANGAN</th> -->
                                <th>TOOLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            
                            foreach ($mesin as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= $value['ip'] ?> <br> <?= $value['lokasi'] ?> </td>
                                <td> <?= $value['jenis'] ?> <br> <?= $value['keterangan'] ?> </td>
                                <!-- <td>  </td> -->
                                <td class="text-center">
                                    <?php if ($value['jenis'] == 'Primary') : ?>
                                        <a href="#" >
                                            <button class="btn btn-xs btn-warning"> Tidak boleh diedit </button>
                                        </a>
                                    <?php else : ?>
                                    <a href="<?= base_url($this->link."index/".enc_data($value['id_mesin'])) ?>" >
                                        <button class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i> </button>
                                    </a> 
                                    <?php endif ?>
                                    <!-- | 
                                    <a href="<?= base_url($this->link) ?>cek_koneksi/<?= enc_data($value['id_mesin']) ?>" >
                                        <button class="btn btn-xs btn-info"> <i class="fa fa-refresh"></i> Cek Koneksi </button>
                                    </a> -->
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php if (empty($id_mesin)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Tambah Mesin Finger
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>insert">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">IP Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="ip_mesin" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Lokasi Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="lokasi" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label text-right">Keterangan </label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="keterangan"/>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> SIMPAN 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>

    <?php if (!empty($id_mesin)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Edit Mesin Finger
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>update">
                    <input type='hidden' class="form-control" name="id_mesin" value="<?= enc_data($dt['id_mesin']) ?>" required/>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">IP Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="ip_mesin" value="<?= $dt['ip'] ?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Lokasi Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="lokasi" value="<?= $dt['lokasi'] ?>" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label text-right">Keterangan </label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="keterangan" value="<?= $dt['keterangan'] ?>"/>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> UPDATE 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>