<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading bg-white">
            <i class="fa  fa-plus "></i> Tambah Ijin DosKar
            <small class="text-muted"> </small>
        </div>
        <div class="panel-body">
            <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>simpan">
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">DosKar <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <select name="user_key" class="form-control select2" required>
                            <option value="">Pilih Dosen / Karyawan</option>
                            <?php foreach ($doskar as $key => $value) : ?>
                                <option value="<?= $value['user_key'] ?>"> <?= $value['nama_doskar'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Jenis Ijin <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <select name="jenis" class="form-control select2" required>
                            <option value="">Pilih Jenis</option>
                            <?php foreach ($jenis as $key => $jns) : ?>
                                <option value="<?= $jns['id_jenis'] ?>"> <?= $jns['jenis_absensi'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Tanggal Awal <em class="text-danger"> *</em> </label>
                    <div class="col-sm-3">
                        <input type='date' class="form-control" name="awal" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Tanggal Akhir</label>
                    <div class="col-sm-3">
                        <input type='date' class="form-control" name="akhir"/>
                    </div>
                    <div class="col-sm-6">
                        <code>Wajib diisi jika ijin lebih dari 1 hari.</code>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-3 control-label text-right">Keterangan </label>
                    <div class="col-sm-9">
                        <input type='text' class="form-control" name="keterangan"/>
                    </div>
                </div>
                <hr>
                <div class="col-sm-4"></div> 
                <div class="col-sm-4">
                    <button class="btn btn-info btn-sm">
                        <i class="fa fa-save"></i> SIMPAN 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>