<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Pengajuan Ijin</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> PENGAJUAN IJIN
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?= base_url($this->link) ?>" class="form-inline text-right" method='post'>
                        <select class="form-control" name="bulan" required>
                            <option value="01"> Januari </option>
                            <option value="02"> Februari </option>
                            <option value="03"> Maret </option>
                            <option value="04"> April </option>
                            <option value="05"> Mei </option>
                            <option value="06"> Juni </option>
                            <option value="07"> Juli </option>
                            <option value="08"> Agustus </option>
                            <option value="09"> September </option>
                            <option value="10"> Oktober </option>
                            <option value="11"> November </option>
                            <option value="12"> Desember </option>
                        </select>

                        <select class="form-control" name="tahun" required>
                            <?php 
                            $thn = date('Y') - 4;
                            for ($i = date('Y'); $i >= $thn; $i--): ?>
                                <option value="<?= $i ?>"> <?= $i ?> </option>
                            <?php endfor ?>
                        </select>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="font-bold text-u-c">
                            DAFTAR PENGAJUAN IJIN <br> BULAN <?= tanggal_bt($periode['bulan_tahun']) ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <form action="<?= base_url($this->link.'tambah') ?>" class="form-inline text-right" method='post'>
                            <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tambah</button>
                        </form>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>NAMA DOSKAR</th>
                                <th>TANGGAL</th>
                                <th>KETERANGAN</th>
                                <th>TOOLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            foreach ($dt as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= $value['nama_doskar'] ?> <br> Jenis : <?= $value['jenis_absensi'] ?> </td>
                                <td> Awal : <?= tanggal_indonesia($value['tanggal_awal']) ?> <br> Akhir : <?= tanggal_indonesia($value['tanggal_akhir']) ?> </td>
                                <td> Status : <?= $value['status'] ?> <br> <?= $value['keterangan'] ?> </td>
                                <td class="text-center">
                                    <div class="col-sm-1">
                                    <form action="<?= base_url($this->link.'edit') ?>" class="form-inline text-center" method='post'>
                                        <input type="hidden" name="id_ijin" value="<?= enc_data($value['id_ijin']) ?>">
                                        <button type="submit" class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i>  </button>
                                    </form>
                                    </div>
                                    <div class="col-sm-1">
                                    <form action="<?= base_url($this->link.'hapus') ?>" class="form-inline text-center" method='post' onclick="return confirm('Apakah Anda yakin akan menghapus data ini ???');" >
                                        <input type="hidden" name="id_ijin" value="<?= enc_data($value['id_ijin']) ?>">
                                        <button type="submit" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i>  </button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php /* if (empty($id_ijin)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Tambah Ijin DosKar
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>insert">
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">DosKar</label>
                        <div class="col-sm-9">
                            <select name="user_key" class="form-control select2" required>
                                <option value="">Pilih DosKar</option>
                                <?php foreach ($doskar as $key => $value) : ?>
                                    <option value="<?= $value['user_key'] ?>"> <?= $value['nama_doskar'] ?> </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Jenis Ijin</label>
                        <div class="col-sm-9">
                            <select name="jenis" class="form-control" required>
                                <option value="">Pilih Jenis</option>
                                <?php foreach ($jenis as $key => $jns) : ?>
                                    <option value="<?= $jns['id_jenis'] ?>"> <?= $jns['jenis_absensi'] ?> </option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Tanggal Awal</label>
                        <div class="col-sm-9">
                            <input type='date' class="form-control" name="awal" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Tanggal Akhir</label>
                        <div class="col-sm-9">
                            <input type='date' class="form-control" name="akhir" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label text-right">Keterangan </label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="keterangan"/>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> SIMPAN 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>

    <?php if (!empty($id_ijin)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Edit Ijin DosKar
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>update">
                    <input type='hidden' class="form-control" name="id_mesin" value="<?= enc_data($dt['id_mesin']) ?>" required/>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">IP Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="ip_mesin" value="<?= $dt['ip'] ?>" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label">Lokasi Mesin</label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="lokasi" value="<?= $dt['lokasi'] ?>" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-3 control-label text-right">Keterangan </label>
                        <div class="col-sm-9">
                            <input type='text' class="form-control" name="keterangan" value="<?= $dt['keterangan'] ?>"/>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> UPDATE 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif */ ?>
</div>