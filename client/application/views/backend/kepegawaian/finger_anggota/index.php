<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Peserta Finger</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-4">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> PESERTA FINGER
                    </div>
                </div>
                <div class="col-sm-2">
                    <form action="<?= base_url($this->link.'tambah') ?>" class="form-inline text-right" method='post'>
                        <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-plus"></i> Tambah User</button>
                    </form>
                </div>
                <div class="col-sm-2">
                    <form action="<?= base_url($this->link.'shared_user') ?>" class="form-inline text-right" method='post'>
                        <button type="submit" class="btn btn-sm btn-info" disabled> <i class="fa fa-users"></i> Shared User</button>
                    </form>
                </div>
                <div class="col-sm-2">
                    <form action="<?= base_url($this->link.'download_jari') ?>" class="form-inline text-right" method='post'>
                        <button type="submit" class="btn btn-sm btn-info" disabled> <i class="fa fa-cloud-download"></i> Download Jari</button>
                    </form>
                </div>
                <div class="col-sm-2">
                    <form action="<?= base_url($this->link.'shared_jari') ?>" class="form-inline text-right" method='post'>
                        <button type="submit" class="btn btn-sm btn-info" disabled> <i class="fa fa-hand-o-up"></i> Shared Jari</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>NAMA DOSKAR</th>
                                <th>PEKERJAAN</th>
                                <th>KETERANGAN</th>
                                <th>SIDIK JARI</th>
                                <th>TOOLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            foreach ($dt as $key => $value) : ?>
                            <tr <?= $value['status'] == 'Tidak' ? 'class = "alert alert-danger"' : ''; ?> >
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= $value['finger_id'] ?> - <?= $value['nickname'] ?> <br> <?= $value['nama_doskar'] ?> </td>
                                <td> <?= $value['pekerjaan'] ?> <br> <?= $value['ket'] ?> - <?= $value['tarif'] ?></td>
                                <td> Status : <?= $value['status'] ?> <br> Shift : <?= $value['shift'] ?> </td>
                                <td> </td>
                                <td class="text-center">
                                    <div class="col-sm-1">
                                    <form action="<?= base_url($this->link.'edit') ?>" class="form-inline text-center" method='post'>
                                        <input type="hidden" name="id_peserta" value="<?= enc_data($value['id_peserta']) ?>">
                                        <button type="submit" class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i> </button>
                                    </form>
                                    </div>
                                    <div class="col-sm-1">
                                    <form action="<?= base_url($this->link.'hapus') ?>" class="form-inline text-center" method='post' onclick="return confirm('Apakah Anda yakin akan menghapus data ini ???');" >
                                        <input type="hidden" name="id_peserta" value="<?= enc_data($value['id_peserta']) ?>">
                                        <button type="submit" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i>  </button>
                                    </form>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>