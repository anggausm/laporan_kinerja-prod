<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading bg-white">
            <i class="fa  fa-plus "></i> Edit Ijin DosKar <?= $id_peserta ?>
            <small class="text-muted"> </small>
        </div>
        <div class="panel-body">
            <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link.'update') ?>">
                <input type='hidden' class="form-control" name="id_peserta" value="<?= enc_data($id_peserta) ?>" required/>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">DosKar <em class="text-danger"> *</em> </label>
                    <div class="col-sm-8">
                        <select name="user_key" class="form-control select2" required>
                            <option value="">Pilih Dosen / Karyawan</option>
                            <?php foreach ($doskar as $key => $value) : ?>
                                <option value="<?= $value['user_key'] ?>" <?= $value['user_key'] == $edit['dt']['user_key'] ? 'selected' : ''; ?> disabled=""> <?= $value['nama_doskar'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label"> Jam Finger <em class="text-danger"> *</em> </label>
                    <div class="col-sm-8">
                        <select name="jam" class="form-control select2" required>
                            <option value="">Pilih Jam Finger</option>
                            <?php foreach ($edit['jam'] as $key => $jam) : ?>
                                <option value="<?= $jam['id_setting_jam'] ?>" <?= $jam['id_setting_jam'] == $edit['dt']['id_setting_jam'] ? 'selected' : ''; ?> > <?= $jam['pekerjaan'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label"> Tarif Transport <em class="text-danger"> *</em> </label>
                    <div class="col-sm-8">
                        <select name="tarif" class="form-control select2" required>
                            <option value="">Pilih Tarif</option>
                            <?php foreach ($edit['tarif'] as $key => $tarif) : ?>
                                <option value="<?= $tarif['id_tarif'] ?>" <?= $tarif['id_tarif'] == $edit['dt']['id_tarif'] ? 'selected' : ''; ?> > 
                                    <?= $tarif['ket'] ?> - <?= rp_titik($tarif['biaya_transport']) ?> 
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label"> ID Finger <em class="text-danger"> *</em> </label>
                    <div class="col-sm-4">
                        <input type='text' class="form-control" name="finger_id" value="<?= $edit['dt']['finger_id'] ?>" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label"> Nickname <em class="text-danger"> *</em> </label>
                    <div class="col-sm-4">
                        <input type='text' class="form-control" name="nickname" value="<?= $edit['dt']['nickname'] ?>" required/>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-3 control-label text-right"> Shift <em class="text-danger"> *</em> </label>
                    <div class="col-sm-8">
                        <label class="radio-inline">
                            <input type="radio" name="shift" id="inlineRadio1" value="Pagi" <?= $edit['dt']['shift'] == 'Pagi' ? 'checked' : ''; ?> > Pagi
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="shift" id="inlineRadio2" value="Sore" <?= $edit['dt']['shift'] == 'Sore' ? 'checked' : ''; ?> > Sore
                        </label>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-3 control-label text-right"> Status <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <label class="radio-inline">
                            <input type="radio" name="status" id="inlineRadio1" value="Finjer" <?= $edit['dt']['status'] == 'Finjer' ? 'checked' : ''; ?> > Finger
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" id="inlineRadio2" value="Tidak" <?= $edit['dt']['status'] == 'Tidak' ? 'checked' : ''; ?> > Tidak
                        </label>
                    </div>
                </div>
                <hr>
                <div class="col-sm-4"></div> 
                <div class="col-sm-4">
                    <button class="btn btn-info btn-sm">
                        <i class="fa fa-save"></i> SIMPAN 
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
echo "<pre>";
print_r ($dt);
echo "</pre>";
?>