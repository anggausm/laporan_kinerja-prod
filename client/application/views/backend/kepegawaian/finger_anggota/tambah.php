<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading bg-white">
            <i class="fa  fa-plus "></i> Tambah Ijin DosKar
            <small class="text-muted"> </small>
        </div>
        <div class="panel-body">
            <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>simpan">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">DosKar <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <select name="user_key" class="form-control select2" required>
                            <option value="">Pilih Dosen / Karyawan</option>
                            <?php foreach ($doskar as $key => $value) : ?>
                                <option value="<?= $value['user_key'] ?>"> <?= $value['nama_doskar'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"> Jam Finger <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <select name="jam" class="form-control select2" required>
                            <option value="">Pilih Jam Finger</option>
                            <?php foreach ($dt['jam'] as $key => $jam) : ?>
                                <option value="<?= $jam['id_setting_jam'] ?>"> <?= $jam['pekerjaan'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"> Tarif Transport <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <select name="tarif" class="form-control select2" required>
                            <option value="">Pilih Tarif</option>
                            <?php foreach ($dt['tarif'] as $key => $tarif) : ?>
                                <option value="<?= $tarif['id_tarif'] ?>"> <?= $tarif['ket'] ?> - <?= rp_titik($tarif['biaya_transport']) ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"> ID Finger <em class="text-danger"> *</em> </label>
                    <div class="col-sm-3">
                        <input type='text' class="form-control" name="finger_id" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"> Nickname <em class="text-danger"> *</em> </label>
                    <div class="col-sm-3">
                        <input type='text' class="form-control" name="nickname" required/>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label text-right"> Shift <em class="text-danger"> *</em> </label>
                    <div class="col-sm-9">
                        <label class="radio-inline">
                            <input type="radio" name="shift" id="inlineRadio1" value="Pagi" checked> Pagi
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="shift" id="inlineRadio2" value="Sore"> Sore
                        </label>
                    </div>
                </div>
                <hr>
                <div class="col-sm-4"></div> 
                <div class="col-sm-4">
                    <button class="btn btn-info btn-sm">
                        <i class="fa fa-save"></i> SIMPAN 
                    </button>
                </div>
                <div class="col-sm-4"></div> 
            </form>
        </div>
    </div>
</div>