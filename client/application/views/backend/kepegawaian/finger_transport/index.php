<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Olah Transport</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-3">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> OLAH TRANSPORT 
                    </div>
                </div>
                <div class="col-sm-9">
                    <form action="<?= base_url($this->link) ?>" class="form-inline text-right" method='post'>
                        <div class="form-group form-group-sm m-b-xs">
                            <input type="date" class="form-control" name="tanggal">
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Tampilkan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="font-bold text-center">
                    OLAH TRANSPORT DOSEN DAN KARYAWAN <br>
                    TANGGAL <?= strtoupper(tanggal_indonesia($tanggal)) ?>
                </div>
                <br>
                <form action="<?= base_url($this->link) ?>olah_transport" class="form-inline" method='post'>
                    <div class="form-group form-group-sm m-b-xs">
                        <input type="hidden" class="form-control" name="tanggal" value="<?= enc_data($tanggal) ?>">

                        <select class="form-control" name="jenis" required>
                            <option value="Reguler"> Reguler </option>
                            <option value="Puasa"> Puasa </option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-sm btn-info">Proses</button>
                </form>
                <p class="font-bold">
                    NB : <br>
                    1. Pastikan Ijin, Cuti, dll selesai diinputkan. Jika ada perubahan pada Ijin, Cuti dll dapat di klik PROSES lagi. <br>
                    <!-- 2. Jika melewati dari tanggal  dapat diinputkan pada bulan berikutnya. -->
                </p>
                <br>
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>NAMA DOSKAR</th>
                                <th>JAM PRESENSI</th>
                                <th>PEKERJAAN</th>
                                <th>JENIS</th>
                                <th>TRANSPORT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            foreach ($transport as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= $value['id_finger'] ?> <br> <?= $value['nama_doskar'] ?> </td>
                                <td> Masuk : <?= $value['jam_masuk'] ?> <br> Pulang : <?= $value['jam_pulang'] ?> </td>
                                <td> Pekerjaan : <?= $value['pekerjaan'] ?> <br> Shift : <?= $value['shift'] ?> </td>
                                <td> Presensi : <?= $value['jenis_presensi'] ?> <br> Keterangan : <?= $value['status_presensi'] ?> </td>
                                <td> <?= rp_titik($value['transport']) ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>