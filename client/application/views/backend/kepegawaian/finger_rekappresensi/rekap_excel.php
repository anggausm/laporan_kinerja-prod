<?php
$this->load->helper('excel');
$this->load->library('phpexcel');
$filepath       = "file/finger/rekap_transport.xlsx";
$objReader      = PHPExcel_IOFactory::createReader('Excel2007');
$this->phpexcel = $objReader->load($filepath);
$objWorksheet   = $this->phpexcel->setActiveSheetIndex(0);

$objWorksheet->setCellValue('A3', 'PERIODE ' .strtoupper(tanggal_indonesia($periode['awal'])). ' SAMPAI ' . strtoupper(tanggal_indonesia($periode['akhir'])));
$letter = 1;
// $objWorksheet->getColumnDimension(generateExcelColumnLetters($letter++))->setAutoSize(true);

$no     = 1;
$line   = 6;
foreach ($transport as $value) {
    $letter = 1;
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $no);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['id_doskar']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['user_key']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['nama_doskar']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['ijin_masuk']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['ijin_pulang']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['cuti']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['tugas']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['sakit']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['telat_masuk']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['telat_pulang']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['hari_khusus']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['bolos']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['hadir']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['j_absen']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['hitung_transport']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, $value['tarif']);
    $objWorksheet->setCellValue(generateExcelColumnLetters($letter++) . $line, '=P' . $line . ' * Q' . $line . '');

    $line++;
    $no++;
}

$nm_file = 'rekap-transport-' . date('YmdHis');
$nm_file = strtolower($nm_file);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename=' . $nm_file . '.xlsx');
header('Cache-Control: max-age=0');
// output
$obj_writer = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
$obj_writer->save('php://output');
?>