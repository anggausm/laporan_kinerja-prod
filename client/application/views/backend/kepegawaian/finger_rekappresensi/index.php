<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Rekap Transport</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-5">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Rekap Transport
                    </div>
                </div>
                
                <div class="col-sm-7">
                    <form action="<?= base_url($this->link) ?>" class="form-inline text-right"  method='post'>
                        <div class="form-group form-group-sm m-b-xs">
                            <input type='date' class="form-control" name="awal" required />
                        </div>
                        sampai
                        <div class="form-group form-group-sm m-b-xs">
                            <input type='date' class="form-control" name="akhir" required />
                        </div>

                        <button type="submit" class="btn btn-sm btn-info"> Tampilkan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<?php if (!empty($periode['awal'])) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-heading p">
                <div class="row">
                    <div class="col-sm-8">
                        <p class="font-bold">
                            REKAP TRANSPORT <br>
                            PERIODE <?= strtoupper(tanggal_indonesia($periode['awal'])) ?> SAMPAI <?= strtoupper(tanggal_indonesia($periode['akhir'])) ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <form action="<?= base_url($this->link) ?>download_transport_excel" class="form-inline text-right"  method='post'>
                            <input type='hidden' class="form-control" name="awal" value="<?= enc_data($periode['awal']) ?>" />
                            <input type='hidden' class="form-control" name="akhir" value="<?= enc_data($periode['akhir']) ?>" />
                            <button type="submit" class="btn btn-sm btn-info"> <i class="fa fa-file-excel-o"></i> Download </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th class="text-center">NO</th>
                                <th class="text-center" style="width:30%">NAMA</th>
                                <th class="text-center">TELAT</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">REKAP</th>
                                <th class="text-center">HITUNG</th>
                                <th class="text-center">TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            foreach ($detail as $key => $value) : ?>
                            <tr>
                                <td> <?= $no++ ?> </td>
                                <td>
                                    <a class="text-info-lt m-r-xs" href="<?= base_url($this->link . 'info/' . enc_data_url($value['user_key']).'/'.enc_data_url($periode['awal']).'/'.enc_data_url($periode['akhir'])); ?>">
                                        <?= $value['nama_doskar']; ?>
                                    </a>
                                    <br> Tarif : <?= rp_titik($value['tarif']) ?> 
                                </td>
                                <td> Masuk : <?= $value['telat_masuk'] ?> <br> Pulang : <?= $value['telat_pulang'] ?> </td>
                                <td> Masuk : <?= $value['ijin_masuk'] ?> <br> Pulang : <?= $value['ijin_pulang'] ?> </td>
                                <td> Cuti : <?= $value['cuti'] ?> <br> Sakit : <?= $value['sakit'] ?> </td>
                                <td> Tugas : <?= $value['tugas'] ?> <br> Merah : <?= $value['hari_khusus'] ?> </td>
                                <td> Bolos : <?= $value['bolos'] ?> <br> Hadir : <?= $value['hadir'] ?> </td>
                                <td> Finjer : <?= $value['j_absen'] ?> <br> Hitung : <?= $value['hitung_transport'] ?> </td>
                                <td class="text-right"> <?= rp_titik($value['j_transport']) ?> </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>