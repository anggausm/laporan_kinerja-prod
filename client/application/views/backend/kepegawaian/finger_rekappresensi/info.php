<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Rekap Transport</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-8">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Rekap Transport
                    </div>
                </div>
                
                <div class="col-sm-4">
                <form action="<?= base_url($this->link) ?>" class="form-inline text-right"  method='post'>
                    <input type='hidden' class="form-control" name="awal" value="<?= $periode['awal']?>"/>
                    <input type='hidden' class="form-control" name="akhir" value="<?= $periode['akhir']?>"/>

                    <button type="submit" class="btn btn-sm btn-warning">
                        <i class = "fa fa-backward"></i> 
                        Kembali 
                    </button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<?php if (!empty($periode['awal'])) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="card r-2x">
            <div class="card-heading p">
                <div class="row">
                    <div class="col-sm-8">
                        <table class="font-bold">
                            <tr>
                                <td>Finger ID &emsp;</td>
                                <td>: <?= $dt['doskar']['id_finger'] ?></td>
                            </tr>
                            <tr>
                                <td>Nama </td>
                                <td>: <?= $dt['doskar']['nama_doskar'] ?></td>
                            </tr>
                            <tr>
                                <td>Tanggal </td>
                                <td>: <?= tanggal_indonesia($periode['awal']) ?> - <?= tanggal_indonesia($periode['akhir']) ?></td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="col-sm-4">
                    </div>
                </div>
            </div>
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='25' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th class="text-center">NO</th>
                                <th class="text-center">TANGGAL</th>
                                <th class="text-center">JAM</th>
                                <th class="text-center">TELAT</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">IJIN</th>
                                <th class="text-center">REKAP</th>
                                <th class="text-center">STATUS</th>
                                <th class="text-center">TRANSPORT</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            
                            foreach ($dt['logs'] as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= nama_hari(date('l', strtotime($value['tanggal']))) ?> <br> <?= $value['tanggal'] ?> </td>
                                <td> Masuk : <?= $value['jam_masuk'] ?> <br> Pulang : <?= $value['jam_pulang'] ?> </td>
                                <td> Masuk : <?= $value['telat_masuk'] ?> <br> Pulang : <?= $value['telat_pulang'] ?> </td>
                                <td> Masuk : <?= $value['ijin_masuk'] ?> <br> Pulang : <?= $value['ijin_pulang'] ?> </td>
                                <td> Cuti : <?= $value['cuti'] ?> <br> Sakit : <?= $value['sakit'] ?> </td>
                                <td> Tugas : <?= $value['tugas'] ?> <br> Merah : <?= $value['hari_khusus'] ?> </td>
                                <td> Bolos : <?= $value['bolos'] ?> <br> Hadir : <?= $value['hadir'] ?> </td>
                                <td> Jenis : <?= $value['jenis_absensi'] ?> <br> Shift : <?= $value['shift'] ?> </td>
                                <td class="text-right"> <?= rp_titik($value['transport']) ?> </td>
                                <td class="text-center">
                                    <form action="<?= base_url($this->link) ?>update" class="form-inline"  method='post'>
                                        <input type='hidden' class="form-control" name="id_transport" value="<?= enc_data($value['id']) ?>"/>
                                        <input type='hidden' class="form-control" name="awal" value="<?= enc_data_url($periode['awal']) ?>"/>
                                        <input type='hidden' class="form-control" name="akhir" value="<?= enc_data_url($periode['akhir']) ?>"/>
                                        <input type='hidden' class="form-control" name="key" value="<?= enc_data_url($value['user_key']) ?>"/>
                                        <?php if ($value['transport'] > 0) : ?>
                                        <input type='hidden' class="form-control" name="hitung_transport" value="<?= enc_data('0') ?>"/>
                                        <button type="submit" class="btn btn-sm btn-warning">
                                            <i class = "fa fa-refresh"></i> 
                                        </button>
                                        <?php endif ?>

                                        <?php if ($value['transport'] == 0) : ?>
                                        <input type='hidden' class="form-control" name="hitung_transport" value="<?= enc_data('1') ?>"/>
                                        <button type="submit" class="btn btn-sm btn-info">
                                            <i class = "fa fa-money"></i> 
                                        </button>
                                        <?php endif ?>
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>