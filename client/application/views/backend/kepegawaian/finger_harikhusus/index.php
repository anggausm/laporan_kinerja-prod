<div class="row">
    <div class="col-sm-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb green-200 text-black-dk">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>dashboard/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active">Hari Khusus</li>
            </ol>
        </nav>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card r-2x p">
            <div class="row">
                <div class="col-sm-6">
                    <div class="font-bold text-u-c">
                        <i class="icon mdi-action-view-list i-20"></i> Hari Khusus
                    </div>
                </div>
                <div class="col-sm-6">
                    <!-- <form action="<?= base_url($this->link) ?>tambah" class="form-inline text-right"  method='post'>
                        <button type="submit" class="btn btn-sm btn-info" name="button">Tambah Mesin</button>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->flashdata('pesan'))) : ?>
    <div class="alert <?php print $this->session->flashdata('type') ?> text-center" role="alert">
        <?php print $this->session->flashdata('pesan') ?>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-md-8">
        <div class="card r-2x">
            <div class="card-body p">
                <div class="table-responsive">
                    <table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }" data-page-length='10' style="width:100%">
                        <thead>
                            <tr class="bg-info">
                                <th>NO</th>
                                <th>TANGGAL</th>
                                <th>JENIS</th>
                                <th>KETERANGAN</th>
                                <th>TOOLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $no = 1;
                            
                            foreach ($dt as $key => $value) : ?>
                            <tr>
                                <td class="text-center"> <?= $no++ ?> </td>
                                <td> <?= tanggal_indonesia($value['tanggal']) ?> </td>
                                <td> <?= $value['jenis'] ?> </td>
                                <td> <?= $value['keterangan'] ?> </td>
                                <td class="text-center">
                                    <a href="<?= base_url($this->link."index/".enc_data($value['id'])) ?>" >
                                        <button class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i> </button>
                                    </a> 
                                    
                                    <a href="<?= base_url($this->link."destroy/".enc_data($value['id'])) ?>" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')">
                                        <button class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i> </button>
                                    </a> 
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php if (empty($idne)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Tambah Hari Khusus
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>insert">
                    <div class="form-group">
                        <label for="" class="control-label">Tanggal</label>
                        <input type="date" name="tanggal" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Jenis Tanggal</label>
                        <select name="jenis" class="form-control select2" required>
                            <option value="">Pilih Jenis Tanggal</option>
                            <?php foreach ($jenis as $key => $jns) : ?>
                                <option value="<?= $jns['id_jenis'] ?>"> <?= $jns['nama_jenis'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" required>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> SIMPAN 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>

    <?php if (!empty($idne)) : ?>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading bg-white">
                <i class="fa  fa-plus "></i> Edit Hari Khusus
                <small class="text-muted"> </small>
            </div>
            <div class="panel-body">
                <form  class="form-horizontal p-h-xs" method="POST" action="<?= base_url($this->link) ?>update">
                    <input type='hidden' class="form-control" name="id" value="<?= enc_data($idne) ?>" required/>
                    <div class="form-group">
                        <label for="" class="control-label">Tanggal</label>
                        <input type="date" name="tanggal" value="<?= $edit['tanggal'] ?>" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Jenis Tanggal</label>
                        <select name="jenis" class="form-control select2" required>
                            <option value="">Pilih Jenis Tanggal</option>
                            <?php foreach ($jenis as $key => $jns) : ?>
                                <option value="<?= $jns['id_jenis'] ?>" <?= $edit['id_jenis'] == $jns['id_jenis'] ? 'selected' : ''; ?>> <?= $jns['nama_jenis'] ?> </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" value="<?= $edit['keterangan'] ?>" required>
                    </div>
                    <hr>
                    <div class="col-sm-4"></div> 
                    <div class="col-sm-4">
                        <button class="btn btn-info btn-sm">
                            <i class="fa fa-save"></i> UPDATE 
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>