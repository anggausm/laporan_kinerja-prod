<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>laporan/presensi">Detail Presendi</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Unit  <?php print $rs_data['result']['jabatan']['unit_bagian'];?></b>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs"   >
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">NIS </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['user']['nis'];?></label>
								</div>
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">NAMA </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['user']['nama_doskar'];?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Unit </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['jabatan']['unit_bagian'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Jabatan Struktural </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['jabatan']['nama_sub_struktural'];?>  </label>
								</div>
							</div>
						</div>
					</form>
					<br>
					<h5><i class="fa  fa-calendar"></i> Riwayat  Presensi</h5>
					<hr>
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>laporan/presensi/detail_presesni" enctype="multipart/form-data">
						<input type="hidden" name="user_key" value="<?=enc_data($rs_data['result']['user']['user_key'])?>">
						<input type="hidden" name="id_sub_struktural" value="<?=enc_data($rs_data['result']['jabatan']['id_sub_struktural'])?>">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Tahun </label>
									<div class="col-sm-8">
										<select class="form-control" name="tahun">
											<option value="2020" <?php if ($tahun == '2020') {print 'selected=""';}?> >2020</option>
											<option value="2021" <?php if ($tahun == '2021') {print 'selected=""';}?> >2021</option>
											<option value="2022" <?php if ($tahun == '2022') {print 'selected=""';}?> >2022</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Bulan </label>
									<div class="col-sm-8">
										<select class="form-control" name="bulan">
											<option value="01" <?php if ($bulan == '01') {print 'selected=""';}?>>Januari</option>
											<option value="02" <?php if ($bulan == '02') {print 'selected=""';}?> >Februari</option>
											<option value="03" <?php if ($bulan == '03') {print 'selected=""';}?> >Maret</option>
											<option value="04" <?php if ($bulan == '04') {print 'selected=""';}?> >April</option>
											<option value="05" <?php if ($bulan == '05') {print 'selected=""';}?> >Mei</option>
											<option value="06" <?php if ($bulan == '06') {print 'selected=""';}?> >Juni</option>
											<option value="07" <?php if ($bulan == '07') {print 'selected=""';}?> >Juli</option>
											<option value="08" <?php if ($bulan == '08') {print 'selected=""';}?> >Agustus</option>
											<option value="09" <?php if ($bulan == '09') {print 'selected=""';}?> >September</option>
											<option value="10" <?php if ($bulan == '10') {print 'selected=""';}?> >Oktober</option>
											<option value="11" <?php if ($bulan == '11') {print 'selected=""';}?> >November</option>
											<option value="12" <?php if ($bulan == '12') {print 'selected=""';}?> >Desember</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Riwayat Pekerjaan</button>
							</div>
						</div>
					</form>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:15%" class="text-center">Hari  </th>
												<th style="width:15%" class="text-center"> Tanggal </th>
												<!-- <th style="width:15%" class="text-center">Jam Masuk</th>
												<th style="width:15%" class="text-center">Jam Pulang</th> -->
												<th style="width:15%" class="text-center">Rencana Kerja</th>
												<!-- <th style="width:20%" class="text-center">Lokasi Masuk / Pulang</th> -->
											</tr>
										</thead>
										<tbody>
											<?php
$no = 1;
foreach ($rs_data['result']['result'] as $value) {
    ?>
											<!--   -->
											<tr>
												<td class="text-center">
													<?=$no?>
												</td>
												<td><?=$value['hari'];?></td>
												<td><?=$value['tanggal'];?></td>
											<!-- 	<td class="text-center">
													< ?php if (empty($value['jam_masuk'])) {
        ?>
												 <i class="fa fa-frown-o" style="color: #ff4e09"></i> Tidak Presensi
													< ?php } else {
        print "<i class='fa fa-check-square-o' style='color:#0c9508'></i>" .
         $value['jam_masuk'];
    }?>
												</td>
												<td class="text-center">
											 < ?php if (empty($value['jam_pulang'])) {
        ?>
												 <i class="fa fa-frown-o" style="color: #ff4e09"></i> Tidak Presensi
													< ?php } else {
         print "<i class='fa fa-check-square-o' style='color:#0c9508'></i>" .
            $value['jam_pulang'];
    } ?>
     </td>  -->
												<td style="text-align: center;">
													<?php if (empty($value['jm_task'])) {?>
													<button class="btn btn-warning btn-xs"><i class="fa fa-file"></i> <b>0</b></button>
													<?php } else {?>
													<a  data-toggle="modal" data-target="#task_list" v-on:click ="task_list('<?php print $value['id_absen'];?>')">
														<button class="btn btn-primary btn-xs"><i class="fa fa-file"></i> <b><?=$value['jm_task'];?></b></button>
													</a>
													<?php }?>
												</td>
												<!-- <td>
													< ?php if (!empty($value['kordinat_masuk'])) {?>
													<a  data-toggle="modal" data-target="#masuk" v-on:click ="pres_masuk('<?php print $value['kordinat_masuk'];?>')">
														<button class="btn btn-primary waves-effect btn-xs"><i class="fa  fa-map-marker"></i> Masuk </button>
													</a>
													< ?php }?>
													< ?php if (!empty($value['kordinat_pulang'])) {?>
													<a  data-toggle="modal" data-target="#pulang" v-on:click ="pres_pulang('<?php print $value['kordinat_pulang'];?>')">
														<button class="btn  btn-accent btn-xs "><i class="fa fa-map-marker"></i> Pulang</button>
													</a>
													< ?php }?>
												</td> -->
											</tr>
											<?php
$no++;
}
?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="masuk" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lokasi Presensi Masuk</h4>
				</div>
				<div class="modal-body">
					<div id="peta"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div id="pulang" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lokasi Presensi Pulang</h4>
				</div>
				<div class="modal-body">
					<div id="peta_pulang"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div id="task_list" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Daftar Pekerjaan</h4>
				</div>
				<div class="modal-body">
					<!-- [ { "id_task": "a6990606-8521-11ea-a0e9-0e02d85c309a", "id_absen": "a45d2df6-8521-11ea-a0e9-0e02d85c309a", "judul_kerjaan": "Persiapan Data Pelaporan Feeder Dikti (Mahasiswa Baru)", "keterangan": null, "type_riport": null, "dokumen": null, "link": null, "jam_riport": null, "cek": "0", "keterangan_cek": null } ]
					-->
					<div class="table-responsive">
						<table   class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr>
									<th style="width:5%" class="text-center">No</th>
									<th style="width:30%" class="text-center">Judul Pekerjaan</th>
									<th style="width:40%" class="text-center">Keterangan </th>
									<th style="width:20%" class="text-center">Url</th>
									<th style="width:10%" class="text-center">File</th>
								</tr>
							</thead>
							<tbody>
								<tr  v-for="(v_my_task , index ) in my_task">
									<td>{{index + 1}}</td>
									<td>{{v_my_task.judul_kerjaan}}</td>
									<td>{{v_my_task.keterangan}}</td>
									<td>{{v_my_task.link}}</td>
									<td>
										<div v-if ="v_my_task.dokumen !== null">
											<a v-bind:href=" base_url +  'laporan/presensi/download_files/'+ v_my_task.id_task" target= '_blank'>
												<button class="btn btn-info btn-xs"><i class="fa fa-download"></i> download </button>
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var app = new Vue({
	    el: '#app',
	    data: {
	        loading: '1',
	        kor_masuk: '',
	        id_absen: '',
	        my_task: '',
	        base_url: "<?php print $base_url;?>",
	    },
	    mounted: function() {},
	    methods: {
	        pres_masuk: function(kor_masuk) {
	            console.log(kor_masuk);
	            var mapOptions = {
	                center: [-6.981928, 110.452198],
	                zoom: 13
	            }
	            var peta = new L.map('peta', mapOptions);
	           	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: ''}).addTo(peta);
	            var markerGroup = L.layerGroup();
				peta.addLayer(markerGroup);

				markerGroup.clearLayers()
	            var latlong = kor_masuk.split(',');
	            var marker = L.marker([ latlong[0],latlong[1] ]);
	            markerGroup.addLayer(marker);
	            peta.fitBounds([ [latlong[0],latlong[1]]  ]);

				document.getElementById('masuk').style.display = 'block';
	            setTimeout(function() {peta.invalidateSize()}, 1000);
	        },
	        pres_pulang: function(kor_masuk) {
	            console.log(kor_masuk);
	            var mapOptions = {
	                center: [-6.981928, 110.452198],
	                zoom: 13
	            }
	            var peta_pulang = new L.map('peta_pulang', mapOptions);
	            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: ''}).addTo(peta_pulang);
	            var markerGroupPulang = L.layerGroup();
				peta_pulang.addLayer(markerGroupPulang)

	            markerGroupPulang.clearLayers()
	            var latlong = kor_masuk.split(',');
	            var marker = L.marker([ latlong[0],latlong[1] ]);
	            markerGroupPulang.addLayer(marker);
	            peta_pulang.fitBounds([ [latlong[0],latlong[1]] ]);

	            document.getElementById('pulang').style.display = 'block';
	            setTimeout(function() {peta_pulang.invalidateSize()}, 1000);
	        },
	        task_list: function(id_absen) {
	                axios({
	                    method: 'put',
	                    url: base_url('laporan/presensi/get_task_list'),
	                    data: {
	                        id_absen: id_absen
	                    },
	                    config: {
	                        headers: {
	                            'Content-Type': 'multipart/form-data'
	                        }
	                    }
	                }).then(function(response) {
	                    crud: '';
	                    if (response.data.status == '200') {
	                        return app.my_task = response.data.result;
	                    } else {
	                        if (confirm("maaf data gagal diload")) {
	                            return app.my_task = [];
	                        }
	                    }
	                }).catch(function(error) {
	                    console.log(error);
	                });
	            }
	            //
	    }
	})
</script>