<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>kpg/anggota/dosen">Daftar Angota Dosen</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> <b>Unit  <?php print $rs_data['result']['prodil']['unit_bagian'];?></b>
				</div>
				<div class="panel-body">
					<!--  "user_key": "3b6894d0-6f68-11ea-89e7-1cb72c27dd68",
					"nis": "06557003102129",
					"nidn": "0615098202",
					"nama_doskar": "B. VERY CHRISTIOKO, S.Kom., M.Kom.",
					"id_struktural": "8",
					"id_sub_struktural": "92",
					"fakultas": null,
					"kode_khusus": null,
					"keterangan": "Ka. UPT. PD USM (Pangkalan Data USM)",
					"unit_bagian": "Pangkalan Data USM",
					"nama_sub_struktural": "Ka. UPT. PD USM (Pangkalan Data USM)",
					"kode_sto": "8.2",
					"level": "2",
					"level_pimpinan": "2" -->
					<form  class="form-horizontal p-h-xs"   >
						<div class="row">
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">NIS </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['prodil']['nis'];?></label>
								</div>
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">NAMA </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['prodil']['nama_doskar'];?> </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Unit </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['prodil']['unit_bagian'];?>  </label>
								</div>
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Jabatan Struktural </label>
									<label class="col-sm-8 control-label text-right">: <?=$rs_data['result']['prodil']['nama_sub_struktural'];?>  </label>
								</div>
							</div>
						</div>
					</form>
					<hr>
					<div class="table-responsive">
						<table  class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr>
									<th style="width:5%" class="text-center" rowspan="2">No</th>
									<th style="width:25%" rowspan="2">Nama Lengkap</th>
									<th style="width:30%" rowspan="2">Jabatan Struktural</th>
							<!-- 		<th style="width:20%" colspan="2"  class="text-center">Presensi Hari Ini</th> -->
									<th style="width:15%" class="text-center" rowspan="2">Detail</th>
								</tr>
							<!-- 	<tr>
									<th class="text-center">Berangkat</th>
									<th class="text-center">Pulang</th>
								</tr> -->
							</thead>
							<tbody>
								<?php
$no = 1;
foreach ($rs_data['result']['anggota'] as $value) {
    ?>
								<tr>
									<td class="text-center"><?=$no?></td>
									<td><?=$value['nama_doskar'];?></td>
									<td><?=$value['nama_sub_struktural'];?></td>
									<!-- <td class="text-center">
									   <?=$value['jam_masuk'];?>
										</td>
										<td  class="text-center">
										<?=$value['jam_pulang'];?>

										</td> -->
									<td class="text-center">
										<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>laporan/presensi/detail_presesni" enctype="multipart/form-data">
											<input type="hidden" name="user_key" value="<?=enc_data($value['user_key'])?>">
											<input type="hidden" name="id_sub_struktural" value="<?=enc_data($value['id_sub_struktural'])?>">
											<button class="btn btn-info btn-xs"><i class="fa fa-list"></i> Rincian Presensi</button>
										</form>
									</td>
								</tr>
								<?php
$no++;
}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>