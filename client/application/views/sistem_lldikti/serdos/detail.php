<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<i class="fa fa-home putih"></i> 
						<a href="<?php print base_url() . 'app'?>"> Portal</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php print base_url()?>sistem_lldikti/ajuan_jafa"> Rekap Pengajuan Jabatan Fungsional </a>
					</li>
					<li class="breadcrumb-item">
						<a href="#"> Detail </a>
					</li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<p>
						<b> Status : 
							<?php
							echo $status['result']['ajuan_jafa'];
							?>
						</b>
					</p>
					<div class="table-responsive">
						<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr>
									<th style="width:5%"  class="text-center">No</th>
									<th style="width:10%" class="text-center"> NIDN </th>
									<th style="width:40%"> Nama Dosen </th>
									<th style="width:15%"> Bidang Ilmu </th>
									<th style="width:15%"> Jafa Ajuan </th>
									<th style="width:5%" class="text-center"> KUM </th>
									<?php
									if ($crud['u'] == 1) 
									{ ?>
										<th style="width:10%" class="text-center"> Edit </th>
									<?php
									}
									?>
								</tr>
							</thead>
							<tbody>
								<?php
								$no 	= 1;
								foreach ($detail['result']['rs'] as $value) 
								{
    								?>
									<tr>
										<td class="text-center"> <?=$no?> </td>
										<td class="text-center"> <?=$value['nidn']?> </td>
										<td> <?=$value['nama_doskar']?> </td>
										<td> <?=$value['bidang_ilmu']?> </td>
										<td> <?=$value['nama_jafa']?> </td>
										<td class="text-center"> <?=$value['kum']?> </td>
										<?php
										if ($crud['u'] == 1) 
										{ ?>
											<td class="text-center">
											<form method="POST" action="<?php print base_url() . 'sistem_lldikti/ajuan_jafa/edit'?>">
												<input type="hidden" name="id" value="<?php print enc_data($value['id_data'])?>">
												<button class="btn btn-xs btn-warning waves-effect waves-effect">
													<i class="fa fa-pencil"></i> Edit
												</button>
											</form>
										<?php
										}
										?>
										</td>
									</tr>
									<?php
									$no++;
								}
								?>
							</tbody>
						</table>
					</div>
				</div>	
			</div>	
		</div>
	</div>
</div>