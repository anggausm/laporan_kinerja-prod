<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<i class="fa fa-home putih"></i> 
						<a href="<?php print base_url() . 'app'?>"> Portal</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php print base_url()?>sistem_lldikti/ajuan_jafa"> Rekap Pengajuan Jabatan Fungsional </a>
					</li>
					<li class="breadcrumb-item">
						<a href="#"> Edit </a>
					</li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="panel-heading bg-white">
						<i class="fa fa-user"></i> <b>Form Update Pengajuan Jabatan Fungsional </b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url() . 'sistem_lldikti/ajuan_jafa/update'?>">
							<input type="hidden" name="id" value="<?php print enc_data($ajuan['id_data'])?>">
							<div class="row">
								<div class="col-md-9">
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right">Nama Dosen </label>
										<div class="col-sm-9">
											<select class="form-control" name="dosen" required="">
												<option value=""> Piliha Dosen </option>
												<?php
												foreach ($dosen['result'] as $dsn) {
													?>
													<option value="<?=$dsn['user_key']?>" <?php if ($dsn['user_key'] == $ajuan['user_key']) {
														echo "selected";
													} ?> > 
														<?=$dsn['nama_doskar']?> 
													</option>
													<?php
												} ?> 
				                            </select>
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right"> Bidang Ilmu </label>
										<div class="col-sm-9">
											<input type="text" class="form-control" name ="bidang" value="<?= $ajuan['bidang_ilmu'] ?>" required="">
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right"> Ajuan JaFa </label>
										<div class="col-sm-9">
											<select class="form-control form-control-sm" name="jafa" required="">
												<option value=""> Piliha Jabatan Fungsional </option>
			                                    <?php
												foreach ($jafa['result'] as $jf) {
													?>
													<option value="<?=$jf['id_jafa']?>" <?php if ($jf['id_jafa'] == $ajuan['id_jafa']) {
														echo "selected";
													} ?>> <?=$jf['nama_jafa']?> </option>
													<?php
												} ?> 
				                            </select>
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right"> Status Ajuan </label>
										<div class="col-sm-9">
											<select class="form-control form-control-sm" name="status_ajuan" required="">
												<option value=""> Piliha Status Ajuan </option>
				                                    <?php
													foreach ($jenis['result'] as $jns) {
														?>
														<option value="<?=$jns['id']?>" <?php if ($jns['id'] == $ajuan['id_jafa_ajuan']) {
															echo "selected";
														} ?>> 
														<?= $jns['ajuan_jafa'] ?> 
														</option>
														<?php
													} ?> 
				                            </select>
										</div>
									</div>
								</div>
							</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-info pull-center" style="margin-top: 10px; margin-bottom: 10px"> Update </button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>