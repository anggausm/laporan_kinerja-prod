<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<i class="fa fa-home putih"></i> 
						<a href="<?php print base_url() . 'app'?>"> Portal</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php print base_url()?>sistem_lldikti/ajuan_jafa"> Rekap Pengajuan Jabatan Fungsional </a>
					</li>
					<li class="breadcrumb-item">
						<a href="#"> Tambah </a>
					</li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="panel-heading bg-white">
						<i class="fa fa-user"></i> <b>Form Tambah Pengajuan Jabatan Fungsional</b>
						<small class="text-muted"> </small>
					</div>
					<div class="panel-body">
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url() . 'sistem_lldikti/ajuan_jafa/simpan'?>">
							<div class="row">
								<div class="col-md-9">
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right">Nama Dosen </label>
										<div class="col-sm-9">
											<select class="form-control select2" name="dosen" required="">
												<option value=""> Piliha Dosen </option>
												<?php
												foreach ($dosen['result'] as $dsn) {
													?>
													<option value="<?=$dsn['user_key']?>"> <?=$dsn['nama_doskar']?> </option>
													<?php
												} ?> 
				                            </select>
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right"> Bidang Ilmu </label>
										<div class="col-sm-9">
											<input type="text" class="form-control" name ="bidang" required="">
										</div>
									</div>
									<div class="form-group ">
										<label class="col-sm-3 control-label text-right"> Ajuan JaFa </label>
										<div class="col-sm-9">
											<select class="form-control form-control-sm" name="jafa" required="">
												<option value=""> Piliha Jabatan Fungsional </option>
			                                    <?php
												foreach ($jafa['result'] as $jf) {
													?>
													<option value="<?=$jf['id_jafa']?>"> <?=$jf['nama_jafa']?> </option>
													<?php
												} ?> 
				                            </select>
										</div>
									</div>
								</div>
							</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-info pull-center" style="margin-top: 10px; margin-bottom: 10px">Simpan</button>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>