<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<i class="fa fa-home putih"></i> 
						<a href="<?php print base_url() . 'app'?>"> Portal</a>
					</li>
					<li class="breadcrumb-item">
						<a href="<?php print base_url()?>sistem_lldikti/ajuan_jafa"> Rekap Pengajuan Jabatan Fungsional </a>
					</li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<?php if ($crud['c'] == 1) 
				{ ?>
				<div class="panel-heading bg-white">
					<form method="POST" action="<?php print base_url() . 'sistem_lldikti/ajuan_jafa/tambah'?>">
						<button class="btn btn-info btn-s">
							<i class="fa fa-save"></i> Tambah 
						</button>
					</form>
				</div>
				<?php 
				}
				?>
				<div class="panel-body">
					<div class="table-responsive">
						<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
							<thead>
								<tr>
									<th style="width:5%"  class="text-center">No</th>
									<th style="width:65%"> Status Ajuan </th>
									<th style="width:15%" class="text-center"> Jumlah </th>
									<th style="width:15%" class="text-center">Detail</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no 	= 1;
								$jml 	= 0;
								foreach ($get_data['result']['rs'] as $value) 
								{
    								?>
									<tr>
										<td class="text-center"> <?=$no?> </td>
										<td> <?=$value['ajuan_jafa']?> </td>
										<td class="text-center"> <?=$value['j_peserta']?> </td>
										<td class="text-center">
										<form method="POST" action="<?php print base_url() . 'sistem_lldikti/ajuan_jafa/detail'?>">
											<input type="hidden" name="id" value="<?php print enc_data($value['id'])?>">
											<button class="btn btn-xs btn-accent waves-effect waves-effect">
												<i class="fa fa-list"></i> Detail 
											</button>
										</form>
										</td>
									</tr>
									<?php
									$jml = $jml + $value['j_peserta'];
									$no++;
								}
								?>
							</tbody>
						</table>
						<p>
							<b>TOTAL AJUAN : <?= $jml ?> </b>
						</p>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>