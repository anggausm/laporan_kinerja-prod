<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/wfh">Presensi Kerja Online</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/wfh/mulai_kerja">Mulai Bekerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-file"></i>  Laporan Pekerjaan
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">

							<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>absensi/wfh/update_laporan" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group ">
											<input type="hidden" name="id_task" value="<?php print $task['result']['id_task']?>">
											<label class="col-sm-4 control-label text-right">Rencana Pekerjaan  </label>
											<div class="col-sm-8">
												<input type="text" class="form-control"  name ="judul_kerjaan" value="<?php print $task['result']['judul_kerjaan'];?>" required=""  >
											</div>
										</div>
										<div class="form-group ">
											<label class="col-sm-4 control-label text-right">File Laporan</label>
											<div class="col-sm-8">
												<input type="file" class="form-control"  name ="nama_file"  >
												<i>* untuk type file yang di ijinkan jpg, jpeg, png, pdf, xls, xlsx, ppt, doc, docx</i>
											</div>
										</div>
										<div class="form-group ">
											<label class="col-sm-4 control-label text-right">Url Laporan </label>
											<div class="col-sm-8">
												<input type="text" class="form-control"  name ="link" value=""  value="<?php print $task['result']['link'];?>" >
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group ">
											<label class="col-sm-4 control-label text-right">Catatan </label>
											<div class="col-sm-8">
												<textarea name="keterangan" placeholder="Catatan" class="form-control" rows="5" required=""><?php print $task['result']['keterangan'];?></textarea>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<div class="col-sm-2"></div>
								<div class="col-sm-4"><button class="btn btn-info btn-sm"><i class="fa fa-save"></i> Simpan Laporan </button></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>