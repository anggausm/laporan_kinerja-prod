<style type="text/css">
	#refreshButton {
		position: absolute;
		top: 10px;
		right: 10px;
		padding: 10px;
		z-index: 1000;
	}
</style>
<div >
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/wfh">Presensi Kerja Online</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
	if (!empty($this->session->flashdata('pesan'))) {
		?>
		<div class="alert alert-success" role="alert">
			<?php print $this->session->flashdata('pesan')?>
		</div>
		<?php
	}
	?>
	<div class="row">
		<div class="col-md-12">
			
			<div class="alert alert-info">
				<i class="fa fa-warning"></i>   &nbsp;  Untuk mengakses aplikasi presensi melalui smartphone, anda bisa mendownload aplikasi di tombol download di bawah ini. (Versi terbaru rilis v2.0.1)<br>
		<a href="https://drive.google.com/file/d/18ytBX4yMqk-pcj2LAX4MwMZj-jfEAW68/view?usp=sharing" target="_blank" >
					<button class="btn btn-info">
						<i class="fa fa-download"></i> Download
					</button>
				</a>
			</div>

			<div id="peta" style="height: 200px" style="z-index: 1">
				<a class="btn btn-sm btn-primary" href="" id="refreshButton"><i class="fa fa-refresh"></i> Refresh</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-coffee"></i>  Presensi Kerja Online  <span style="float: right;"><b id="txt"></b></span>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body" id="app">
					<div class="btn-groups">
						<div  v-cloak>
							<div v-if="errorStr">
								<div class="alert alert-warning" role="alert" >
									<p style="text-align: justify;">	Tombol presensi tidak aktif, Silahkan refresh halaman browser untuk mengaktifkan fungsi Location Service dan klik Allow Location, <br>atau aktifkan terlebih dahulu fungsi Location Service pada smartphone sebelum melakukan presensi</p>
								</div>
							</div>
							<div v-if="gettingLocation">
								<i>Getting your location...</i>
							</div>
							<div v-if="location">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<form method="POST" action="<?php print base_url()?>absensi/wfh/presensi">

											<input type="hidden" name="hari" value="<?php print enc_data($hari);?>">
											<input type="hidden" name="tanggal" value="<?php print enc_data($tgl);?>">
											<input type="hidden" name="jam" value="<?php print enc_data($jam);?>">
											<input type="hidden" name="latitude"   v-model="latitude">
											<input type="hidden" name="longitude" v-model="longitude">
											<input type="hidden" name="latitude2" v-model ="latitude2" id="latitude2">
											<input type="hidden" name="longitude2" v-model ="longitude2" id="longitude2">

											<button type="submit" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect" style="width: 100%">
												<i class="fa  fa-play fa-3x pull-left"></i>
												<b class="block clear text-left m-v-xs">Klik disini untuk <b class="text-lg block font-bold">Mulai Kerja</b>
												<?php print $hari . ', ' . $tgl;?>
												<?php //print $hari . ', ' . $tgl . ' <br>Jam ' . $jam;?>

											</button>

										</form>
									</div>
								</div>

							</div>

						</div>
					</div>
					<div id="mapid"></div>

					<!-- <div class="btn-groups">
						<form method="POST" action="<?php print base_url()?>absensi/wfh/presensi">
							<input type="hidden" name="hari" value="<?php print $hari;?>">
							<input type="hidden" name="tanggal" value="<?php print $tgl;?>">
							<input type="hidden" name="hari" value="<?php print $jam;?>">
							<input type="hidden" name="latitude" v-model ="location.coords.latitude">
							<input type="hidden" name="longitude" v-model="location.coords.longitude">
							<button type="submit" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect">
							<i class="fa  fa-play fa-3x pull-left"></i>
							<bu class="block clear text-left m-v-xs">Klik disini untuk <b class="text-lg block font-bold">Mulai Kerja</b>
							<?php print $hari . ', ' . $tgl . ' <br>Jam ' . $jam;?>
						</span>
						</button>
					</form> -->
				</div>
			</div>
			<div id="mapid"></div>
			<!-- <div class="md-whiteframe-z0 bg-white">
				<ul class="nav nav-md nav-tabs nav-lines b-info">
					<li class="active">
						<a href="" data-toggle="tab" data-target="#tab_1" aria-expanded="false"><i class="fa fa-file"></i> Panduan Kerja Online</a>
					</li>
					<li class="">
						<a href="" data-toggle="tab" data-target="#tab_2" aria-expanded="false"><i class="fa fa-file"></i> Panduan Mengatasi Error</a>
					</li>
				</ul>
				<div class="tab-content p m-b-md b-t b-t-2x">
					<div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_1">
						<iframe src="https://apps.usm.ac.id/kpg/file/Panduan_presensi_WFH.pdf" width="100%" height="500px">
						</iframe>
					</div>
					<div role="tabpanel" class="tab-pane animated fadeIn active" id="tab_2">
						<iframe src="https://apps.usm.ac.id/kpg/file/PANDUAN_MENGATASI_ERROR.pdf" width="100%" height="500px">
						</iframe>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>
</div>

<?php

if ($notifikasi == '1') {
	?>
	<script type="text/javascript">
		alert("Untuk melakukan presensi, disarankan menggunakan HP Smartphone Anda dengan GPS diaktifkan.");
	</script>

<?php }
?>
<!-- JS App-->
<script type="text/javascript">
	var notifikasi = '<?php echo $notifikasi ?>'
	var latitude = '<?php echo $latitude ?>'
	var longitude = '<?php echo $longitude ?>'
	var mapOptions = {
		center: [-6.981928, 110.452198],
		zoom: 13
	}
	var peta = new L.map('peta', mapOptions);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: ''
	}).addTo(peta);
	var markerGroup = L.layerGroup();
	peta.addLayer(markerGroup);

	function onLocationFound(e) {
		var radius = e.accuracy / 2;
		document.getElementById('latitude2').value = e.latlng.lat
		document.getElementById('longitude2').value = e.latlng.lng
	}

	var app = new Vue({
		el: '#app',
		data: {
			location: null,
			gettingLocation: false,
			errorStr: null,
			longitude: null,
			latitude: null,
			longitude2: null,
			latitude2: null
		},
		created() {
	        //do we support geolocation
	        if (!("geolocation" in navigator)) {
	        	this.errorStr = 'Geolocation is not available.';
	        	return;
	        }
	        this.gettingLocation = true;
	        // get position
	        navigator.geolocation.getCurrentPosition(pos => {
	        	this.gettingLocation = false;
	        	this.location = pos;
	        	if (latitude != '' && longitude != '' && notifikasi == '1') {
	        		this.latitude = latitude;
	        		this.longitude = longitude;
	        		var marker = L.marker([latitude, longitude]);
	        		markerGroup.addLayer(marker);
	        		peta.fitBounds([
	        			[latitude, longitude]
	        			]);
	        	} else {
	        		this.latitude = pos.coords.latitude;
	        		this.longitude = pos.coords.longitude;
	        		var marker = L.marker([pos.coords.latitude, pos.coords.longitude]);
	        		markerGroup.addLayer(marker);
	        		peta.fitBounds([
	        			[pos.coords.latitude, pos.coords.longitude]
	        			]);
	        	}
	            // markerGroup.clearLayers()
	            // var marker = L.marker([pos.coords.latitude, pos.coords.longitude]);
	            // markerGroup.addLayer(marker);
	            // peta.fitBounds([
	            //     [pos.coords.latitude, pos.coords.longitude]
	            // ]);

	        }, err => {
	        	this.gettingLocation = false;
	        	this.errorStr = err.message;
	        })
	        setInterval(function() {
	        	peta.on('locationfound', onLocationFound);
	        	peta.locate({
	        		setView: false,
	        		maxZoom: 19
	        	});
	        }, 1000);

	    }
	})
	</script