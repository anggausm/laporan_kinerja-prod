<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/riwayat ">Riwayat  Laporan Pekerjaan </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i> Laporan Pekerjaan Piket
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>absensi/piket/riwayat" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Tahun </label>
									<div class="col-sm-8">
										<select class="form-control" name="tahun">
											<option value="2020" <?php if ($tahun == '2020') {print 'selected=""';}?> >2020</option>
											<option value="2021" <?php if ($tahun == '2021') {print 'selected=""';}?> >2021</option>
											<option value="2022" <?php if ($tahun == '2022') {print 'selected=""';}?> >2022</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-4 control-label text-right">Bulan </label>
									<div class="col-sm-8">
										<select class="form-control" name="bulan">
											<option value="01" <?php if ($bulan == '01') {print 'selected=""';}?>>Januari</option>
											<option value="02" <?php if ($bulan == '02') {print 'selected=""';}?> >Februari</option>
											<option value="03" <?php if ($bulan == '03') {print 'selected=""';}?> >Maret</option>
											<option value="04" <?php if ($bulan == '04') {print 'selected=""';}?> >April</option>
											<option value="05" <?php if ($bulan == '05') {print 'selected=""';}?> >Mei</option>
											<option value="06" <?php if ($bulan == '06') {print 'selected=""';}?> >Juni</option>
											<option value="07" <?php if ($bulan == '07') {print 'selected=""';}?> >Juli</option>
											<option value="08" <?php if ($bulan == '08') {print 'selected=""';}?> >Agustus</option>
											<option value="09" <?php if ($bulan == '09') {print 'selected=""';}?> >September</option>
											<option value="10" <?php if ($bulan == '10') {print 'selected=""';}?> >Oktober</option>
											<option value="11" <?php if ($bulan == '11') {print 'selected=""';}?> >November</option>
											<option value="12" <?php if ($bulan == '12') {print 'selected=""';}?> >Desember</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Riwayat Pekerjaan</button>
							</div>
						</div>
					</form>
					<br>
					<h5><i class="fa  fa-calendar"></i> Riwayat  Pekerjaan <b>Bulan <?php print $get_default['result']['bulan']?></b></h5>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th style="width:5%" class="text-center">No</th>
												<th style="width:15%" class="text-center">Hari / Tanggal </th>
												<th style="width:15%">Rencana Kerja</th>
												<th style="width:15%" class="text-center">Jam Masuk</th>
												<th style="width:15%" class="text-center">Jam Pulang</th>
												<th style="width:20%" class="text-center">Lokasi Masuk / Pulang</th>
												<th style="width:15%" class="text-center">Detail</th>
											</tr>
										</thead>
										<tbody>
											<?php
$no = 1;
foreach ($get_default['result']['rs'] as $value) {
    // print var_dump($value);
    ?>
											<!--   -->
											<tr>
												<td class="text-center">
													<?=$no?>
												</td>
												<td><?=$value['hari'] . ', ' . $value['tgl']?></td>
												<td style="text-align: center;"><?php print $value['jm_task'];?></td>
												<td class="text-center">
													<?php if (empty($value['jam_masuk'])) {
        ?>
													<!-- <i class="fa fa-frown-o" style="color: #ff4e09"></i> Tidak Presensi -->

													<?php } else {
        print "<i class='fa fa-check-square-o' style='color:#0c9508'></i>" .
            $value['jam_masuk'];
    }?>
												</td>
												<td class="text-center">
													<?php if (empty($value['jam_pulang'])) {

        ?>
													<!-- <i class="fa fa-frown-o" style="color: #ff4e09"></i> Tidak Presensi -->
													<?php } else {
        print "<i class='fa fa-check-square-o' style='color:#0c9508'></i>" .
            $value['jam_pulang'];
    }?> </td>
												<td class="text-center">

													<?php if (!empty($value['kordinat_masuk'])) {?>
														<a  data-toggle="modal" data-target="#masuk" v-on:click ="pres_masuk('<?php print $value['kordinat_masuk'];?>')">
														<button class="btn btn-primary waves-effect btn-xs"><i class="fa  fa-map-marker"></i> Masuk </button>
													</a>


													<?php }?>

													<?php if (!empty($value['kordinat_pulang'])) {?>
															<a  data-toggle="modal" data-target="#pulang" v-on:click ="pres_pulang('<?php print $value['kordinat_pulang'];?>')">
														 <button class="btn  btn-accent btn-xs "><i class="fa fa-map-marker"></i> Pulang</button>
													</a>


													<?php }?>

												</td>
												<td  class="text-center">
													<?php
if (empty($value['id_absen'])) {
    } else {
        ?>
													<form method="POST" action="<?php print base_url() . 'absensi/piket/riwayat/detail_task'?>">
														<input type="hidden" name="id_absen" value="<?php print enc_data($value['id_absen'])?>">
														<button class="btn btn-info btn-xs"><i class="fa fa-check-square-o"></i> Rincian Pekerjaan </button>
													</form>
													<?php }?>
												</td>
											</tr>
											<?php
$no++;
}
?>
										</tbody>
									</table>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



	<div id="masuk" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lokasi Presensi Masuk</h4>
				</div>
				<div class="modal-body">
					<div id="peta"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="pulang" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Lokasi Presensi Pulang</h4>
				</div>
				<div class="modal-body">
					<div id="peta_pulang"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>



<script type="text/javascript">
	var mapOptions = {
	    center: [-6.981928, 110.452198],
	    zoom: 13
	}
	var peta = new L.map('peta', mapOptions);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: ''}).addTo(peta);
	var markerGroup = L.layerGroup();
	peta.addLayer(markerGroup)

	var peta_pulang = new L.map('peta_pulang', mapOptions);
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: ''}).addTo(peta_pulang);
	var markerGroupPulang = L.layerGroup();
	peta_pulang.addLayer(markerGroupPulang)

	var app = new Vue({
	    el: '#app',
	    data: {
	        loading: '1',
	        kor_masuk: '',
	    },
	    mounted: function() {},
	    methods: {
	        pres_masuk: function(kor_masuk) {
	            // console.log(kor_masuk);
	            markerGroup.clearLayers()
	            var latlong = kor_masuk.split(',');
	            var marker = L.marker([ latlong[0],latlong[1] ]);
	            markerGroup.addLayer(marker);
	            peta.fitBounds([ [latlong[0],latlong[1]]  ]);
	            document.getElementById('masuk').style.display = 'block';
	            setTimeout(function() {peta.invalidateSize()}, 1000);
	        },
	        pres_pulang: function(kor_masuk) {
	            // console.log(kor_masuk);
	            markerGroupPulang.clearLayers()
	            var latlong = kor_masuk.split(',');
	            var marker = L.marker([ latlong[0],latlong[1] ]);
	            markerGroupPulang.addLayer(marker);
	            peta_pulang.fitBounds([ [latlong[0],latlong[1]] ]);
	            document.getElementById('pulang').style.display = 'block';
	            setTimeout(function() {peta_pulang.invalidateSize()}, 1000);
	        }
	    },
	})

</script>