<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url('restruktur/pimpinan_fakultas') ?>">Re Struktur Pimpinan Fakultas </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if (!empty($this->session->flashdata('pesan'))): ?>
	<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('pesan'); ?>
	</div>
	<?php endif?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa  fa-folder-open"></i>  Re Struktur Pimpinan Fakultas
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>restruktur/pimpinan_fakultas">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group ">
									<label class="col-sm-3 control-label text-right">Fakultas</label>
									<div class="col-sm-9">
										<select class="form-control" name="kode_fakultas" required>
											<option value="">Pilih Fakultas</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<button class="btn btn-info btn-sm"><i class="fa fa-search"></i> Filter</button>
							</div>
						</div>
					</form>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th class="text-center" width="2%">No</th>
												<th class="text-center">Unit </th>
												<th class="text-center">Sub Struktural </th>
												<th class="text-center">Anggota</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 1;?>
											<?php foreach ($data as $value): ?>
											<tr>
												<td class="text-center"><?php echo $no++ ?></td>
												<td><?php echo $value['unit_bagian'] ?></td>
												<td><?php echo $value['nama_sub_struktural'] ?><br><?php echo $value['programstudi'] ?></td>
												<td><?php echo $value['ket_anggota'] ?></td>
												<td>
													<a href="<?php echo site_url("restruktur/pimpinan_fakultas/edit/$value[id_sub_struktural]") ?>" class="btn btn-sm btn-primary">
														Re Struktur
													</a>
												</td>
											</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>