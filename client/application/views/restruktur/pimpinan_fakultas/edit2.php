<div id="app">
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php echo site_url('restruktur/pimpinan_fakultas') ?>">Re Struktur Pimpinan Fakultas </a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php if (!empty($this->session->flashdata('pesan'))): ?>
	<div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('pesan'); ?>
	</div>
	<?php endif?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-folder-open"></i>  Re Struktur Pimpinan Fakultas
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Unit</label>
								<input type="text" class="form-control" disabled value="<?php echo $struktural['unit_bagian'] ?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Struktural</label>
								<input type="text" class="form-control" disabled value="<?php echo $struktural['nama_sub_struktural'] ?>">
							</div>
						</div>
					</div>
					<hr>
					<form method="POST" action="<?php echo site_url('restruktur/pimpinan_fakultas/simpan') ?>">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Doskar</label>
									<select class="form-control select2" name="doskar_usm" required>
										<option value="">Pilih Doskar</option>
										<?php foreach ($doskar_usm as $value): ?>
										<option value="<?php echo $value['user_key'] ?>"><?php echo $value['nama_doskar'] ?></option>
										<?php endforeach?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Fakultas</label>
									<select class="form-control select2" name="fakultas" required>
										<option value="">Pilih Fakultas</option>
										<?php foreach ($fakultas as $value): ?>
										<option value="<?php echo $value['kode_fakultas'] ?>"><?php echo $value['fakultas'] ?></option>
										<?php endforeach?>
									</select>
								</div>
							</div>
						</div>
						<?php if ($struktural['id_sub_struktural'] == 113 || $struktural['id_sub_struktural'] == 60): ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Program Studi</label>
									<select class="form-control select2" name="program_studi" required>
										<option value="">Pilih Program Studi</option>
										<?php foreach ($program_studi as $value): ?>
										<option value="<?php echo $value['kode_khusus'] ?>"><?php echo $value['programstudi'] ?></option>
										<?php endforeach?>
									</select>
								</div>
							</div>
						</div>
						<?php endif?>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" name="id_sub_struktural" value="<?php echo $struktural['id_sub_struktural'] ?>">
								<button type="submit" class="btn btn-primary">Tambah Anggota</button>
							</div>
						</div>
					</form>
					<div class="row">
						<div class="col-md-12">
							<div class="btn-groups">
								<div class="table-responsive">
									<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered">
										<thead>
											<tr>
												<th class="text-center" width="2%">No</th>
												<th class="text-center">Nama Doskar</th>
												<th class="text-center">Struktural</th>
												<th class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 1;?>
											<?php foreach ($anggota as $value): ?>
											<tr>
												<td class="text-center"><?php echo $no++ ?></td>
												<td><?php echo $value['nama_doskar'] ?></td>
												<td><?php echo $value['ket_struktural'] ?> <?php echo $value['fakultas'] ?> <?php echo $value['programstudi'] ?></td>
												<td class="text-center">
													<a href="<?php echo site_url("restruktur/pimpinan_fakultas/delete/$struktural[id_sub_struktural]/$value[id_angota_struktural]") ?>" class="btn btn-sm btn-danger"  onclick="return confirm('Anda yakin ingin menghapus data ini?')">
														Hapus
													</a>
												</td>
											</tr>
											<?php endforeach?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>