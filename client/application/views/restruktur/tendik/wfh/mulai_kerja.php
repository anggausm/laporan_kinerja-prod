<style type="text/css">
#refreshButton {
	position: absolute;
	top: 10px;
	right: 10px;
	padding: 10px;
	z-index: 1000;
}
</style>
<div >
	<div class="row">
		<div class="col-sm-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><i class="fa fa-home putih"></i> <a href="<?php print base_url() . 'app'?>"> Portal</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/wfh">Presensi Kerja Online</a></li>
					<li class="breadcrumb-item"><a href="<?php print base_url()?>absensi/wfh/mulai_kerja">Mulai Bekerja</a></li>
					<span  style="float: right;">
						<a onclick="window.history.go(-1); return false;"><i class="fa fa-chevron-left putih" ></i> &nbsp;Kembali</a>
					</span>
				</ol>
			</nav>
		</div>
	</div>
	<?php
if (!empty($this->session->flashdata('pesan'))) {
    ?>
	<div class="alert alert-success" role="alert">
		<?php print $this->session->flashdata('pesan')?>
	</div>
	<?php
}
?>
	<div class="row">
		<div class="alert alert-warning"><i class="fa fa-warning"></i> Sebelum melakukan presensi, pastikan titik lokasi sesuai dengan posisi anda, jika belum klik tombol Refresh di sisi kanan atas di peta.</div>
		<div class="col-md-12">
			<div id="peta" style="height: 200px" style="z-index: 1">
				<a class="btn btn-sm btn-primary" href="" id="refreshButton"><i class="fa fa-refresh"></i> Refresh</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading bg-white">
					<i class="fa fa-list"></i> Rencana Kerja
					<div class="pull-right">   <b id="txt"></b>
					</div>
					<small class="text-muted"> </small>
				</div>
				<div class="panel-body">
					<div class="row"  id="app">
						<div class="btn-groups text-center"  >
							<div class="col-md-6">
								<div style="float:  left;" style="width: 100%">
									<a href="" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect" style="width: 100%">
										<i class="fa  fa-clock-o fa-3x pull-left"></i>
										<span class="block clear text-left m-v-xs">Hari <?php print $wfh['result']['detail_wfh']['hari'];?>, <?php print $wfh['result']['detail_wfh']['tgl'];?>
											<br> Mulai Kerja Jam  <b class="text-lg block font-bold"><?php print $wfh['result']['detail_wfh']['jam_masuk'];?><br></b>
										</span>
									</a>
								</div>
							</div>
							<div class="col-md-6">
								<div style="float: right;">
									<?php
if (empty($wfh['result']['detail_wfh']['jam_pulang'])) {?>
									<div  v-cloak>
										<div v-if="errorStr">
											<div class="alert alert-warning" role="alert">
												<p style="text-align: justify;">
													Tombol presensi tidak aktif, Silahkan refresh halaman browser untuk mengaktifkan fungsi Location Service dan klik Allow Location, atau aktifkan terlebih dahulu fungsi Location Service pada smartphone sebelum melakukan presensi
												</p>
											</div>
										</div>
										<div v-if="gettingLocation">
											<i>Getting your location...</i>
										</div>
										<div v-if="location">
											<form action="<?php print base_url()?>absensi/wfh/selesai_kerja" method ="POST">
												<input type="hidden" name="latitude" v-model ="latitude">
												<input type="hidden" name="longitude" v-model="longitude">
												<input type="hidden" name="latitude2" v-model ="latitude2" id="latitude2">
												<input type="hidden" name="longitude2" v-model ="longitude2" id="longitude2">
												<button type="submit" class="text-sm btn btn-lg btn-rounded btn-stroke btn-warning m-r waves-effect" style="width: 100%" onclick="return confirm('Apakah anda telah selesai bekerja, klik YA atau OK untuk selesai kerja.');">
												<i class="fa  fa-paper-plane fa-3x pull-left"></i>
												<span class="block clear text-left m-v-xs">
													.<br>
													Klik disini setelah <b class="text-lg block font-bold">Selesai Kerja</b>
												</span>
												</button>
											</form>
										</div>
									</div>
									<?php
} else {
    ?>
									<div style="float: right;">
										<a href="" class="text-sm btn btn-lg btn-rounded btn-stroke btn-info m-r waves-effect" style="width: 100%">
											<i class="fa fa-smile-o fa-3x pull-left"></i>
											<span class="block clear text-left m-v-xs">Hari <?php print $wfh['result']['detail_wfh']['hari'];?>, <?php print $wfh['result']['detail_wfh']['tgl'];?>
												<br> Selesai Kerja Jam  <b class="text-lg block font-bold"><?php print $wfh['result']['detail_wfh']['jam_pulang'];?><br></b>
											</span>
											<!-- 			<br> Lokasi Kordinat anda {{ location.coords.latitude }}, {{ location.coords.longitude}} -->
										</a>
									</div>
									<?php
}
?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<?php
if (empty($wfh['result']['detail_wfh']['jam_pulang'])) {?>
						<h5><i class="fa fa-plus"></i> Rencana Pekerjaan Hari Ini </h5>
						<form  class="form-horizontal p-h-xs" method="POST" action="<?php print base_url()?>absensi/wfh/add_task_list" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-10">
									<div class="form-group ">
										<div class="col-sm-12">
											<input type="text" name="judul_kerjaan" placeholder="Tambah rencana pekerjaan hari ini" class="form-control" required="" minlength="10">
										</div>
									</div>
								</div>
								<div  class="col-md-2 col-xs-12">
									<button class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Tambah Rencana Pekerjaan</button>
								</div>
							</div>
						</form>
						<hr>
						<?php }?>
						<div class="table-responsive">
							<table ui-jp="dataTable" class="table table-striped b-t b-b table-bordered" ui-options="{  }"  data-page-length='5'  style="width:100%">
								<thead>
									<tr>
										<th  style="width:5%"  >No </th>
										<th  style="width:65%" >Rencana Pekerjaan </th>
										<th  style="width:15%" >File Laporan </th>
										<th  style="width:10%; text-align: center;" >Status</th>
										<th  style="width:15%" style="text-align: center;"> </th>
									</tr>
								</thead>
								<tbody>
									<?php
$no = 1;
foreach ($wfh['result']['get_task_day'] as $value) {
    ?>
									<tr >
										<td style="text-align: center;"><?php print $no;?></td>
										<td><?php print $value['judul_kerjaan'];?><br>
										Catatan : <?php print $value['keterangan'];?></td>
										<td class="text-center">
											<?php if (!empty($value['dokumen'])) {?>
											<a href="<?php print base_url()?>absensi/wfh/download_files/<?php print $value['id_task']?>">
												<button class="btn btn-warning btn-xs"><i class="fa fa-file"></i> Dokumen Laporan</button>
											</a>
											<?php }?>
											<?php if (!empty($value['link'])) {?>
											<a href="<?=$value['link']?>" target ="_blank">
												<button class="btn btn-primary btn-xs"><i class="fa fa-link"></i> Link Laporan </button>
											</a>
											<?php }?>
										</td>
										<td style="text-align: center;">
											<?php
if (empty($value['jam_riport'])) {
        ?>
											<button class="btn btn-warning btn-xs"><i class="fa fa-warning"></i></button>
											<?php
} else {
        ?>
											<button class="btn btn-info btn-xs"><i class="fa fa-check"></i><?php print $value['jam_riport']?></button>
											<?php
}
    ?>
										</td>
										<td style="text-align: center;">
											<?php
if (empty($value['jam_riport'])) {
        $nm = "Buat Laporan";
        ?>
											<a href="<?php print base_url()?>absensi/wfh/delete_laporan/<?php print $value['id_task'];?>" onclick="return confirm('Apakah anda akan menghapus data ini.');">
												<button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
											</a>
											<?php
} else {
        $nm = "Perbaharui Laporan";
    }?>
											<a href="<?php print base_url()?>absensi/wfh/laporan/<?php print $value['id_task'];?>"  >
												<button class="btn btn-info btn-xs"><i class="fa fa-file-text"></i>  <?php print $nm;?></button>
											</a>
										</td>
									</tr>
								</tr>
								<?php
$no++;
}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
var notifikasi = '<?php echo $notifikasi ?>'
var latitude = '<?php echo $latitude ?>'
var longitude = '<?php echo $longitude ?>'

var mapOptions = {
    center: [-6.981928, 110.452198],
    zoom: 13
}
var peta = new L.map('peta', mapOptions);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: ''
}).addTo(peta);
var markerGroup = L.layerGroup();
peta.addLayer(markerGroup)
function onLocationFound(e) {
    var radius = e.accuracy / 2;
    document.getElementById('latitude2').value = e.latlng.lat
    document.getElementById('longitude2').value = e.latlng.lng
}

// function onLocationError(e) {
//     console.log(e.message);
// }
const app = new Vue({
    el: '#app',
    data: {
        location: null,
        longitude: null,
        latitude: null,
        longitude2: null,
        latitude2: null,
        gettingLocation: false,
        errorStr: null
    },
    created() {
    	markerGroup.clearLayers()
        //do we support geolocation
        if (!("geolocation" in navigator)) {
            this.errorStr = 'Geolocation is not available.';
            return;
        }
        this.gettingLocation = true;
        // get position
        navigator.geolocation.getCurrentPosition(pos => {
            this.gettingLocation = false;
            this.location = '1';
            if (latitude != '' && longitude != '' && notifikasi == '1') {
            	this.latitude = latitude;
	            this.longitude = longitude;
	            var marker = L.marker([latitude, longitude]);
	            markerGroup.addLayer(marker);
	            peta.fitBounds([
	                [latitude, longitude]
	            ]);
            }else{
            	this.latitude = pos.coords.latitude;
	            this.longitude = pos.coords.longitude;
	            var marker = L.marker([pos.coords.latitude, pos.coords.longitude]);
	            markerGroup.addLayer(marker);
	            peta.fitBounds([
	                [pos.coords.latitude, pos.coords.longitude]
	            ]);
            }

        }, err => {
            this.gettingLocation = false;
            this.errorStr = err.message;
        })
        setInterval(function(){
        	peta.on('locationfound', onLocationFound);
			// peta.on('locationerror', onLocationError);
			peta.locate({
			    setView: false,
			    maxZoom: 19
			});
        }, 1000);

    }
})
</script>