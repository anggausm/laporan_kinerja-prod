<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Portal Universitas Semarang</title>
    <meta name="description" content="portal Universitas Semarang, USM Jaya, PDPT Terus Berkarya" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/libs/assets/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/libs/assets/font-awesome/css/font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/libs/jquery/waves/dist/waves.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/html/styles/material-design-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/libs/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/html/styles/font.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/html/styles/app.css" type="text/css" />
     <link rel="stylesheet" href="<?php print base_url();?>/assets/backend/html/styles/pu-icon/pu-icon.css" type="text/css" />
    <link rel="shortcut icon" href="<?php print base_url();?>assets/frondend/images/ico/favicon.png">
  </head>
  <body>
    <div class="app">
      <!-- / aside -->
      <!-- content -->
      <div id="content" class="app-content" role="main">
        <div class="box">
          <!-- Content Navbar -->
         <?php print $header;?>
          <!-- Content -->
          <div class="box-row">
            <div class="box-cell">
              <div class="box-inner padding">
                <div class="col-sm-4">
                  <?php print $profil;?>
              </div>
              <div class="col-sm-8">
                <?php print $konten;?>
            </div>
          </div>
        </div>
        <nav>
          <ul class="nav b-t b">
            <li>
              <a href="http://pd.usm.ac.id" target="_blank" md-ink-ripple="" class=" waves-effect">
                <i class="icon mdi-action-help i-20"></i>
                <span>Pangkalan Data Universitas Semarang</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- / content -->
  </div>
  <script  href="<?php print base_url();?>/assets/backend/libs/jquery/jquery/dist/jquery.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/libs/jquery/waves/dist/waves.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-load.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-jp.config.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-jp.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-nav.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-toggle.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-form.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-waves.js"></script>
  <script  href="<?php print base_url();?>/assets/backend/html/scripts/ui-client.js"></script>
</body>
</html>