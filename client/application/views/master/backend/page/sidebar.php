<aside id="aside" class="app-aside modal fade " role="menu">
    <div class="left">
      <div class="box bg-white">

        <div class="box-row">
          <?php
$profil = $user['result'];
$servers = $_SERVER['HTTP_REFERER'];
?>
          <div class="box-cell scrollable hover">
            <div class="box-inner">
              <div class="p hidden-folded blue-50" style="background-image:url(<?php print base_url()?>/assets/backend/html/images/bg.png); background-size:cover">
                <div class="rounded w-xs bg-white inline pos-rlt">
                  <img src="https://apps.usm.ac.id/fl/file/sso/profile/<?=$profil['foto']?>" class="img-responsive rounded">
                </div>
                <a class="block m-t-sm"  >
                  <span class="block font-bold"> <?php print $profil['users'];?></span>

                  <?php print $profil['nm_lengkap'];?>
                </a>
              </div>
              <?php
?>
              <div id="nav">
                <nav ui-nav>
                  <ul class="nav">
                    <br>
                    <li>
                      <a md-ink-ripple href="<?php print $this->app->server_portal() . 'app';?>">
                        <i class="icon mdi-device-now-widgets i-20" style="color: #ff961a"></i>
                        <span class="font-normal">Portal</span>
                      </a>

                    </li>

                    <?php if (empty($menu)) {} else {

    foreach ($menu as $value) {
        if (empty($value['sub_menu'])) {?>
<li <?php if ($_SERVER['HTTP_REFERER'] == base_url() . '' . $value['url']) {print ' class="active"';}?>>
                      <a md-ink-ripple="" href="<?php print base_url() . '' . $value['url']?>" class=" waves-effect">
                        <i class="fa <?php print $value['icon']?> i-20 "></i>&nbsp;&nbsp;
                        <span class="font-normal"><?php print $value['nm_menu']?></span>
                      </a>
                    </li>
   <?php } else {
            ?>
       <li>
                      <a md-ink-ripple>
                        <span class="pull-right text-muted">
                          <i class="fa fa-caret-down"></i>
                        </span>
                        <i class="fa <?php print $value['icon']?> i-20 "></i>&nbsp;&nbsp;
                        <span class="font-normal"><?php print $value['nm_menu']?></span>
                      </a>
                      <ul class="nav nav-sub">
                        <?php foreach ($value['sub_menu'] as $v_sub) {
                ?>
                          <li <?php if ($servers == base_url() . '' . $v_sub['url']) {print ' class="active"';}?> >
                          <a md-ink-ripple href="<?php print base_url() . '' . $v_sub['url']?>">• &nbsp;&nbsp;<?php print $v_sub['nm_menu'];
                ?></a>
                        </li>
                       <?php }?>


                      </ul>
                    </li>

  <?php }}}?>
    </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <nav>
          <ul class="nav b-t b">
            <li>
              <a href="#" target="_blank" md-ink-ripple>
                <span>Core V.1 ddk </span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </aside>
