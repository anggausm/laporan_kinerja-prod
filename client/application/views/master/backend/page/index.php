<!DOCTYPE html>
<html lang="en">
  <!-- Wb Santosa @dudokz--->
  <head>
    <meta charset="utf-8" />
    <title>PORTAL 1.1| Universitas Semarang</title>
    <meta name="description" content="Aplikasi Portal Universitas Semarang, USM JAYA, USM SELALU JAYA, PDPT KARYA NYATA." />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/assets/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/assets/font-awesome/css/font-awesome.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/jquery/waves/dist/waves.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/html/styles/material-design-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/html/styles/font.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/html/styles/app.css?v=1.2" type="text/css" />
    <link rel="shortcut icon" href="<?php print base_url();?>assets/frondend/images/ico/favicon.png">
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/loading/load.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url()?>assets/backend/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php print base_url();?>assets/date/css/bootstrap-datetimepicker.min.css" type="text/css" />
        <!-- Load --->
        <script src="<?php print base_url();?>assets/date/js/moment-with-locales.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/vue/vue.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/axios/axios.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/axios/base_url.js"></script>
    <script type="text/javascript">  function base_url(url) {   return "<?php print base_url();?>" + url;}</script>
    <link  href="<?php print base_url();?>assets/backend/libs/select2/dist/css/select2.css"  rel="stylesheet" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>
    <style type="text/css">
    #peta{
    height:60vh;
    }
    #peta_pulang{
    height:60vh;
    }
    </style>
    <script src="<?php print base_url()?>assets/backend/libs/jquery/jquery/dist/jquery.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/jquery/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
  </head>
  <body  style="margin:0;" onload="startTime()">
    <div class="app">
      <!-- aside -->
      <?php print $sidebar;?>
      <!-- content -->
      <div id="content" class="app-content" role="main">
        <div class="box">
          <!-- Content Navbar -->
          <div class="navbar md-whiteframe-z1 no-radius blue">
            <?php print $header;?>
          </div>
          <!-- Content -->
          <div class="box-row">
            <div class="box-cell">
              <div class="box-inner padding">
                <?php print $konten?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- / content -->
    </div>
    <script src="<?php print base_url()?>assets/backend/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
    <script src="<?php print base_url()?>assets/backend/libs/jquery/waves/dist/waves.js"></script>
    <script src="<?php print base_url();?>assets/backend/libs/select2/dist/js/select2.min.js"> </script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('.select2').select2();
    });
    </script>
    <script>
    function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
    }

    //  $('#myModal').modal('show'); 
    </script>
    <!-- <script src="<?php print base_url()?>assets/backend/libs/loading/load.js"></script>
    -->
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-load.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-jp.config.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-jp.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-nav.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-toggle.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-form.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-waves.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/ui-client.js"></script>
    <script src="<?php print base_url()?>assets/backend/html/scripts/sidebarmenu.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3kg7YWugGl1lTXmAmaBGPNhDW9pEh5bo&libraries=geometry"></script>
    <script src="<?php print base_url();?>assets/date/js/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php print base_url();?>assets/date/libs.js"></script>
    <?php if (!empty($add_js)): ?>
            <?php foreach ($add_js as $value): ?>
                <?php echo $value . "\n"; ?>
            <?php endforeach?>
        <?php endif?>
  </body>
</html>