<footer class="site-footer">
        <div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="footer-widget">
                <h4 class="footer-widget-title">Contact Us</h4>
                <p>Universitas Semarang
                    Jl. Soekarno-Hatta
                    Tlogosari, Semarang 50196
                    <br>
                    <i class="fa fa-phone"></i> 024-6702757<br>
                    <i class="fa fa-fax"></i> 024-6702272<br>
                    WhatsApp 087832501116 <br>
                    WhatsApp PD USM : 082134884746</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="footer-widget">
                <h4 class="footer-widget-title">Website</h4>
                <ul class="list-links">
                    <li><a href="http://usm.ac.id/" target="_blank">USM</a></li>
                    <li><a href="http://fh.usm.ac.id/" target="_blank">Fak. Hukum</a></li>
                    <li><a href="http://teknik.usm.ac.id/" target="_blank">Fak. Teknik</a></li>
                    <li><a href="http://tp.usm.ac.id/" target="_blank">Fak. Teknologi Hasil Pertanian</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="footer-widget">
                <h4 class="footer-widget-title"> </h4>
                <ul class="list-links">
                    <li><a href="http://psikologi.usm.ac.id/" target="_blank">Fak. Psikologi</a></li>
                    <li><a href="http://ftik.usm.ac.id/" target="_blank">Fak. FTIK</a></li>
                    <li><a href="http://pasca.usm.ac.id/" target="_blank">Pasca Sarjana</a></li>
                </ul>
            </div>
        </div>

        </div>

    </div> <!-- /.row -->



 <!-- /.container -->    </footer>
