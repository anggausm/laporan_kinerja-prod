<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('generateExcelColumnLetters')) {
    function generateExcelColumnLetters($n)
    {
        $letters = '';

        while ($n > 0) {
            $remainder = ($n - 1) % 26;
            $letters = chr(65 + $remainder) . $letters;
            $n = floor(($n - 1) / 26);
        }

        return $letters;
    }
}
