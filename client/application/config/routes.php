<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
//
$route['logout']['GET'] = "App_auth/logout";
$route['login']['POST'] = "App_auth/cek_login";

/*
Registrasi Mahasiswa
 */
$route['registrasi']['GET'] = "App_auth/logout";

/*REST API PRESENSI*/

$route['api/presensi/reguler/mulai']['PATCH'] = "rest_api/presensi_online/insert_presensi";
$route['api/presensi/reguler/selesai']['PATCH'] = "rest_api/presensi_online/selesai";
$route['api/presensi/reguler/cek']['PATCH'] = "rest_api/presensi_online/cek_presensi";

$route['api/presensi/piket/mulai']['PATCH'] = "rest_api/presensi_piket/insert_presensi";
$route['api/presensi/piket/selesai']['PATCH'] = "rest_api/presensi_piket/selesai";
$route['api/presensi/piket/cek']['PATCH'] = "rest_api/presensi_piket/cek_presensi";

//Laporan Pekerjaan
$route['api/presensi/reguler/renc_kerja/data']['PATCH'] = "rest_api/presensi_online/get_rencana_kerja";
$route['api/presensi/reguler/renc_kerja/data_edit']['PATCH'] = "rest_api/presensi_online/laporan";
$route['api/presensi/reguler/renc_kerja/add']['PATCH'] = "rest_api/presensi_online/add_task_list";
$route['api/presensi/reguler/renc_kerja/delete']['DELETE'] = "rest_api/presensi_online/delete_laporan";
$route['api/presensi/reguler/renc_kerja/download/(:num)']['GET'] = "rest_api/presensi_online/download_files/$i";
$route['api/presensi/reguler/renc_kerja/update']['POST'] = "rest_api/presensi_online/update_laporan";
//Histori Presensi
$route['api/presensi/histori/default']['PUT'] ="rest_api/histori_presensi/get_histori_default";
$route['api/presensi/histori/laporan']['PUT'] ="rest_api/histori_presensi/get_histori_presensi";
$route['api/presensi/histori/detail']['PATCH'] ="rest_api/histori_presensi/detail_presensi";
$route['api/presensi/histori/download']['PUT'] ="rest_api/histori_presensi/download_task";
/*
VERSI APLIKASI
 */

$route['api/versi/app']['PUT'] = "rest_api/versi/get_versi";
$route['api/banner/app']['PUT'] = "rest_api/beranda/get_banner";
$route['api/biodata/app']['PUT'] = "rest_api/beranda/get_biodata";