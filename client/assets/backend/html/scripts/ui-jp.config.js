// lazyload config

var jp_config = {
  easyPieChart:   [ './assets/backend/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
  sparkline:      [ './assets/backend/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
  plot:           [ './assets/backend/libs/jquery/flot/jquery.flot.js',
                    './assets/backend/libs/jquery/flot/jquery.flot.resize.js',
                    './assets/backend/libs/jquery/flot/jquery.flot.pie.js',
                    './assets/backend/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                    './assets/backend/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                    './assets/backend/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
  vectorMap:      [ './assets/backend/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                    './assets/backend/libs/jquery/bower-jvectormap/jquery-jvectormap.css', 
                    './assets/backend/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                    './assets/backend/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
  dataTable:      [],
  footable:       [
                    './assets/backend/libs/jquery/footable/dist/footable.all.min.js',
                    './assets/backend/libs/jquery/footable/css/footable.core.css'
                  ]
};
