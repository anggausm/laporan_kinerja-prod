<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "845f00eb-884d-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model(array('restruktur/m_tendik', 'restruktur/m_referensi'));
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_tendik->get_data();
    }
    public function simpan()
    {
        $this->app->cekRequest('POST');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_tendik->simpan();
    }
    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_tendik->edit();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_tendik->update();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_tendik->delete();
    }

    public function doskar_usm()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_referensi->doskar_usm();
    }

}
