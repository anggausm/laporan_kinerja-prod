<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Histori extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "662c130b-71c2-11ea-8d6b-1cb72c27dd68";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('absensi/m_histori');
    }

    public function index()
    {
    }
    public function default_history()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_histori->default_history();
    }

    public function histori_bulanan()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_histori->histori_bulanan();
    }

}
