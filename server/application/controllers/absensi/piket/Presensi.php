<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Presensi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "1efcc005-965e-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('absensi/piket/m_presensi');
    }

    public function index()
    {
    }
    public function insert_a_berangkat()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->insert_a_berangkat();
    }
    public function selesai()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->selesai();
    }
    public function cek_absen()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $token = $this->tkn;
        $tanggal = date('Y-m-d');
        $rs = $this->m_presensi->cek_insert($tanggal, $token['user_key']);
        return $this->app->respons_data($rs, 'Selamat Bekerja, Sehat selalu, Semangat selalu USM JAYA....', 200);
    }

    public function cek_absen_mobile()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $token = $this->tkn;
        $tanggal = date('Y-m-d');
        $rs = $this->m_presensi->cek_insert($tanggal, $token['user_key']);
        if (empty($rs)) {
            $rs = '0';
            $detail = "0";
        } else {
            $rs = $this->m_presensi->cek_insert($tanggal, $token['user_key']);
            $detail = $this->m_presensi->get_detail_presensi($rs['id_absen']);
        }
        return $this->app->respons_data(array("id_presensi" => $rs, "detail_presensi" => $detail), 'Selamat Bekerja, Sehat selalu, Semangat selalu USM JAYA....', 200);
    }

    public function get_wfh()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->get_wfh();
    }

    public function insert_task()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->insert_task();
    }
    public function delete_task()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_presensi->delete_task();
    }

    public function get_task()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->get_task();
    }

    public function riport_wfh()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_presensi->riport_wfh();
    }

    public function download_file()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_presensi->download_file();
    }
}
