<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Riwayat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "2a1d4643-965e-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('absensi/piket/m_riwayat');
    }

    public function index()
    {
    }
    public function default_history()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_riwayat->default_history();
    }

    public function histori_bulanan()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_riwayat->histori_bulanan();
    }

}
