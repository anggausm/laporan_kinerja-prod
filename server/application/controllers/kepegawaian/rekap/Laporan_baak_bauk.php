<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Laporan_baak_bauk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "442bedbd-9582-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/rekap/m_laporan_baak_bauk');
    }

    public function index()
    {
    }

    public function get_unit_lembaga()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_baak_bauk->get_unit_lembaga();
    }
    public function get_list_anggota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_baak_bauk->get_list_anggota();
    }
    public function detail_presensi()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_baak_bauk->detail_presensi();
    }
    public function download_file()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_baak_bauk->download_file();
    }

}
