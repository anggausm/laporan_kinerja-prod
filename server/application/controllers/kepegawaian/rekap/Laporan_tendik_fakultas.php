<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Laporan_tendik_fakultas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "dd3281ba-9581-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/rekap/m_laporan_tendik_fakultas');
    }

    public function index()
    {
    }

    public function get_fakultsa()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_tendik_fakultas->get_fakultsa();
    }
    public function get_list_anggota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_tendik_fakultas->get_list_anggota();
    }
    public function detail_presensi()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_tendik_fakultas->detail_presensi();
    }
    public function download_file()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_laporan_tendik_fakultas->download_file();
    }

}
