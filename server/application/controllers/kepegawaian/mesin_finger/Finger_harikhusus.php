<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_harikhusus extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_harikhusus();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_harikhusus');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_harikhusus->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_finger_harikhusus->insert();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_harikhusus->edit();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_harikhusus->update();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_finger_harikhusus->delete();
    }
}



// hari khusus
// $route['api/kpg/finger_harikhusus/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/get_data';
// $route['api/kpg/finger_harikhusus/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/insert';
// $route['api/kpg/finger_harikhusus/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/edit';
// $route['api/kpg/finger_harikhusus/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/update';
// $route['api/kpg/finger_harikhusus/delete']      ['DELETE']  = 'kepegawaian/mesin_finger/finger_harikhusus/delete';