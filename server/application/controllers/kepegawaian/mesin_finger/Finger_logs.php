<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_logs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_log();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_logs');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_logs->get_data();
    }

    // public function insert()
    // {
    //     $this->app->cekRequest('PATCH');
    //     $this->app->cek_akses($this->menu_key, 'c');
    //     $this->m_finger_logs->insert();
    // }

    // public function edit()
    // {
    //     $this->app->cekRequest('PUT');
    //     $this->app->cek_akses($this->menu_key, 'r');
    //     $this->m_finger_logs->edit();
    // }

    // public function update()
    // {
    //     $this->app->cekRequest('PATCH');
    //     $this->app->cek_akses($this->menu_key, 'u');
    //     $this->m_finger_logs->update();
    // }
}


// Mesin Finger
// $route['api/kpg/finger_logs/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_logs/get_data';
// $route['api/kpg/finger_logs/simpan'] 	['PATCH'] 	= 'kepegawaian/mesin_finger/finger_logs/insert';
// $route['api/kpg/finger_logs/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_logs/edit';
// $route['api/kpg/finger_logs/update'] 	['PATCH'] 	= 'kepegawaian/mesin_finger/finger_logs/update';