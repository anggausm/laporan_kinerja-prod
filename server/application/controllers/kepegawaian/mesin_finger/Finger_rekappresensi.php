<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_rekappresensi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_rekappresensi();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_rekappresensi');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_rekappresensi->get_data();
    }

    public function info()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_rekappresensi->get_info();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_rekappresensi->update();
    }
}


// rekap transport
// $route['api/kpg/finger_rekap_presensi/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/get_data';
// $route['api/kpg/finger_rekap_presensi/detail'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/info';
// $route['api/kpg/finger_rekap_presensi/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/update';