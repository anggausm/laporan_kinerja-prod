<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_presensikhusus extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_presensikhusus();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_presensikhusus');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_presensikhusus->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_finger_presensikhusus->insert();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_presensikhusus->edit();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_presensikhusus->update();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_finger_presensikhusus->delete();
    }
}


// Tugas Belajar
// $route['api/kpg/finger_tubel/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_tubel/get_data';
// $route['api/kpg/finger_tubel/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_tubel/insert';
// $route['api/kpg/finger_tubel/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_tubel/edit';
// $route['api/kpg/finger_tubel/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_tubel/update';