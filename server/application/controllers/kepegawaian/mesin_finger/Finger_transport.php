<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_transport extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_olahtransport();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_transport');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_transport->get_data();
    }

    public function get_olah()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_transport->olah_transport();
    }
}


// olah transport
// $route['api/kpg/finger_olah/get_data'] 	['GET'] 	= 'kepegawaian/mesin_finger/finger_transport/get_data';