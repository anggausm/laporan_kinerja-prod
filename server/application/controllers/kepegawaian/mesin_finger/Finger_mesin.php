<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_mesin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_mesin();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_mesin');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_mesin->get_data();
    }
    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_finger_mesin->insert();
    }
    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_mesin->edit();
    }
    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_mesin->update();
    }
}


// Mesin Finger
// $route['api/kpg/finger_mesin/get_data']     ['PUT']     = 'kepegawaian/mesin_finger/finger_mesin/get_data';
// $route['api/kpg/finger_mesin/simpan']       ['PATCH']   = 'kepegawaian/mesin_finger/finger_mesin/insert';
// $route['api/kpg/finger_mesin/edit']         ['PUT']     = 'kepegawaian/mesin_finger/finger_mesin/edit';
// $route['api/kpg/finger_mesin/update']       ['PATCH']   = 'kepegawaian/mesin_finger/finger_mesin/update';