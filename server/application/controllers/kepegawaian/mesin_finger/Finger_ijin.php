<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Finger_ijin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kepegawaian_ijin();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/mesin_finger/m_finger_ijin');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_ijin->get_data();
    }

    public function get_jenis()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_finger_ijin->get_jenis();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_finger_ijin->insert();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_ijin->edit();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_finger_ijin->update();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_finger_ijin->delete();
    }
}


// Ijin
// $route['api/kpg/finger_ijin/get_jenis'] ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/get_jenis';
// $route['api/kpg/finger_ijin/get_data']  ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/get_data';
// $route['api/kpg/finger_ijin/simpan']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_ijin/insert';
// $route['api/kpg/finger_ijin/edit']      ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/edit';
// $route['api/kpg/finger_ijin/update']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_ijin/update';
// $route['api/kpg/finger_ijin/delete']    ['DELETE']  = 'kepegawaian/mesin_finger/finger_ijin/delete';