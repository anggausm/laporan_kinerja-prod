<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Periode extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "2138e7a2-98a9-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/wfh/m_periode');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_periode->get_data();
    }
    public function add_data()
    {
        $this->app->cekRequest('POST');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_periode->add_data();
    }
    public function get_edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_periode->get_edit();
    }
    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_periode->update();
    }
    public function delete_data()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_periode->delete_data();
    }

}
