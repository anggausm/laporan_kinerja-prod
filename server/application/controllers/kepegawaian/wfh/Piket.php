<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Piket extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "2ae2ff64-98a9-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kepegawaian/wfh/m_piket');
    }

    public function index()
    {
    }

    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_piket->get_periode();
    }
    public function get_level()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_piket->get_level();
    }
    public function get_level_list()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_piket->get_level_list();
    }
    public function get_anggota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_piket->get_anggota();
    }

    public function jadwal_piket()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_piket->jadwal_piket();
    }
    public function simpan_jadwal_piket()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_piket->simpan_jadwal_piket();
    }
    public function delete_jadwal_piket()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_piket->delete_jadwal_piket();
    }
}
