<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_pmb extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        //PENAMBAHAN KHUSUS UNTUK RES KE APLIKASI LAIN PERLU CEK SSO TOKEN
        $this->load->model('rest/m_rest_pmb');
    }

    public function index()
    {
    }
    public function get_nama_nis()
    {
        $this->app->cekRequest('PUT');
        $this->m_rest_pmb->get_nama_nis();
    }
}
