<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Level extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //  $this->token->cek_akses_token();
        $this->tkn = $this->token->checkGlobalToken();
        //  $this->menu_key = "97938d99-38f9-11ea-8a5f-1cb72c27dd68";
        $this->token->cek_akses_app($this->tkn); //cek akses aplikasi
        $this->load->model('root/acl/m_level');
    }

    public function index()
    {
    }
    public function insert_data()
    {
        $this->app->cekRequest('PATCH');
        $this->m_level->insert_data();
    }
    public function update_data()
    {
        $this->app->cekRequest('PATCH');
        $this->m_level->update_data();
    }
    public function delete_data()
    {
        $this->app->cekRequest('DELETE');
        $this->m_level->delete_data();
    }

    public function act_simpan()
    {
        $this->app->cekRequest('PATCH');
        $this->m_level->act_simpan();
    }
    public function insert_data_roots()
    {
        $this->app->cekRequest('PATCH');
        $this->m_level->insert_data_roots();
    }
}
