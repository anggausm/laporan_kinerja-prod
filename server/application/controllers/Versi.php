<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Versi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_versi');
    }

    public function index()
    {
    }
    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->m_versi->get_data();
    }

}
