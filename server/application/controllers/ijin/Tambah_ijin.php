<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tambah_ijin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "55053042-9592-11ea-aca3-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('ijin/m_ijin');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_ijin->get_data();
    }

    public function doskar_usm()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_ijin->doskar_usm();
    }

    public function jenis_ijin()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_ijin->jenis_ijin();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_ijin->insert();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_ijin->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_ijin->update();
    }
}