<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cuti_histori extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "5f2c8e37-c6aa-11ea-95bf-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('ijin/m_cuti_histori');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_cuti_histori->get_data();
    }
}