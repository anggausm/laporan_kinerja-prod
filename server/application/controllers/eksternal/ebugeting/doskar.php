<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ebug_doskar extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "34f96dc1-8449-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('eksternal/m_ebugeting');
    }

    public function index()
    { }

    public function personal()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_ebugeting->personal();
    }
}