<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jafa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "9dc639fc-b528-11ea-95bf-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('dikti/m_jafa');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->default();
    }

    public function jenis()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->jenis_ajuan();
    }

    public function status()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->jenis_pengajuan();
    }
    
    public function detail()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->detail();
    }

    public function dosen_usm()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->dosen_usm();
    }

    public function jabfung()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->jabfung();
    }

    public function insert()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_jafa->insert();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jafa->edit();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_jafa->update();
    }

}
