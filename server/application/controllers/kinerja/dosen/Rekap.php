<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        // $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->load->model('kinerja/dosen/m_rekap');
    }

    public function get_pertanyaan()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_rekap->get_pertanyaan();
    }

    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_rekap->get_periode();
    }

    public function save()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_rekap->save();
    }
    public function ajukan()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_rekap->ajukan();
    }
}

/* 
$route['api/kinerja/dosen/rekap/get_pertanyaan']['PUT'] = 'kinerja/dosen/rekap/get_pertanyaan'; 
$route['api/kinerja/dosen/rekap/get_periode']['PUT'] = 'kinerja/dosen/rekap/get_periode';
$route['api/kinerja/dosen/rekap/save']['POST'] = 'kinerja/dosen/rekap/save';
$route['api/kinerja/dosen/rekap/ajukan']['POST'] = 'kinerja/dosen/rekap/ajukan';
*/