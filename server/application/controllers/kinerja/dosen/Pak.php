<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kinerja_dosen_pak();
        $this->load->model('kinerja/dosen/m_pak');
    }

    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_periode();
    }

    public function get_periode_pilih()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_periode_pilih();
    }

    public function get_pak()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_pak();
    }

    public function get_pak_kegiatan()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_pak_kegiatan();
    }

    public function get_pak_kegiatan_detail()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_pak_values();
    }

    public function get_pak_kegiatan_lihat()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->get_pak_lihat();
    }

    public function pak_kegiatan_import()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pak->pak_kegiatan_import();
    }

    public function pak_value_destroy()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pak->pak_value_destroy();
    }

    public function pak_update_url()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_pak->pak_update_url();
    }

    public function pak_insert_value_pesan()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pak->pak_insert_value_pesan();
    }

    public function pak_ajukan_pkd()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pak->pak_ajukan_pkd();
    }

    public function pak_kegiatan_form()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pak->pak_kegiatan_form();
    }

    public function pak_kegiatan_insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pak->pak_kegiatan_insert();
    }

    public function pak_kegiatan_update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_pak->pak_kegiatan_update();
    }

    public function pak_corevalues_integritas()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->pak_corevalues_integritas();
    }

    public function pak_tugas_tambahan()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pak->pak_tugas_tambahan();
    }

}

/* 
// PAK DOSEN
$route['api/kinerja/pak_dosen/get_periode']     ['PUT']     = 'kinerja/dosen/pak/get_periode';
$route['api/kinerja/pak_dosen/get_periode_pilih']['PUT']    = 'kinerja/dosen/pak/get_periode_pilih';
$route['api/kinerja/pak_dosen/get_pak']         ['PUT']     = 'kinerja/dosen/pak/get_pak';
$route['api/kinerja/pak_dosen/get_pak_keg']     ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan';
$route['api/kinerja/pak_dosen/get_pak_detail']  ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan_detail';
$route['api/kinerja/pak_dosen/get_pak_lihat']   ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan_lihat';
$route['api/kinerja/pak_dosen/send_pak_import'] ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_import';
$route['api/kinerja/pak_dosen/value_hapus']     ['PATCH']   = 'kinerja/dosen/pak/pak_value_destroy';
$route['api/kinerja/pak_dosen/update_url']      ['PATCH']   = 'kinerja/dosen/pak/pak_update_url';
$route['api/kinerja/pak_dosen/pesan_value']     ['PATCH']   = 'kinerja/dosen/pak/pak_insert_value_pesan';
$route['api/kinerja/pak_dosen/ajukan_pkd']      ['PATCH']   = 'kinerja/dosen/pak/pak_ajukan_pkd';
$route['api/kinerja/pak_dosen/kegiatan_form']   ['PUT']     = 'kinerja/dosen/pak/pak_kegiatan_form';
$route['api/kinerja/pak_dosen/kegiatan_simpan'] ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_insert';
$route['api/kinerja/pak_dosen/kegiatan_edit']   ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_update';
$route['api/kinerja/pak_dosen/corevalues_integritas']['PUT']= 'kinerja/dosen/pak/pak_corevalues_integritas';
$route['api/kinerja/pak_dosen/tugas_tambahan']  ['PUT']     = 'kinerja/dosen/pak/pak_tugas_tambahan';
*/