<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Penilaian_diri extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kinerja/kependidikan/m_penilaian_diri');
    }

    public function index()
    {
    }

    public function get_data()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_diri->get_data();
    }

    public function get_periode()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_diri->get_periode();
    }

    public function simpan()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_diri->simpan();
    }

}


// Penilaian Kinerja
// $route['api/kinerja/kependidikan/pertanyaan']['GET'] = 'kinerja/kependidikan/penilaian_diri';