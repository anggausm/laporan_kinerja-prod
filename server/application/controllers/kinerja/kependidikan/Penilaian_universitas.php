<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Penilaian_universitas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        // $this->menu_key = $this->lib_menu_key->kinerja_kependidikan_diri();
        $this->load->model('kinerja/kependidikan/m_penilaian_universitas');
    }

    public function get_asesi()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_universitas->get_asesi();
    }
    public function get_pertanyaan()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_universitas->get_pertanyaan();
    }
    public function save()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_universitas->save();
    }
    public function verifikasi()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_penilaian_universitas->verifikasi();
    }
}

/* 
$route['api/kinerja/kependidikan/penilaian_universitas/get_asesi']['PUT'] = 'kinerja/kependidikan/penilaian_universitas/get_asesi';
$route['api/kinerja/kependidikan/penilaian_universitas/get_pertanyaan']['PUT'] = 'kinerja/kependidikan/penilaian_universitas/get_pertanyaan';
$route['api/kinerja/kependidikan/penilaian_universitas/save']['POST'] = 'kinerja/kependidikan/penilaian_universitas/save';
$route['api/kinerja/kependidikan/penilaian_universitas/verifikasi']['POST'] = 'kinerja/kependidikan/penilaian_universitas/verifikasi';
*/