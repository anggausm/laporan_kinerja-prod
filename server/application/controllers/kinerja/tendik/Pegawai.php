<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pegawai extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "c41bab23-4fd2-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_pegawai');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_pegawai->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pegawai->delete_data();
    }

    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        $data['all_periode'] = $this->m_pegawai->get_semua_periode();
        $data['periode_now'] = $this->m_pegawai->get_periode_sekarang();
        $data['data_periode'] = $this->m_pegawai->get_periode_asid();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function get_data_periode()
    {
        $this->app->cekRequest('PUT');
        $this->m_pegawai->get_data_periode();
    }

    public function get_skp_peg()
    {
        $this->app->cekRequest('PUT');
        $this->m_pegawai->get_skp_peg();
    }

    public function get_rincian_ind()
    { $this->app->cekRequest('PUT');
        $this->m_pegawai->get_rincian_ind();
    }

    //================== ijazah
    public function insert_ijazah()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->insert_ijz();
    }

    public function data_ijazah()
    { 
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $data['all_ijazah']      = $this->m_pegawai->get_ijazah();
        $data['ijazah_terakhir'] = $this->m_pegawai->get_data_ijazah();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function edit_ijazah()
    { $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->get_ijazah_edit();
    }

    public function update_ijazah()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->update_ijazah();
    }

    public function delete_ijazah()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pegawai->delete_ijazah();
    }

    //=================== SK
    public function insert_sk()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->insert_sk();
    }

    public function data_sk()
    { 
        $this->app->cekRequest('PUT');
        $data['sk']    = $this->m_pegawai->get_sk();
        $data['skj']    = $this->m_pegawai->get_sk_jns();
        $data['skpegawai']    = $this->m_pegawai->get_sk_pegawai();
        $data['skpanitia']    = $this->m_pegawai->get_sk_panitia();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function insert_sk_pegawai()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->insert_sk_pegawai();
    }

     public function edit_sk()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->edit_sk();
    }

     public function update_sk_peg()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->update_sk_pegawai();
    }

    public function delete_sk()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pegawai->delete_sk();
    }

    //==========================sertifikat

    public function data_sertifikat()
    { 
        $this->app->cekRequest('PUT');
        
        $data['sert']    = $this->m_pegawai->get_sertifikat_user();
        $data['sertp']    = $this->m_pegawai->get_sertifikat_periode();
        $data['sertj']    = $this->m_pegawai->get_sertifikat_perjenis();
        $data['sert1']    = $this->m_pegawai->get_sertifikat_tipe1();
        $data['sert2']    = $this->m_pegawai->get_sertifikat_tipe2();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function insert_sertifikat()
    {
        $this->app->cekRequest('PATCH');
        $this->m_pegawai->insert_sertifikat();
    }

    public function edit_data_sertifikat()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->edit_data_sert();
    }

     public function update_sertifikat()
    {
        $this->app->cekRequest('PATCH');
        $this->m_pegawai->update_sertifikat();
    }

     public function delete_sertifikat()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pegawai->delete_sertifikat();
    }


    //=======================================

    public function get_log_book()
    {
        $this->app->cekRequest('PUT');

        $data['plog']    = $this->m_pegawai->get_period_log();
        $data['rlog']    = $this->m_pegawai->get_rekap_log();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function get_wd()
    {
        $this->app->cekRequest('PUT');
       
        $data['hari_kerja']    =  $this->m_pegawai->get_working_days();
        $data['presensi']    = $this->m_pegawai->get_presensi();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    //============================================

    public function data_event()
    { 
        $this->app->cekRequest('PUT');
        
        $data['events']    = $this->m_pegawai->get_semua_event();
        $data['event1']    = $this->m_pegawai->get_event_wajib();
        $data['event2']    = $this->m_pegawai->get_event_pilihan();
        $data['event_usr']    = $this->m_pegawai->get_event_user();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function insert_foto()
    {
        $this->app->cekRequest('PATCH');
        $this->m_pegawai->insert_foto();
    }


    public function get_unit()
    {
        $this->app->cekRequest('PUT');
        $this->m_pegawai->get_unit();
    }

    public function get_prodi()
    {
        $this->app->cekRequest('PUT');
        $this->m_pegawai->get_prodi();
    }

    public function cek_data_pegawai()
    { 
        $this->app->cekRequest('PUT');
        $this->m_pegawai->cek_data_pegawai();
    }

    public function cek_jenis_pegawai()
    { 
        $this->app->cekRequest('PUT');
        $this->m_pegawai->cek_jenis_pegawai();
    }

    public function data_lpp()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->get_lpp();
    }

    public function get_list_rekan()
    {
        $this->app->cekRequest('GET');
        $this->m_pegawai->get_list_rekan();
    }

    public function get_list_tanya()
    {
        $this->app->cekRequest('GET');
        $this->m_pegawai->get_list_tanya();
    }

    public function simpan_kuesioner()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->simpan_kuesioner();
    }
    
    public function get_nilai_kuesioner()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->get_nilai_kuesioner();
    }

    public function get_event_pst()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->get_event_pst();
    }

    public function delete_foto()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_pegawai->delete_foto();
    }

    public function insert_ajuan()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_pegawai->insert_ajuan();
    }

    public function cek_skp_pegawai()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pegawai->cek_skp_pegawai();
    }
   
    

    
}