<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_log extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "59f09a3f-5043-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_validasi_log');
    }

    public function index()
    {

    }

    public function periode_aktif()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_log->get_periode_aktif();
    }

    public function periode_log()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_log->get_periode_log();
    }

    public function log_pegawai()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $data['logp']   = $this->m_validasi_log->data_log_pegawai();
        $data['vlog']   = $this->m_validasi_log->data_log_valid();
        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function data_periode_log()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_log->edit_periode_log();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_validasi_log->insert_data();
    }

    public function get_list()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_log->get_list();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_validasi_log->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_validasi_log->delete_data();
    }

    // Route Validasi Log
// $route['api/kinerja/val_log/p_aktif']['PUT']    = 'kinerja/tendik/validasi_log/periode_aktif';
// $route['api/kinerja/val_log/p_log']['PUT']      = 'kinerja/tendik/validasi_log/periode_log';
// $route['api/kinerja/val_log/log_pegawai']['PUT']= 'kinerja/tendik/validasi_log/log_pegawai';
// $route['api/kinerja/val_log/data_pl']['PUT']    = 'kinerja/tendik/validasi_log/data_periode_log';
// $route['api/kinerja/val_log/simpan']['PATCH']   = 'kinerja/tendik/validasi_log/insert';
// $route['api/kinerja/val_log/hapus']['DELETE']   = 'kinerja/tendik/validasi_log/delete';

    

    
}