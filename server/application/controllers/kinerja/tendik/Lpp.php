<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lpp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "91614dbd-5f5e-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_lpp');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_lpp->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_lpp->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_lpp->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_lpp->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_lpp->delete_data();
    }

// LPP
// $route['api/kinerja/lpp/default']['PUT']    = 'kinerja/tendik/lpp/get_data';
// $route['api/kinerja/lpp/simpan']['PATCH']   = 'kinerja/tendik/lpp/insert';
// $route['api/kinerja/lpp/edit']['PUT']       = 'kinerja/tendik/lpp/edit';
// $route['api/kinerja/lpp/update']['PATCH']   = 'kinerja/tendik/lpp/update';
// $route['api/kinerja/lpp/hapus']['DELETE']   = 'kinerja/tendik/lpp/delete';
    

    
}