<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Skp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "28198d72-5044-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_skp');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        // $this->app->cek_akses($this->menu_key, 'c');
        $this->m_skp->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        // $this->app->cek_akses($this->menu_key, 'u');
        $this->m_skp->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        // $this->app->cek_akses($this->menu_key, 'd');
        $this->m_skp->delete_data();
    }

    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_periode();
    }

    public function get_ind_utama()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_ind_utama();
    }

    public function get_sub_ind()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_sub_ind();
    }

    public function get_ind_nilai()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_ind_nilai();
    }

    public function get_jenis_peg()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_jenis_peg();
    }

    public function get_skp_data()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_skp->get_skp_data();
    }

    

    
}