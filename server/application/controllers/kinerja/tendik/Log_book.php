<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log_book extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "45268465-4fce-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_log_book');
    }

    public function index()
    {

    }

   public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_log_book->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_log_book->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_log_book->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_log_book->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_log_book->delete_data();
    }

    public function periode_log()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $data['all_periode'] = $this->m_log_book->get_semua_periode();
        $data['periode_log'] = $this->m_log_book->get_periode_log_aktif();
        $data['p_log_pk'] = $this->m_log_book->get_periode_log_pk();
        $data['periode_aktif'] = $this->m_log_book->get_periode_aktif();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    // Route API log book
    // $route['api/kinerja/log_book/default']['PUT']   = 'kinerja/tendik/log_book/get_data';
    // $route['api/kinerja/log_book/simpan']['PATCH']  = 'kinerja/tendik/log_book/insert';
    // $route['api/kinerja/log_book/edit']['PUT']      = 'kinerja/tendik/log_book/edit';
    // $route['api/kinerja/log_book/update']['PATCH']  = 'kinerja/tendik/log_book/update';
    // $route['api/kinerja/log_book/hapus']['DELETE']  = 'kinerja/tendik/log_book/delete';
    // $route['api/kinerja/log_book/p_log']['PUT']     = 'kinerja/log_book/periode_log';


    

    
}