<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi_skp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "e28f2625-6456-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/tendik/m_validasi_skp');
    }

    public function index()
    {

    }

    public function periode_aktif()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_periode_aktif();
    }

    public function periode_skp()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_periode_skp();
    }

    public function log_pegawai()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $data['sk_1_2']   = $this->m_validasi_skp->sk_pegawai();
        $data['ijazah_1_2']   = $this->m_validasi_skp->ijazah();
        $data['sertifikat_1_2']   = $this->m_validasi_skp->sertifikat_komp();
        $data['sertifikat_2_4']   = $this->m_validasi_skp->sertifikat_core();
        $data['sertifikat_3_1']   = $this->m_validasi_skp->sertifikat_penunjang();
        $data['sk_3_2']   = $this->m_validasi_skp->sk_panitia();
        $data['foto_event']   = $this->m_validasi_skp->get_foto_event();
        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

    public function data_periode_skp()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->edit_periode_skp();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_validasi_skp->insert_data();
    }

    public function update()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_validasi_skp->update_data();
    }

    public function revisi()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_validasi_skp->update_revisi();
    }
    

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_validasi_skp->delete_data();
    }

     public function unvalidasi()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_validasi_skp->unvalidasi();
    }

    public function get_list()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_list();
    }

    public function get_skp()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        
        $data['skp_peg']   = $this->m_validasi_skp->get_skp();;
        $data['skp_valid']   = $this->m_validasi_skp->get_skp_valid();
        $data['nilai']   = $this->m_validasi_skp->get_rekap_nilai();

        return $this->app->respons_data($data, 'Data Berhasil Diload', 200);
    }

     public function get_skp_valid()
    {
        $this->app->cekRequest('GET');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_validasi_skp->get_skp_valid();
    }

    
    // Validasi SKP
// $route['api/kinerja/val_skp/p_aktif']['PUT']    = 'kinerja/tendik/validasi_skp/periode_aktif';
// $route['api/kinerja/val_skp/p_log']['PUT']      = 'kinerja/tendik/validasi_skp/periode_log';
// $route['api/kinerja/val_skp/log_pegawai']['PUT']= 'kinerja/tendik/validasi_skp/log_pegawai';
// $route['api/kinerja/val_skp/data_pl']['PUT']    = 'kinerja/tendik/validasi_skp/data_periode_log';
// $route['api/kinerja/val_skp/simpan']['PATCH']   = 'kinerja/tendik/validasi_skp/insert';
// $route['api/kinerja/val_skp/hapus']['DELETE']   = 'kinerja/tendik/validasi_skp/delete';
// $route['api/kinerja/val_skp/list']['GET']       = 'kinerja/tendik/validasi_skp/get_list';
// $route['api/kinerja/val_skp/skp_pegawai']['GET']= 'kinerja/tendik/validasi_skp/get_skp';
// $route['api/kinerja/val_skp/update_validasi']['PATCH']  = 'kinerja/tendik/validasi_skp/update';
// $route['api/kinerja/val_skp/update_revisi']['DELETE']   = 'kinerja/tendik/validasi_skp/revisi';
// $route['api/kinerja/val_skp/get_skp_valid']['GET']  = 'kinerja/tendik/validasi_skp/get_skp_valid';


    

    
}