<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_dosen extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        // $this->menu_key = "845f00eb-884d-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kinerja/setup/m_rekap_dosen');
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_rekap_dosen->get_data();
    }
}
/* 
$route['api/kinerja/setup/rekap_dosen/get_data']['PUT'] = 'kinerja/setup/rekap_dosen/get_data';
 */