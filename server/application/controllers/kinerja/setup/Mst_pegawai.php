<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mst_pegawai extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "4b1f8c9d-6f22-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/setup/m_mst_pegawai');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_mst_pegawai->get_data();
    }

    // public function insert()
    // {
    //     $this->app->cekRequest('PATCH');
    //     $this->app->cek_akses($this->menu_key, 'c');
    //     $this->m_mst_pegawai->insert_data();
    // }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_mst_pegawai->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_mst_pegawai->update_data();
    }

    // public function delete()
    // {
    //     $this->app->cekRequest('DELETE');
    //     $this->app->cek_akses($this->menu_key, 'd');
    //     $this->m_mst_pegawai->delete_data();
    // }

  // Setup jenis_pegawai
// $route['api/kinerja/setup/mst_pegawai/default']['PUT']    = 'kinerja/setup/mst_pegawai/get_data';
// $route['api/kinerja/setup/mst_pegawai/update']['PATCH']   = 'kinerja/setup/mst_pegawai/update';

    

    
}