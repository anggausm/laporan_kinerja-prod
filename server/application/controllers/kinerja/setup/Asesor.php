<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asesor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        // $this->menu_key = "845f00eb-884d-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('kinerja/setup/m_asesor');
    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_asesor->get_data();
    }
    public function get_periode()
    {
        $this->app->cekRequest('PUT');
        // $this->app->cek_akses($this->menu_key, 'r');
        $this->m_asesor->get_periode();
    }
    public function insert()
    {
        $this->app->cekRequest('POST');
        // $this->app->cek_akses($this->menu_key, 'c');
        $this->m_asesor->insert();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        // $this->app->cek_akses($this->menu_key, 'd');
        $this->m_asesor->delete();
    }
}
/* 
$route['api/kinerja/setup/asesor/get_data']['PUT'] = 'kinerja/setup/asesor/get_data';
$route['api/kinerja/setup/asesor/get_periode']['PUT'] = 'kinerja/setup/asesor/get_periode';
$route['api/kinerja/setup/asesor/insert']['POST'] = 'kinerja/setup/asesor/insert'; 
$route['api/kinerja/setup/asesor/delete']['DELETE'] = 'kinerja/setup/asesor/delete';
 */