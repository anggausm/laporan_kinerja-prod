<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jenis_pegawai extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "1330daa7-6900-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/setup/m_jenis_pegawai');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jenis_pegawai->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_jenis_pegawai->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_jenis_pegawai->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_jenis_pegawai->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_jenis_pegawai->delete_data();
    }

  // Setup jenis_pegawai
// $route['api/kinerja/setup/jenis_pegawai/default']['PUT']    = 'kinerja/setup/jenis_pegawai/get_data';
// $route['api/kinerja/setup/jenis_pegawai/simpan']['PATCH']   = 'kinerja/setup/jenis_pegawai/insert';
// $route['api/kinerja/setup/jenis_pegawai/edit']['PUT']       = 'kinerja/setup/jenis_pegawai/edit';
// $route['api/kinerja/setup/jenis_pegawai/update']['PATCH']   = 'kinerja/setup/jenis_pegawai/update';
// $route['api/kinerja/setup/jenis_pegawai/hapus']['DELETE']   = 'kinerja/setup/jenis_pegawai/delete';

    

    
}