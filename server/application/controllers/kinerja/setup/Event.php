<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Event extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "84480e89-64da-11ef-9a0f-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);

        $this->load->model('kinerja/setup/m_event');
    }

    public function index()
    {

    }

    public function get_data()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_event->get_data();
    }

    public function insert()
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'c');
        $this->m_event->insert_data();
    }

    public function edit()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_event->edit();
    }

    public function update($user_key = '')
    {
        $this->app->cekRequest('PATCH');
        $this->app->cek_akses($this->menu_key, 'u');
        $this->m_event->update_data();
    }

    public function delete()
    {
        $this->app->cekRequest('DELETE');
        $this->app->cek_akses($this->menu_key, 'd');
        $this->m_event->delete_data();
    }

    // Setup event
    // $route['api/kinerja/setup/event/default']['PUT']    = 'kinerja/setup/event/get_data';
    // $route['api/kinerja/setup/event/simpan']['PATCH']   = 'kinerja/setup/event/insert';
    // $route['api/kinerja/setup/event/edit']['PUT']       = 'kinerja/setup/event/edit';
    // $route['api/kinerja/setup/event/update']['PATCH']   = 'kinerja/setup/event/update';
    // $route['api/kinerja/setup/event/hapus']['DELETE']   = 'kinerja/setup/event/delete';

    

    
}