<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/* OPEN API : APLIKASI LAP KINERJA */
class General extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->load->model('kinerja/m_general');
    }

    public function get_periode_aktif()
    {
        $this->app->cekRequest('PUT');
        $this->m_general->get_periode_aktif();
    }
}

/*  
$route['api/kinerja/general/get_periode_aktif']['PUT'] = 'kinerja/general/get_periode_aktif';
*/