<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pekerjaan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "3661f8eb-8789-11ea-9e9d-1cb72c27dd68";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('laporan/m_pekerjaan');
    }

    public function index()
    {
    }
    public function get_angggota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_pekerjaan->get_angggota();
    }
}
