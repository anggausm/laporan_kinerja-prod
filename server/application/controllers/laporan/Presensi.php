<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Presensi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->token->cekToken();
        $this->tkn = $this->token->checkGlobalToken();
        $this->menu_key = "e3380e53-87c0-11ea-b58e-56cb879f2d55";
        $this->token->cek_akses_app($this->tkn);
        $this->load->model('laporan/m_presensi');
    }

    public function index()
    {
    }
    public function get_angota()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_presensi->get_angota();
    }
    public function get_data_presensi_default()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_presensi->get_data_presensi_default();
    }
    public function detail_task()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_presensi->detail_task();
    }
    public function download_file()
    {
        $this->app->cekRequest('PUT');
        $this->app->cek_akses($this->menu_key, 'r');
        $this->m_presensi->download_file();
    }
}
