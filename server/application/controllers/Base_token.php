<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Base_token extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //   $this->load->model('root/m_base_token');
        $this->sso_url = $this->app->sso_url(); //"http://localhost/app_portal/sso/index.php/";
        $this->id_app = $this->token->id_aplikasi();
    }

    public function index()
    {
    }

    //Ambil Data SSO Token

    public function get_playload()
    {
        $this->app->cekRequest('PATCH');
        $token = $this->input->get_request_header('Authorization');
        //disini bisa menambahkan payload sesuai dengan kebutuhan alpikasi.
        $rs_data = json_decode($this->curl->request_manual_token('PATCH', $this->sso_url . 'api/cek/sso_token', $token, json_encode(array('id_app' => $this->id_app))), true);

        //buka hasil dat cek token dan di tata kembali untuk menjadi payload

        $arr = array(
            'user_key' => $rs_data['result']['user_key'],
            'id_aplikasi' => $rs_data['result']['id_aplikasi'],
            'level_key' => $rs_data['result']['level_key'],
            'username' => $rs_data['result']['username'],
            'status' => $rs_data['result']['status'],
            'kode_khusus' => 'kode_khusus',
            //untuk payload bisa ditambahi sesuai dengan kebutuhan aplikasi anda
        );
        if ($rs_data['status'] == '200') {
            return $this->token->respons_data($arr, 'Token teridentifikasi', '200');
        } else {
            return $this->token->respons_data(array(), 'Token tidak teridentifikasi', '201');
        }

    }

    public function cek_base_token()
    {
        $token = $this->token->checkGlobalToken();
        return $this->token->respons_data($token, 'Token teridentifikasi', '200');
    }

}
