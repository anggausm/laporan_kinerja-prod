<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Beranda extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_beranda');
    }

    public function index()
    {
    }
    public function get_banner()
    {
        $this->app->cekRequest('PUT');
        $this->m_beranda->get_banner();
    }

}
