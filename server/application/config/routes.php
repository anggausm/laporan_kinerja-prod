<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;

//versoning app android
$route['api/versi']['PUT'] = 'versi/get_data';
$route['api/banner']['PUT'] = 'beranda/get_banner';
/*
REST LIB
 */

$route['api/akademik/struktural']['PUT'] = 'rest/akademik/akses_fakultas_tu';

/*
Re New Token
 */
$route['api/new/token']['PATCH'] = 'base_token/get_playload';
$route['api/cek_token']['PATCH'] = 'base_token/cek_base_token';

/*
======================== ROOTS AKSES APPS ======================
 */
//Menu
$route['api/root_app/menu/inserts']['PATCH'] = 'root/acl/menu/insert_data';
$route['api/root_app/menu/updatess']['PATCH'] = 'root/acl/menu/update_data';
$route['api/root_app/menu/deletos']['DELETE'] = 'root/acl/menu/delete_data';
//Sub Menu
$route['api/root_app/menu/sub_insert']['PATCH'] = 'root/acl/menu/sub_insert';
$route['api/root_app/menu/sub_update']['PATCH'] = 'root/acl/menu/sub_update';
$route['api/root_app/menu/sub_deletos']['DELETE'] = 'root/acl/menu/sub_deletos';

//LEVEL
$route['api/root_app/level/inserts']['PATCH'] = 'root/acl/level/insert_data';
$route['api/root_app/level/inserts_root']['PATCH'] = 'root/acl/level/insert_data_roots';
$route['api/root_app/level/updatess']['PATCH'] = 'root/acl/level/update_data';
$route['api/root_app/level/deletos']['DELETE'] = 'root/acl/level/delete_data';
$route['api/root_app/level/kelola_akses']['PATCH'] = 'root/acl/level/act_simpan';
//USER
$route['api/root_app/user/deletos']['DELETE'] = 'root/acl/user/delete_data';
$route['api/root_app/user/inserts']['PATCH'] = 'root/acl/user/insert_data';

/*
Laporan Kinerja WFH
 */
$route['api/wfh/presensi']['PATCH'] = 'absensi/wfh/insert_a_berangkat';
$route['api/wfh/selesai']['PATCH'] = 'absensi/wfh/selesai';
$route['api/wfh/cek_presensi']['PATCH'] = 'absensi/wfh/cek_absen';
$route['api/wfh/cek_presensi_mobile']['PATCH'] = 'absensi/wfh/cek_absen_mobile';
$route['api/wfh/get_wfh']['PATCH'] = 'absensi/wfh/get_wfh';
$route['api/wfh/insert_task']['PATCH'] = 'absensi/wfh/insert_task';
$route['api/wfh/delete_task']['DELETE'] = 'absensi/wfh/delete_task';
$route['api/wfh/get_task']['PATCH'] = 'absensi/wfh/get_task';
$route['api/wfh/task_riport']['PATCH'] = 'absensi/wfh/riport_wfh';
$route['api/wfh/download']['PUT'] = 'absensi/wfh/download_file';
/*
Histori
 */
$route['api/histori/default']['PUT'] = 'absensi/histori/default_history';
$route['api/histori/histori_laporan']['PUT'] = 'absensi/histori/histori_bulanan';

/*
PIKET
 */
$route['api/piket/presensi']['PATCH'] = 'absensi/piket/presensi/insert_a_berangkat';
$route['api/piket/selesai']['PATCH'] = 'absensi/piket/presensi/selesai';
$route['api/piket/cek_presensi']['PATCH'] = 'absensi/piket/presensi/cek_absen';
$route['api/piket/cek_presensi_mobile']['PATCH'] = 'absensi/piket/presensi/cek_absen_mobile';
$route['api/piket/get_wfh']['PATCH'] = 'absensi/piket/presensi/get_wfh';
$route['api/piket/insert_task']['PATCH'] = 'absensi/piket/presensi/insert_task';
$route['api/piket/delete_task']['DELETE'] = 'absensi/piket/presensi/delete_task';
$route['api/piket/get_task']['PATCH'] = 'absensi/piket/presensi/get_task';
$route['api/piket/task_riport']['PATCH'] = 'absensi/piket/presensi/riport_wfh';
$route['api/piket/download']['PUT'] = 'absensi/piket/presensi/download_file';
/*
Histori PIKET
 */
$route['api/piket/histori/default']['PUT'] = 'absensi/piket/riwayat/default_history';
$route['api/piket/histori/histori_laporan']['PUT'] = 'absensi/piket/riwayat/histori_bulanan';

/*
Angota
 */
$route['api/angota/dosen']['PUT'] = 'anggota/dosen/get_angggota';
$route['api/angota/tendik']['PUT'] = 'anggota/tendik/get_angggota';
//Laporan
$route['api/laporan/presensi/anggota']['PUT'] = 'laporan/presensi/get_angota';
$route['api/laporan/presensi/data_presensi']['PUT'] = 'laporan/presensi/get_data_presensi_default';
$route['api/laporan/presensi/detail_task']['PUT'] = 'laporan/presensi/detail_task';
$route['api/laporan/presensi/download_file']['PUT'] = 'laporan/presensi/download_file';

/*
Re Struktur
 */
$route['api/restruktur/tendik']['PUT'] = 'restruktur/tendik/get_data';
$route['api/restruktur/tendik/simpan']['POST'] = 'restruktur/tendik/simpan';
$route['api/restruktur/tendik/edit']['PUT'] = 'restruktur/tendik/edit';
$route['api/restruktur/tendik/update']['PATCH'] = 'restruktur/tendik/update';
$route['api/restruktur/tendik/delete']['DELETE'] = 'restruktur/tendik/delete';
$route['api/restruktur/tendik/doskar_usm']['PUT'] = 'restruktur/tendik/doskar_usm';

$route['api/restruktur/unit_lembaga']['PUT'] = 'restruktur/unit_lembaga/get_data';
$route['api/restruktur/unit_lembaga/simpan']['POST'] = 'restruktur/unit_lembaga/simpan';
$route['api/restruktur/unit_lembaga/edit']['PUT'] = 'restruktur/unit_lembaga/edit';
$route['api/restruktur/unit_lembaga/update']['PATCH'] = 'restruktur/unit_lembaga/update';
$route['api/restruktur/unit_lembaga/delete']['DELETE'] = 'restruktur/unit_lembaga/delete';
$route['api/restruktur/unit_lembaga/doskar_usm']['PUT'] = 'restruktur/unit_lembaga/doskar_usm';

$route['api/restruktur/pimpinan_fakultas']['PUT'] = 'restruktur/pimpinan_fakultas/get_data';
$route['api/restruktur/pimpinan_fakultas/simpan']['POST'] = 'restruktur/pimpinan_fakultas/simpan';
$route['api/restruktur/pimpinan_fakultas/edit']['PUT'] = 'restruktur/pimpinan_fakultas/edit';
$route['api/restruktur/pimpinan_fakultas/update']['PATCH'] = 'restruktur/pimpinan_fakultas/update';
$route['api/restruktur/pimpinan_fakultas/delete']['DELETE'] = 'restruktur/pimpinan_fakultas/delete';
$route['api/restruktur/pimpinan_fakultas/doskar_usm']['PUT'] = 'restruktur/pimpinan_fakultas/doskar_usm';
$route['api/restruktur/pimpinan_fakultas/fakultas']['PUT'] = 'restruktur/pimpinan_fakultas/fakultas';
$route['api/restruktur/pimpinan_fakultas/program_studi']['PUT'] = 'restruktur/pimpinan_fakultas/program_studi';

/*
Biodata
 */
$route['api/biodata/tendik']['PUT'] = 'biodata/tendik/get_biodata';
$route['api/biodata/tendik/edit']['PUT'] = 'biodata/tendik/edit';
$route['api/biodata/tendik/update']['PATCH'] = 'biodata/tendik/update';

/*
KEPEGAWAIAN
 */
//fakultas
$route['api/kpg/lap/tendik_fakultas/fak']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/get_fakultsa';
$route['api/kpg/lap/tendik_fakultas/list_anggota']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/get_list_anggota';
$route['api/kpg/lap/tendik_fakultas/presensi']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/detail_presensi';
$route['api/kpg/lap/tendik_fakultas/download']['PUT'] = 'kepegawaian/rekap/laporan_tendik_fakultas/download_file';
//upt
$route['api/kpg/lap/upt_lembaga/unit_lembaga']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/get_unit_lembaga';
$route['api/kpg/lap/upt_lembaga/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/get_list_anggota';
$route['api/kpg/lap/upt_lembaga/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/detail_presensi';
$route['api/kpg/lap/upt_lembaga/download']['PUT'] = 'kepegawaian/rekap/Laporan_upt_lembaga/download_file';
//dosen
$route['api/kpg/lap/dosen/fak']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/get_fakultsa';
$route['api/kpg/lap/dosen/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/get_list_anggota';
$route['api/kpg/lap/dosen/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/detail_presensi';
$route['api/kpg/lap/dosen/download']['PUT'] = 'kepegawaian/rekap/Laporan_dosen/download_file';
//Bauk
$route['api/kpg/lap/baak_bauk/unit_lembaga']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/get_unit_lembaga';
$route['api/kpg/lap/baak_bauk/list_anggota']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/get_list_anggota';
$route['api/kpg/lap/baak_bauk/presensi']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/detail_presensi';
$route['api/kpg/lap/baak_bauk/download']['PUT'] = 'kepegawaian/rekap/Laporan_baak_bauk/download_file';
//WFH
$route['api/kpg/wfh/periode/get']['PUT'] = 'kepegawaian/wfh/periode/get_data';
$route['api/kpg/wfh/periode/add']['POST'] = 'kepegawaian/wfh/periode/add_data';
$route['api/kpg/wfh/periode/edit']['PUT'] = 'kepegawaian/wfh/periode/get_edit';
$route['api/kpg/wfh/periode/update']['PATCH'] = 'kepegawaian/wfh/periode/update';
$route['api/kpg/wfh/periode/delete']['DELETE'] = 'kepegawaian/wfh/periode/delete_data';
//Piket
$route['api/kpg/wfh/piket/periode']['PUT'] = 'kepegawaian/wfh/piket/get_periode';
$route['api/kpg/wfh/piket/level']['PUT'] = 'kepegawaian/wfh/piket/get_level';
$route['api/kpg/wfh/piket/level_list']['PUT'] = 'kepegawaian/wfh/piket/get_level_list';
$route['api/kpg/wfh/piket/anggota']['PUT'] = 'kepegawaian/wfh/piket/get_anggota';
$route['api/kpg/wfh/piket/jadwal_piket']['PUT'] = 'kepegawaian/wfh/piket/jadwal_piket';
$route['api/kpg/wfh/piket/simpan_jadwal']['PATCH'] = 'kepegawaian/wfh/piket/simpan_jadwal_piket';
$route['api/kpg/wfh/piket/delete_jadwal']['DELETE'] = 'kepegawaian/wfh/piket/delete_jadwal_piket';

/*
Shift
 */
$route['api/kpg/shift/list']['PUT'] = 'kepegawaian/shift/get_list';
$route['api/kpg/shift/edit']['PUT'] = 'kepegawaian/shift/edit';
$route['api/kpg/shift/update']['PATCH'] = 'kepegawaian/shift/update';

// Mesin Finger
$route['api/kpg/finger_mesin/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_mesin/get_data';
$route['api/kpg/finger_mesin/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_mesin/insert';
$route['api/kpg/finger_mesin/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_mesin/edit';
$route['api/kpg/finger_mesin/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_mesin/update';


// Anggota finger
$route['api/kpg/finger_anggota/get_data']  ['PUT'] 	    = 'kepegawaian/mesin_finger/finger_anggota/get_data';
$route['api/kpg/finger_anggota/tambah']    ['PUT'] 	    = 'kepegawaian/mesin_finger/finger_anggota/add';
$route['api/kpg/finger_anggota/simpan']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_anggota/insert';
$route['api/kpg/finger_anggota/edit']      ['PUT'] 	    = 'kepegawaian/mesin_finger/finger_anggota/edit';
$route['api/kpg/finger_anggota/update']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_anggota/update';
$route['api/kpg/finger_anggota/delete']    ['DELETE']   = 'kepegawaian/mesin_finger/finger_anggota/delete';

// Mesin Log Finger
$route['api/kpg/finger_online_koordinat/get'] ['PUT'] 	= 'kepegawaian/mesin_finger/finger_online_logs/get_data';
// $route['api/kpg/finger_logs/simpan'] 	['PATCH'] 	= 'kepegawaian/mesin_finger/finger_logs/insert';
// $route['api/kpg/finger_logs/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_logs/edit';
// $route['api/kpg/finger_logs/update'] 	['PATCH'] 	= 'kepegawaian/mesin_finger/finger_logs/update';

// Mesin Log Finger Online
$route['api/kpg/finger_logs/get_data'] 	    ['PUT'] 	= 'kepegawaian/mesin_finger/finger_logs/get_data';

// Ijin
$route['api/kpg/finger_ijin/get_jenis'] ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/get_jenis';
$route['api/kpg/finger_ijin/get_data']  ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/get_data';
$route['api/kpg/finger_ijin/simpan']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_ijin/insert';
$route['api/kpg/finger_ijin/edit']      ['PUT'] 	= 'kepegawaian/mesin_finger/finger_ijin/edit';
$route['api/kpg/finger_ijin/update']    ['PATCH'] 	= 'kepegawaian/mesin_finger/finger_ijin/update';
$route['api/kpg/finger_ijin/delete']    ['DELETE']  = 'kepegawaian/mesin_finger/finger_ijin/delete';

// Tugas Belajar
$route['api/kpg/finger_tubel/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_tubel/get_data';
$route['api/kpg/finger_tubel/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_tubel/insert';
$route['api/kpg/finger_tubel/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_tubel/edit';
$route['api/kpg/finger_tubel/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_tubel/update';

// olah transport
$route['api/kpg/finger_olah/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_transport/get_data';
$route['api/kpg/finger_olah/get_olah'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_transport/get_olah';

// hari khusus
$route['api/kpg/finger_harikhusus/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/get_data';
$route['api/kpg/finger_harikhusus/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/insert';
$route['api/kpg/finger_harikhusus/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/edit';
$route['api/kpg/finger_harikhusus/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_harikhusus/update';
$route['api/kpg/finger_harikhusus/delete']      ['DELETE']  = 'kepegawaian/mesin_finger/finger_harikhusus/delete';

// presensi khusus
$route['api/kpg/finger_presensikhusus/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_presensikhusus/get_data';
$route['api/kpg/finger_presensikhusus/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_presensikhusus/insert';
$route['api/kpg/finger_presensikhusus/edit'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_presensikhusus/edit';
$route['api/kpg/finger_presensikhusus/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_presensikhusus/update';
$route['api/kpg/finger_presensikhusus/delete']      ['DELETE']  = 'kepegawaian/mesin_finger/finger_presensikhusus/delete';

// rekap transport
$route['api/kpg/finger_rekap_presensi/get_data'] 	['PUT'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/get_data';
// $route['api/kpg/finger_rekap_presensi/simpan'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/insert';
$route['api/kpg/finger_rekap_presensi/detail'] 		['PUT'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/info';
$route['api/kpg/finger_rekap_presensi/update'] 		['PATCH'] 	= 'kepegawaian/mesin_finger/finger_rekappresensi/update';
// $route['api/kpg/finger_rekap_presensi/delete']      ['DELETE']  = 'kepegawaian/mesin_finger/finger_rekappresensi/delete';





// ijin studi
$route['api/ijin/study/default']['PUT'] 		= 'ijin/studi/get_data';

// histori ijin studi

// absen khusus

// =================================== Rekap Transport ============================================
// posting
$route['api/transport/posting/default']['PUT'] = 'transport/all_posting/get_data';
$route['api/transport/posting/datacek']['POST'] = 'transport/all_posting/cek_data';
$route['api/transport/posting/simpan']['POST'] = 'transport/all_posting/insert';

// histori posting




// =================================== API SISTEM E-BUGETING ============================================
$route['api/lapker/doskar/personal']['PUT'] = 'eksternal/ebugeting/Ebug_doskar/personal';

// =================================== END API SISTEM E-BUGETING ============================================









// perorangan

// =================================== SISTEM LLDIKTI ============================================
// ajuan jafa
$route['api/lldikti/jafa/default']['PUT'] 	= 'dikti/jafa/get_data';
$route['api/lldikti/jafa/get_edit']['PUT'] 	= 'dikti/jafa/edit';
$route['api/lldikti/jafa/get_dosen']['PUT'] = 'dikti/jafa/dosen_usm';
$route['api/lldikti/jafa/get_jafa']['PUT'] 	= 'dikti/jafa/jabfung';
$route['api/lldikti/jafa/get_jenis']['PUT'] = 'dikti/jafa/jenis';
$route['api/lldikti/jafa/get_status']['PUT'] = 'dikti/jafa/status';
$route['api/lldikti/jafa/update']['PATCH'] 	= 'dikti/jafa/update';
$route['api/lldikti/jafa/detail']['PUT'] 	= 'dikti/jafa/detail';
$route['api/lldikti/jafa/simpan']['PUT'] 	= 'dikti/jafa/insert';
$route['api/lldikti/jafa/update']['PATCH'] 	= 'dikti/jafa/update';

// ajuan serdos
$route['api/lldikti/serdos/default']['PUT'] 	= 'dikti/serdos/get_data';
$route['api/lldikti/serdos/simpan']['POST'] 	= 'dikti/serdos/insert';
$route['api/lldikti/serdos/edit']['PUT'] 		= 'dikti/serdos/edit';
$route['api/lldikti/serdos/update']['PATCH'] 	= 'dikti/serdos/update';


// cek app
$route['api/cek_app/cek']['PUT']     = 'welcome/tc_cek_presensi';
$route['api/pmb/pencetak']['PUT']     = 'rest/rest_pmb/get_nama_nis';


// LAPORAN KINERJA (JUNI 2024)
$route['api/kinerja/kependidikan/pertanyaan/periode']['GET']    = 'kinerja/kependidikan/penilaian_diri/get_periode';
$route['api/kinerja/kependidikan/pertanyaan']['GET']            = 'kinerja/kependidikan/penilaian_diri/get_data';
$route['api/kinerja/kependidikan/pertanyaan/simpan']['POST']    = 'kinerja/kependidikan/penilaian_diri/simpan';

$route['api/kinerja/kependidikan/rekan/periode']['GET']     = 'kinerja/kependidikan/penilaian_rekan/get_periode';
$route['api/kinerja/kependidikan/rekan/list']['GET']        = 'kinerja/kependidikan/penilaian_rekan/get_list';
$route['api/kinerja/kependidikan/rekan/pertanyaan']['GET']  = 'kinerja/kependidikan/penilaian_rekan/get_pertanyaan';
$route['api/kinerja/kependidikan/rekan/simpan']['POST']     = 'kinerja/kependidikan/penilaian_rekan/simpan';

$route['api/kinerja/setup/asesor/get_data']['PUT'] = 'kinerja/setup/asesor/get_data';
$route['api/kinerja/setup/asesor/get_periode']['PUT'] = 'kinerja/setup/asesor/get_periode';
$route['api/kinerja/setup/asesor/insert']['POST'] = 'kinerja/setup/asesor/insert'; 
$route['api/kinerja/setup/asesor/delete']['DELETE'] = 'kinerja/setup/asesor/delete';

$route['api/kinerja/dosen/rekap/get_pertanyaan']['PUT'] = 'kinerja/dosen/rekap/get_pertanyaan'; 
$route['api/kinerja/dosen/rekap/get_periode']['PUT'] = 'kinerja/dosen/rekap/get_periode';
$route['api/kinerja/dosen/rekap/save']['POST'] = 'kinerja/dosen/rekap/save';
$route['api/kinerja/dosen/rekap/ajukan']['POST'] = 'kinerja/dosen/rekap/ajukan';

$route['api/kinerja/kependidikan/penilaian_pimpinan/get_asesi']['PUT'] = 'kinerja/kependidikan/penilaian_pimpinan/get_asesi';
$route['api/kinerja/kependidikan/penilaian_pimpinan/get_pertanyaan']['PUT'] = 'kinerja/kependidikan/penilaian_pimpinan/get_pertanyaan';
$route['api/kinerja/kependidikan/penilaian_pimpinan/save']['POST'] = 'kinerja/kependidikan/penilaian_pimpinan/save';
$route['api/kinerja/kependidikan/penilaian_pimpinan/verifikasi']['POST'] = 'kinerja/kependidikan/penilaian_pimpinan/verifikasi';


$route['api/kinerja/setup/rekap_dosen/get_data']['PUT'] = 'kinerja/setup/rekap_dosen/get_data';

$route['api/kinerja/setup/rekap_kependidikan/get_data']['PUT'] = 'kinerja/setup/rekap_kependidikan/get_data';

$route['api/kinerja/general/get_periode_aktif']['PUT'] = 'kinerja/general/get_periode_aktif';

$route['api/kinerja/kependidikan/penilaian_universitas/get_asesi']['PUT'] = 'kinerja/kependidikan/penilaian_universitas/get_asesi';
$route['api/kinerja/kependidikan/penilaian_universitas/get_pertanyaan']['PUT'] = 'kinerja/kependidikan/penilaian_universitas/get_pertanyaan';
$route['api/kinerja/kependidikan/penilaian_universitas/save']['POST'] = 'kinerja/kependidikan/penilaian_universitas/save';
$route['api/kinerja/kependidikan/penilaian_universitas/verifikasi']['POST'] = 'kinerja/kependidikan/penilaian_universitas/verifikasi';



// ============================= LAPORAN KINERJA DOSEN V.1 ===========================================
// PAK DOSEN
$route['api/kinerja/pak_dosen/get_periode']     ['PUT']     = 'kinerja/dosen/pak/get_periode';
$route['api/kinerja/pak_dosen/get_periode_pilih']['PUT']    = 'kinerja/dosen/pak/get_periode_pilih';
$route['api/kinerja/pak_dosen/get_pak']         ['PUT']     = 'kinerja/dosen/pak/get_pak';
$route['api/kinerja/pak_dosen/get_pak_keg']     ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan';
$route['api/kinerja/pak_dosen/get_pak_detail']  ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan_detail';
$route['api/kinerja/pak_dosen/get_pak_lihat']   ['PUT']     = 'kinerja/dosen/pak/get_pak_kegiatan_lihat';
$route['api/kinerja/pak_dosen/send_pak_import'] ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_import';
$route['api/kinerja/pak_dosen/value_hapus']     ['PATCH']   = 'kinerja/dosen/pak/pak_value_destroy';
$route['api/kinerja/pak_dosen/update_url']      ['PATCH']   = 'kinerja/dosen/pak/pak_update_url';
$route['api/kinerja/pak_dosen/pesan_value']     ['PATCH']   = 'kinerja/dosen/pak/pak_insert_value_pesan';
$route['api/kinerja/pak_dosen/ajukan_pkd']      ['PATCH']   = 'kinerja/dosen/pak/pak_ajukan_pkd';
$route['api/kinerja/pak_dosen/kegiatan_form']   ['PUT']     = 'kinerja/dosen/pak/pak_kegiatan_form';
$route['api/kinerja/pak_dosen/kegiatan_simpan'] ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_insert';
$route['api/kinerja/pak_dosen/kegiatan_edit']   ['PATCH']   = 'kinerja/dosen/pak/pak_kegiatan_update';
$route['api/kinerja/pak_dosen/corevalues_integritas']['PUT']= 'kinerja/dosen/pak/pak_corevalues_integritas';
$route['api/kinerja/pak_dosen/tugas_tambahan']  ['PUT']     = 'kinerja/dosen/pak/pak_tugas_tambahan';


// =================================== Kinerja Pegawai ============================================
// log book
$route['api/kinerja/log_book/default']['PUT'] 	= 'kinerja/tendik/log_book/get_data';
$route['api/kinerja/log_book/simpan']['PATCH'] 	= 'kinerja/tendik/log_book/insert';
$route['api/kinerja/log_book/edit']['PUT'] 		= 'kinerja/tendik/log_book/edit';
$route['api/kinerja/log_book/update']['PATCH'] 	= 'kinerja/tendik/log_book/update';
$route['api/kinerja/log_book/hapus']['DELETE'] 	= 'kinerja/tendik/log_book/delete';
$route['api/kinerja/log_book/p_log']['PUT'] 	= 'kinerja/tendik/log_book/periode_log';

// kinerja pegawai
$route['api/kinerja/pegawai/default']['PUT'] 		= 'kinerja/tendik/pegawai/get_data';
$route['api/kinerja/pegawai/simpan']['PATCH'] 		= 'kinerja/tendik/pegawai/insert';
$route['api/kinerja/pegawai/edit']['PUT'] 			= 'kinerja/tendik/pegawai/edit';
$route['api/kinerja/pegawai/update']['PATCH'] 		= 'kinerja/tendik/pegawai/update';
$route['api/kinerja/pegawai/hapus']['DELETE'] 		= 'kinerja/tendik/pegawai/delete';
$route['api/kinerja/pegawai/periode']['PUT'] 		= 'kinerja/tendik/pegawai/get_periode';
$route['api/kinerja/pegawai/skp_peg']['PUT'] 		= 'kinerja/tendik/pegawai/get_skp_peg';
$route['api/kinerja/pegawai/rincian']['PUT'] 		= 'kinerja/tendik/pegawai/get_rincian_ind';
$route['api/kinerja/pegawai/data_periode']['PUT'] 	= 'kinerja/tendik/pegawai/get_data_periode';
$route['api/kinerja/pegawai/get_log_book']['PUT'] 	= 'kinerja/tendik/pegawai/get_log_book';
$route['api/kinerja/pegawai/get_wd']['PUT'] 		= 'kinerja/tendik/pegawai/get_wd';
$route['api/kinerja/pegawai/cek_data_pegawai']['PUT'] 	= 'kinerja/tendik/pegawai/cek_data_pegawai';
$route['api/kinerja/pegawai/cek_jenis_pegawai']['PUT'] 	= 'kinerja/tendik/pegawai/cek_jenis_pegawai';
$route['api/kinerja/pegawai/get_lpp']['PUT'] 			= 'kinerja/tendik/pegawai/data_lpp';
$route['api/kinerja/pegawai/cek_skp_pegawai']['PUT'] 	= 'kinerja/tendik/pegawai/cek_skp_pegawai';

//kuesioner dan event
$route['api/kinerja/pegawai/list_rekan']['GET'] 	= 'kinerja/tendik/pegawai/get_list_rekan';
$route['api/kinerja/pegawai/list_pertanyaan']['GET'] 	= 'kinerja/tendik/pegawai/get_list_tanya';
$route['api/kinerja/pegawai/simpan_kuesioner']['PATCH'] 	= 'kinerja/tendik/pegawai/simpan_kuesioner';
$route['api/kinerja/pegawai/nilai_kuesioner']['PUT'] 	= 'kinerja/tendik/pegawai/get_nilai_kuesioner';
$route['api/kinerja/pegawai/edit_event_peserta']['PUT'] 	= 'kinerja/tendik/pegawai/get_event_pst';
$route['api/kinerja/pegawai/simpan_ajuan']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_ajuan';

//====================ijazah
$route['api/kinerja/pegawai/simpan_ijazah']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_ijazah';
$route['api/kinerja/pegawai/get_ijazah']['PUT'] 		= 'kinerja/tendik/pegawai/data_ijazah';
$route['api/kinerja/pegawai/ijazah_edit']['PUT'] 		= 'kinerja/tendik/pegawai/edit_ijazah';
$route['api/kinerja/pegawai/update_ijazah']['PATCH'] 	= 'kinerja/tendik/pegawai/update_ijazah';
$route['api/kinerja/pegawai/hapus_ijazah']['DELETE'] 	= 'kinerja/tendik/pegawai/delete_ijazah';
//====================sertifikat
$route['api/kinerja/pegawai/get_sertifikat']['PUT'] 		= 'kinerja/tendik/pegawai/data_sertifikat';
$route['api/kinerja/pegawai/simpan_sertifikat']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_sertifikat';
$route['api/kinerja/pegawai/edit_sert']['PUT'] 				= 'kinerja/tendik/pegawai/edit_data_sertifikat';
$route['api/kinerja/pegawai/update_sertifikat']['PATCH'] 	= 'kinerja/tendik/pegawai/update_sertifikat';
$route['api/kinerja/pegawai/hapus_sertifikat']['DELETE'] 	= 'kinerja/tendik/pegawai/delete_sertifikat';
//=====================sk
$route['api/kinerja/pegawai/get_unit']['PUT'] 		= 'kinerja/tendik/pegawai/get_unit';
$route['api/kinerja/pegawai/get_prodi']['PUT'] 		= 'kinerja/tendik/pegawai/get_prodi';
$route['api/kinerja/pegawai/simpan_sk']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_sk';
$route['api/kinerja/pegawai/get_sk']['PUT'] 		= 'kinerja/tendik/pegawai/data_sk';
$route['api/kinerja/pegawai/simpan_sk_pegawai']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_sk_pegawai';
$route['api/kinerja/pegawai/edit_sk']['PUT'] 		= 'kinerja/tendik/pegawai/edit_sk';
$route['api/kinerja/pegawai/update_sk_pegawai']['PATCH'] 	= 'kinerja/tendik/pegawai/update_sk_peg';
$route['api/kinerja/pegawai/hapus_sk']['DELETE'] 	= 'kinerja/tendik/pegawai/delete_sk';

//====================event
$route['api/kinerja/pegawai/get_event']['PUT'] 		= 'kinerja/tendik/pegawai/data_event';
$route['api/kinerja/pegawai/simpan_foto']['PATCH'] 	= 'kinerja/tendik/pegawai/insert_foto';
$route['api/kinerja/pegawai/edit_foto']['PUT'] 		= 'kinerja/tendik/pegawai/edit_ijazah';
$route['api/kinerja/pegawai/update_foto']['PATCH'] 	= 'kinerja/tendik/pegawai/update_ijazah';
$route['api/kinerja/pegawai/hapus_foto']['DELETE'] 	= 'kinerja/tendik/pegawai/delete_foto';

// Penilaian Kependidikan
// $route['api/kinerja/kependidikan/pertanyaan']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_data';
// $route['api/kinerja/kependidikan/pertanyaan/periode']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_periode';
// $route['api/kinerja/kependidikan/pertanyaan/simpan']['GET'] 	= 'kinerja/kependidikan/penilaian_diri/get_data';
//REKAP TENDIK
$route['api/kinerja/setup/rekap_tendik/default']['PUT'] = 'kinerja/setup/rekap_tendik/get_data';


// Validasi Log
$route['api/kinerja/val_log/p_aktif']['PUT'] 	= 'kinerja/tendik/validasi_log/periode_aktif';
$route['api/kinerja/val_log/p_log']['PUT'] 		= 'kinerja/tendik/validasi_log/periode_log';
$route['api/kinerja/val_log/log_pegawai']['PUT']= 'kinerja/tendik/validasi_log/log_pegawai';
$route['api/kinerja/val_log/data_pl']['PUT'] 	= 'kinerja/tendik/validasi_log/data_periode_log';
$route['api/kinerja/val_log/simpan']['PATCH'] 	= 'kinerja/tendik/validasi_log/insert';
$route['api/kinerja/val_log/hapus']['DELETE'] 	= 'kinerja/tendik/validasi_log/delete';
$route['api/kinerja/val_log/list']['GET'] 		= 'kinerja/tendik/validasi_log/get_list';

// Validasi SKP
$route['api/kinerja/val_skp/update_unvalidasi']['DELETE'] 	= 'kinerja/tendik/validasi_skp/unvalidasi';
$route['api/kinerja/val_skp/p_log']['PUT'] 		= 'kinerja/tendik/validasi_skp/periode_log';
$route['api/kinerja/val_skp/log_pegawai']['PUT']= 'kinerja/tendik/validasi_skp/log_pegawai';
$route['api/kinerja/val_skp/data_pl']['PUT'] 	= 'kinerja/tendik/validasi_skp/data_periode_log';
$route['api/kinerja/val_skp/simpan']['PATCH'] 	= 'kinerja/tendik/validasi_skp/insert';
$route['api/kinerja/val_skp/hapus']['DELETE'] 	= 'kinerja/tendik/validasi_skp/delete';
$route['api/kinerja/val_skp/list']['GET'] 		= 'kinerja/tendik/validasi_skp/get_list';
$route['api/kinerja/val_skp/skp_pegawai']['GET']= 'kinerja/tendik/validasi_skp/get_skp';
$route['api/kinerja/val_skp/update_validasi']['PATCH'] 	= 'kinerja/tendik/validasi_skp/update';
$route['api/kinerja/val_skp/update_revisi']['DELETE'] 	= 'kinerja/tendik/validasi_skp/revisi';
$route['api/kinerja/val_skp/get_skp_valid']['GET'] 	= 'kinerja/tendik/validasi_skp/get_skp_valid';

// LPP
$route['api/kinerja/lpp/default']['PUT'] 	= 'kinerja/tendik/lpp/get_data';
$route['api/kinerja/lpp/simpan']['PATCH'] 	= 'kinerja/tendik/lpp/insert';
$route['api/kinerja/lpp/edit']['PUT'] 		= 'kinerja/tendik/lpp/edit';
$route['api/kinerja/lpp/update']['PATCH'] 	= 'kinerja/tendik/lpp/update';
$route['api/kinerja/lpp/hapus']['DELETE'] 	= 'kinerja/tendik/lpp/delete';

//=========================================SETUP KINERJA
// SKP Satuan Kinerja Pegawai
$route['api/kinerja/skp/default']['PUT'] 		= 'kinerja/tendik/skp/get_data';
$route['api/kinerja/skp/simpan']['PATCH'] 		= 'kinerja/tendik/skp/insert';
$route['api/kinerja/skp/edit']['PUT'] 			= 'kinerja/tendik/skp/edit';
$route['api/kinerja/skp/update']['PATCH'] 		= 'kinerja/tendik/skp/update';
$route['api/kinerja/skp/hapus']['DELETE'] 		= 'kinerja/tendik/skp/delete';
$route['api/kinerja/skp/periode']['PUT'] 		= 'kinerja/tendik/skp/get_periode';
$route['api/kinerja/skp/ind_utama']['PUT'] 		= 'kinerja/tendik/skp/get_ind_utama';
$route['api/kinerja/skp/sub_ind']['PUT'] 		= 'kinerja/tendik/skp/get_sub_ind';
$route['api/kinerja/skp/ind_nilai']['PUT'] 		= 'kinerja/tendik/skp/get_ind_nilai';
$route['api/kinerja/skp/jenis_peg']['PUT'] 		= 'kinerja/tendik/skp/get_jenis_peg';
$route['api/kinerja/skp/get_data_skp']['PUT'] 	= 'kinerja/tendik/skp/get_skp_data';

// Setup event
$route['api/kinerja/setup/event/default']['PUT'] 	= 'kinerja/setup/event/get_data';
$route['api/kinerja/setup/event/simpan']['PATCH'] 	= 'kinerja/setup/event/insert';
$route['api/kinerja/setup/event/edit']['PUT'] 		= 'kinerja/setup/event/edit';
$route['api/kinerja/setup/event/update']['PATCH'] 	= 'kinerja/setup/event/update';
$route['api/kinerja/setup/event/hapus']['DELETE'] 	= 'kinerja/setup/event/delete';

// Setup jenis_pegawai
$route['api/kinerja/setup/jenis_pegawai/default']['PUT'] 	= 'kinerja/setup/jenis_pegawai/get_data';
$route['api/kinerja/setup/jenis_pegawai/simpan']['PATCH'] 	= 'kinerja/setup/jenis_pegawai/insert';
$route['api/kinerja/setup/jenis_pegawai/edit']['PUT'] 		= 'kinerja/setup/jenis_pegawai/edit';
$route['api/kinerja/setup/jenis_pegawai/update']['PATCH'] 	= 'kinerja/setup/jenis_pegawai/update';
$route['api/kinerja/setup/jenis_pegawai/hapus']['DELETE'] 	= 'kinerja/setup/jenis_pegawai/delete';

// Setup master_pegawai
$route['api/kinerja/setup/mst_pegawai/default']['PUT'] 	= 'kinerja/setup/mst_pegawai/get_data';
$route['api/kinerja/setup/mst_pegawai/simpan']['PATCH'] 	= 'kinerja/setup/mst_pegawai/insert';
$route['api/kinerja/setup/mst_pegawai/edit']['PUT'] 		= 'kinerja/setup/mst_pegawai/edit';
$route['api/kinerja/setup/mst_pegawai/update']['PATCH'] 	= 'kinerja/setup/mst_pegawai/update';
$route['api/kinerja/setup/mst_pegawai/hapus']['DELETE'] 	= 'kinerja/setup/mst_pegawai/delete';

// Setup jenis_pegawai
$route['api/kinerja/periode_log/default']['PUT'] 	= 'kinerja/tendik/periode_log/get_data';
$route['api/kinerja/periode_log/simpan']['PATCH'] 	= 'kinerja/tendik/periode_log/insert';
$route['api/kinerja/periode_log/edit']['PUT'] 		= 'kinerja/tendik/periode_log/edit';
$route['api/kinerja/periode_log/update']['PATCH'] 	= 'kinerja/tendik/periode_log/update';
$route['api/kinerja/periode_log/hapus']['DELETE'] 	= 'kinerja/tendik/periode_log/delete';