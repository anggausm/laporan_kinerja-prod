<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_pimpinan_fakultas extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

//     public function get_data()
    //     {
    //         $data = json_decode(file_get_contents("php://input"));
    //         $sql = "SELECT a.user_key,a.nama_doskar,b.*,
    // IFNULL((SELECT fakultas FROM program_studi WHERE kode_fakultas=b.fakultas LIMIT 1),'') fakultas,
    // IFNULL((SELECT programstudi FROM program_studi WHERE kode_khusus=b.kode_khusus LIMIT 1),'') programstudi,
    // (SELECT GROUP_CONCAT(CONCAT(kode_sto,' ',nama_sub_struktural) SEPARATOR ' <br>')
    //     FROM tes_struktural_anggota b
    //     JOIN tes_struktural_sub c
    //     ON c.id_sub_struktural=b.id_sub_struktural
    //     WHERE user_key=a.user_key AND (c.id_struktural = '4' OR c.id_struktural = '5') ) ket_struktural
    // FROM tes_doskar_usm a
    // JOIN (SELECT user_key,kode_sto,`status`,id_angota_struktural,id_struktural,a.fakultas,a.kode_khusus FROM tes_struktural_anggota a
    //     JOIN tes_struktural_sub b ON b.id_sub_struktural=a.id_sub_struktural)b
    //     ON b.user_key=a.user_key
    // WHERE (b.id_struktural = '4' OR b.id_struktural = '5')
    // GROUP BY a.user_key
    // ORDER BY b.kode_sto";
    //         $query = $this->db->query($sql, array());
    //         if ($query->num_rows() > 0) {
    //             $result = $query->result_array();
    //             $query->free_result();
    //             return $this->app->respons_data($result, 'Data berhasil diload', 200);
    //         } else {
    //             return $this->app->respons_data(array(), 'data gagal diload', 200);
    //         }
    //     }

    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT (SELECT GROUP_CONCAT(
                    CONCAT(c.nama_doskar,' (Keterangan : ',b.keterangan,')')
                    SEPARATOR ' <br>')
    FROM tes_struktural_anggota b
     JOIN tes_doskar_usm c ON c.user_key=b.user_key
     WHERE b.id_sub_struktural=a.id_sub_struktural) ket_anggota,
     a.id_sub_struktural,a.id_struktural,a.unit_bagian,a.nama_sub_struktural,a.kode_sto FROM tes_struktural_sub a
WHERE (a.id_struktural = '4' OR a.id_struktural = '5')";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function simpan()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('user_key', 'Anggota', 'trim|required');
        $this->form_validation->set_rules('id_sub_struktural', 'Sub Struktural', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }

        $struktural_sub = $this->db->get_where('tes_struktural_sub', array('id_sub_struktural' => $data['id_sub_struktural']))->row_array();

        $data['user_key'] = $data['user_key'];
        $data['id_sub_struktural'] = $data['id_sub_struktural'];
        $data['keterangan'] = $struktural_sub['nama_sub_struktural'];
        if (!empty($data['fakultas'])) {
            $data['fakultas'] = $data['fakultas'];
        }
        if (!empty($data['kode_khusus'])) {
            $data['kode_khusus'] = $data['kode_khusus'];
        }

        $this->db->insert('tes_struktural_anggota', $data);

        if ($this->db->affected_rows() > 0) {
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        }
    }
    public function edit()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM tes_struktural_sub WHERE id_sub_struktural= ?";
        $query = $this->db->query($sql, array($data['id_sub_struktural']));
        if ($query->num_rows() > 0) {
            $struktural = $query->row_array();
            $query->free_result();

            $sql = "SELECT *,(SELECT GROUP_CONCAT(CONCAT('Unit Bagian : ',d.unit_bagian,' (',c.keterangan,')') SEPARATOR ' <br>')
        FROM tes_struktural_anggota c
        JOIN tes_struktural_sub d
        ON d.id_sub_struktural=c.id_sub_struktural
        WHERE user_key=a.user_key) ket_struktural,
        IFNULL((SELECT fakultas FROM program_studi WHERE kode_fakultas=a.fakultas LIMIT 1),'') fakultas,
        IFNULL((SELECT CONCAT('(',programstudi,')') FROM program_studi WHERE kode_khusus=a.kode_khusus LIMIT 1),'') programstudi
        FROM tes_struktural_anggota a
                    JOIN tes_doskar_usm b ON b.user_key=a.user_key
                    WHERE id_sub_struktural= ?";
            $query = $this->db->query($sql, array($data['id_sub_struktural']));
            $anggota = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('struktural' => $struktural, 'anggota' => $anggota), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('struktural' => array(), 'anggota' => array()), 'Data gagal diload', 200);
        }
    }
    public function update()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('id_angota_struktural', 'Anggota', 'trim|required');
        $this->form_validation->set_rules('id_sub_struktural', 'Sub Struktural', 'trim|required');
        $this->form_validation->set_rules('user_key', 'Anggota Baru', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }

        $this->db->trans_start();
        //Proses Pemindahan Struktural Dosen Dalam Fakultas Jika Sub Struktural Dekan, WadekI, WadekII, Kajur, Kaprog, Sekjur
        if ($data['id_sub_struktural'] == '54' || $data['id_sub_struktural'] == '55' || $data['id_sub_struktural'] == '56' ||
            $data['id_sub_struktural'] == '57' || $data['id_sub_struktural'] == '58' || $id_sub_struktural == '59') {
            $id_sub_struktural_dosen = '60';
            // Proses Hapus Struktural Dosen Anggota Baru
            $this->db->where('user_key', $data['user_key']);
            $this->db->where('id_sub_struktural', $id_sub_struktural_dosen);
            $this->db->delete('tes_struktural_anggota');
            // END Proses Hapus Struktural Dosen Anggota Baru

            // Proses insert/pindah anggota sebelum menjadi Struktural Dosen
            $this->db->from('tes_struktural_anggota');
            $this->db->where('id_angota_struktural', $data['id_angota_struktural']);
            $anggota_sebelum = $this->db->get()->row_array();
            $doskar_usm = $this->db->get_where('tes_doskar_usm', array('user_key' => $anggota_sebelum['user_key']))->row_array();
            // $struktural_sub = $this->db->get_where('tes_struktural_sub', array('id_sub_struktural' => $id_sub_struktural_dosen))->row_array();

            // Jika doskar/anggota sebelum tidak punya HOMEBASE berarti Bukan Dosen Tetap/DOSEN
            // Yang Di Insert adalah doskar yang punya HOMEBASE
            if (!empty($doskar_usm['fakultas'])) {
                $this->db->insert('tes_struktural_anggota', array(
                    'id_sub_struktural' => $id_sub_struktural_dosen,
                    'user_key' => $anggota_sebelum['user_key'],
                    'keterangan' => 'DOSEN',
                    'fakultas' => $doskar_usm['fak_unit'],
                    'kode_khusus' => $doskar_usm['fakultas'],
                ));
            }
            // END Proses insert/pindah anggota sebelum menjadi Struktural Dosen
        }

        // Update Anggota Struktural
        $this->db->where('id_angota_struktural', $data['id_angota_struktural']);
        $this->db->update('tes_struktural_anggota', array('user_key' => $data['user_key']));

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function delete()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('id_angota_struktural', 'Anggota', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        $this->db->delete('tes_struktural_anggota', array('id_angota_struktural' => $data['id_angota_struktural']));
        if ($this->db->affected_rows() > 0) {
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        }
    }
}
