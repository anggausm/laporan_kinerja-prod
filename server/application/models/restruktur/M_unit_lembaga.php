<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_unit_lembaga extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

//     public function get_data()
    //     {
    //         $data = json_decode(file_get_contents("php://input"));
    //         $sql = "SELECT a.user_key,a.nama_doskar,b.*,
    // (SELECT GROUP_CONCAT(CONCAT(kode_sto,' ',nama_sub_struktural) SEPARATOR ' <br>')
    //     FROM tes_struktural_anggota b
    //     JOIN tes_struktural_sub c
    //     ON c.id_sub_struktural=b.id_sub_struktural
    //     WHERE user_key=a.user_key AND (c.id_struktural = '2' OR c.id_struktural = '3' OR c.id_struktural = '6' OR c.id_struktural = '7' OR c.id_struktural = '8')) ket_struktural
    // FROM tes_doskar_usm a
    // JOIN (SELECT user_key,kode_sto,`status`,id_angota_struktural,id_struktural FROM tes_struktural_anggota a
    //     JOIN tes_struktural_sub b ON b.id_sub_struktural=a.id_sub_struktural)b
    //     ON b.user_key=a.user_key
    // WHERE (b.id_struktural = '2' OR b.id_struktural = '3' OR b.id_struktural = '6' OR b.id_struktural = '7' OR b.id_struktural = '8')
    // GROUP BY a.user_key
    // ORDER BY b.kode_sto";
    //         $query = $this->db->query($sql, array());
    //         if ($query->num_rows() > 0) {
    //             $result = $query->result_array();
    //             $query->free_result();
    //             return $this->app->respons_data($result, 'Data berhasil diload', 200);
    //         } else {
    //             return $this->app->respons_data(array(), 'data gagal diload', 200);
    //         }
    //     }

    // public function edit()
    // {
    //     $data = json_decode(file_get_contents("php://input"), true);
    //     $sql = "SELECT a.user_key,a.nama_doskar,b.* FROM tes_doskar_usm a
    //     JOIN (SELECT user_key,kode_sto,`status`,a.id_sub_struktural,a.id_angota_struktural FROM tes_struktural_anggota a
    //             JOIN tes_struktural_sub b
    //             ON b.id_sub_struktural=a.id_sub_struktural) b
    //     ON b.user_key=a.user_key";
    //     $conditions = [];
    //     $parameters = [];
    //     $conditions[] = "b.`status` = 'Tendik'";
    //     if (!empty($data['user_key'])) {
    //         $conditions[] = "a.user_key=?";
    //         $parameters[] = $data['user_key'];
    //     }
    //     if (!empty($data['id_sub_struktural'])) {
    //         $conditions[] = "b.id_sub_struktural=?";
    //         $parameters[] = $data['id_sub_struktural'];
    //     }
    //     if (!empty($data['id_angota_struktural'])) {
    //         $conditions[] = "b.id_angota_struktural=?";
    //         $parameters[] = $data['id_angota_struktural'];
    //     }
    //     if ($conditions) {
    //         $sql .= " WHERE " . implode(" AND ", $conditions);
    //     }
    //     $query = $this->db->query($sql, $parameters);
    //     if ($query->num_rows() > 0) {
    //         $result = $query->row_array();
    //         $query->free_result();
    //         return $this->app->respons_data($result, 'Data berhasil diload', 200);
    //     } else {
    //         return $this->app->respons_data(array(), 'data gagal diload', 200);
    //     }
    // }

    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT (SELECT GROUP_CONCAT(
                    CONCAT(c.nama_doskar,' (Keterangan : ',b.keterangan,')')
                    SEPARATOR ' <br>')
    FROM tes_struktural_anggota b
     JOIN tes_doskar_usm c ON c.user_key=b.user_key
     WHERE b.id_sub_struktural=a.id_sub_struktural) ket_anggota,
     a.id_sub_struktural,a.id_struktural,a.unit_bagian,a.nama_sub_struktural,a.kode_sto FROM tes_struktural_sub a
WHERE (a.id_struktural = '2' OR a.id_struktural = '3' OR a.id_struktural = '6' OR a.id_struktural = '7' OR a.id_struktural = '8')";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function simpan()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('user_key', 'Anggota', 'trim|required');
        $this->form_validation->set_rules('id_sub_struktural', 'Sub Struktural', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }

        $struktural_sub = $this->db->get_where('tes_struktural_sub', array('id_sub_struktural' => $data['id_sub_struktural']))->row_array();

        $data['user_key'] = $data['user_key'];
        $data['id_sub_struktural'] = $data['id_sub_struktural'];
        $data['keterangan'] = $struktural_sub['nama_sub_struktural'];

        $this->db->insert('tes_struktural_anggota', $data);

        if ($this->db->affected_rows() > 0) {
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        }
    }

    public function edit()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM tes_struktural_sub WHERE id_sub_struktural= ?";
        $query = $this->db->query($sql, array($data['id_sub_struktural']));
        if ($query->num_rows() > 0) {
            $struktural = $query->row_array();
            $query->free_result();

            $sql = "SELECT *,(SELECT GROUP_CONCAT(CONCAT('Unit Bagian : ',d.unit_bagian,' (',c.keterangan,')') SEPARATOR ' <br>')
        FROM tes_struktural_anggota c
        JOIN tes_struktural_sub d
        ON d.id_sub_struktural=c.id_sub_struktural
        WHERE user_key=a.user_key) ket_struktural,
        IFNULL((SELECT fakultas FROM program_studi WHERE kode_fakultas=a.fakultas LIMIT 1),'') fakultas,
        IFNULL((SELECT CONCAT('(',programstudi,')') FROM program_studi WHERE kode_khusus=a.kode_khusus LIMIT 1),'') programstudi
        FROM tes_struktural_anggota a
                    JOIN tes_doskar_usm b ON b.user_key=a.user_key
                    WHERE id_sub_struktural= ?";
            $query = $this->db->query($sql, array($data['id_sub_struktural']));
            $anggota = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('struktural' => $struktural, 'anggota' => $anggota), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('struktural' => array(), 'anggota' => array()), 'Data gagal diload', 200);
        }
    }

    public function update()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('id_angota_struktural', 'Anggota', 'trim|required');
        $this->form_validation->set_rules('id_sub_struktural', 'Sub Struktural', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }

        $datas['id_sub_struktural'] = $data['id_sub_struktural'];
        $struktural_sub = $this->db->get_where('tes_struktural_sub', array('id_sub_struktural' => $data['id_sub_struktural']))->row_array();
        $datas['keterangan'] = $struktural_sub['nama_sub_struktural'];

        $this->db->where('id_angota_struktural', $data['id_angota_struktural']);
        $this->db->update('tes_struktural_anggota', $datas);
        if ($this->db->affected_rows() >= 0) {
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        }
    }

    public function delete()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('id_angota_struktural', 'Anggota', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        $this->db->delete('tes_struktural_anggota', array('id_angota_struktural' => $data['id_angota_struktural']));
        if ($this->db->affected_rows() > 0) {
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        }
    }
}
