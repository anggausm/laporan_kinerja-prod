<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_akses extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function cek_akses($app_id = '', $menu_key = '', $crud = '', $user_key = '', $level_key = '')
    {
        //$token['id_app'], $api_url, $crud, $token['user_key'], $token['level_key']
        $sql = "SELECT * FROM (
                        SELECT id_levakses, menu_key, id_aplikasi  FROM `sso_level_akses`
                        WHERE level_id = '$level_key'
                        and menu_key ='$menu_key' and akses_$crud ='1' and id_aplikasi ='$app_id'
                )a
                 JOIN (
                            SELECT menu_key, id_aplikasi FROM sso_menu WHERE st ='1'
                            UNION
                            SELECT menu_key, id_aplikasi FROM sso_menu_sub WHERE st ='1'
                )b on a.menu_key = b.menu_key and a.id_aplikasi = b.id_aplikasi";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

}
