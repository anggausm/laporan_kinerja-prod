<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_Log extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        $this->server_log = $this->app->server_log();
    }

    public function index()
    {

    }

    public function get_last($nm_tabel = '', $tbl_key = '', $id_key = '')
    {
        $sql = "SELECT * FROM $nm_tabel ORDER BY $tbl_key DESC LIMIT 1   ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function get_data($nm_tabel = '', $tbl_key = '', $id_key = '')
    {
        $sql = "SELECT * FROM $nm_tabel WHERE $tbl_key  = '$id_key' ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function insert_log($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $act = '', $data = '', $keterangan = '', $tabel_master_log)
    {

        //$this->rest_log_save($user, $nm_tabel, $tbl_key, $id_key, $act, $data, $keterangan, $tabel_master_log);
        $sql = "INSERT INTO `log_sso` (`user`, nm_tabel, tbl_key, id_key, act, `data`,  keterangan) value ( ?, ?, ?, ?, ?, ?, ?) ";
        return $this->db->query($sql, array($user, $nm_tabel, $tbl_key, $id_key, $act, $data, $keterangan));

    }
    /*
    ACT
     */
    public function log_c($user = '', $nm_tabel = '', $tbl_key = '', $keterangan = '', $tabel_master_log = '')
    {
        $data = $this->get_last($nm_tabel, $tbl_key);
        $rs_data = json_encode($data);
        return $this->insert_log($user, $nm_tabel, $tbl_key, $data[$tbl_key], 'C', $rs_data, $keterangan, $tabel_master_log);
    }

    public function log_u($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $keterangan = '', $tabel_master_log = '')
    {
        $rs_data = json_encode($this->get_data($nm_tabel, $tbl_key, $id_key));
        return $this->insert_log($user, $nm_tabel, $tbl_key, $id_key, 'U', $rs_data, $keterangan, $tabel_master_log);
    }

    public function log_d($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $data, $keterangan = '', $tabel_master_log = '')
    {
        return $this->insert_log($user, $nm_tabel, $tbl_key, $id_key, 'D', $data, $keterangan, $tabel_master_log);
    }

    public function rest_log_save($user = '', $nm_tabel = '', $tbl_key = '', $id_key = '', $act = '', $data = '', $keterangan = '', $tabel_master_log)
    {

        $json_data = str_replace('\"', '**', json_encode(array(
            'tabel_name' => $tabel_master_log,
            'user' => $user,
            'nm_tabel' => $nm_tabel,
            'tbl_key' => $tbl_key,
            'id_key' => $id_key,
            'act' => $act,
            'data' => str_replace(' ', '__', $data),
            'keterangan' => $keterangan,
        )));
        $headers = array(
            "Content-type:  application/json;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"run\"",
            "Authorization: '' ",
            "X-HTTP-Method-Override: PATCH",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->server_log);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, false); //di set false agar ouputan header yang di kirim tidak di ikutkan
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
