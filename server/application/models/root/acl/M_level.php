<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_level extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }
    public function insert_data()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        $level_key = $data->level_key;

        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        $data_insert = array(
            'id_aplikasi' => $data->id_aplikasi,
            'level_key' => $level_key,
            'level_name' => str_replace('**', ' ', $data->level_name),
            'ket' => str_replace('**', ' ', $data->ket),
            'id_grub_user' => $data->id_grub_user,
            'c' => $token['user_key'],
        );
        if ($this->db->insert('sso_level', $data_insert)) {
            $this->log_app->log_data($token['user_key'], 'sso_level', 'level_key', $level_key, 'C', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    public function insert_data_roots()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        $level_key = $data->level_key;

        if ($token['user_key'] != 'ce4152ef-1ba8-11e8-a3d6-1cb72c27dd68') {
            return print '0';
        }
        if ($this->cek_insert_level($level_key)) {
            return print '1';
        }

        $data_insert = array(
            'id_aplikasi' => $data->id_aplikasi,
            'level_key' => $level_key,
            'level_name' => str_replace('**', ' ', $data->level_name),
            'ket' => str_replace('**', ' ', $data->ket),
            'id_grub_user' => $data->id_grub_user,
            'c' => $token['user_key'],
        );
        if ($this->db->insert('sso_level', $data_insert)) {
            $this->log_app->log_data($token['user_key'], 'sso_level', 'level_key', $level_key, 'C', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }

    public function cek_insert_level($level_key = '')
    {

        $sql = "SELECT level_key FROM `sso_level` WHERE level_key ='$level_key'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        // if ($data->level_key == '87350590-1ba9-11e8-a3d6-1cb72c27dd68') {
        //     return $this->app->respons_data(array(), 'Data Gagal di Proses', 200);
        // }

        $data_update = array(
            'id_aplikasi' => $data->id_aplikasi,
            'level_name' => str_replace('**', ' ', $data->level_name),
            'ket' => str_replace('**', ' ', $data->ket),
            'id_grub_user' => $data->id_grub_user,
            'st' => $data->st,
        );
        $where = array('level_key' => $data->level_key);
        if ($this->db->update('sso_level', $data_update, $where)) {
            $this->log_app->log_data($token['user_key'], 'sso_level', 'level_key', $data->level_key, 'U', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }
    public function delete_data()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        if ($token['user_key'] != $data->c) {
            return print '0';
        }

        // if ($data->level_key == '87350590-1ba9-11e8-a3d6-1cb72c27dd68') {
        //     return $this->app->respons_data(array(), 'Data Gagal di Proses', 200);
        // }

        $sql = "DELETE FROM sso_level WHERE level_key = ? ";
        if ($this->db->query($sql, array($data->level_key))) {
            $this->log_app->log_data($token['user_key'], 'sso_level', 'level_key', $data->level_key, 'D', json_encode($data), 'Miroring SSO', 'log_sso');
            return print '1';
        } else {
            return print '0';
        }
    }
    //Proses Kelola Level

    /*========================  INSERT ==========================*/
    /* Simpan Data  */
    public function act_simpan()
    {
        $token = $this->tkn;
        $user_key = $token['user_key'];
        $data = json_decode(file_get_contents("php://input"), true);
        //
        if ($token['id_app'] != $data['id_aplikasi']) {
            return print '0';
        }
        $jm_data = $data['jm_data'];
        $id_aplikasi = $data['id_aplikasi'];
        $level_key = $data['level_key'];
        $this->clear_data($id_aplikasi, $level_key);
        $jm_insert = 0;
        for ($i = 1; $i <= $jm_data; $i++) {
            if (!empty($data["menu_key_$i"])) {
                $menu_key = $data["menu_key_$i"];

                $ibu = $data["ibu_$i"];

                if (!empty($data["status_$i"])) {
                    $status = $data["status_$i"];
                } else {

                }
                //
                if (!empty($data["r_$i"])) {
                    $r = '1';
                } else {
                    $r = '0';
                }
                //
                if (!empty($data["u_$i"])) {
                    $u = '1';
                } else {
                    $u = '0';
                }
                //
                if (!empty($data["c_$i"])) {
                    $c = '1';
                } else {
                    $c = '0';
                }
                //
                if (!empty($data["d_$i"])) {
                    $d = '1';
                } else {
                    $d = '0';
                }
                //   return print_r($this->simpan($id_aplikasi, $level_key, $menu_key, $status, $c, $r, $u, $d, $user_key));
                if ($status == 'menu') {
                    //     if ($this->cek_menu_app($menu_key, $id_aplikasi)) { //di aktifkan jika UI sudah Selesai
                    $this->simpan($id_aplikasi, $level_key, $menu_key, $status, $c, $r, $u, $d, $user_key);
                    $jm_insert = $jm_insert + 1;
                    //    } else {

                    //    }
                } else {
                    if ($this->cek_induk($id_aplikasi, $level_key, $ibu)) {
                        $this->simpan($id_aplikasi, $level_key, $menu_key, $status, $c, $r, $u, $d, $user_key);
                    }

                }

            } else {

            }
        }
        //log data
        $this->log_app->log_data($token['user_key'], 'sso_level_akses', 'level_key', $level_key, 'U', file_get_contents("php://input"), 'Miroring SSO', 'log_sso');

        if ($jm_insert > 0) {
            return print '1';
        } else {
            return print '0';
        }

    }

    public function cek_menu_app($menu_key = '', $id_aplikasi = '')
    {
//mengecek apakah id menu smempunyai id aplikasi sama dengan yang di punyai
        $sql = "SELECT * FROM sso_menu WHERE menu_key = ? and id_aplikasi = ? ";
        $query = $this->db->query($sql, array($menu_key, $id_aplikasi));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function cek_induk($id_aplikasi = '', $level_id = '', $menu_key = '')
    {
        $sql = "SELECT * FROM `sso_level_akses` WHERE id_aplikasi = ? and   level_id = ? and menu_key = ?  ";
        $query = $this->db->query($sql, array($id_aplikasi, $level_id, $menu_key));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function clear_data($id_aplikasi = '', $id_level = '')
    {

        $sql = "DELETE FROM `sso_level_akses` WHERE id_aplikasi = ? and level_id = ? ";
        $this->db->query($sql, array($id_aplikasi, $id_level));

    }

    public function simpan($id_aplikasi = '', $level_id = '', $menu_key = '', $status = '', $akses_c = '', $akses_r = '', $akses_u = '', $akses_d = '', $c = '')
    {

        if ($this->cek_insert($id_aplikasi, $level_id, $menu_key)) {
            if ($akses_r == '1') {
                $data_insert = array(
                    'id_aplikasi' => $id_aplikasi,
                    'level_id' => $level_id,
                    'menu_key' => $menu_key,
                    'status' => $status,
                    'akses_c' => $akses_c,
                    'akses_r' => $akses_r,
                    'akses_u' => $akses_u,
                    'akses_d' => $akses_d,
                    'c' => $c,
                );
                //   return $data_insert;
                $this->db->insert('sso_level_akses', $data_insert);
            } else {

            }

        }

    }

    public function cek_insert($id_aplikasi = '', $level_id = '', $menu_key = '')
    {
        $sql = "SELECT * FROM `sso_level_akses` WHERE id_aplikasi = ? AND level_id = ? and menu_key =?";
        $query = $this->db->query($sql, array($id_aplikasi, $level_id, $menu_key));
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

}
