<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_rest_pmb extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function get_nama_nis()
    {
        $data = json_decode(file_get_contents("php://input"));
        $user_key = $data->user_key;
        $sql = "SELECT nama_doskar, nis FROM doskar_usm WHERE user_key ='$user_key'";
        $query = $this->db->query($sql, array($data->user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array( $data ), 'data gagal diload 1', 200);
        }
    }
}
