<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_akademik extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function akses_fakultas_tu()
    {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT b.unit_bagian, b.nama_sub_struktural, a.* FROM (SELECT id_sub_struktural, fakultas, kode_khusus, keterangan FROM struktural_anggota
				WHERE keterangan not like '%dosen%' and  user_key = ? )a
				JOIN struktural_sub b on a.id_sub_struktural = b.id_sub_struktural";
        $query = $this->db->query($sql, array($data->user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array( $data ), 'data gagal diload 1', 200);
        }
    }
}
