<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_periode extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function get_data()
    {
        $sql = "SELECT id_piket_wfh, periode_wfh, keterangan, date_format(tgl_mulai,'%d %M %Y') tgl_mulai, date_format(tgl_selesai,'%d %M %Y') tgl_selesai , c FROM `piket_wfh_periode`";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
    public function get_last_insert()
    {
        $sql = "SELECT id_piket_wfh FROM piket_wfh_periode ORDER BY id_piket_wfh desc LIMIT 1";
        $query = $this->db->query($sql, array());
        $result = $query->row_array();
        return $result['id_piket_wfh'];

    }
    public function add_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $data_insert = array(
            "periode_wfh" => $data->periode_wfh,
            "keterangan" => $data->keterangan,
            "tgl_mulai" => $data->tgl_mulai,
            "tgl_selesai" => $data->tgl_selesai,
        );
        if ($this->db->insert('piket_wfh_periode', $data_insert)) {
            $id_piket_wfh = $this->get_last_insert();
            $this->log_app->log_data($token['user_key'], 'piket_wfh_periode', 'id_piket_wfh', $id_piket_wfh, 'C', json_encode($data_insert), ' ', 'log_kinerja');
            return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan diload', 200);
        }
    }

    public function get_edit()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_piket_wfh = $data->id_piket_wfh;
        $sql = "SELECT id_piket_wfh, periode_wfh, keterangan, tgl_mulai, tgl_selesai, c FROM `piket_wfh_periode` WHERE id_piket_wfh = ? ";
        $query = $this->db->query($sql, array($id_piket_wfh));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
    public function update()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $data_u = array(
            "periode_wfh" => $data->periode_wfh,
            "keterangan" => $data->keterangan,
            "tgl_mulai" => $data->tgl_mulai,
            "tgl_selesai" => $data->tgl_selesai,
        );
        if ($this->db->update('piket_wfh_periode', $data_u, "id_piket_wfh = $data->id_piket_wfh")) {
            $this->log_app->log_data($token['user_key'], 'piket_wfh_periode', 'id_piket_wfh', $data->id_piket_wfh, 'U', json_encode($data_u), ' ', 'log_kinerja');
            return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan diload', 200);
        }

    }

    public function get_data_eelete($id = '')
    {
        $sql = "SELECT * FROM piket_wfh_periode WHERE id_piket_wfh = ?";
        $query = $this->db->query($sql, array($id));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function cek_delete($id_piket_wfh = '')
    {

        $sql = "SELECT id_piket_wfh FROM `piket_wfh_jadwal` WHERE id_piket_wfh = ? ";
        $query = $this->db->query($sql, array($id_piket_wfh));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_data()
    {
        $token = $this->tkn;
        $data = json_decode(file_get_contents("php://input"));
        $rs_data = $this->get_data_eelete($data->id_piket_wfh);
        if ($this->cek_delete($data->id_piket_wfh)) {
            return $this->app->respons_data(array(), 'Maaf data tidak bisa di hapus, di karenakan periode sudah ter transaksikan  dengan penjadwalan piket.', 200);
        }
        $sql = "DELETE FROM piket_wfh_periode WHERE id_piket_wfh = ? ";
        if ($this->db->query($sql, array($data->id_piket_wfh))) {
            $this->log_app->log_data($token['user_key'], 'piket_wfh_periode', 'id_piket_wfh', $data->id_piket_wfh, 'D', json_encode($rs_data), ' ', 'log_kinerja');
            return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan diload', 200);
        }
    }
}
