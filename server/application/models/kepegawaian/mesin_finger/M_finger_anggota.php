<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_anggota extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }
    public function index()
    {

    }

    public function get_data()
    {
        $sql    = "SELECT a.id_peserta, b.id_doskar, b.user_key, b.nama_doskar, a.finger_id, a.nickname, c.pekerjaan, d.ket, format(d.biaya_transport, 0, 'id_ID') tarif, a.status, a.shift
                    FROM absensi_peserta a
                        LEFT JOIN doskar_usm b ON a.id_doskar = b.id_doskar
                        LEFT JOIN absesni_setingan_jam c ON a.id_setting_jam = c.id_setting_jam AND a.shift = c.shift AND c.jenis_presensi = ?
                        LEFT JOIN absensi_tarif d ON a.id_tarif = d.id_tarif
                    ORDER BY a.id_peserta DESC ";

        $query  = $this->db->query($sql, array('Reguler'));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function add()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $result = array('jam'   => $this->_jam(),
                        'tarif' => $this->_tarif()
                    );

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function _jam()
    {
        $sql    = "SELECT a.id_setting_jam, a.pekerjaan 
                    FROM absesni_setingan_jam a
                    WHERE a.shift = ?
                        AND a.status = ?
                        AND a.jenis_presensi = ?
                    ORDER BY a.pekerjaan ";
        $query  = $this->db->query($sql, array('Pagi', '1', 'Reguler'));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _tarif()
    {
        $sql    = "SELECT a.id_tarif, a.ket, a.biaya_transport
                    FROM absensi_tarif a
                    WHERE a.status = ?
                    ORDER BY a.urutan ";
        $query  = $this->db->query($sql, array('aktif'));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _cek_peserta($post = '')
    {
        $sql    = "SELECT * FROM absensi_peserta a WHERE a.user_key = ? ";
        $query  = $this->db->query($sql, array($post['user_key']));
        $result = $query->row_array();
        $query->free_result();

        if(empty($result['user_key'])) {
            return false;
        } else {
            return true;
        }
    }

    public function _cek_fingerid($post = '')
    {
        $sql    = "SELECT * FROM absensi_peserta a WHERE a.finger_id = ? ";
        $query  = $this->db->query($sql, array($post['finger_id']));
        $result = $query->row_array();
        $query->free_result();

        if(empty($result['finger_id'])) {
            return false;
        } else {
            return true;
        }
    }

    public function insert()
    {
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token      = $this->tkn;

        // cek peserta/user_key
        $cek_pst    = $this->_cek_peserta($post);
        if($cek_pst === true) { // jika peserta belum ada.
            return $this->app->respons_data(false, 'Data gagal disimpan karena sudah terdaftar sebagai peserta finger.', 201);
        }

        // cek finger id
        $cek_finger = $this->_cek_fingerid($post);
        if($cek_finger === true) { // jika peserta belum ada.
            return $this->app->respons_data(false, 'Data gagal disimpan karena id finger sudah ada.', 201);
        }

        $this->db->trans_start();
        $this->db->insert('absensi_peserta', $post);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 201);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_peserta', 'id_peserta', $last_id, 'C', json_encode($post), 'input peserta presensi', 'log_sso');

            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }


        
        // if($cek === false) { // jika peserta belum ada.
        //     $this->db->trans_start();
        //     $this->db->insert('absensi_peserta', $post);
        //     $last_id = $this->db->insert_id();
        //     $this->db->trans_complete();

        //     if ($this->db->trans_status() === false) {
        //         $this->db->trans_rollback();
        //         return $this->app->respons_data(false, 'Data gagal disimpan', 201);
        //     } else {
        //         $this->db->trans_commit();
        //         $this->log_app->log_data($token['user_key'], 'absensi_peserta', 'id_peserta', $last_id, 'C', json_encode($post), 'input peserta presensi', 'log_sso');

        //         return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        //     }
        // } else {
        //     return $this->app->respons_data(false, 'Data gagal disimpan karena sudah terdaftar sebagai peserta finger.', 201);
        // }
    }

    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * FROM absensi_peserta WHERE id_peserta = ? ";
        $query  = $this->db->query($sql, $post->id_peserta);
        $result = $query->row_array();
        $query->free_result();

        $arr    = array('jam'   => $this->_jam(),
                        'tarif' => $this->_tarif(),
                        'dt'    => $result
                    );

        return $this->app->respons_data($arr, 'Data berhasil diload', 200);
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $set    = array('finger_id'     => $post['finger_id'],
                        'nickname'      => $post['nickname'],
                        'id_setting_jam'=> $post['jam'],
                        'id_tarif'      => $post['tarif'],
                        'shift'         => $post['shift'],
                        'status'        => $post['status']
                    );
        $where  = array('id_peserta' => $post['id_peserta']);

        $this->db->trans_start();
        $this->db->update('absensi_peserta', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_peserta', 'id_peserta', json_encode($set), 'U', json_encode($where), 'peserta finger update', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function delete()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        $where  = array('id_peserta' => $post['id_peserta']);

        $this->db->trans_start();
        $this->db->delete('absensi_peserta', $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal didelete', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_ijin', 'id_ijin', json_encode($where), 'D', json_encode($post['dt']), 'peserta finger delete', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil didelete', 200);
        }
    }
}