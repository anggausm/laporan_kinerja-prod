<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_ijin extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }
    public function index()
    {

    }

    public function get_data()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $sql    = "SELECT a.id_ijin, a.nama_doskar, b.jenis_absensi, a.tanggal_awal, a.tanggal_akhir, a.status, a.keterangan
                    FROM absensi_ijin a
                        JOIN absensi_jenis b ON a.jenis_ijin = b.id_jenis
                    -- ORDER BY FIELD(a.status, 'Menunggu', 'ACC', 'Ditolak'), a.tanggal_awal DESC
					WHERE DATE_FORMAT(a.tanggal_awal, '%Y-%m') = ?
                    ORDER BY a.id_ijin DESC ";

        $query  = $this->db->query($sql, array($post['bulan_tahun']));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_jenis()
    {
        $sql    = "SELECT id_jenis, jenis_absensi FROM absensi_jenis WHERE (kategori = ? OR kategori = ?) AND status = ? ORDER BY jenis_absensi";
        $query  = $this->db->query($sql, array('Ijin', 'Cuti', 'Aktif'));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token      = $this->tkn;
        $post['c']  = $token['username'];
        $post['dc'] = date('Y-m-d H:i:s');

        $this->db->trans_start();
        $this->db->insert('absensi_ijin', $post);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_ijin', 'id_ijin', $last_id, 'C', json_encode($post), 'input ijin doskar', 'log_sso');

            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * FROM absensi_ijin WHERE id_ijin = ? ";
        $query  = $this->db->query($sql, $post->id_ijin);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $this->db->trans_start();
        $this->db->update('absensi_ijin', $post['set'], $post['where']);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_ijin', 'id_ijin', json_encode($post['set']), 'U', json_encode($post['where']), 'update ijin doskar', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function delete()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $this->db->trans_start();
        $this->db->delete('absensi_ijin', $post['where']);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal didelete', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_ijin', 'id_ijin', json_encode($post['where']), 'D', json_encode($post['dt']), 'delete ijin doskar', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil didelete', 200);
        }
    }
}