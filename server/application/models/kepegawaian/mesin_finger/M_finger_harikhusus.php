<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_harikhusus extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_data()
    {
        $result = array('data' => $this->_list_data(), 'jenis' => $this->_list_jenis() );
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    
    public function _list_data()
    {
        $sql    = "SELECT a.id_tanggal AS id, a.tanggal, a.jenis_presensi AS id_jenis, b.jenis_absensi AS jenis, a.keterangan
                    FROM absensi_hari_khusus a
                        JOIN absensi_jenis b ON a.jenis_presensi = b.id_jenis
                    ORDER BY a.tanggal DESC LIMIT 150 ";
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _list_jenis()
    {
        $sql    = "SELECT id_jenis, jenis_absensi as nama_jenis 
                    FROM absensi_jenis WHERE kategori = ? ";
        $query  = $this->db->query($sql, array('Merah'));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
    
    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT a.id_tanggal, a.tanggal, a.jenis_presensi AS id_jenis, a.keterangan
                    FROM absensi_hari_khusus a WHERE a.id_tanggal = ? ";
        $query  = $this->db->query($sql, $post->id);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        $data   = array();

        $data['tanggal']        = $post['tanggal'];
        $data['jenis_presensi'] = $post['jenis'];
        $data['keterangan']     = $post['keterangan'];
        $data['c']              = $token['username'];
        $data['dc']             = date('Y-m-d H:i:s');

        $this->db->trans_start();
        $this->db->insert('absensi_hari_khusus', $data);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 201);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_hari_khusus', 'id_tanggal', $last_id, 'C', json_encode($data), 'input hari khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $set['tanggal']         = $post['tanggal'];
        $set['jenis_presensi']  = $post['jenis'];
        $set['keterangan']      = $post['keterangan'];
        $set['u']               = $token['username'];
        $set['du']              = date('Y-m-d H:i:s');

        $where['id_tanggal'] = $post['id'];

        $this->db->trans_start();
        $this->db->update('absensi_hari_khusus', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_hari_khusus', 'id_tanggal', $post['id'], 'U', json_encode($set), 'Update hari khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function delete()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $where['id_tanggal'] = $post['id'];
        $this->db->trans_start();
        $this->db->delete('absensi_hari_khusus', $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_hari_khusus', 'id_tanggal', $post['id'], 'D', json_encode($set), 'Hapus hari khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        }
    }
}