<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_tubel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_data()
    {
        $sql    = "SELECT a.id_tubel AS id, a.user_key, a.nama_doskar, a.kategori, a.status
                    FROM absensi_tubel a
                    ORDER BY a.id_tubel DESC ";
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    
    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * FROM absensi_tubel WHERE id_tubel = ? ";
        $query  = $this->db->query($sql, $post->id);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        $data   = array();

        $data['user_key']       = $post['key'];
        $data['nama_doskar']    = $post['nama'];
        $data['kategori']       = $post['kategori'];
        $data['status']         = 'Aktif';
        $data['c']              = $token['username'];
        $data['dc']             = date('Y-m-d H:i:s');

        
        // return $this->app->respons_data($data, 'Data gagal disimpan', 201);

        $this->db->trans_start();
        $this->db->insert('absensi_tubel', $data);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 201);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_tubel', 'id_tubel', $last_id, 'C', json_encode($data), 'input tugas belajar', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $set['user_key']       = $post['key'];
        $set['nama_doskar']    = $post['nama'];
        $set['kategori']       = $post['kategori'];
        $set['status']         = $post['status'];
        $set['u']               = $token['username'];
        $set['du']              = date('Y-m-d H:i:s');

        $where['id_tubel']      = $post['id'];

        $this->db->trans_start();
        $this->db->update('absensi_tubel', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_tubel', 'id_tubel', $post['id'], 'U', json_encode($set), 'Update tugas belajar', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }
}