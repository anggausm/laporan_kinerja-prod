<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_presensikhusus extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_data()
    {
        $sql    = "SELECT id_presensi_khusus AS id, nama_doskar, jabatan FROM absensi_presensi_khusus ORDER BY id_presensi_khusus DESC ";
        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    
    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * FROM absensi_presensi_khusus WHERE id_presensi_khusus = ? ";
        $query  = $this->db->query($sql, $post->id);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        $data   = array();

        $data['user_key']       = $post['key'];
        $data['nama_doskar']    = $post['nama'];
        $data['jabatan']        = $post['jabatan'];
        $data['c']              = $token['username'];
        $data['dc']             = date('Y-m-d H:i:s');

        
        // return $this->app->respons_data($data, 'Data gagal disimpan', 201);

        $this->db->trans_start();
        $this->db->insert('absensi_presensi_khusus', $data);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 201);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_presensi_khusus', 'id_presensi_khusus', $last_id, 'C', json_encode($data), 'input presensi khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $set['user_key']       = $post['key'];
        $set['nama_doskar']    = $post['nama'];
        $set['jabatan']        = $post['jabatan'];
        $set['u']               = $token['username'];
        $set['du']              = date('Y-m-d H:i:s');

        $where['id_presensi_khusus']      = $post['id'];

        $this->db->trans_start();
        $this->db->update('absensi_presensi_khusus', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_presensi_khusus', 'id_presensi_khusus', $post['id'], 'U', json_encode($set), 'Update presensi khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function delete()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $where['id_presensi_khusus'] = $post['id'];
        $this->db->trans_start();
        $this->db->delete('absensi_presensi_khusus', $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'absensi_presensi_khusus', 'id_presensi_khusus', $post['id'], 'D', json_encode($set), 'Hapus presensi khusus', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        }
    }
}