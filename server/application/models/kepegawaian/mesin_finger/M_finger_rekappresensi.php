<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_rekappresensi extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_data()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();

        $sql    = "SELECT * FROM (
                    SELECT a.user_key, c.id_doskar, a.id_setting_jam, a.nama_doskar, SUM(a.ijin_masuk) ijin_masuk, SUM(a.ijin_pulang) ijin_pulang, SUM(a.cuti) cuti, 
                        SUM(a.tugas) tugas, SUM(a.sakit) sakit, SUM(a.telat_masuk) telat_masuk, SUM(a.telat_pulang) telat_pulang, 
                        SUM(a.hari_khusus) hari_khusus, SUM(a.area_masuk) masuk_area_usm, SUM(a.area_pulang) pulang_area_usm,
                        SUM(a.bolos) bolos, SUM(a.hadir) hadir, 
                        (SUM(a.hari_khusus) + SUM(a.bolos) + SUM(a.hadir) + SUM(a.sakit) + SUM(a.cuti)) j_absen, 
                        SUM(a.status_transport) hitung_transport, a.tarif, (SUM(a.status_transport) * a.tarif) j_transport
                    FROM (SELECT * FROM finger_transport WHERE tanggal BETWEEN ? AND ? GROUP BY user_key, tanggal) a
                        JOIN absensi_peserta c ON a.user_key = c.user_key
                    WHERE (DATE_FORMAT(a.tanggal, '%w') = '1' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '2' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '3' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '4' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '5')
                        AND c.status = 'Finjer'
                        AND a.id_setting_jam != '3'
                    GROUP BY a.user_key

                    UNION

                    SELECT a.user_key, c.id_doskar, a.id_setting_jam, a.nama_doskar, SUM(a.ijin_masuk) ijin_masuk, SUM(a.ijin_pulang) ijin_pulang, SUM(a.cuti) cuti, 
                        SUM(a.tugas) tugas, SUM(a.sakit) sakit, SUM(a.telat_masuk) telat_masuk, SUM(a.telat_pulang) telat_pulang, 
                        SUM(a.hari_khusus) hari_khusus, SUM(a.area_masuk) masuk_area_usm, SUM(a.area_pulang) pulang_area_usm,
                        SUM(a.bolos) bolos, SUM(a.hadir) hadir, 
                        (SUM(a.hari_khusus) + SUM(a.bolos) + SUM(a.hadir) + SUM(a.sakit) + SUM(a.cuti)) j_absen, 
                        SUM(a.status_transport) hitung_transport, a.tarif, (SUM(a.status_transport) * a.tarif) j_transport
                    FROM (SELECT * FROM finger_transport WHERE tanggal BETWEEN ? AND ? GROUP BY user_key, tanggal) a
                        JOIN absensi_peserta c ON a.user_key = c.user_key
                    WHERE c.status = 'Finjer'
                        AND a.id_setting_jam = '3'
                    GROUP BY a.user_key
                ) a
                ORDER BY FIELD(a.id_setting_jam, '3'), a.nama_doskar ";

        $query  = $this->db->query($sql, array($post['awal'], $post['akhir'], $post['awal'], $post['akhir']));
        $result = $query->result_array();
        $query->free_result();

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_info()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $result = array('doskar'    => $this->_get_peserta($post),
                        'logs'      => $this->_get_transport($post));

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function _get_transport($post = '')
    {
        $sql    = "SELECT a.*, b.jenis_absensi 
                    FROM (
                    SELECT a.*
                    FROM (SELECT * FROM finger_transport WHERE tanggal BETWEEN ? AND ? GROUP BY user_key, tanggal) a
                        JOIN absensi_peserta c ON a.user_key = c.user_key
                    WHERE (DATE_FORMAT(a.tanggal, '%w') = '1' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '2' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '3' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '4' 
                            OR DATE_FORMAT(a.tanggal, '%w') = '5')
                        AND c.status = 'Finjer'
                        AND a.id_setting_jam != '3'

                    UNION

                    SELECT a.*
                    FROM (SELECT * FROM finger_transport WHERE tanggal BETWEEN ? AND ? GROUP BY user_key, tanggal) a
                        JOIN absensi_peserta c ON a.user_key = c.user_key
                    WHERE c.status = 'Finjer'
                        AND a.id_setting_jam = '3'
                ) a
                LEFT JOIN absensi_jenis b ON a.jenis_finger = b.id_jenis
				WHERE a.user_key = ?
                ORDER BY a.tanggal ";

        $query  = $this->db->query($sql, array($post['awal'], $post['akhir'], $post['awal'], $post['akhir'], $post['user_key']));
        $result = $query->result_array();
        $query->free_result();

        return $result;
    }

    public function _get_peserta($post = '')
    {
        $sql    = "SELECT a.user_key, a.id_finger, a.nama_doskar
                    FROM finger_transport a
                    WHERE a.user_key = ? AND a.tanggal BETWEEN ? AND ? 
                    ORDER BY a.tanggal DESC LIMIT 1 ";

        $query  = $this->db->query($sql, array($post['user_key'], $post['awal'], $post['akhir']));
        $result = $query->row_array();
        $query->free_result();

        return $result;
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        
        $trans      = $this->_get_logs($post);
        $cek_backup = $this->_cek_backup_log($post['id']);
        if (empty($cek_backup['id'])) {
            // proses insert
            $this->db->insert('finger_transport_master', $trans);
        }
        
        if ($post['status'] == '1') {
            $transport  = $trans['tarif'];
            $set        = array('status_transport' => '1', 'transport' => $transport, 'jenis_finger' => '11');
        } else {
            $jns_finger = !empty($cek_backup['jenis_finger']) ? $cek_backup['jenis_finger'] : $trans['jenis_finger'];
            $transport  = '0';
            $set        = array('status_transport' => '0', 'transport' => '0', 'jenis_finger' => $jns_finger);
        }
        
        $where      = array('id' => $post['id']);

        $this->db->trans_start();
        $this->db->update('finger_transport', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'finger_transport', 'id', $post['id'], 'U', json_encode($set), 'update transport', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }

    public function _get_logs($post = '')
    {
        $sql    = "SELECT *
                    FROM finger_transport a
                    WHERE a.id = ? ";

        $query  = $this->db->query($sql, array($post['id']));
        $result = $query->row_array();
        $query->free_result();

        return $result;
    }

    public function _cek_backup_log($id = '')
    {
        $sql    = "SELECT *
                    FROM finger_transport_master a
                    WHERE a.id = ? ";

        $query  = $this->db->query($sql, array($id));
        $result = $query->row_array();
        $query->free_result();

        return $result;
    }
}