<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_transport extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }
    public function index()
    { }

    public function get_data()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $this->form_validation->set_data($post);
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 201);
        }

        $sql    = "SELECT a.id, a.id_finger, a.user_key, a.nama_doskar, a.tanggal, a.jam_masuk, a.jam_pulang, a.shift, c.pekerjaan, a.transport,
                        if(a.ijin_masuk = '1' AND a.ijin_pulang = '1', 'Ijin Masuk dan Ijin Pulang', b.jenis_absensi) status_presensi, a.jenis_presensi
                    FROM `finger_transport` a
                        LEFT JOIN absensi_jenis b ON a.jenis_finger = b.id_jenis
                        LEFT JOIN absesni_setingan_jam_new c ON a.id_setting_jam = c.id
                    WHERE a.tanggal = ?
                    ORDER BY a.nama_doskar ";

        $query  = $this->db->query($sql, array($post['tanggal']));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function olah_transport()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $this->form_validation->set_data($post);
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis Presensi', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 201);
        }

        // ambil peserta presensi
        $peserta    = $this->_olah_peserta();
        foreach ($peserta as $key => $pst) {
            // cek pekerjaan antara satpam dan selain satpam
            // id_setting_jam 3 adalah satpam
            if ($pst['id_setting_jam'] == '3') {
                // satpam
                // menentukan shift satpam, jam masuk dan pulang
                $cek_shift = $this->_shift_satpam(array('tanggal' => $post['tanggal'], 'finger_id' => $pst['finger_id']));
                // menentukan bolos atau hadir
                if (!empty($cek_shift['jam_masuk']) && !empty($cek_shift['jam_pulang'])) {
                    // masuk
                    $hadir = '1';
                    $bolos = '0';
                    $shift = $cek_shift['shift'];
                    $jenis_finger = '1';
                } else {
                    // bolos
                    $hadir = '0';
                    $bolos = '1';
                    $shift = $pst['shift'];
                    $jenis_finger = '1';
                }
            }
            else
            {
                // selain satpam
                // menentukan shift selain satpam, jam masuk dan pulang
                $cek_shift = $this->_shift_selainsatpam(array('tanggal' => $post['tanggal'], 'finger_id' => $pst['finger_id']));
                // menentukan bolos atau hadir
                if (!empty($cek_shift['jam_masuk']) && !empty($cek_shift['jam_pulang'])) {
                    // masuk
                    $hadir = '1';
                    $bolos = '0';
                    $jenis_finger = '1';

                    if ($pst['id_setting_jam'] == '5' || $pst['id_setting_jam'] == '8') {
                        // jika dosen dan pengemudi shift sesuai shift default (tidak ada shift sore untuk dosen dan pengemudi)
                        $shift = $pst['shift'];
                    } else {
                        // selain dosen dan pengemudi shift di sesuaikan fingernya
                        $shift = $cek_shift['shift'];
                    }

                } else {
                    // bolos
                    $hadir = '0';
                    $bolos = '1';
                    $shift = $pst['shift'];
                    $jenis_finger = '1';
                }
            }

            // cek keterlambatan presensi
            $arr_telat = array('id_setting_jam' => $pst['id_setting_jam'],
                                'jenis_presensi'=> $post['jenis'],
                                'tanggal'       => $post['tanggal'],
                                'shift'         => $shift,
                                'jam_masuk'     => $cek_shift['jam_masuk'],
                                'jam_pulang'    => $cek_shift['jam_pulang']);
            $telat = $this->_cek_terlambat($arr_telat);
            if ($telat['masuk'] == '0' && $telat['pulang'] == '0') {
                $telat_masuk        = $telat['masuk'];
                $telat_pulang       = $telat['pulang'];
                $status_transport   = '1';
            } else {
                $telat_masuk        = $telat['masuk'];
                $telat_pulang       = $telat['pulang'];
                $status_transport = '0';
            }

            // cek kondisi untuk : presensi khusus(senat, pejabat, pengelola), tugas belajar, ijin, cuti, hari khusus(nasional, blok)
            // cek presensi khusus senat, senat harus presensi dengan mengabaikan jam presensi.
            /*
            $cek_senat = $this->db->get_where('absensi_presensi_khusus', array('user_key' => $pst['user_key'], 'jabatan' => 'Senat'))->row_array();
            if (!empty($cek_senat)) {
                if (!empty($cek_shift['jam_masuk']) || !empty($cek_shift['jam_pulang'])) {
                    // jika ada presensi salah satu, maka cek
                    $telat_masuk        = '0';
                    $telat_pulang       = '0';
                    $hadir              = '1';
                    $bolos              = '0';
                    $jenis_finger       = '11'; // presensi khusus
                    $status_transport   = '1';
                }
            }

            // cek presensi khusus pejabat dan pengelola, meski tidak presesnsi tetap dapat transport
            $cek_pejabat = $this->db->get_where('absensi_presensi_khusus', array('user_key' => $pst['user_key']))->row_array();
            if (!empty($cek_pejabat)) {
                $telat_masuk        = '0';
                $telat_pulang       = '0';
                $hadir              = '1';
                $bolos              = '0';
                $jenis_finger       = '11'; // presensi khusus
                $status_transport   = '1';
            }
            */ 

            // cek tugas belajar
            $cek_tubel = $this->db->get_where('absensi_tubel', array('user_key' => $pst['user_key'], 'kategori' => 'Luar Kota', 'status' => 'Aktif'))->row_array();
            if (!empty($cek_tubel)) {
                $telat_masuk        = '0';
                $telat_pulang       = '0';
                $hadir              = '1';
                $bolos              = '0';
                $jenis_finger       = '13'; // studi luar kota
                $status_transport   = '1';
            }

            // cek ijin masuk
            $ijin_masuk     = '0';
            $ijin_pulang    = '0';
            $cek_ijin_masuk = $this->_cek_ijin_mp(array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'jenis_ijin' => '2'));
            if (!empty($cek_ijin_masuk)) {
                $telat_masuk = '0';
                $ijin_masuk  = '1';
                $jenis_finger= '2'; // ijin masuk
            }

            // cek ijin pulang
            $cek_ijin_pulang = $this->_cek_ijin_mp(array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'jenis_ijin' => '3'));
            if (!empty($cek_ijin_pulang)) {
                $telat_pulang = '0';
                $ijin_pulang  = '1';
                $jenis_finger = '3'; // ijin pulang
            }

            // cek ijin sakit dan cuti
            $sakit  = '0';
            $cuti   = '0';
            $cek_ijin = $this->_cek_ijin(array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal']));
            if (!empty($cek_ijin)) {
                $telat_masuk  = '0';
                $telat_pulang = '0';
                $hadir        = '0';
                $bolos        = '0';
                $jenis_finger = $cek_ijin['jenis_ijin']; // sesuai kode sakit/cuti yang diambil
                ${$cek_ijin['field']}   = '1';
                $status_transport       = '1';
            }

            // cek ijin DINAS
            $cek_dinas = $this->_cek_ijin_mp(array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'jenis_ijin' => '17'));
            if (!empty($cek_dinas)) {
                $telat_masuk  = '0';
                $telat_pulang = '0';
                $hadir        = '1';
                $bolos        = '0';
                $jenis_finger = '17'; // sesuai kode sakit/cuti yang diambil
                $status_transport = '1';
            }

            // cek status_transport terbaru setelah ijin masuk dan pulang
            if ($telat_masuk == '0' && $telat_pulang == '0') {
                $status_transport = '1';
            } else {
                $status_transport = '0';
            }

            // cek tanggal khusus (libur nasional, cuti bersama, blok presensi) 
            $hari_khusus = '0';
            $cek_tgl_khusus = $this->db->get_where('absensi_hari_khusus', array('tanggal' => $post['tanggal']))->row_array();
            if (!empty($cek_tgl_khusus)) {
                $bolos  = '0';
                $sakit  = '0';
                $cuti   = '0';
                $jenis_finger = $cek_tgl_khusus['jenis_presensi']; //id libur nas dkk
                if ($cek_tgl_khusus['jenis_presensi'] == '16') {
                    $hadir          = '1';
                    $hari_khusus    = '0';
                } else {
                    $hadir          = '0';
                    $hari_khusus    = '1';
                }
                $status_transport   = '1';
            }

            // cek tugas, jika ada tugas tranport dihilangkan
            $tugas = '0';
            $cek_ijin_tugas = $this->_cek_ijin_mp(array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'jenis_ijin' => '4')); // 4 tugas; 6 BK/alasan penting
            if (!empty($cek_ijin_tugas)) {
                $tugas              = '1';
                $jenis_finger       = $cek_ijin_tugas['jenis_ijin']; // tugas
                $status_transport   = '0';
                if ($bolos == '1') {
                    $bolos = '0';
                    $hadir = '1';
                }
            }

            // menentukan transport
            $transport = $status_transport * $pst['tarif'];
            
            // data untuk insert dan update
            $data   = array('user_key'      => $pst['user_key'],
                            'id_finger'     => $pst['finger_id'],
                            'nama_doskar'   => $pst['nama_doskar'],
                            'tanggal'       => $post['tanggal'],
                            'jam_masuk'     => $cek_shift['jam_masuk'],
                            'jam_pulang'    => $cek_shift['jam_pulang'],
                            'telat_masuk'   => $telat_masuk,
                            'telat_pulang'  => $telat_pulang,
                            'bolos'         => $bolos,
                            'hadir'         => $hadir,
                            'status_transport' => $status_transport,
                            'tarif'         => $pst['tarif'],
                            'transport'     => $transport,
                            'area_masuk'    => $cek_shift['area_masuk'],
                            'area_pulang'   => $cek_shift['area_pulang'],
                            'ijin_masuk'    => $ijin_masuk,
                            'ijin_pulang'   => $ijin_pulang,
                            'cuti'          => $cuti,
                            'tugas'         => $tugas,
                            'sakit'         => $sakit,
                            'hari_khusus'   => $hari_khusus,
                            'jenis_finger'  => $jenis_finger,
                            'shift'         => $shift,
                            'jenis_presensi'=> $post['jenis'],
                            'id_setting_jam'=> $pst['id_setting_jam']);
            
        // }

            // cek transport sudah pernah diinput atau belum, kalau belum = insert; kalau sudah = update
            $cek_transport = $this->db->get_where('finger_transport', array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal']))->row_array();
            if (empty($cek_transport)) {
                // insert
                $this->db->insert('finger_transport', $data);
                $status = "Insert";

                $log = array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'tanggal_proses' => date('Y-m-d H:i:s'), 'username' => $token['username'], 'data' => json_encode($data), 'jenis' => 'Insert');
                $this->db->insert('finger_transport_log', $log);
            } else {
                // update
                $this->_update($data);
                $status = "Update";

                $log = array('user_key' => $pst['user_key'], 'tanggal' => $post['tanggal'], 'tanggal_proses' => date('Y-m-d H:i:s'), 'username' => $token['username'], 'data' => json_encode($data), 'jenis' => 'Update');
                $this->db->insert('finger_transport_log', $log);
            }
        }
        
        $this->log_app->log_data($token['user_key'], 'finger_transport', 'id_transport', '', 'C', json_encode(array()), ''.$status.' ijin doskar', 'log_sso');
        return $this->app->respons_data(true, 'Data tanggal '. $post['tanggal'] .' berhasil '. $status .'', 200);
    }

    public function _olah_peserta()
    {
        $token  = $this->tkn;
        $sql    = "SELECT a.id_doskar, a.user_key, b.finger_id, a.nama_doskar, b.shift, c.biaya_transport AS tarif, d.id_setting_jam, d.pekerjaan
                    FROM doskar_usm a
                        JOIN absensi_peserta b ON a.user_key = b.user_key
                        JOIN absensi_tarif c ON b.id_tarif = c.id_tarif
                        JOIN (SELECT id_setting_jam, pekerjaan FROM absesni_setingan_jam WHERE status = ? GROUP BY id_setting_jam) d ON b.id_setting_jam = d.id_setting_jam
                    WHERE b.status = ?
                    ORDER BY a.id_doskar ";

        $query  = $this->db->query($sql, array('1', 'Finjer'));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _shift_satpam($post = '')
    {
        // cek awal finger untuk menentukan shift pagi atau sore
        $cek_shift = $this->db->get_where('finger_logs', array('finger_date' => $post['tanggal'], 'finger_id' => $post['finger_id'], 'finger_time <=' => '12:00:00'))->row_array();
        if (!empty($cek_shift)) {
            // jika shift pagi
            // ambil jam masuk (dari pukul 00:00:00 sampai 12:00:00)
            $shift  = "Pagi";
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time <= ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '12:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $masuk  = $result;

            // ambil jam pulang
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time > ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '12:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $pulang  = $result;
        }
        else {
            // jika shift sore
            $shift  = "Sore";
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time >= ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '15:00:01'));
            $result = $query->row_array();
            $query->free_result();
            $masuk  = $result;

            // ambil jam pulang
            $next   = date('Y-m-d',strtotime("+1 day", strtotime($post['tanggal'])));
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time < ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($next, $post['finger_id'], '09:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $pulang  = $result;
        }

        $arr    = array('shift'         => $shift, 
                        'jam_masuk'     => $masuk['finger_time'],
                        'jam_pulang'    => $pulang['finger_time'], 
                        'area_masuk'    => $masuk['finger_lokasi'], 
                        'area_pulang'   => $pulang['finger_lokasi'] );
        return $arr;
    }

    public function _shift_selainsatpam($post = '')
    {
        // cek awal finger untuk menentukan shift pagi atau sore
        $cek_shift = $this->db->get_where('finger_logs', array('finger_date' => $post['tanggal'], 'finger_id' => $post['finger_id'], 'finger_time <=' => '12:00:00'))->row_array();
        if (!empty($cek_shift)) {
            // jika shift pagi
            // ambil jam masuk (dari pukul 00:00:00 sampai 12:00:00)
            $shift  = "Pagi";
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time <= ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '12:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $masuk  = $result;

            // ambil jam pulang
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time > ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '12:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $pulang  = $result;
        }
        else {
            // jika shift sore
            $shift  = "Sore";
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time BETWEEN ? AND ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '12:00:01', '19:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $masuk  = $result;

            // ambil jam pulang
            $sql    = "SELECT * FROM finger_logs a WHERE a.finger_date = ? AND a.finger_id = ? AND a.finger_time > ? ORDER BY finger_time DESC LIMIT 1";
            $query  = $this->db->query($sql, array($post['tanggal'], $post['finger_id'], '19:00:00'));
            $result = $query->row_array();
            $query->free_result();
            $pulang  = $result;
        }

        $arr    = array('shift'         => $shift, 
                        'jam_masuk'     => $masuk['finger_time'],
                        'jam_pulang'    => $pulang['finger_time'], 
                        'area_masuk'    => $masuk['finger_lokasi'], 
                        'area_pulang'   => $pulang['finger_lokasi'] );
        return $arr;
    }

    public function _cek_terlambat($post = '')
    {
        $tgl    = $post['tanggal'];
        $masuk  = $post['jam_masuk'];
        $pulang = $post['jam_pulang'];

        $sql    = "SELECT 
                        IF(DATE_FORMAT(?, '%w') = 5, IF(? BETWEEN a.masuk_awal_jumat AND a.masuk_akhir_jumat, '0', '1'), IF(? BETWEEN a.masuk_awal AND a.masuk_akhir, '0', '1')) masuk,
                        IF(DATE_FORMAT(?, '%w') = 5, IF(? BETWEEN a.pulang_awal_jumat AND a.pulang_akhir_jumat, '0', '1'), IF(? BETWEEN a.pulang_awal AND a.pulang_akhir, '0', '1')) pulang
                    FROM absesni_setingan_jam a
                    WHERE a.id_setting_jam = ?
                        AND a.jenis_presensi = ?
                        AND a.shift = ?
                        AND a.status = ? ";

        $query  = $this->db->query($sql, array($tgl, $masuk, $masuk, $tgl, $pulang, $pulang, $post['id_setting_jam'], $post['jenis_presensi'], $post['shift'], '1'));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _cek_ijin_mp($post = '')
    {
        $sql    = "SELECT * FROM absensi_ijin a 
                    WHERE a.user_key = ?
                        AND ? BETWEEN a.tanggal_awal AND a.tanggal_akhir 
                        AND a.jenis_ijin = ? 
	                    AND a.status = ? ";

        $query  = $this->db->query($sql, array($post['user_key'], $post['tanggal'], $post['jenis_ijin'], 'ACC'));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _cek_ijin($post = '')
    {
        $sql    = "SELECT a.*, b.jenis_absensi, b.field 
                    FROM absensi_ijin a
                        JOIN absensi_jenis b ON a.jenis_ijin = b.id_jenis 
                    WHERE a.user_key = ?
                        AND ? BETWEEN a.tanggal_awal AND a.tanggal_akhir  
	                    AND (b.field = ? OR b.field = ?)
	                    AND a.status = ? ";

        $query  = $this->db->query($sql, array($post['user_key'], $post['tanggal'], 'sakit', 'cuti', 'ACC'));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _update($post = '')
    {
        $set    = array('id_finger'     => $post['id_finger'],
                        'nama_doskar'   => $post['nama_doskar'],
                        'id_setting_jam'=> $post['id_setting_jam'],
                        'jenis_presensi'=> $post['jenis_presensi'],
                        'shift'         => $post['shift'],
                        'jam_masuk'     => $post['jam_masuk'],
                        'jam_pulang'    => $post['jam_pulang'],
                        'telat_masuk'   => $post['telat_masuk'],
                        'telat_pulang'  => $post['telat_pulang'],
                        'bolos'         => $post['bolos'],
                        'hadir'         => $post['hadir'],
                        'status_transport' => $post['status_transport'],
                        'tarif'         => $post['tarif'],
                        'transport'     => $post['transport'],
                        'area_masuk'    => $post['area_masuk'],
                        'area_pulang'   => $post['area_pulang'],
                        'ijin_masuk'    => $post['ijin_masuk'],
                        'ijin_pulang'   => $post['ijin_pulang'],
                        'tugas'         => $post['tugas'],
                        'sakit'         => $post['sakit'],
                        'cuti'          => $post['cuti'],
                        'hari_khusus'   => $post['hari_khusus'],
                        'jenis_finger'  => $post['jenis_finger'] );
        $where  = array('user_key' => $post['user_key'], 'tanggal' => $post['tanggal'] );
        $this->db->update('finger_transport', $set, $where);
    }
}