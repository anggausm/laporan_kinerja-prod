<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_finger_online_logs extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_data()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $result = array('peserta'   => $this->_peserta(),
                        'doskar'    => $this->_doskar($post),
                        'logs'      => $this->_log($post) 
                    );

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function _peserta()
    {
        $sql    = "SELECT a.user_key, a.nama_doskar
                    FROM doskar_usm a 
                    WHERE a.user_key != '' AND a.user_key != '-' AND a.user_key IS NOT NULL 
                    ORDER BY a.nama_doskar ";

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _doskar($post = '')
    {
        $sql    = "SELECT a.user_key, a.nama_doskar
                    FROM doskar_usm a 
                    WHERE a.user_key = ? ";

        $query  = $this->db->query($sql, array($post['user_key']));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _log($post = '')
    {
        $sql    = "SELECT a.user_key, a.hari, a.tanggal, a.jam_masuk, a.jam_pulang, a.kordinat_masuk, a.kordinat_pulang
                    FROM wfh_absensi a 
                    WHERE a.user_key = ? 
                            AND a.tanggal BETWEEN ? AND ? 
                    ORDER BY a.tanggal ";
                    
        $query  = $this->db->query($sql, array($post['user_key'], $post['awal'], $post['akhir']));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    /*
    public function edit()
    {
        $post   = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * FROM finger_mesin WHERE id_mesin = ? ";
        $query  = $this->db->query($sql, $post->id_mesin);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;
        $data   = array();

        $data['ip']         = $post['ip_mesin'];
        $data['lokasi']     = $post['lokasi'];
        $data['keterangan'] = $post['keterangan'];
        $data['jenis']      = 'Secondary';
        $data['status']     = 'Aktif';
        $data['c']          = $token['username'];
        $data['dc']         = date('Y-m-d H:i:s');

        $this->db->trans_start();
        $this->db->insert('finger_mesin', $data);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'finger_mesin', 'id_mesin', $last_id, 'C', json_encode($data), 'input mesin finger', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function update()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token  = $this->tkn;

        $set['ip']         = $post['ip_mesin'];
        $set['lokasi']     = $post['lokasi'];
        $set['keterangan'] = $post['keterangan'];
        $set['u']          = $token['username'];
        $set['du']         = date('Y-m-d H:i:s');

        $where['id_mesin'] = $post['id'];

        $this->db->trans_start();
        $this->db->update('finger_mesin', $set, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal diupdate', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'finger_mesin', 'id_mesin', $post['id_mesin'], 'U', json_encode($set), 'Update mesin finger', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }
    */
}