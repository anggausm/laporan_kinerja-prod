<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_laporan_baak_bauk extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function get_unit_lembaga()
    {
        $sql = "SELECT * FROM (
                SELECT kode_sto id_struktural, unit_bagian, kode_sto FROM (
                SELECT id_struktural, unit_bagian, kode_sto FROM struktural_sub WHERE id_struktural ='2'  and `level` <=2  GROUP BY unit_bagian)a
                UNION
                SELECT kode_sto id_struktural, unit_bagian, kode_sto FROM (
                SELECT id_struktural, unit_bagian, kode_sto FROM struktural_sub WHERE id_struktural ='3' and `level` <=2    GROUP BY unit_bagian)a
                )a ORDER BY a.kode_sto ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function detail_unit($id_struktural = '')
    {
        $sql = "SELECT  unit_bagian FROM struktural_sub WHERE kode_sto = ? ";
        $query = $this->db->query($sql, array($id_struktural));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_list_anggota()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_struktural = $data->id_struktural;
        $detail_struktural = $this->detail_unit($id_struktural);
        $sql = "SELECT a.*, b.user_key , c.nis, c.nama_doskar FROM (
                SELECT  id_sub_struktural, nama_sub_struktural, kode_sto, `level` FROM struktural_sub WHERE id_struktural ='2' or id_struktural ='3'
                 )a
                JOIN (SELECT id_angota_struktural, id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE keterangan !='dosen' )b
                on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key =c.user_key
               WHERE   a.kode_sto like '$id_struktural%'  ORDER BY b.id_angota_struktural";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array("unit_bagian" => $detail_struktural, "rs" => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_task_day($id_absen = '')
    {

        $sql = "SELECT id_task, judul_kerjaan, keterangan, dokumen, link, jam_task, jam_riport FROM wfh_my_task WHERE id_absen ='$id_absen'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function detail_doskar($user_key = '')
    {
        $sql = "SELECT a.*, c.kode_sto, c.nama_sub_struktural FROM (
		        SELECT user_key, nama_doskar, nis FROM doskar_usm WHERE user_key = '$user_key')a
				JOIN struktural_anggota b on a.user_key = b.user_key
				JOIN struktural_sub c on b.id_sub_struktural = c.id_sub_struktural
				WHERE b.keterangan not like '%dosen%'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function detail_presensi()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $data->user_key;
        $id_struktural = $data->id_struktural;
        $bulan = $data->bulan;
        $detail_unit = $this->detail_unit($id_struktural);
        $doskar = $this->detail_doskar($user_key);
        $sql = "SELECT a.*, b.id_absen,  b.jam_masuk, b.jam_pulang, b.kordinat_masuk, b.kordinat_pulang FROM (SELECT a.*, b.keterangan FROM (
    		SELECT date_format(tanggal, '%d %M %Y') tgl,  tanggal, hari FROM wfh_kalender WHERE tanggal like '$bulan%')a
			LEFT JOIN (SELECT tanggal, keterangan FROM wfh_libur WHERE jenis ='Libur Nasional') b on a.tanggal = b.tanggal)a
			LEFT JOIN (SELECT   tanggal, id_absen, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang, radius_berangkat, radius_pulang
			FROM wfh_absensi WHERE user_key = '$user_key' and tanggal like '$bulan%')b on a.tanggal = b.tanggal ORDER BY a.tanggal ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();

            foreach ($result as $value) {
                $task = $this->get_task_day($value['id_absen']);
                $arr[] = array("tanggal" => $value['tanggal'], "keterangan" => $value['keterangan'], "tgl" => $value['tgl'], "hari" => $value['hari'],
                    "id_absen" => $value['id_absen'], "jam_masuk" => $value['jam_masuk'], "jam_pulang" => $value['jam_pulang'], "kordinat_masuk" => $value['kordinat_masuk'], "kordinat_pulang" => $value['kordinat_pulang'], "radius_berangkat" => $value['radius_berangkat'], "radius_pulang" => $value['radius_pulang'], "task" => $task);
            }
            $rs_data = array("profil" => $doskar, "presensi" => $arr, 'unit_bagian' => $detail_unit);
            return $this->app->respons_data($rs_data, 'Data berhasil diload', 200);
        } else {
            $rs_data = array("profil" => $doskar, "presensi" => array(), 'unit_bagian' => $detail_unit);
            return $this->app->respons_data($rs_data, 'Data berhasil diload', 200);
        }
    }

    public function download_file()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_task = $data->id_task;
        $sql = "SELECT id_task, REPLACE(id_task,'-','')jdl_file, judul_kerjaan, dokumen FROM wfh_my_task WHERE id_task = ? ";
        $query = $this->db->query($sql, array($id_task));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

}
