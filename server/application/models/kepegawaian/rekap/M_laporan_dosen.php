<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_laporan_dosen extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function get_fakultsa()
    {
        $sql = "SELECT kode_fakultas, kode_khusus, kode_prodi, fakultas FROM program_studi GROUP BY kode_fakultas ORDER BY kode_khusus";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function detail_fakultas($kode_fakultas = '')
    {
        $sql = "SELECT kode_fakultas, kode_khusus, kode_prodi, fakultas FROM program_studi WHERE kode_fakultas = ?  GROUP BY kode_fakultas ";
        $query = $this->db->query($sql, array($kode_fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_list_anggota()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $kode_fakultas = $data->kode_fakultas;
        $detail_fak = $this->detail_fakultas($kode_fakultas);
        $sql = "SELECT a.*, b.user_key ,c.nis,  c.nidn, c.nama_doskar FROM (
        SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='4' and kode_sto not like '4.2.%'
UNION
SELECT id_sub_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE id_struktural ='5' and kode_sto not like '5.2%'
                )a
                JOIN (SELECT id_sub_struktural, user_key, keterangan FROM struktural_anggota WHERE  fakultas ='$kode_fakultas')b
                on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key =c.user_key ORDER BY a.`level`, a.kode_sto";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array("fakultas" => $detail_fak, "rs" => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_task_day($id_absen = '')
    {

        $sql = "SELECT id_task, judul_kerjaan, keterangan, dokumen, link, jam_task, jam_riport FROM wfh_my_task WHERE id_absen ='$id_absen'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function detail_doskar($user_key = '')
    {
        $sql = "SELECT a.*, c.kode_sto, c.nama_sub_struktural FROM (
		        SELECT user_key, nama_doskar, nis FROM doskar_usm WHERE user_key = '$user_key')a
				JOIN struktural_anggota b on a.user_key = b.user_key
				JOIN struktural_sub c on b.id_sub_struktural = c.id_sub_struktural ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }

    }

    public function detail_presensi()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $data->user_key;
        $kode_fakultas = $data->kode_fakultas;
        $bulan = $data->bulan;
        $detail_fakultas = $this->detail_fakultas($kode_fakultas);
        $doskar = $this->detail_doskar($user_key);
        $sql = "SELECT a.*, b.id_absen,  b.jam_masuk, b.jam_pulang, b.kordinat_masuk, b.kordinat_pulang FROM (SELECT a.*, b.keterangan FROM (
    		SELECT date_format(tanggal, '%d %M %Y') tgl,  tanggal, hari FROM wfh_kalender WHERE tanggal like '$bulan%')a
			LEFT JOIN (SELECT tanggal, keterangan FROM wfh_libur WHERE jenis ='Libur Nasional') b on a.tanggal = b.tanggal)a
			LEFT JOIN (SELECT   tanggal, id_absen, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang, radius_berangkat, radius_pulang
			FROM wfh_absensi WHERE user_key = '$user_key' and tanggal like '$bulan%')b on a.tanggal = b.tanggal ORDER BY a.tanggal ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();

            foreach ($result as $value) {
                $task = $this->get_task_day($value['id_absen']);
                $arr[] = array("tanggal" => $value['tanggal'], "keterangan" => $value['keterangan'], "tgl" => $value['tgl'], "hari" => $value['hari'],
                    "id_absen" => $value['id_absen'], "jam_masuk" => $value['jam_masuk'], "jam_pulang" => $value['jam_pulang'], "kordinat_masuk" => $value['kordinat_masuk'], "kordinat_pulang" => $value['kordinat_pulang'], "radius_berangkat" => $value['radius_berangkat'], "radius_pulang" => $value['radius_pulang'], "task" => $task);
            }
            $rs_data = array("profil" => $doskar, "presensi" => $arr, 'fakultas' => $detail_fakultas);
            return $this->app->respons_data($rs_data, 'Data berhasil diload', 200);
        } else {
            $rs_data = array("profil" => $doskar, "presensi" => array(), 'fakultas' => $detail_fakultas);
            return $this->app->respons_data($rs_data, 'Data berhasil diload', 200);
        }
    }

    public function download_file()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_task = $data->id_task;
        $sql = "SELECT id_task, REPLACE(id_task,'-','')jdl_file, judul_kerjaan, dokumen FROM wfh_my_task WHERE id_task = ? ";
        $query = $this->db->query($sql, array($id_task));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

}
