<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_allposting extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
     
    public function get_data()
    {
    	$tgl_awal 	= '2020-12-25';
    	$tgl_akhir 	= '2021-01-24';
    	// CEK BULAN

        $sql = "SELECT b.user_key, b.nama_doskar, SUM(a.ijin_masuk) ijin_masuk, SUM(a.ijin_pulang) ijin_pulang, SUM(a.cuti) cuti, 
                    SUM(a.tugas) tugas, SUM(a.sakit) sakit, SUM(a.telat_masuk) telat_masuk, SUM(a.telat_pulang) telat_pulang, 
                    SUM(a.hari_khusus) hari_khusus, SUM(a.bolos) bolos, SUM(a.hadir) hadir, 
                    (SUM(a.hari_khusus) + SUM(a.bolos) + SUM(a.hadir)) j_absen, 
                    SUM(a.status_transport) hitung_transport, a.tarif, (SUM(a.status_transport) * a.tarif) j_transport
				FROM absensi_histori a
					JOIN doskar_usm b ON a.user_key = b.user_key
                    JOIN absensi_peserta c ON b.user_key = c.user_key
				WHERE a.tanggal BETWEEN ? AND ?
					AND (DATE_FORMAT(a.tanggal, '%w') = ? OR DATE_FORMAT(a.tanggal, '%w') = ? OR DATE_FORMAT(a.tanggal, '%w') = ? OR DATE_FORMAT(a.tanggal, '%w') = ? OR DATE_FORMAT(a.tanggal, '%w') = ?)
                    AND c.status = ?
				GROUP BY a.user_key
				ORDER BY b.nama_doskar, a.tanggal";
        $query = $this->db->query($sql, array($tgl_awal, $tgl_akhir, '1', '2', '3', '4', '5', 'Finjer'));

        if ($query->num_rows() > 0) 
        {
            $result = $query->result_array();
            $query->free_result();
            $result;
        } 

        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        // $dataqu     = file_get_contents("php://input");
        // $data       = json_decode($dataqu, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];

        $datare		= $this->get_data();
        $data  		= $datare['result']['rs'];

        foreach ($data as $dt) {
        	$this->db->insert('absensi_histori_rekap', $dt);
        }

        if ($this->db->affected_rows() > 0) 
        {
            $this->log_app->log_data($user_key, 'absensi_histori_rekap', 'user_key, bulan_tahun', ' ', 'C', $data, ' ', 'log_absensi_histori_rekap');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        } 
        else 
        {
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        }
    }
}