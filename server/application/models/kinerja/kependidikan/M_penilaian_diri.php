<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_penilaian_diri extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }
    public function index()
    {

    }

    public function get_data()
    {
        $token      = $this->tkn;
        $sql    = "SELECT a.id_pertanyaan, a.pertanyaan, a.jenis, a.kategori, FORMAT(b.nilai, 0) nilai
                    FROM kinerja_pernyataan a
                        LEFT JOIN 
                        (
                            SELECT * FROM kinerja_nilai WHERE dinilai_user_key = ? AND penilai_user_key = ?
                        ) b ON a.id_pertanyaan = b.id_pertanyaan
                    WHERE a.jenis != 'Tenaga Pendidik'
                        AND a.kategori = 'Diri Sendiri'
                    ORDER BY a.id_pertanyaan ";

        $query  = $this->db->query($sql, array($token['user_key'], $token['user_key']));
        $result = $query->result_array();
        $query->free_result();
        $data   = array('data' => $result, 'status' => $this->_cek_data($token[user_key]));
        return $this->app->respons_data($data, 'Data berhasil diload', 200);
    }

    public function get_periode()
    {
        $sql = "SELECT * FROM kinerja_periode a WHERE a.status = ? ";
        $query  = $this->db->query($sql, array('1'));
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function simpan()
    {
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token      = $this->tkn;

        // cek data sudah input atau belum
        $cek = $this->_cek_data($token['user_key']);

        if ($cek === false) {
            foreach ($post['nilai'] as $key => $value) {
                $data = array('dinilai_user_key'    => $token['user_key'],
                                'penilai_user_key'  => $token['user_key'],
                                'id_pertanyaan'     => $key, 
                                'nilai'             => $value['nilai'],
                                'c'                 => $token['username'],
                                'dc'                => date('Y-m-d H:i:s') );
                
                $dt[]   = array('dinilai_user_key'    => $token['user_key'],
                                'penilai_user_key'  => $token['user_key'],
                                'id_pertanyaan'     => $key, 
                                'nilai'             => $value['nilai'] );

                $this->db->trans_start();
                $this->db->insert('kinerja_nilai', $data);
                $last_id = $this->db->insert_id();
                $this->db->trans_complete();
            }
            $this->log_app->log_data($token['user_key'], 'kinerja_nilai', 'id_nilai', $last_id, 'C', json_encode($dt), 'input penilaian kinerja sendiri', 'log_kinerja');
        } else {
            // proses update
            // foreach ($post['nilai'] as $key => $value) {
            //     $set   = array('nilai'         => $value['nilai'] );
                
            //     $dt[]   = array('nilai'         => $value['nilai'] );

            //     $where  = array('dinilai_user_key' => $token['user_key'], 'penilai_user_key' => $token['user_key'], 'id_pertanyaan'  => $key);

            //     $this->db->trans_start();
            //     $this->db->update('kinerja_nilai', $set, $where);
            //     $this->db->trans_complete();
            // }
            // $this->log_app->log_data($token['user_key'], 'kinerja_nilai', 'id_nilai', json_encode($dt), 'U', json_encode($where), 'update penilaian kinerja sendiri', 'log_kinerja');
        }

        return $this->app->respons_data(true, 'Data berhasil diload', 200);
    }

    public function _cek_data($user_key)
    {
        $sql    = "SELECT * 
                    FROM kinerja_nilai a 
                    WHERE a.dinilai_user_key = ?
                        AND a.penilai_user_key = ? ";

        $query  = $this->db->query($sql, array($user_key, $user_key));
        $result = $query->row_array();
        $query->free_result();
        
        if(empty($result)) {
            return false;
        } else {
            return true;
        }
    }
}