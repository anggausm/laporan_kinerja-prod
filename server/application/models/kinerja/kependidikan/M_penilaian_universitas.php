<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_penilaian_universitas extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_asesi()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $data['fak_unit'] = empty($data['fak_unit']) ? '' : $data['fak_unit'];
        $data['penilai_user_key'] = empty($data['penilai_user_key']) ? '' : $data['penilai_user_key'];

        $sql = "SELECT b.id_nilai,a.dinilai_nama,a.dinilai_user_key,a.dinilai_fak_unit,a.dinilai_jenis_pekerjaan
        ,c.status_verifikasi_univ,c.tgl_verifikator_mengisi,c.id_ajuan
        ,CASE 
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi_univ='1' THEN '1'
            ELSE '0'
        END AS disabled
        ,CASE 
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi_univ='1' THEN 'Sudah Dinilai'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi='0' AND c.status_ajuan='1' THEN 'Fakultas Belum Verifikasi'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi_univ='0' AND c.status_ajuan='1' AND c.tgl_verifikasi_univ IS NOT NULL THEN 'Belum Diverifikasi'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND (c.id_ajuan IS NULL OR c.status_ajuan='0') THEN 'Belum Mengajukan'
            ELSE 'Belum Dinilai'
        END AS status_nilai
        ,a.*
        FROM (SELECT * FROM kinerja_asesor WHERE kategori='universitas' AND penilai_user_key='$token[user_key]') a
        LEFT JOIN (
            SELECT a.id_nilai,a.dinilai_user_key,a.penilai_user_key,b.kategori,b.jenis
            FROM kinerja_nilai a
            JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
            WHERE ((b.jenis = 'Tenaga Kependidikan' AND b.kategori = 'Pimpinan') OR (b.jenis = 'Tenaga Pendidik' AND b.kategori = 'Dosen'))
            GROUP BY a.dinilai_user_key,a.penilai_user_key
        ) b ON a.dinilai_user_key=b.dinilai_user_key AND a.penilai_user_key=b.penilai_user_key
        LEFT JOIN (
            SELECT * FROM kinerja_ajuan GROUP BY user_key 
        ) c ON a.dinilai_user_key=c.user_key
        ORDER BY a.dinilai_id_doskar";
        $query  = $this->db->query($sql, array());
        $last_query = $this->db->last_query();
        $result = $query->result_array();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    public function get_pertanyaan()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $result = array();
        $result['periode'] = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        $get = $this->_get_pertanyaan_dosen($data);
        foreach ($get as $key => $value) {
            $result[$key] = $value;
        }
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    public function _get_pertanyaan_dosen($data = array())
    {
        $result['disabled'] = '0';
        $result['disabled_verifikasi'] = '0';
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai_univ']) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if ($now > $cek_periode['penilaian_selesai_univ']) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if ($check_ajuan['status_verifikasi_univ'] == '1') {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if (empty($check_ajuan['status_ajuan'])) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if (empty($check_ajuan)) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai['penilai_user_key_univ'])) {
            $result['disabled_verifikasi'] = '1';
        }

        if (empty($cek_nilai['penilai_user_key'])) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $result['kinerja_ajuan'] = $check_ajuan;

        $sql = "SELECT b.id_nilai,b.dinilai_user_key,b.penilai_user_key,b.penilai_user_key_univ,b.jumlah,b.nilai,b.link_dokumen,b.jumlah_pimpinan,b.nilai_pimpinan,b.keterangan,b.jumlah_univ,b.nilai_univ,b.keterangan_univ,a.*
        FROM kinerja_pernyataan a
        LEFT JOIN (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$data[dinilai_user_key]') b ON a.id_pertanyaan = b.id_pertanyaan
        WHERE a.jenis = 'Tenaga Pendidik' AND a.kategori = 'Dosen'
        ORDER BY a.urutan";
        $query  = $this->db->query($sql, array());
        $pertanyaan = $query->result_array();
        $last_query = $this->db->last_query();
        $result['pertanyaan'] = $pertanyaan;
        return $result;
    }

    public function save()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $this->_save_dosen($data);
    }
    public function _save_dosen($data = array())
    {
        $token = $this->tkn;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('dinilai_user_key', 'Dinilai User', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        if (empty($data['kinerja_nilai'])) {
            return $this->app->respons_data(false, 'Data nilai kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai_univ']) {
            return $this->app->respons_data(false, 'Penilaian Belum dimulai', 200);
        }
        if ($now > $cek_periode['penilaian_selesai_univ']) {
            return $this->app->respons_data(false, 'Penilaian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if (empty($check_ajuan)) {
            return $this->app->respons_data(false, 'Dosen belum mengisi form.', 200);
        }
        if (empty($check_ajuan['status_ajuan'])) {
            return $this->app->respons_data(false, 'Form belum diajukan. tidak dapat verifikasi', 200);
        }
        if ($check_ajuan['status_verifikasi_univ'] == '1') {
            return $this->app->respons_data(false, 'Form telah diverifikasi. tidak dapat verifikasi lagi', 200);
        }
        if ($data['dinilai_user_key'] == $token['user_key']) {
            return $this->app->respons_data(false, 'Maaf form penilaian dosen tidak bisa menilai diri sendiri', 200);
        }

        // cek data sudah input atau belum
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai)) {
            return $this->app->respons_data(false, 'Dosen Belum mengisi form', 200);
        }
        if (empty($cek_nilai['penilai_user_key'])) {
            return $this->app->respons_data(false, 'Fakultas belum mengisi form', 200);
        }
        $this->db->trans_start();
        $data_save = array();
        foreach ($data['kinerja_nilai'] as $key => $value) {
            $arr['penilai_user_key_univ'] = $token['user_key'];
            $arr['jumlah_univ'] = $value['jumlah_univ'];
            $arr['nilai_univ'] = $value['jumlah_univ'] * $value['skor'];
            $arr['keterangan_univ'] = $value['keterangan_univ'];
            $arr['id_nilai'] = $value['id_nilai'];
            $arr['u'] = $token['username'];
            $arr['du'] = date('Y-m-d H:i:s');
            $data_save[] = $arr;
        }
        $this->db->update_batch('kinerja_nilai', $data_save, 'id_nilai');

        $data_ajuan = array(
            'tgl_verifikator_mengisi_univ' => date('Y-m-d H:i:s'),
            'verifikator_user_key_univ' => $token['user_key'],
            'u' => $token['username'],
            'du' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_ajuan', $check_ajuan['id_ajuan']);
        $this->db->update('kinerja_ajuan', $data_ajuan);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_nilai', '', '', 'U', json_encode($data_save), 'universitas mengisi', 'log_kinerja');
            $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $check_ajuan['id_ajuan'], 'U', json_encode($data_ajuan), 'universitas mengisi', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }
    /* untuk form dosen */
    public function verifikasi()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token = $this->tkn;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('dinilai_user_key', 'Dinilai User', 'required');
        $this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if ($data['jenis_pekerjaan'] != 'Tenaga Pendidik') {
            return $this->app->respons_data(false, 'Verifikasi hanya untuk form dosen', 200);
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai_univ']) {
            return $this->app->respons_data(false, 'Penilaian Belum dimulai', 200);
        }
        if ($now > $cek_periode['penilaian_selesai_univ']) {
            return $this->app->respons_data(false, 'Penilaian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if (empty($check_ajuan['status_ajuan'])) {
            return $this->app->respons_data(false, 'Form belum diajukan. tidak dapat verifikasi', 200);
        }
        if ($check_ajuan['status_verifikasi_univ'] == '1') {
            return $this->app->respons_data(false, 'Form telah diverifikasi. tidak dapat verifikasi lagi', 200);
        }
        // cek nilai sudah input atau belum
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai)) {
            return $this->app->respons_data(false, 'Dosen Belum mengisi form', 200);
        }
        if (empty($cek_nilai['penilai_user_key'])) {
            return $this->app->respons_data(false, 'Fakultas belum mengisi form', 200);
        }
        if (empty($cek_nilai['penilai_user_key_univ'])) {
            return $this->app->respons_data(false, 'Tidak dapat diverifikasi. Simpan Form Penilaian terlebih dahulu.', 200);
        }
        $this->db->trans_start();
        $data_ajuan = array(
            'status_verifikasi_univ' => '1',
            'tgl_verifikasi_univ' => date('Y-m-d H:i:s'),
            'verifikator_user_key_univ' => $token['user_key'],
            'u' => $token['username'],
            'du' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_ajuan', $check_ajuan['id_ajuan']);
        $this->db->update('kinerja_ajuan', $data_ajuan);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal diverifikasi', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $check_ajuan['id_ajuan'], 'U', json_encode($data_ajuan), 'universitas verifikasi : form dosen', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil diverifikasi', 200);
        }
    }
}
