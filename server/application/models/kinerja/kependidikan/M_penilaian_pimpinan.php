<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_penilaian_pimpinan extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_asesi()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $data['fak_unit'] = empty($data['fak_unit']) ? '' : $data['fak_unit'];
        $data['penilai_user_key'] = empty($data['penilai_user_key']) ? '' : $data['penilai_user_key'];

        $sql = "SELECT b.id_nilai,a.dinilai_nama,a.dinilai_user_key,a.dinilai_fak_unit,a.dinilai_jenis_pekerjaan
        ,c.status_verifikasi,c.tgl_verifikator_mengisi,c.id_ajuan
        ,CASE
            WHEN a.dinilai_jenis_pekerjaan !='Tenaga Pendidik' AND b.dinilai_user_key IS NOT NULL THEN '1'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi='1' THEN '1'
            ELSE '0'
        END AS disabled
        ,CASE
            WHEN a.dinilai_jenis_pekerjaan !='Tenaga Pendidik' AND b.dinilai_user_key IS NOT NULL THEN 'Sudah Dinilai'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi='1' THEN 'Sudah Dinilai'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND c.status_verifikasi='0' AND c.status_ajuan='1' THEN 'Belum Diverifikasi'
            WHEN a.dinilai_jenis_pekerjaan='Tenaga Pendidik' AND (c.id_ajuan IS NULL OR c.status_ajuan='0') THEN 'Belum Mengajukan'
            ELSE 'Belum Dinilai'
        END AS status_nilai
        ,a.*
        FROM (SELECT * FROM kinerja_asesor WHERE kategori='pimpinan' AND penilai_user_key='$token[user_key]') a
        LEFT JOIN (
            SELECT a.id_nilai,a.dinilai_user_key,a.penilai_user_key,b.kategori,b.jenis
            FROM kinerja_nilai a
            JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
            WHERE ((b.jenis = 'Tenaga Kependidikan' AND b.kategori = 'Pimpinan') OR (b.jenis = 'Tenaga Pendidik' AND b.kategori = 'Dosen'))
            GROUP BY a.dinilai_user_key,a.penilai_user_key
        ) b ON a.dinilai_user_key=b.dinilai_user_key AND a.penilai_user_key=b.penilai_user_key
        LEFT JOIN (
            SELECT * FROM kinerja_ajuan GROUP BY user_key 
        ) c ON a.dinilai_user_key=c.user_key
        ORDER BY a.dinilai_id_doskar";

        // $sql = "SELECT b.status_verifikasi,b.tgl_verifikator_mengisi,b.id_ajuan,a.*,c.nama_doskar AS dinilai_nama_doskar
        // ,d.nama_doskar AS penilai_nama_doskar
        // FROM (
        //     SELECT a.dinilai_user_key,a.penilai_user_key,b.kategori,b.jenis
        //     FROM kinerja_nilai a 
        //     JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        //     GROUP BY a.dinilai_user_key,a.penilai_user_key
        // ) a
        // LEFT JOIN (
        //     SELECT * FROM kinerja_ajuan GROUP BY user_key 
        // ) b ON a.dinilai_user_key=b.user_key
        // LEFT JOIN doskar_usm c ON a.dinilai_user_key=c.user_key
        // LEFT JOIN doskar_usm d ON a.penilai_user_key=d.user_key
        // "; 
        $query  = $this->db->query($sql, array());
        $last_query = $this->db->last_query();
        $result = $query->result_array();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    public function get_pertanyaan()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $result = array();
        $result['periode'] = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (
            in_array($data['jenis_pekerjaan'], array('Tenaga Kependidikan', 'Tenaga Penunjang'))
        ) {
            $get = $this->_get_pertanyaan_tendik($data);
            foreach ($get as $key => $value) {
                $result[$key] = $value;
            }
        }
        if ($data['jenis_pekerjaan'] == 'Tenaga Pendidik') {
            $get = $this->_get_pertanyaan_dosen($data);
            foreach ($get as $key => $value) {
                $result[$key] = $value;
            }
        }
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    public function _get_pertanyaan_dosen($data = array())
    {
        $result['disabled'] = '0';
        $result['disabled_verifikasi'] = '0';
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai']) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if ($now > $cek_periode['penilaian_selesai']) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if ($check_ajuan['status_verifikasi'] == '1') {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if (empty($check_ajuan['status_ajuan'])) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        if (empty($check_ajuan)) {
            $result['disabled'] = '1';
            $result['disabled_verifikasi'] = '1';
        }
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai['penilai_user_key'])) {
            $result['disabled_verifikasi'] = '1';
        }
        $result['kinerja_ajuan'] = $check_ajuan;

        $sql = "SELECT b.id_nilai,b.dinilai_user_key,b.penilai_user_key,b.jumlah,b.nilai,b.link_dokumen,b.jumlah_pimpinan,b.nilai_pimpinan,b.keterangan,a.*
        FROM kinerja_pernyataan a
        LEFT JOIN (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$data[dinilai_user_key]') b ON a.id_pertanyaan = b.id_pertanyaan
        WHERE a.jenis = 'Tenaga Pendidik' AND a.kategori = 'Dosen'
        ORDER BY a.urutan";
        $query  = $this->db->query($sql, array());
        $pertanyaan = $query->result_array();
        $last_query = $this->db->last_query();
        $result['pertanyaan'] = $pertanyaan;
        return $result;
    }
    public function _get_pertanyaan_tendik($data = array())
    {
        $token = $this->tkn;
        $result['disabled'] = '0';
        $sql = "SELECT a.*,b.id_nilai FROM kinerja_pernyataan a
        JOIN kinerja_nilai b ON b.dinilai_user_key = '$data[dinilai_user_key]' AND a.id_pertanyaan = b.id_pertanyaan
        WHERE a.jenis = 'Tenaga Kependidikan' AND a.kategori = 'Pimpinan'
        ORDER BY a.urutan";
        $query  = $this->db->query($sql, array());
        $check_nilai = $query->row_array();
        $result['last_query_nilai'] = $this->db->last_query();
        if (!empty($check_nilai)) {
            $result['disabled'] = '1';
        }
        $sql = "SELECT a.*,b.id_nilai,b.dinilai_user_key,FORMAT(b.nilai, 0) nilai
                    FROM kinerja_pernyataan a
                    LEFT JOIN (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$data[dinilai_user_key]') b ON a.id_pertanyaan = b.id_pertanyaan
                    WHERE a.jenis = 'Tenaga Kependidikan' AND a.kategori = 'Pimpinan'
                    ORDER BY a.urutan";
        $query  = $this->db->query($sql, array());
        $pertanyaan = $query->result_array();
        $result['last_query'] = $this->db->last_query();
        $result['pertanyaan'] = $pertanyaan;
        return $result;
    }

    public function save()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        if (
            in_array($data['jenis_pekerjaan'], array('Tenaga Kependidikan', 'Tenaga Penunjang'))
        ) {
            $this->_save_tendik($data);
        } elseif ($data['jenis_pekerjaan'] == 'Tenaga Pendidik') {
            $this->_save_dosen($data);
        }
    }
    public function _save_tendik($data = array())
    {
        $token = $this->tkn;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('dinilai_user_key', 'Dinilai User', 'required');
        $this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        if (empty($data['kinerja_nilai'])) {
            return $this->app->respons_data(false, 'Data nilai kosong, gagal disimpan', 200);
        }

        $sql = "SELECT a.id_pertanyaan,a.kategori,a.jenis,b.dinilai_user_key,b.penilai_user_key,c.nama_doskar 
        FROM (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$data[dinilai_user_key]' AND penilai_user_key != '$token[user_key]') b
        JOIN kinerja_pernyataan a ON a.id_pertanyaan = b.id_pertanyaan
        LEFT JOIN doskar_usm c ON b.penilai_user_key=c.user_key 
        WHERE a.kategori='Pimpinan'";
        $query  = $this->db->query($sql, array());
        $cek_form_penilaian = $query->row_array();
        // CHECK 1 TENDIK HANYA DAPAT DINILAI 1 PIMPINAN
        if (!empty($cek_form_penilaian)) {
            return $this->app->respons_data(false, "Maaf form Penilaian Pimpinan sudah diisi oleh $cek_form_penilaian[nama_doskar]", 200);
        }

        $check_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key'], 'penilai_user_key' => $token['user_key']))->row_array();
        $penilai = $this->db->get_where('doskar_usm', array('user_key' => $token['user_key']))->row_array();
        $dinilai = $this->db->get_where('doskar_usm', array('user_key' => $data['dinilai_user_key']))->row_array();
        if (!empty($check_nilai)) {
            return $this->app->respons_data(false, "Tidak dapat dinilai lagi, gagal disimpan. $penilai[nama_doskar] telah Menilai $dinilai[nama_doskar]", 200);
        }
        $this->db->trans_start();
        $data_save = array();
        foreach ($data['kinerja_nilai'] as $key => $value) {
            $arr['dinilai_user_key'] = $data['dinilai_user_key'];
            $arr['penilai_user_key'] = $token['user_key'];
            $arr['id_pertanyaan'] = $value['id_pertanyaan'];
            $arr['nilai'] = $value['nilai'];
            $arr['c'] = $token['username'];
            $arr['dc'] = date('Y-m-d H:i:s');
            $data_save[] = $arr;
        }
        $this->db->insert_batch('kinerja_nilai', $data_save);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_nilai', '', '', 'C', json_encode($data_save), 'pimpinan mengisi tenaga kependidikan', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }
    public function _save_dosen($data = array())
    {
        $token = $this->tkn;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('dinilai_user_key', 'Dinilai User', 'required');
        $this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        if (empty($data['kinerja_nilai'])) {
            return $this->app->respons_data(false, 'Data nilai kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai']) {
            return $this->app->respons_data(false, 'Penilaian Belum dimulai', 200);
        }
        if ($now > $cek_periode['penilaian_selesai']) {
            return $this->app->respons_data(false, 'Penilaian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if (empty($check_ajuan)) {
            return $this->app->respons_data(false, 'Dosen belum mengisi form.', 200);
        }
        if (empty($check_ajuan['status_ajuan'])) {
            return $this->app->respons_data(false, 'Form belum diajukan. tidak dapat verifikasi', 200);
        }
        if ($check_ajuan['status_verifikasi'] == '1') {
            return $this->app->respons_data(false, 'Form telah diverifikasi. tidak dapat verifikasi lagi', 200);
        }
        if ($data['dinilai_user_key'] == $token['user_key']) {
            return $this->app->respons_data(false, 'Maaf form penilaian dosen tidak bisa menilai diri sendiri', 200);
        }

        // cek data sudah input atau belum
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai)) {
            return $this->app->respons_data(false, 'Dosen Belum mengisi form', 200);
        }
        $this->db->trans_start();
        $data_save = array();
        foreach ($data['kinerja_nilai'] as $key => $value) {
            $arr['penilai_user_key'] = $token['user_key'];
            $arr['jumlah_pimpinan'] = $value['jumlah_pimpinan'];
            $arr['nilai_pimpinan'] = $value['jumlah_pimpinan'] * $value['skor'];
            $arr['keterangan'] = $value['keterangan'];
            $arr['id_nilai'] = $value['id_nilai'];
            $arr['u'] = $token['username'];
            $arr['du'] = date('Y-m-d H:i:s');
            $data_save[] = $arr;
        }
        $this->db->update_batch('kinerja_nilai', $data_save, 'id_nilai');

        $data_ajuan = array(
            'tgl_verifikator_mengisi' => date('Y-m-d H:i:s'),
            'verifikator_user_key' => $token['user_key'],
            'u' => $token['username'],
            'du' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_ajuan', $check_ajuan['id_ajuan']);
        $this->db->update('kinerja_ajuan', $data_ajuan);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_nilai', '', '', 'U', json_encode($data_save), 'Pimpinan ke Dosen', 'log_kinerja');
            $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $check_ajuan['id_ajuan'], 'U', json_encode($data_ajuan), 'pimpinan mengisi : Pimpinan ke Dosen', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil diupdate', 200);
        }
    }
    /* untuk form dosen */
    public function verifikasi()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token = $this->tkn;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('dinilai_user_key', 'Dinilai User', 'required');
        $this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if ($data['jenis_pekerjaan'] != 'Tenaga Pendidik') {
            return $this->app->respons_data(false, 'Verifikasi hanya untuk form dosen', 200);
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['penilaian_mulai']) {
            return $this->app->respons_data(false, 'Penilaian Belum dimulai', 200);
        }
        if ($now > $cek_periode['penilaian_selesai']) {
            return $this->app->respons_data(false, 'Penilaian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $data['dinilai_user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if (empty($check_ajuan['status_ajuan'])) {
            return $this->app->respons_data(false, 'Form belum diajukan. tidak dapat verifikasi', 200);
        }
        if ($check_ajuan['status_verifikasi'] == '1') {
            return $this->app->respons_data(false, 'Form telah diverifikasi. tidak dapat verifikasi lagi', 200);
        }
        // cek nilai sudah input atau belum
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $data['dinilai_user_key']))->row_array();
        if (empty($cek_nilai)) {
            return $this->app->respons_data(false, 'Dosen Belum mengisi form', 200);
        }
        if (empty($cek_nilai['penilai_user_key'])) {
            return $this->app->respons_data(false, 'Tidak dapat diverifikasi. Simpan Form Penilaian terlebih dahulu.', 200);
        }
        $this->db->trans_start();
        $data_ajuan = array(
            'status_verifikasi' => '1',
            'tgl_verifikasi' => date('Y-m-d H:i:s'),
            'verifikator_user_key' => $token['user_key'],
            'u' => $token['username'],
            'du' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_ajuan', $check_ajuan['id_ajuan']);
        $this->db->update('kinerja_ajuan', $data_ajuan);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal diverifikasi', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $check_ajuan['id_ajuan'], 'U', json_encode($data_ajuan), 'pimpinan verifikasi : form dosen', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil diverifikasi', 200);
        }
    }
}
