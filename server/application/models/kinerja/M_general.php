<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_general extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_periode_aktif()
    {
        $token = $this->tkn;
        $sql = "SELECT * FROM kinerja_periode a WHERE a.status = ? ";
        $query  = $this->db->query($sql, array('1'));
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
}
