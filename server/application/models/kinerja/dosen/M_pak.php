<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_pak extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
        $this->sample   = '3b68908b-6f68-11ea-89e7-1cb72c27dd68'; // khoirudin 3b689547-6f68-11ea-89e7-1cb72c27dd68
    }

    public function get_periode()
    {
        $sql    = "SELECT a.id_periode, a.nama_periode, a.bulan_tahun, kategori, a.pengisian_mulai, a.pengisian_selesai, a.penilaian_mulai, a.penilaian_selesai, a.versi, a.id_semester, a.nama_semester
                    FROM kinerja_periode a
                    WHERE a.kategori = 'Dosen' AND a.status = '1'
                    ORDER BY a.bulan_tahun DESC ";

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_pak()
    {
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode    = $this->_periode($post['id_periode']);
        $result['periode']          = $periode;
        $result['profil']           = $this->_profil();
        $result['unsur']            = $this->_unsur_pak($periode);
        $result['histori']          = $this->_pak_histori($periode);
        $result['kriteria']         = $this->_kriteria_nilai($periode);
        $result['kriteria_score']   = $this->_cek_kriteria_score($periode);
        $result['button']           = $this->_cek_button($periode);

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode_pilih()
    {
        $post   = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $sql    = "SELECT a.id_periode, a.nama_periode, a.bulan_tahun, kategori, a.pengisian_mulai, a.pengisian_selesai, a.penilaian_mulai, a.penilaian_selesai, a.versi, a.id_semester, a.nama_semester
                    FROM kinerja_periode a
                    WHERE a.id_periode = ?
                    ORDER BY a.bulan_tahun DESC ";

        $query  = $this->db->query($sql, $post['id_periode']);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function _periode($id_periode = '')
    {
        $sql    = "SELECT a.id_periode, a.nama_periode, a.bulan_tahun, kategori, a.pengisian_mulai, a.pengisian_selesai, a.penilaian_mulai, a.penilaian_selesai, a.versi, a.id_semester, a.nama_semester
                    FROM kinerja_periode a
                    WHERE a.id_periode = ?
                    ORDER BY a.bulan_tahun DESC ";

        $query  = $this->db->query($sql, $id_periode);
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _profil()
    {
        $token  = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $sql    = "SELECT * 
                    FROM pak_profil a WHERE a.user_key = ? ";

        $query  = $this->db->query($sql, $token['user_key']);
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _unsur_pak($post = '')
    {
        $token  = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $sql    = "SELECT x.*
                    , IFNULL((SELECT ROUND((SUM(a.skor) * b.nilai), 3) j_skor_diajukan
                        FROM pak_values a
                            JOIN pak_3_kegiatan b ON a.id_kegiatan = b.id_kegiatan
                        WHERE b.id_unsur = x.id_unsur AND a.user_key = ? AND a.id_periode = ?
                        GROUP BY b.id_unsur
                    ), '0') usulan_dosen
                    , IFNULL((SELECT ROUND((SUM(a.skor) * b.nilai), 3) j_skor_verifikator
                        FROM pak_values a
                            JOIN pak_3_kegiatan b ON a.id_kegiatan = b.id_kegiatan
                        WHERE b.id_unsur = x.id_unsur AND a.user_key = ? AND a.id_periode = ? AND a.status = 'Selesai'
                        GROUP BY b.id_unsur
                    ), '0') nilai_asesor
                FROM pak_1_unsur x
                WHERE x.versi = ? 
                ORDER BY x.urutan ";

        $query  = $this->db->query($sql, array($token['user_key'], $post['id_periode'], $token['user_key'], $post['id_periode'], $post['versi']));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _cek_kriteria_score($post = '')
    {
        $token  = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        // cek histori pengajuan
        $sql_1    = "SELECT * FROM pak_values WHERE user_key = ? AND id_periode = ? ";
        $query_1  = $this->db->query($sql_1, array($token['user_key'], $post['id_periode']));
        $result_1 = $query_1->row_array();
        

        if (!empty($result_1)) {
            // cek nilai 0 pada prasyarat wajib
            $sql    = "SELECT
                        (
                            SELECT COUNT(*) prasyarat_dosen FROM 
                            (
                                    SELECT x.*
                                        , IFNULL((SELECT SUM(a.skor) j_skor_diajukan
                                                FROM pak_values a
                                                        JOIN pak_3_kegiatan b ON a.id_kegiatan = b.id_kegiatan
                                                WHERE b.id_unsur = x.id_unsur AND a.user_key = ? AND a.id_periode = ?
                                                GROUP BY b.id_unsur
                                        ), '0') j_skor_diajukan
                                    FROM pak_1_unsur x
                                    WHERE x.versi = ? 
                                            AND x.syarat = 'Tidak boleh kosong'
                            ) a WHERE a.j_skor_diajukan = '0'
                        ) AS prasyarat_dosen,
                        (
                            SELECT COUNT(*) prasyarat_verifikator FROM 
                            (
                                    SELECT x.*
                                        , IFNULL((SELECT SUM(a.skor) j_skor_diajukan
                                                FROM pak_values a
                                                        JOIN pak_3_kegiatan b ON a.id_kegiatan = b.id_kegiatan
                                                WHERE b.id_unsur = x.id_unsur AND a.user_key = ? AND a.id_periode = ? AND a.status = 'Selesai'
                                                GROUP BY b.id_unsur
                                        ), '0') j_skor_diajukan
                                    FROM pak_1_unsur x
                                    WHERE x.versi = ? 
                                            AND x.syarat = 'Tidak boleh kosong'
                            ) a WHERE a.j_skor_diajukan = '0'
                        ) AS prasyarat_verifikator ";

            $query  = $this->db->query($sql, array($token['user_key'], $post['id_periode'], $post['versi'], $token['user_key'], $post['id_periode'], $post['versi']));
            $result = $query->row_array();
            $query->free_result();
            
            if ($result['prasyarat_dosen'] == 0) { 
                $dosen = 'Ya'; 
            } else { 
                $dosen = 'Tidak';
            }

            if ($result['prasyarat_verifikator'] == 0) { 
                $verifikator = 'Ya'; 
            } else { 
                $verifikator = 'Tidak';
            }
            
            $response = array('hitung_dosen' => $dosen, 'hitung_verifikator' => $verifikator );
        } else {
            $response = array('hitung_dosen' => 'Buruk', 'hitung_verifikator' => 'Buruk');
        }

        return $response;
    }

    public function _cek_button($post = '')
    {
        $token  = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        $tgl_now    = date('Y-m-d');
        $periode    = $this->_periode($post['id_periode']);

        $sql    = "SELECT IF(a.status = 'Diajukan' OR a.status = 'Selesai', 'Disable', 'Enable') status_button, a.status
                        , (SELECT x.akses FROM pak_profil x WHERE x.user_key = a.key_dosen AND x.id_periode = a.id_periode ORDER BY x.id_pak_dosen DESC LIMIT 1) akses
                    FROM pak_ajuan_histori a
                    WHERE a.key_dosen = ? AND a.id_periode = ?
                    ORDER BY a.id_ajuan DESC LIMIT 1 ";

        $query  = $this->db->query($sql, array($token['user_key'], $post['id_periode']));
        $result = $query->row_array();
        $query->free_result();

        if ($result['akses'] == 'Open') {
            // status button enable dan status harus revisi
            $response   = array('status_button' => 'Enable', 'status' => $result['status']);
        } else {
            // cek periode pengisian
            if ($tgl_now >= $periode['pengisian_mulai'] && $tgl_now <= $periode['pengisian_selesai']) {
                if (!empty($result)) {
                    $response   = $result;
                } else {
                    $response   = array('status_button' => 'Enable', 'status' => '');
                }
               
            } else {
                $response   = array('status_button' => 'Disbale', 'status' => 'Selesai');
            }            
        }

        return $response;
    }

    public function _unsur($post = '')
    {
        $sql    = "SELECT * FROM pak_1_unsur a WHERE a.versi = ? ";

        $query  = $this->db->query($sql, $post['versi']);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
    
    public function _unsur_detail($id_unsur)
    {
        $sql    = "SELECT * FROM pak_1_unsur a WHERE a.id_unsur = ? ";

        $query  = $this->db->query($sql, $id_unsur);
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }
    
    public function _kegiatan_detail($id_kegiatan)
    {
        $sql    = "SELECT id_kegiatan, id_unsur, id_subunsur, nama_unsur, nama_subunsur, nama_kegiatan, satuan, a.nomor, a.jenis_insert, a.link, a.nm_tabel, 
                        a.field_rincian, a.nilai_kegiatan, nilai, otomatis_selesai
                    FROM pak_3_kegiatan a WHERE a.id_kegiatan = ? ";

        $query  = $this->db->query($sql, $id_kegiatan);
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _pak_histori($post = '')
    {
        $token  = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $sql    = "SELECT *, 
                        DATE_FORMAT(a.tanggal, '%Y-%m-%d') tgl, 
                        DATE_FORMAT(a.tanggal, '%H:%i:%s') jam,
	                    IF(a.status = 'Diajukan', 'blue', IF(a.status = 'Revisi', 'red', IF(a.status = 'Selesai', 'teal', ''))) label
                    FROM pak_ajuan_histori a 
                    WHERE a.key_dosen = ? AND a.id_periode = ? 
                    ORDER BY a.tanggal";

        $query  = $this->db->query($sql, array($token['user_key'], $post['id_periode']));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function _kriteria_nilai($periode = '')
    {
        $sql    = "SELECT a.nilai, a.score, a.kategori FROM pak_kriteria_nilai a WHERE a.id_periode = ? ORDER BY a.urutan ";

        $query  = $this->db->query($sql, $periode['id_periode']);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function get_pak_kegiatan()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;
        $result['unsur']    = $this->_unsur_detail($post['id_unsur']);
        $sql                = "SELECT x.id_kegiatan, x.nomor, x.nama_kegiatan, x.satuan, x.value
                                    , IFNULL((SELECT ROUND((SUM(a.skor) * x.nilai), 3) j_skor_diajukan FROM pak_values a WHERE a.user_key = ? AND a.id_periode = ? AND a.id_kegiatan = x.id_kegiatan), '0') j_skor_diajukan
                                    , IFNULL((SELECT ROUND((SUM(a.skor) * x.nilai), 3) j_skor_diajukan FROM pak_values a WHERE a.user_key = ? AND a.id_periode = ? AND a.id_kegiatan = x.id_kegiatan AND a.status = 'Selesai'), '0') j_skor_verifikator
                                FROM pak_3_kegiatan x 
                                WHERE x.id_unsur = ? AND x.versi = ?
                                ORDER BY x.nomor ";

        $query              = $this->db->query($sql, array($token['user_key'], $periode['id_periode'], $token['user_key'], $periode['id_periode'], $post['id_unsur'], $periode['versi']));
        $result['kegiatan'] = $query->result_array();
        $query->free_result();

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_pak_values()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;
        $result['kegiatan'] = $this->_kegiatan_detail($post['id_kegiatan']);
        $nm_tabel           = $result['kegiatan']['nm_tabel'];
        $filed_rincian      = $result['kegiatan']['field_rincian'];
        $nilai              = $result['kegiatan']['nilai'];
        $result['ketentuan']= $this->_ketentuan($post['id_kegiatan'], 'ketentuan');
        $result['berkas']   = $this->_ketentuan($post['id_kegiatan'], 'berkas');
        $result['penilaian']= $this->_ketentuan($post['id_kegiatan'], 'poin');
        $result['button']   = $this->_cek_button($periode);
        
        $sql    = "SELECT a.id_values, a.id_kegiatan, a.id_periode, a.skor, a.status, c.nilai, ROUND((c.nilai * a.skor), 3) AS nilai_skp $filed_rincian
                    FROM pak_values a
                        JOIN $nm_tabel b ON a.id_tabel = b.id
                        JOIN pak_3_kegiatan c ON a.id_kegiatan = c.id_kegiatan
                    WHERE a.user_key = ? AND a.id_periode = ? AND a.id_kegiatan = ? ";
        
        $query              = $this->db->query($sql, array($token['user_key'], $post['id_periode'], $post['id_kegiatan']));
        $result['sub_keg']  = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload '.$result['kegiatan']['id_subunsur'], 200);
    }

    public function _ketentuan($id_kegiatan = '', $jenis = '')
    {
        $sql    = "SELECT a.urutan, a.keterangan FROM pak_ketentuan a WHERE a.id_kegiatan = ? AND a.jenis = ? AND a.status = 'Tampil' ORDER BY a.urutan";

        $query  = $this->db->query($sql, array($id_kegiatan, $jenis));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function pak_kegiatan_import()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $kegiatan   = $this->_kegiatan_detail($post['id_kegiatan']);

        // menghapus data di tabel pak_values
        $this->db->trans_start();
        $where  = array('user_key' => $token['user_key'], 'id_kegiatan' => $post['id_kegiatan'], 'id_periode' => $post['id_periode']);
        $this->db->delete('pak_values', $where);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'pak_values', 'id_values', '', 'D', json_encode($where), 'Reload mengajar', 'log_sso');
            $response_1 = 'Hapus Nilai : Berhasil, ';
        } else {
            $response_1 = 'Hapus Nilai : Gagal, ';
        }

        // menghapus data di tabel pendukung
        $this->db->trans_start();
        $where  = array('user_key' => $token['user_key'], 'id_semester' => $post['id_semester']);
        $this->db->delete($kegiatan['nm_tabel'], $where);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], $kegiatan['nm_tabel'], 'id', '', 'D', json_encode($where), 'Reload mengajar', 'log_sso');
            $response_1 .= 'Hapus data pendukung : Berhasil, ';
        } else {
            $response_1 .= 'Hapus data pendukung : Gagal, ';
        }

        // insert data baru ke values
        foreach ($post['data'] as $value) {
            $response_2             = '';
            $value['id_semester']   = $post['id_semester'];
            $value['c']             = $token['username'];
            $value['dc']            = date('Y-m-d H:i:s');
            
            // jika nilai kegiatan 501 atau 502
            $nilaikeg = $kegiatan['nilai_kegiatan'];
            if ($nilaikeg == '501') {
                // CEK APAKAH SUDAH ADA KEGIATAN DALAM SUBUNSUR DALAM PER PERIODE
                $cek_event      = $this->_cek_ikut_event($post['id_kegiatan'], $post['id_periode']);
                if ($cek_event == '0') {
                    $nilai_kegiatan = 1;
                } else {
                    $nilai_kegiatan = 0;
                }

                $value['skor'] = $nilai_kegiatan;
                $value['kum']  = '0';

            } elseif ($nilaikeg == '502') {
                // CEK APAKAH SUDAH ADA KEGIATAN DALAM SUBUNSUR DALAM PER PERIODE
                $cek_event      = $this->_cek_ikut_event($post['id_kegiatan'], $post['id_periode']);
                if ($cek_event < '2') {
                    $nilai_kegiatan = 1;
                } else {
                    $nilai_kegiatan = 0;
                }

                $value['skor'] = $nilai_kegiatan;
                $value['kum']  = '0';
                
            }



            $this->db->trans_start();
            $this->db->insert($kegiatan['nm_tabel'], $value);
            $last_id = $this->db->insert_id();
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], $kegiatan['nm_tabel'], 'id', $last_id, 'C', json_encode($value), 'Reload mengajar', 'log_sso');
                $response_2 = 'Insert data pendukung : Berhasil, ';
                $send_response = true;

                // insert mengajar pada values PKD
                $this->db->trans_start();
                $values_pkd     = array('id_kegiatan'   => $post['id_kegiatan'],
                                        'id_periode'    => $post['id_periode'],
                                        'user_key'      => $token['user_key'],
                                        'skor'          => $value['skor'],
                                        'kum'           => $value['kum'],
                                        'id_tabel'      => $last_id,
                                        'c'             => $token['username'],
                                        'dc'            => date('Y-m-d H:i:s') );
                
                if ($kegiatan['otomatis_selesai'] == 'Ya') {
                    $values_pkd['status'] = 'Selesai';
                }

                $this->db->insert('pak_values', $values_pkd);
                $last_id = $this->db->insert_id();
                $this->db->trans_complete();

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    $this->log_app->log_data($token['user_key'], 'pak_values', 'id_values', $last_id, 'C', json_encode($values_pkd), 'Reload mengajar', 'log_sso');
                    $response_2 .= 'Insert Skor : Berhasil';
                    $send_response = true;
                    
                } else {
                    $response_2 .= 'Insert Skor : Gagal';
                    $send_response = false;
                }
            } else {
                $response_2 = 'Insert data pendukung : Gagal, ';
                $send_response = false;
            }
        }
        
        return $this->app->respons_data($send_response, $response_1 . ' ' . $response_2, 200);
    }

    public function get_pak_lihat()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;

        if (empty($post['id_values'])) {
            $result['value']    = array();
            $result['kegiatan'] = $this->_kegiatan_detail('22', $periode['versi']);
        } else {
            $result['value']    = $this->_get_value($post['id_values']);
            $result['kegiatan'] = $this->_kegiatan_detail($result['value']['id_kegiatan']);
        }
        $result['value']    = $this->_get_value($post['id_values']);
        $result['kegiatan'] = $this->_kegiatan_detail($result['value']['id_kegiatan']);
        $result['detail']   = $this->_get_detail_kegiatan($result['kegiatan']['nm_tabel'], $result['value']['id_tabel']);
        $result['pesan']    = $this->_get_values_pesan($post['id_values']);
        $result['button']   = $this->_cek_button($periode);

        return $this->app->respons_data($result, 'Data berhasil diload | '.$post['id_values'], 200);
    }

    public function _get_value($id_value = '')
    {
        $sql    = "SELECT * FROM pak_values a WHERE a.id_values = ? LIMIT 1";

        $query  = $this->db->query($sql, array($id_value));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _get_detail_kegiatan($tabel = '', $id_tabel = '')
    {
        $sql    = "SELECT *, SUBSTRING(a.link_berkas, -3, 3) file_type FROM $tabel a WHERE a.id = ?";

        $query  = $this->db->query($sql, array($id_tabel));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }

    public function _get_values_pesan($id_value = '')
    {
        $sql    = "SELECT id_pesan, kategori, DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, TIME(tanggal) jam, pesan, IF(kategori = 'Dosen', 'info', 'warning') style
                    FROM pak_values_pesan a WHERE a.id_values = ? ORDER BY tanggal ASC";

        $query  = $this->db->query($sql, array($id_value));
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }

    public function pak_value_destroy()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $value      = $this->_get_value($post['id_values']);
        $kegiatan   = $this->_kegiatan_detail($value['id_kegiatan']);

        $this->db->trans_start();
        $where  = array('user_key' => $token['user_key'], 'id_values' => $post['id_values']);
        $this->db->delete('pak_values', $where);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'pak_values', 'id_values', $post['id_values'], 'D', json_encode($value), 'Hapus master value', 'log_sso');

            // proses delete pada tabel kegiatan
            $this->db->trans_start();
            $where  = array('id' => $value['id_tabel']);
            $this->db->delete($kegiatan['nm_tabel'], $where);
            $this->db->trans_complete();
            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], $kegiatan['nm_tabel'], 'id', $value['id_tabel'], 'D', json_encode($kegiatan), 'Hapus master value kegiatan', 'log_sso');

                $pesan_kegiatan     = 'berhasil';
            } else {
                $pesan_kegiatan     = 'gagal';
            }

            $response   = array('status' => true, 'id_kegiatan' => $kegiatan['id_kegiatan']);
            $pesan      = 'Data utama berhasil dihapus dan data pendukung '.$pesan_kegiatan.' dihapus.';
        } else {
            $response   = array('status' => false, 'id_kegiatan' => $kegiatan['id_kegiatan']);
            $pesan      = 'Data utama gagal dihapus.';
        }

        return $this->app->respons_data($response, $pesan, 200);
    }

    public function pak_update_url()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $value      = $this->_get_value($post['id_values']);
        $kegiatan   = $this->_kegiatan_detail($value['id_kegiatan']);

        $this->db->trans_start();
        $where  = array('user_key'      => $token['user_key'], 'id' => $value['id_tabel']);
        $set    = array('link_berkas'   => $post['url'], 'u' => $token['user_key'], 'du' => date('Y-m-d H:i:s'));
        $this->db->update($kegiatan['nm_tabel'], $set, $where);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], $kegiatan['nm_tabel'], 'id', $value['id_tabel'], 'U', json_encode($where), 'Update URL value kegiatan', 'log_sso');
            return $this->app->respons_data(true, 'URL berhasil disimpan.', 200);
        } else {
            return $this->app->respons_data(false, 'URL gagal disimpan.', 201);
        }
    }

    public function pak_insert_value_pesan()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        // $value      = $this->_get_value($post['id_values']);
        // $kegiatan   = $this->_kegiatan_detail($value['id_kegiatan']);

        $this->db->trans_start();
        $values     = array('id_values'   => $post['id_values'],
                            'user_key'    => $token['user_key'],
                            'kategori'    => $post['kategori'],
                            'tanggal'     => date('Y-m-d H:i:s'),
                            'pesan'       => $post['pesan'] );

        $this->db->insert('pak_values_pesan', $values);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'pak_values_pesan', 'id_pesan', $last_id, 'C', json_encode($values), 'Insert Pesan Values', 'log_sso');
            return $this->app->respons_data(true, 'Pesan berhasil disimpan.', 200);
        } else {
            return $this->app->respons_data(false, 'Pesan gagal disimpan.', 201);
        }
    }

    public function pak_ajukan_pkd()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        $pesan      = '';
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();

        $sql        = "SELECT * FROM pak_profil WHERE user_key = ? AND id_periode = ? ";
        $query      = $this->db->query($sql, array($token['user_key'], $post['histori']['id_periode']));
        $profil_cek = $query->result_array();
        $query->free_result();

        if (empty($profil_cek)) {
            // proses insert profil
            $this->db->trans_start();
            $values_profil  = array('id_doskar'         => $post['profil']['id_doskar'],
                                    'id_periode'        => $post['histori']['id_periode'],
                                    'user_key'          => $token['user_key'],
                                    'nama_doskar'       => $post['profil']['nama_doskar'],
                                    'nidn'              => $post['profil']['nidn'],
                                    'nis'               => $post['profil']['nis'],
                                    'fakultas'          => $post['profil']['nama_unit1'],
                                    'program_studi'     => $post['profil']['program_studi'],
                                    'kode_khusus'       => $post['profil']['kode_khusus'],
                                    'kode_fakultas'     => substr($post['profil']['kode_khusus'], 0, 1),
                                    'status_dosen'      => $post['profil']['status_kerja'],
                                    'jabatan_akademik'  => $post['profil']['jafa'],
                                    'kum'               => $post['profil']['kum'],
                                    'tmt_jab_akademik'  => $post['profil']['tmt_jafa'],
                                    'status_keaktifan'  => $post['profil']['status_keaktifan'],
                                    'akses'             => 'Close' );

            $this->db->insert('pak_profil', $values_profil);
            $last_id = $this->db->insert_id();
            $this->db->trans_complete();

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], 'pak_profil', 'id_pak_dosen', $last_id, 'C', json_encode($values_profil), 'Profil PAK', 'log_sso');
                $pesan = "Profil berhasil disimpan dan ";
            } else {
                $pesan = "Profil gagal disimpan dan ";
            }
        }

        $this->db->trans_start();
        $values     = array('key_dosen'   => $token['user_key'],
                            'id_periode'  => $post['histori']['id_periode'],
                            'tanggal'     => date('Y-m-d H:i:s'),
                            'status'      => $post['histori']['kategori'],
                            'keterangan'  => $post['histori']['pesan'],
                            'c'           => $token['username'],
                            'dc'          => date('Y-m-d H:i:s') );

        $this->db->insert('pak_ajuan_histori', $values);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();

        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'pak_ajuan_histori', 'id_ajua', $last_id, 'C', json_encode($values), 'Insert histori ajuan', 'log_sso');
            return $this->app->respons_data(true, $pesan . 'Pesan berhasil disimpan.', 200);
        } else {
            return $this->app->respons_data(false, $pesan . 'Pesan gagal disimpan.', 201);
        }
    }

    public function pak_kegiatan_form()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        
        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;
        $result['kegiatan'] = $this->_kegiatan_detail($post['id_kegiatan']);
        $nm_tabel           = $result['kegiatan']['nm_tabel'];
        $result['jenis']    = $this->_kegiatan_jenis($post['id_kegiatan'], $periode);

        if (!empty($post['id'])) {
            $sql            = "SELECT * FROM $nm_tabel WHERE id = ? ";
            $query          = $this->db->query($sql, array($post['id']));
            $result['dt']   = $query->row_array();
            $query->free_result();
        }

        return $this->app->respons_data($result, 'Data berhasil diload.', 200);
    }

    public function _kegiatan_jenis($id_kegiatan, $periode)
    {
        $kegiatan       = $this->_kegiatan_detail($id_kegiatan);

        // if ($kegiatan['id_subunsur'] == '22' || $kegiatan['id_subunsur'] == '23') {
        //     $sql        = "SELECT a.id_event AS id_jenis, CONCAT(a.nama_event, ' (', a.lokasi_event, ')') AS nama_jenis
        //                     FROM kinerja_event a 
        //                     WHERE a.tanggal_mulai BETWEEN '$periode[pengisian_mulai]' AND '$periode[pengisian_selesai]' 
        //                         AND a.sub_unsur = ?
        //                     ORDER BY a.nama_event ";
        //     $query      = $this->db->query($sql, array($kegiatan['nama_subunsur']));
        // } else {
            $sql        = "SELECT id_jenis, nama_jenis FROM pak_3_kegiatan_jenis WHERE id_kegiatan = ? AND aktif = 'Ya' ORDER BY urutan";
            $query      = $this->db->query($sql, $id_kegiatan);
        // }
        
        $result         = $query->result_array();
        $query->free_result();
        
        return $result;
    }

    public function pak_kegiatan_insert()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        
        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;
        $result['kegiatan'] = $this->_kegiatan_detail($post['id_kegiatan']);
        $id_periode         = $post['id_periode'];
        $id_kegiatan        = $post['id_kegiatan'];
        $nm_tabel           = $result['kegiatan']['nm_tabel'];
        $nilaikeg           = $result['kegiatan']['nilai_kegiatan'];
        $nm_jenis           = '';

        // CEK APAKAH KEGIATAN TERMASUK CORE VALUES HARMONI
        // if ($result['kegiatan']['id_subunsur'] == '22') {
        //     // AMBIL DATA EVENT
        //     $event                  = $this->_get_event($post['id_non_jenis']);
        //     $nm_jenis               = $event['lokasi_event'];
        //     $post['nama_kegiatan']  = $event['nama_event'];
        //     $post['tempat']         = $event['lokasi_event'];
        //     $post['tanggal_mulai']  = $event['tanggal_mulai'];
        //     $post['tanggal_selesai']= $event['tanggal_selesai'];
        //     $post['jam_mulai']      = $event['jam_mulai'];
        //     $post['jam_selesai']    = $event['jam_selesai'];
        //     $post['jam_finger']     = $event['jam_finger'];
        //     $post['id_subunsur']    = $result['kegiatan']['id_subunsur'];
        // }

        // MENENTUKAN NILAI KEGIATAN, JIKA NILAI_KEGIATAN 502 MAKA MAKSIMAL NILAI 2
        // SEHINGGA PERLU DILAKUKAN PENGECEKAN, KALAU SUDAH ADA SUBUNSUR DALAM VALUES, MAKA NILAI 0, JIKA BELUM MAKA NILAI 1 MAKSIMAL 2
        if ($nilaikeg == '501') {
            // CEK APAKAH SUDAH ADA KEGIATAN DALAM SUBUNSUR DALAM PER PERIODE
            $cek_event      = $this->_cek_ikut_event($id_kegiatan, $id_periode);
            if ($cek_event == '0') {
                $nilai_kegiatan = 1;
            } else {
                $nilai_kegiatan = 0;
            }
        } elseif ($nilaikeg == '502') {
            // CEK APAKAH SUDAH ADA KEGIATAN DALAM SUBUNSUR DALAM PER PERIODE
            $cek_event      = $this->_cek_ikut_event($id_kegiatan, $id_periode);
            if ($cek_event < '2') {
                $nilai_kegiatan = 1;
            } else {
                $nilai_kegiatan = 0;
            }
        } else {
            $nilai_kegiatan = $nilaikeg;
        }
        
        if (!empty($post['id_jenis'])) {
            $sql            = "SELECT kum, nama_jenis FROM pak_3_kegiatan_jenis WHERE id_jenis = ? ";
            $query          = $this->db->query($sql, $post['id_jenis']);
            $result         = $query->row_array();
            $query->free_result();
            if(!empty($result)) { 
                $kum        = $result['kum'];
                $nm_jenis   = $result['nama_jenis'];
            } else { 
                $kum        = '0';
                $nm_jenis   = $nm_jenis;
            }
        } else {
            $kum        = '0';
            $nm_jenis   = $nm_jenis;
        }

        
        // return $this->app->respons_data($cek_event, $nilai_kegiatan . " | " .$nilaikeg ." | " . $nm_tabel ." | " . $id_kegiatan ." | " . $id_periode, 200);

        unset($post['id_kegiatan']);

        if (!empty($post['id_non_jenis'])) {
            $non_jenis          = $post['id_non_jenis'];
            unset($post['id_non_jenis']);
            $post['id_jenis']   = $non_jenis;
        }

        $post['user_key']   = $token['user_key'];
        $post['nama_jenis'] = $nm_jenis;
        $post['c']          = $token['username'];
        $post['dc']         = date('Y-m-d H:i:s');
        
        $this->db->trans_start();
        $this->db->insert($nm_tabel, $post);
        $last_id = $this->db->insert_id();
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], $nm_tabel, 'id', $last_id, 'C', json_encode($post), 'Aktifitas Kegiatan', 'log_sso');
            $response_1     = 'Insert data pendukung : Berhasil';
            $send_response  = true;

            // insert kegiatan pada values PKD
            $this->db->trans_start();
            $values = array('id_kegiatan'   => $id_kegiatan,
                            'id_periode'    => $id_periode,
                            'user_key'      => $token['user_key'],
                            'skor'          => $nilai_kegiatan,
                            'kum'           => $kum,
                            'id_tabel'      => $last_id,
                            'c'             => $token['username'],
                            'dc'            => date('Y-m-d H:i:s') );

            $this->db->insert('pak_values', $values);
            $last_id = $this->db->insert_id();
            $this->db->trans_complete();

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], 'pak_values', 'id_values', $last_id, 'C', json_encode($values), 'Values Kegiatan', 'log_sso');
                $response_1 .= ', Insert Skor : Berhasil';
                $send_response = true;
                
            } else {
                $response_1 .= ', Insert Skor : Gagal';
                $send_response = false;
            }
        } else {
            $response_1 = 'Insert data pendukung : Gagal';
            $send_response = false;
        }

        return $this->app->respons_data($send_response, $response_1, 200);
    }

    public function pak_kegiatan_update()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        
        $post               = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode            = $this->_periode($post['id_periode']);
        $result['periode']  = $periode;
        $result['kegiatan'] = $this->_kegiatan_detail($post['id_kegiatan']);
        $id_periode         = $post['id_periode'];
        $id_values          = $post['id_values'];
        $id                 = $post['id'];
        $nm_tabel           = $result['kegiatan']['nm_tabel'];
        $nilai_kegiatan     = $result['kegiatan']['nilai_kegiatan'];

        if (!empty($post['id_jenis'])) {
            $sql            = "SELECT kum, nama_jenis FROM pak_3_kegiatan_jenis WHERE id_jenis = ? ";
            $query          = $this->db->query($sql, $post['id_jenis']);
            $result         = $query->row_array();
            $query->free_result();
            if(!empty($result)) { 
                $kum        = $result['kum'];
                $nm_jenis   = $result['nama_jenis'];
            } else { 
                $kum        = '0';
                $nm_jenis   = '';
            }
        } else {
            $kum        = '0';
            $nm_jenis   = '';
        }
        unset($post['id_kegiatan']);
        unset($post['id_values']);
        unset($post['id']);


        $post['user_key']   = $token['user_key'];
        $post['nama_jenis'] = $nm_jenis;
        $post['u']          = $token['username'];
        $post['du']         = date('Y-m-d H:i:s');
        
        $this->db->trans_start();
        $this->db->update($nm_tabel, $post, array('id' => $id));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === true) {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], $nm_tabel, 'id', $id, 'U', json_encode($post), 'Aktifitas Kegiatan', 'log_sso');
            $response_1     = 'Update data pendukung : Berhasil';
            $send_response  = true;

            // Update kegiatan pada values PKD
            $this->db->trans_start();
            $set    = array('skor'          => $nilai_kegiatan,
                            'kum'           => $kum,
                            'u'             => $token['username'],
                            'du'            => date('Y-m-d H:i:s') );
            
            $where  = array('user_key'      => $token['user_key'],
                            'id_values'     => $id_values );

            $this->db->update('pak_values', $set, $where);
            $this->db->trans_complete();

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], 'pak_values', 'id_values', $id_values, 'U', json_encode($set), 'Values Kegiatan', 'log_sso');
                $response_1 .= ', Update Skor : Berhasil';
                $send_response = true;
                
            } else {
                $response_1 .= ', Update Skor : Gagal';
                $send_response = false;
            }
        } else {
            $response_1 = 'Update data pendukung : Gagal';
            $send_response = false;
        }

        return $this->app->respons_data($send_response, $response_1, 200);
    }
    
    public function _get_event($id_event)
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $sql    = "SELECT a.id_event, a.nama_event, a.tanggal_mulai, a.tanggal_selesai, a.jam_mulai, a.jam_selesai, a.lokasi_event, b.user_key, b.jam_finger
                    FROM kinerja_event a
                        LEFT JOIN kinerja_event_peserta b ON a.id_event = b.id_event AND b.user_key = ?
                    WHERE a.id_event = ? ";

        $query  = $this->db->query($sql, array($token['user_key'], $id_event));
        $result = $query->row_array();
        $query->free_result();
        return $result;
    }
    
    public function _cek_ikut_event($id_kegiatan, $id_periode)
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }

        $sql    = "SELECT IFNULL(COUNT(*), 0) j_data
                    FROM pak_values a
                    WHERE a.user_key = ? AND a.id_periode = ? AND a.id_kegiatan = ? ";

        $query  = $this->db->query($sql, array($token['user_key'], $id_periode, $id_kegiatan));
        $result = $query->row_array();
        $query->free_result();

        return $result['j_data'];
    }

    public function pak_corevalues_integritas()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode    = $this->_periode($post['id_periode']);
        $mulai      = $periode['pengisian_mulai'];
        $selesai    = $periode['pengisian_selesai'];
        $semester   = $post['id_semester'];
        $rincian    = "";
        $nama       = "Finger Print";

        // ========================== MENGHITUNG PRESENSI KEHADIRAN KERJA ===================
        // mencari banyak hari
        $sql    = "SELECT COUNT(*) j_hari FROM
                    (
                        WITH RECURSIVE date_series AS (
                            SELECT '$mulai' AS date
                            UNION ALL
                            SELECT DATE_ADD(date, INTERVAL 1 DAY)
                            FROM date_series
                            WHERE date < '$selesai'
                        )
                        SELECT date AS tanggal, DATE_FORMAT(date, '%w') hari
                        FROM date_series
                    ) a
                    WHERE a.hari BETWEEN '1' AND '5'";

        $query  = $this->db->query($sql);
        $result = $query->row_array();
        $query->free_result();
        $j_hari = $result['j_hari'];

        // mencari banyak presensi
        $sql    = "SELECT COUNT(*) j_masuk
                    FROM finger_transport a
                    WHERE a.user_key = ?
                        AND a.tanggal BETWEEN '$mulai' AND '$selesai'
                        AND DATE_FORMAT(a.tanggal, '%w') BETWEEN '1' AND '5'
                        AND a.status_transport = '1'";

        $query  = $this->db->query($sql, $token['user_key']);
        $result = $query->row_array();
        $query->free_result();
        $j_presensi = $result['j_masuk'];

        // hitung prosentase presensi
        if (!empty($j_presensi) || $j_presensi > 0) {
            $presensi = round(($j_presensi / $j_hari) * 100, 0);
        } else {
            $presensi = 0;
        }

        if(!empty($presensi)) {
            $rincian  = "banyak finger ($j_presensi) / banyak hari ($j_hari) = ".$presensi." %";
        }

        /*
        

        // ================== CEK EVENT INTEGRITAS ==========================
        $sql    = "SELECT a.nama_event, b.jam_finger, a.point
                    FROM kinerja_event a
                        JOIN kinerja_event_peserta b ON a.id_event = b.id_event AND b.user_key = ?
                    WHERE a.sub_unsur = 'Integritas' AND a.tanggal_mulai BETWEEN '$mulai' AND '$selesai' ";

        $query  = $this->db->query($sql, array($token['user_key']));
        $result = $query->result_array();
        $query->free_result();

        $nilai_tambah = 0;
        if(!empty($result)) {
            $nama   .= " dan Event Universitas atau Fakultas";
            foreach ($result as $value) {
                $rincian        .= "<br>Event ".$value['nama_event']." (".$value['jam_finger'].") point : ".$value['point']."";
                $nilai_tambah   += $value['point'];
            }
        }
        
        // ============================= AKUMULASI POINT ===========================
        $akumulasi = $presensi + $nilai_tambah;

        if ($akumulasi >= 75) {
            $skor = 1;
        } else {
            $skor = 0;
        }
        
        $nama       .= " : ".$akumulasi." %";
        $response[] = array('id_semester'   => $semester, 
                            'id_periode'    => $post['id_periode'],
                            'user_key'      => $token['user_key'],
                            'nama'          => $nama,
                            'rincian'       => $rincian,
                            'skor'          => $skor,
                            'kum'           => '0',
                            'link_berkas'   => '-' );

        */

        if ($presensi >= 75) {
            $skor = 1;
        } else {
            $skor = 0;
        }

        $response[] = array('id_semester'   => $semester, 
                            'id_periode'    => $post['id_periode'],
                            'user_key'      => $token['user_key'],
                            'nama'          => 'Finger Print sesuai jam kerja selama 1 periode penilaian',
                            'rincian'       => $rincian,
                            'skor'          => $skor,
                            'kum'           => '0',
                            'link_berkas'   => '-' );

        return $this->app->respons_data($response, 'Data berhasil diload.', 200);
    }

    public function pak_tugas_tambahan()
    {
        $token              = $this->tkn;
        if ($token['username'] == 'a_rifai') {
            $token['user_key'] = $this->sample;
        }
        
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        // $periode    = $this->_periode($post['id_periode']);
        $id_periode = $post['id_periode'];
        $semester   = $post['id_semester'];
        
        $sql    = "SELECT '$semester' AS id_semester, '$id_periode' AS id_periode, a.user_key, b.nama_doskar, IF(a.kategori = 'Struktural', 'Jabatan Struktural', a.kategori) AS kategori
                        , a.jabatan, IF(a.penempatan IS NULL, '', a.penempatan) AS penempatan, a.kum, a.skor, '-' AS link_berkas
                    FROM sdm_pejabat a 
                        JOIN doskar_usm b ON a.id_doskar = b.id_doskar
                    WHERE a.user_key = ? 
                    ORDER BY a.skor DESC LIMIT 1 ";

        $query  = $this->db->query($sql, $token['user_key']);
        $result = $query->result_array();
        $query->free_result();
        
        return $this->app->respons_data($result, 'Data berhasil diload.', 200);
    }

}