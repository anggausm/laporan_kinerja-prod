<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_rekap extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_pertanyaan()
    {
        $token = $this->tkn;
        $result['disabled'] = '0';
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        if (empty($cek_periode)) {
            $result['disabled'] = '1';
        }
        // if ($token['username'] == 'angga') {
        //     $cek_periode['pengisian_mulai'] = '2024-06-05';
        // }
        $now = date('Y-m-d');
        if ($now < $cek_periode['pengisian_mulai']) {
            $result['disabled'] = '1';
        }
        if ($now > $cek_periode['pengisian_selesai']) {
            $result['disabled'] = '1';
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $token['user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if ($check_ajuan['status_ajuan'] == '1') {
            $result['disabled'] = '1';
        }
        $result['periode'] = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();;
        $result['kinerja_ajuan'] = $check_ajuan;

        $sql = "SELECT a.*,b.id_nilai,b.dinilai_user_key,b.penilai_user_key,b.jumlah,b.nilai,b.link_dokumen
        FROM kinerja_pernyataan a
        LEFT JOIN (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$token[user_key]') b ON a.id_pertanyaan = b.id_pertanyaan
        WHERE a.jenis = 'Tenaga Pendidik' AND a.kategori = 'Dosen'
        ORDER BY a.urutan";
        $query  = $this->db->query($sql, array());
        $pertanyaan = $query->result_array();
        $result['pertanyaan'] = $pertanyaan;
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode()
    {
        $token = $this->tkn;
        $sql = "SELECT * FROM kinerja_periode a WHERE a.status = ? ";
        $query  = $this->db->query($sql, array('1'));
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function save()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        if (empty($data['kinerja_nilai'])) {
            return $this->app->respons_data(false, 'Data kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        // if ($token['username'] == 'angga') {
        //     $cek_periode['pengisian_mulai'] = '2024-06-05';
        // }
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['pengisian_mulai']) {
            return $this->app->respons_data(false, 'Pengisian Belum dimulai', 200);
        }
        if ($now > $cek_periode['pengisian_selesai']) {
            return $this->app->respons_data(false, 'Pengisian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $token['user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if ($check_ajuan['status_ajuan'] == '1') {
            return $this->app->respons_data(false, 'Form telah diajukan. tidak dapat mengisi lagi', 200);
        }

        $sql = "SELECT a.id_pertanyaan,a.kategori,a.jenis,b.dinilai_user_key
        FROM (SELECT * FROM kinerja_nilai WHERE dinilai_user_key = '$token[user_key]') b
        JOIN kinerja_pernyataan a ON a.id_pertanyaan = b.id_pertanyaan
        WHERE a.jenis = 'Tenaga Kependidikan'";
        $query  = $this->db->query($sql, array());
        $cek_form_penilaian = $query->row_array();
        // JIKA TELAH MENGISI FORM TENDIK TIDAK BOLEH ISI FORM DOSEN
        if ($cek_form_penilaian['jenis'] == 'Tenaga Kependidikan') {
            return $this->app->respons_data(false, 'Maaf anda telah mengisi Form Tenaga Kependidikan. tidak dapat mengisi Form Penilaian Dosen', 200);
        }
        // cek data sudah input atau belum
        $cek_nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $token['user_key']))->row_array();
        $this->db->trans_start();
        if (empty($cek_nilai)) {
            $data_save = array();
            foreach ($data['kinerja_nilai'] as $key => $value) {
                $arr['dinilai_user_key'] = $token['user_key'];
                $arr['id_pertanyaan'] = $value['id_pertanyaan'];
                $arr['jumlah'] = $value['jumlah'];
                $arr['nilai'] = $value['jumlah'] * $value['skor'];
                $arr['link_dokumen'] = trim($value['link_dokumen']);
                $arr['c'] = $token['username'];
                $arr['dc'] = date('Y-m-d H:i:s');
                $data_save[] = $arr;
            }
            $this->db->insert_batch('kinerja_nilai', $data_save);
            $act = 'C';
        } else {
            $data_save = array();
            foreach ($data['kinerja_nilai'] as $key => $value) {
                $nilai = $this->db->get_where('kinerja_nilai', array('dinilai_user_key' => $token['user_key'], 'id_pertanyaan' => $value['id_pertanyaan']))->row_array();
                $arr['jumlah'] = $value['jumlah'];
                $arr['nilai'] = $value['jumlah'] * $value['skor'];
                $arr['link_dokumen'] = trim($value['link_dokumen']);
                $arr['id_nilai'] = $nilai['id_nilai'];
                $arr['u'] = $token['username'];
                $arr['du'] = date('Y-m-d H:i:s');
                $data_save[] = $arr;
            }
            $this->db->update_batch('kinerja_nilai', $data_save, 'id_nilai');
            $act = 'U';
        }
        if (empty($check_ajuan)) {
            $data_ajuan = array(
                'user_key' => $token['user_key'],
                'id_periode' => $cek_periode['id_periode'],
                'status_ajuan' => '0',
                'c' => $token['username'],
                'dc' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('kinerja_ajuan', $data_ajuan);
            $id_ajuan = $this->db->insert_id();
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_nilai', '', '', $act, json_encode($data_save), 'dosen mengisi', 'log_kinerja');
            if (!empty($data_ajuan)) {
                $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $id_ajuan, 'C', json_encode($data_ajuan), 'dosen mengisi', 'log_kinerja');
            }
            $message = $act == 'C' ? 'Data berhasil disimpan' : 'Data berhasil diupdate';
            return $this->app->respons_data(true, $message, 200);
        }
    }

    public function ajukan()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token = $this->tkn;
        if (in_array($token['username'], array('angga'))) {
            $token['user_key'] = $data['user_key'];
        }
        if (empty($token['user_key'])) {
            return $this->app->respons_data(false, 'User kosong, gagal disimpan', 200);
        }
        $cek_periode = $this->db->get_where('kinerja_periode', array('status' => '1'))->row_array();
        // if ($token['username'] == 'angga') {
        //     $cek_periode['pengisian_mulai'] = '2024-06-05';
        // }
        if (empty($cek_periode)) {
            return $this->app->respons_data(false, 'Periode belum ada', 200);
        }
        $now = date('Y-m-d');
        if ($now < $cek_periode['pengisian_mulai']) {
            return $this->app->respons_data(false, 'Pengisian Belum dimulai', 200);
        }
        if ($now > $cek_periode['pengisian_selesai']) {
            return $this->app->respons_data(false, 'Pengisian Telah selesai', 200);
        }
        $check_ajuan = $this->db->get_where('kinerja_ajuan', array('user_key' => $token['user_key'], 'id_periode' => $cek_periode['id_periode']))->row_array();
        if (empty($check_ajuan)) {
            return $this->app->respons_data(false, 'Silahkan isi Form terlebih dahulu', 200);
        }
        if ($check_ajuan['status_ajuan'] == '1') {
            return $this->app->respons_data(false, 'Form telah diajukan. tidak dapat mengajukan lagi', 200);
        }
        $this->db->select('a.*,b.pertanyaan');
        $this->db->from('kinerja_nilai a');
        $this->db->join('kinerja_pernyataan b', 'a.id_pertanyaan = b.id_pertanyaan');
        $this->db->where('a.dinilai_user_key', $token['user_key']);
        $kinerja_nilai = $this->db->get()->result_array();
        foreach ($kinerja_nilai as $key => $value) {
            if ($value['id_pertanyaan'] != '248') {
                if (!empty($value['nilai']) && $value['nilai'] > 0) {
                    if (strpos($value['link_dokumen'], 'apps.usm.ac.id') !== false) {
                        return $this->app->respons_data(false, "Data tidak dapat diajukan, Link dokumen tidak boleh diisi apps.usm.ac.id", 200);
                    }
                    if (empty($value['link_dokumen'])) {
                        if (in_array($token['username'], array('angga'))) {
                            return $this->app->respons_data(false, "Data tidak dapat diajukan, dimohon untuk mengisi Link Dokumen untuk setiap form yang memilih jumlah lebih dari 0", 200);
                        } else {
                            return $this->app->respons_data(false, "Data tidak dapat diajukan, dimohon untuk mengisi Link Dokumen untuk setiap form yang memilih jumlah lebih dari 0", 200);
                        }
                    }
                    // if (!$this->_hasHttpOrHttps($value['link_dokumen'])) {
                    //     return $this->app->respons_data(false, "Data tidak dapat diajukan, dimohon untuk mengisi Link Dokumen dengan https:// atau http:// pada $value[link_dokumen]", 200);
                    // }
                }
            }
        }
        if (in_array($token['username'], array('angga'))) {
            return $this->app->respons_data(false, 'angga', 200);
        }
        $this->db->trans_start();
        $data_ajuan = array(
            'status_ajuan' => '1',
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'u' => $token['username'],
            'du' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id_ajuan', $check_ajuan['id_ajuan']);
        $this->db->update('kinerja_ajuan', $data_ajuan);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(true, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_ajuan', 'id_ajuan', $check_ajuan['id_ajuan'], 'U', json_encode($data_ajuan), 'dosen mengajukan', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }
    function _hasHttpOrHttps($string)
    {
        // Use a regular expression to check if the string starts with http or https
        return preg_match('/^(http|https):\/\//', $string);
    }
}
