<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_skp extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_periode()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_periode where kategori = 'Kependidikan' order by penilaian_mulai DESC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];
        $tanggal = $data['periode'];

        $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.id_indikator_utama,
                        kinerja_skp.id_sub_indikator,
                        kinerja_skp.id_ind_penilaian,
                        kinerja_skp.rincian_indikator,
                        kinerja_skp.periode,
                        kinerja_skp.jenis_pegawai,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_periode.nama_periode,
                        kinerja_jenis_pegawai.nama_jenis_pegawai
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        order by kinerja_skp.jenis_pegawai ";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function cek_insert($id = '')
    {
        $sql = "SELECT id_skp, user_key FROM kinerja_skp WHERE tanggal_skp = '$id'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "id_indikator_utama" => $data['id_indikator_utama'],
            "id_sub_indikator" => $data['id_sub_indikator'],
            "id_ind_penilaian" => $data['id_ind_penilaian'],
            "rincian_indikator" => $data['rincian_indikator'],
             "urutan" => $data['urutan'],
            "no_urut" => $data['no_urut'],
            "skor" => $data['skor'],
            "bobot" => $data['bobot'],
            "jenis_pegawai" => $data['jenis_pegawai'],
            "periode" => $data['periode'],
            "ket" => $data['ket'],
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_skp', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_skp', 'id_indikator_utama', $id, 'C', json_encode($data_insert), 'Insert Data SKP', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            }
        
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_skp
                WHERE id_skp = ?";
        $query = $this->db->query($sql, $data['id']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "id_indikator_utama" => $data['id_indikator_utama'],
            "id_sub_indikator" => $data['id_sub_indikator'],
            "id_ind_penilaian" => $data['id_ind_penilaian'],
            "rincian_indikator" => $data['rincian_indikator'],
            "urutan" => $data['urutan'],
            "no_urut" => $data['no_urut'],
            "skor" => $data['skor'],
            "bobot" => $data['bobot'],
            "jenis_pegawai" => $data['jenis_pegawai'],
            "periode" => $data['periode'],
            "ket" => $data['ket'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_skp', $data_update, array('id_skp' => $data['id_skp']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_skp', 'id_skp', $data['id_skp'], 'U', json_encode($data_update), 'Update Data SKP', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal update', 200);
            }
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_skp WHERE id_skp = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_skp', array('id_skp' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_skp', 'id_skp', $data['id'], 'D', json_encode($data_test), 'Delete Data SKP', 'log_sso');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        }
    }

    public function get_ind_utama()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_indikator_utama";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function get_sub_ind()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_sub_indikator";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function get_ind_nilai()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_indikator_penilaian";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function get_jenis_peg()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_jenis_pegawai";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function get_skp_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $jenisp = $data->jenis_peg;
        $periode = $data->periode;

        $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.id_indikator_utama,
                        kinerja_skp.id_sub_indikator,
                        kinerja_skp.id_ind_penilaian,
                        kinerja_skp.rincian_indikator,
                        kinerja_skp.periode,
                        kinerja_skp.jenis_pegawai,
                        kinerja_skp.urutan,
                        kinerja_skp.no_urut,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_periode.nama_periode,
                        kinerja_jenis_pegawai.nama_jenis_pegawai
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        WHERE kinerja_skp.jenis_pegawai = '$jenisp' AND kinerja_skp.periode = '$periode'
                        ORDER BY kinerja_skp.no_urut ";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }
}