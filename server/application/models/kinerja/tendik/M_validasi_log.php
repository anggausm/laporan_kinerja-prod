<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_validasi_log extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_list()
    {
        $token  = $this->tkn;
        $data = json_decode(file_get_contents("php://input"), true);
        $sql    = "SELECT a.penilai_user_key, a.dinilai_user_key, a.dinilai_nama, a.dinilai_fak_unit, IF (b.validasi_atasan = '1', 'Sudah Divalidasi', 'Belum Divalidasi') status
                        FROM kinerja_asesor a
                            LEFT JOIN
                            (
                                SELECT periode_penilaian, user_key, validasi_atasan, total_nilai
                                FROM kinerja_skp_pegawai
                                GROUP BY user_key, periode_penilaian
                            ) b ON a.id_periode = b.periode_penilaian AND a.dinilai_user_key = b.user_key
                        WHERE a.penilai_user_key = '$token[user_key]' AND a.kategori='pimpinan' 
                        AND a.id_periode = '$data[periode]'
                        ORDER BY a.dinilai_nama";

        $query  = $this->db->query($sql, array($token['user_key']));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode_aktif()
    {
        $token      = $this->tkn;
        $sql    = "SELECT * FROM kinerja_periode
                        WHERE (CURDATE() BETWEEN pengisian_mulai AND pengisian_selesai) 
                        AND kategori = 'Kependidikan'";

        $query  = $this->db->query($sql);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode_log($id_periode = '')
    {
        $data = json_decode(file_get_contents("php://input"));
        // $sql = "WITH RECURSIVE minggu(tanggal_mulai, tanggal_akhir, nomor_minggu) AS (
        //           SELECT '2024-06-01' AS tanggal_mulai, '2024-06-07' AS tanggal_akhir, 1 AS nomor_minggu
        //           UNION ALL
        //           SELECT tanggal_akhir + INTERVAL 1 DAY, tanggal_akhir + INTERVAL 7 DAY, nomor_minggu + 1
        //           FROM minggu
        //           WHERE tanggal_akhir + INTERVAL 7 DAY <= '2024-11-30'
        //         )
        //         SELECT * FROM minggu";

        // $sql = "SELECT * FROM kinerja_period_log where periode_kinerja  = '$data->id_periode' order by tgl_awal ASC";
         $sql = "SELECT  kinerja_period_log.id,
                        kinerja_period_log.nama_log,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir,
                        kinerja_period_log.periode_kinerja,
                        kinerja_periode.pengisian_mulai,
                        kinerja_periode.pengisian_selesai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_period_log
                        LEFT JOIN kinerja_periode ON kinerja_period_log.periode_kinerja = kinerja_periode.id_periode
                        WHERE (CURDATE() BETWEEN kinerja_periode.pengisian_mulai AND kinerja_periode.pengisian_selesai)
                        AND kinerja_period_log.status = '1'
                        ORDER BY kinerja_period_log.tgl_awal ASC  ";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function edit_periode_log() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_period_log
                WHERE id = ?";
        $query = $this->db->query($sql, $data['id_p_log']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }
    
    public function data_log_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"));

         $sql = "SELECT  kinerja_log_book.id_log_book,
                        kinerja_log_book.user_key,
                        kinerja_log_book.tanggal_log_book,
                        kinerja_log_book.tugas,
                        kinerja_log_book.status_pekerjaan,
                        kinerja_log_book.ket,
                        kinerja_period_log.id,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir,
                        kinerja_periode.pengisian_mulai,
                        kinerja_periode.pengisian_selesai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_log_book
                        JOIN kinerja_period_log ON kinerja_log_book.id_periode_log = kinerja_period_log.id
                        JOIN kinerja_periode ON kinerja_period_log.periode_kinerja = kinerja_periode.id_periode
                        WHERE user_key = '$data->userkey' and kinerja_period_log.id = '$data->id_periode'
                        ORDER BY
                        tanggal_log_book ASC"; 
                        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return $this->app->respons_data(false, 'Data gagal diload', 200);
        }
    }

    public function data_log_valid()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  kinerja_hit_log.id,
                        kinerja_hit_log.userkey,
                        kinerja_hit_log.periode_pen,
                        kinerja_hit_log.periode_log,
                        kinerja_hit_log.poin1,
                        kinerja_hit_log.poin2,
                        kinerja_hit_log.poin3,
                        kinerja_hit_log.poin4,
                        kinerja_hit_log.nilai,
                        kinerja_hit_log.`status`,
                        kinerja_period_log.nama_log,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir
                        FROM
                        kinerja_hit_log
                        LEFT JOIN kinerja_period_log ON kinerja_hit_log.periode_pen = kinerja_period_log.periode_kinerja 
                        AND kinerja_hit_log.periode_log = kinerja_period_log.id
                        where kinerja_hit_log.userkey = '$data->userkey' and kinerja_hit_log.status = 1 
                        and kinerja_hit_log.periode_pen = '$data->periode_pen'"; 
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return $this->app->respons_data(false, 'Data gagal diload', 200);
        }
    }

    public function cek_insert($p_pen='', $p_log= '', $id = '')
    {
        $sql = "SELECT periode_pen, periode_log, userkey 
                        FROM kinerja_hit_log 
                        WHERE periode_pen = '$p_pen' 
                        AND periode_log = '$p_log'
                        AND userkey = '$id'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "userkey" => $data['id_peg'],
            "periode_pen" => $data['p_pen'],
            "periode_log" => $data['p_log'],
            "poin1" => $data['indi1'],
            "poin2" => $data['indi2'],
            "poin3" => $data['indi3'],
            "poin4" => $data['indi4'],
            "poin5" => $data['indi5'],
            "poin6" => $data['indi6'],
            "nilai" => $data['nilai'],
            "status" => 1,
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );


        if ($this->cek_insert($data['p_pen'], $data['p_log'], $data['id_peg']) == false) {
            return $this->app->respons_data(false, 'Sudah pernah divalidasi', 200);
        } else {
            if ($this->db->insert('kinerja_hit_log', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_hit_log', 'id', $id, 'C', json_encode($data_insert), 'Validasi Log Book', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            }
        }
        
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_hit_log WHERE id = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(false, 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_hit_log', array('id' => $data['id']))) {
            $this->log_app->log_data($token['username'], 'kinerja_hit_log', 'id', $data['id'], 'D', json_encode($data_test), 'Delete Validasi Log', 'log_sia');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        }
    }
}