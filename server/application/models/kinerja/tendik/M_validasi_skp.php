<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_validasi_skp extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

     public function get_data()
    {
        $token  = $this->tkn;
        $sql    = "SELECT a.penilai_user_key, a.dinilai_user_key, a.dinilai_nama, a.dinilai_fak_unit, IF (b.dinilai_user_key IS NOT NULL OR b.dinilai_user_key = '', 'Sudah Divalidasi', 'Belum Divalidasi') status
                        FROM kinerja_asesor a
                            LEFT JOIN
                            (
                                SELECT * 
                                FROM kinerja_nilai a 
                                GROUP BY a.dinilai_user_key, a.penilai_user_key
                            ) b ON a.penilai_user_key = b.penilai_user_key AND a.dinilai_user_key = b.dinilai_user_key
                        WHERE a.penilai_user_key = ? AND a.kategori='pimpinan'
                        ORDER BY a.dinilai_nama ";

        $query  = $this->db->query($sql, array($token['user_key']));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode_aktif()
    {
        $token      = $this->tkn;
        $sql    = "SELECT * FROM kinerja_periode a WHERE a.kategori = 'Kependidikan' AND a.status = 1 ";

        $query  = $this->db->query($sql, array('1'));
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_periode_log($id_periode = '')
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT * FROM kinerja_period_log where periode_kinerja  = '$data->id_periode' order by tgl_awal ASC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function edit_periode_log() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_period_log
                WHERE id = ?";
        $query = $this->db->query($sql, $data['id_p_log']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }
    
    public function data_log_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  kinerja_log_book.id_log_book,
                        kinerja_log_book.user_key,
                        kinerja_log_book.tanggal_log_book,
                        kinerja_log_book.tugas,
                        kinerja_log_book.status_pekerjaan,
                        kinerja_log_book.ket,
                        kinerja_log_book.cd,
                        kinerja_log_book.c
                        FROM
                            kinerja_log_book
                        WHERE
                         (kinerja_log_book.tanggal_log_book BETWEEN '$data->awal' AND '$data->akhir')
                         AND kinerja_log_book.user_key = '$data->userkey'
                        ORDER BY
                            kinerja_log_book.tanggal_log_book ASC"; 
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function data_log_valid()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  kinerja_hit_log.id,
                        kinerja_hit_log.userkey,
                        kinerja_hit_log.periode_pen,
                        kinerja_hit_log.periode_log,
                        kinerja_hit_log.poin1,
                        kinerja_hit_log.poin2,
                        kinerja_hit_log.poin3,
                        kinerja_hit_log.poin4,
                        kinerja_hit_log.nilai,
                        kinerja_hit_log.`status`,
                        kinerja_period_log.nama_log,
                        kinerja_period_log.tgl_awal,
                        kinerja_period_log.tgl_akhir
                        FROM
                        kinerja_hit_log
                        LEFT JOIN kinerja_period_log ON kinerja_hit_log.periode_pen = kinerja_period_log.periode_kinerja 
                        AND kinerja_hit_log.periode_log = kinerja_period_log.id
                        where kinerja_hit_log.userkey = '$data->userkey' and kinerja_hit_log.status = 1"; 
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function cek_insert($p_pen='', $p_log= '', $id = '')
    {
        $sql = "SELECT periode_pen, periode_log, userkey 
                        FROM kinerja_hit_log 
                        WHERE periode_pen = '$p_pen' 
                        AND periode_log = '$p_log'
                        AND userkey = '$id'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
          $data_insert = array(
            "user_key" => $data['user_key'],
            "id_periode" => $data['id_periode'],
            "skor" => $data['skor'],
            "nilai" => $data['nilai'],
            "kategori" => $data['kategori'],
            "date_validasi_atasan" => date("Y-m-d h:i:s"),
            "validasi_atasan" => $token['user_key'],
            "dc" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );


            if ($this->db->insert('kinerja_rekap_nilai', $data_insert)) {
                $id = $this->db->insert_id();
                // $this->log_app->log_data($token['user_key'], 'kinerja_rekap_nilai', 'id', $id, 'C', json_encode($data_insert), 'Insert rekap nilai kinerja', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }
        
    }

    

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "user_key" => $data['user_key'],
            "periode_penilaian" => $data['periode_penilaian'],
            "validasi_atasan" => $token['user_key'],
            "tgl_validasi" => date("Y-m-d h:i:s"),
            "ket" => $data['ket']
        );

            if ($this->db->update('kinerja_skp_pegawai', $data_update, array('user_key' => $data['user_key'],'periode_penilaian' => $data['periode_penilaian']))) {
                // $this->log_app->log_data($token['username'], 'kinerja_skp_pegawai', 'user_key', $data['user_key'], 'U', json_encode($data_update), 'Update Validasi Penilaian', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function update_revisi()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;

        if ($this->db->delete('kinerja_skp_pegawai', array('user_key' => $data['user_key'], 'periode_penilaian' => $data['id_periode']))) {
            // $this->log_app->log_data($token['user_key'], 'kinerja_skp_pegawai', 'id_periode', $data['id_periode'], 'D', json_encode($data['user_key']), 'Hapus SKP Pegawai', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }

     public function unvalidasi()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['userkey'],$data['periode']);
       
        if (empty($data_test)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_rekap_nilai', array('user_key' => $data['userkey'], 'id_periode' => $data['periode']))) {
        //     // $this->log_app->log_data($token['user_key'], 'kinerja_rekap_nilai', 'user_key', $data['userkey'], 'D', json_encode($data_test), 'Unvalidasi', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }

    public function get_data_edit($userkey = '', $periode= '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_rekap_nilai WHERE user_key = '$userkey' AND id_periode = '$periode'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return false;
        }
    }

    public function get_list()
    {

        $token  = $this->tkn;

        $data = json_decode(file_get_contents("php://input"), true);
        $sql    = "SELECT a.penilai_user_key, a.dinilai_user_key, a.dinilai_nama, a.dinilai_fak_unit, IF (b.ket IS NULL, 'Belum Divalidasi', 'Sudah Divalidasi') status
                        FROM kinerja_asesor a
                            LEFT JOIN
                            (
                                SELECT periode_penilaian, user_key, validasi_atasan, total_nilai, ket
                                FROM kinerja_skp_pegawai
                                GROUP BY user_key, periode_penilaian
                            ) b ON a.id_periode = b.periode_penilaian AND a.dinilai_user_key = b.user_key
                        WHERE a.penilai_user_key = '$token[user_key]' AND a.kategori='pimpinan' 
                        AND a.id_periode = '$data[periode]'
                        ORDER BY a.dinilai_nama";

        $query  = $this->db->query($sql, array($token['user_key']));
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_skp()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT  a.id,
                        a.periode_penilaian,
                        a.id_skp,
                        a.user_key,
                        a.skor,
                        a.bobot,
                        a.nilai,
                        a.total_nilai,
                        a.validasi_atasan,
                        a.ket,
                        b.id_indikator_utama,
                        b.id_sub_indikator,
                        b.id_ind_penilaian,
                        b.rincian_indikator,
                        c.nama_periode,
                        c.pengisian_mulai,
                        c.pengisian_selesai
                        FROM
                        kinerja_skp_pegawai AS a
                        INNER JOIN kinerja_skp AS b ON a.id_skp = b.id_skp AND a.periode_penilaian = b.periode
                        INNER JOIN kinerja_periode AS c ON b.periode = c.id_periode
                        WHERE
                        a.periode_penilaian = '$data[periode]' AND
                        a.user_key = '$data[user_key]'
                        ORDER BY
                        b.no_urut ASC
                        ";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

     public function get_skp_valid()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT  a.id,
                        a.periode_penilaian,
                        a.user_key,
                        a.total_nilai,
                        a.validasi_atasan,
                        a.ket
                        FROM
                        kinerja_skp_pegawai AS a
                        WHERE
                        a.periode_penilaian = '$data[periode]' 
                        AND a.user_key = '$data[user_key]'  
                        AND a.ket = '1'
                        GROUP BY a.periode_penilaian, a.user_key
                        ";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
              return $result;
        } else {
            return array();
        }
    }

    public function get_rekap_nilai()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT  *
                        FROM
                        kinerja_rekap_nilai AS a
                        WHERE
                        a.id_periode = '$data[periode]' AND
                        a.user_key = '$data[user_key]' 
                        ";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
           return $result;
        } else {
            return array();
        }
    }

    public function sk_pegawai()
    {
        $input  = json_decode(file_get_contents("php://input"));

         $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$input->user_key'
                    AND jenis_sk = 'pegawai'
                    ORDER BY exp_sk DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function ijazah()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * from kinerja_ijazah
                    WHERE userkey = '$input->user_key'
                    ORDER BY cd DESC
                    LIMIT 1";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function sertifikat_komp()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));


        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE userkey = '$input->user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '2'
                    ORDER BY cd DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function sertifikat_core()
    {
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE (userkey = '$input->user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '1')
                    OR userkey = '$input->user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '2'
                    ORDER BY tanggal_sertifikat DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function sertifikat_penunjang()
    {

        $input  = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE (userkey = '$input->user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '3')
                    OR (userkey = '$input->user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '4')
                    ORDER BY tanggal_sertifikat DESC LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function sk_panitia()
    {

        $input  = json_decode(file_get_contents("php://input"));
         $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$input->user_key'
                    AND jenis_sk = 'panitia'
                    AND (tanggal_sk BETWEEN  '$input->awal' AND '$input->akhir')
                    ORDER BY tanggal_sk DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_foto_event()
    {
        $input  = json_decode(file_get_contents("php://input"));

        $sql    = "SELECT   ke.id_event,
                            ke.nama_event,
                            ke.tanggal_mulai,
                            ke.tanggal_selesai,
                            ep.file_foto,
                            ep.id,
                            ep.nama_folder
                        FROM
                            kinerja_event AS ke
                        LEFT JOIN (
                            SELECT
                                kinerja_event.id_event,
                                kinerja_event.nama_event,
                                kinerja_event.tanggal_mulai,
                                kinerja_event.tanggal_selesai,
                                kinerja_event.jam_mulai,
                                kinerja_event.jam_selesai,
                                kinerja_event.lokasi_event,
                                kinerja_event_peserta.file_foto,
                                kinerja_event_peserta.nama_folder,
                                kinerja_event_peserta.user_key,
                                kinerja_event_peserta.id
                            FROM
                                kinerja_event
                            JOIN kinerja_event_peserta ON kinerja_event.id_event = kinerja_event_peserta.id_event
                            WHERE
                                kinerja_event_peserta.user_key = '$input->user_key'
                        ) AS ep ON ke.id_event = ep.id_event
                        WHERE
                            (
                                ke.tanggal_mulai BETWEEN '$input->awal'
                                AND '$input->akhir'
                            )
                        AND ke.sub_unsur = 'Harmoni'";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }



}