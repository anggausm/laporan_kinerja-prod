<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_periode_log extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  *   FROM
                            kinerja_period_log 
                            where periode_kinerja = '$data->id'
                            ORDER BY tgl_awal ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_periode_all()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  *   FROM
                            kinerja_periode
                            where kategori = 'Kependidikan'
                            and status = '1'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function cek_insert($id = '')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT id_lpp, userkey FROM kinerja_lpp WHERE periode_lpp = '$id' AND userkey = '$userkey'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "nama_log" => $data['nama_periode'],
            "tgl_awal" => $data['tgl_awal'],
            "tgl_akhir" => $data['tgl_akhir'],
            "periode_kinerja" => $data['id_periode'],
            "status" => $data['status'],
            "dc" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );


            if ($this->db->insert('kinerja_period_log', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_period_log', 'id', $id, 'C', json_encode($data_insert), 'Insert Data Periode Log', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_period_log
                WHERE id = ?";
        $query = $this->db->query($sql, $data['id']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "nama_log" => $data['nama_periode'],
            "tgl_awal" => $data['tgl_awal'],
            "tgl_akhir" => $data['tgl_akhir'],
            "status" => $data['ket'],
            "u" => $token['user_key'],
            "du" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_period_log', $data_update, array('id' => $data['id']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_period_log', 'id', $data['id'], 'U', json_encode($data_update), 'Update Data Periode Log', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_period_log WHERE id = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_period_log', array('id' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_period_log', 'id', $data['id'], 'D', json_encode($data_test), 'Delete Data Periode Log', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }
}