<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_pegawai extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }

    public function get_semua_periode()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_periode where kategori = 'Kependidikan' order by penilaian_mulai DESC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_periode_sekarang()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT * FROM kinerja_periode
                        WHERE (CURDATE() BETWEEN pengisian_mulai AND pengisian_selesai) 
                        AND kategori = 'Kependidikan'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function indikator_utama($jabatan='') {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.id_indikator_utama,
                        kinerja_skp.id_sub_indikator,
                        kinerja_skp.id_ind_penilaian,
                        kinerja_skp.ket,
                        kinerja_skp.periode,
                        kinerja_skp.jenis_pegawai,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_jenis_pegawai.nama_jenis_pegawai,
                        kinerja_skp.rincian_indikator,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_jabatan ON kinerja_jenis_pegawai.id_jenis_pegawai = kinerja_jabatan.id_jenis_pegawai
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        WHERE kinerja_jabatan.nama_jabatan = '$jabatan' 
                        GROUP by kinerja_skp.id_indikator_utama";
                        
        $query = $this->db->query($sql, array());
        $result = $query->result_array();
        return $result;
    }

    public function sub_indikator($jabatan='',$id='') {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.id_indikator_utama,
                        kinerja_skp.id_sub_indikator,
                        kinerja_skp.id_ind_penilaian,
                        kinerja_skp.periode,
                        kinerja_skp.jenis_pegawai,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_skp.rincian_indikator,
                        kinerja_jenis_pegawai.nama_jenis_pegawai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_jabatan ON kinerja_jenis_pegawai.id_jenis_pegawai = kinerja_jabatan.id_jenis_pegawai
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        WHERE kinerja_jabatan.nama_jabatan = '$jabatan' 
                        GROUP by kinerja_skp.id_sub_indikator 
                        AND kinerja_skp.id_indikator_utama = '$id'
                        ORDER BY kinerja_skp.id_sub_indikator ASC";
        $query = $this->db->query($sql, array());
        $result = $query->result_array();
        return $result;
    }

    public function indikator_penilaian($jabatan='',$id='', $id_sub='') {
        $data = json_decode(file_get_contents("php://input"));
        $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.id_indikator_utama,
                        kinerja_skp.id_sub_indikator,
                        kinerja_skp.id_ind_penilaian,
                        kinerja_skp.ket,
                        kinerja_skp.periode,
                        kinerja_skp.jenis_pegawai,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_skp.rincian_indikator,
                        kinerja_jenis_pegawai.nama_jenis_pegawai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_jabatan ON kinerja_jenis_pegawai.id_jenis_pegawai = kinerja_jabatan.id_jenis_pegawai
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        WHERE kinerja_jabatan.nama_jabatan = '$jabatan' 
                        AND kinerja_skp.id_indikator_utama = '$id' 
                        AND kinerja_skp.id_sub_indikator = '$id_sub'";
        $query = $this->db->query($sql, array());
        $result = $query->result_array();
        return $result;
    }

    public function get_rincian_ind() {
        $data = json_decode(file_get_contents("php://input"), true);
        $ind_utama = $this->indikator_utama($data['jabatan']);

        foreach ($ind_utama as $key => $value) {
            $subindikator = $this->sub_indikator($data['jabatan'],$value['id_indikator_utama']);

            $ind_utama[$key]['id_sub_indikator'] = $subindikator;
            foreach ($subindikator as $key2 => $val) {

                $penilaian = $this->indikator_penilaian($data['jabatan'],$val['id_indikator_utama'], $value['id_sub_indikator']);
                $ind_utama[$key]['id_sub_indikator'][$key2]['id_ind_penilaian'] = $penilaian;

            }
        }

        $result = $ind_utama;

        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

        //==============================

    public function get_log_book()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_log_book 
                    WHERE user_key = '$user_key' AND (tanggal_log_book BETWEEN '$input->awal' AND '$input->akhir')
                    ORDER BY cd DESC";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } 
        else 
        {
            return $this->app->respons_data(array(), 'Data gagal diload.', 200);
        }
    }

    public function get_period_log($id_periode = '')
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT * FROM kinerja_period_log where periode_kinerja  = '$data->id_periode' and status = '1' order by tgl_awal ASC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
             return array();
        }
    }

    public function get_rekap_log()
    {
        $token  = $this->tkn;
        $data  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];
        $sql = "SELECT  count(poin1) as p1,
                        count(poin2) as p2,
                        count(poin3) as p3,
                        count(poin4) as p4, 
                        count(poin5) as p5, 
                        count(poin6) as p6, 
                        sum(nilai) as jumlah_nilai
                            FROM
                                kinerja_hit_log a
                            WHERE
                                periode_pen = '$data->id_periode'
                            AND userkey = '$user_key'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    //-------------- sertifikat

    public function get_sertifikat_user()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE userkey = '$user_key'
                    ORDER BY cd DESC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sertifikat_periode()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    ORDER BY cd DESC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sertifikat_perjenis()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '$input->jenis'
                    ORDER BY cd DESC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    
    public function get_sertifikat_tipe1()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE (userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '1')
                    OR userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '2'
                    ORDER BY cd DESC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sertifikat_tipe2()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat 
                    WHERE (userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '3')
                    OR userkey = '$user_key'
                    AND (tanggal_sertifikat BETWEEN '$input->awal' AND '$input->akhir')
                    AND jenis_sertifikat = '4'
                    ORDER BY cd DESC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function insert_sertifikat()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "no_sertifikat" => $data['no_sertifikat'],
            "nama_sertifikat" => $data['nama_sertifikat'],
            "tanggal_sertifikat" => $data['tanggal_sertifikat'],
            "tanggal_berlaku" => $data['tanggal_berlaku'],
            "penyelenggara" => $data['penyelenggara'],
            "nm_file" => $data['nm_file'],
            "nm_folder" => $data['nm_folder'],
            "tingkat_sertifikat" => $data['tingkat_sertifikat'],
             "jenis_sertifikat" => $data['jenis_sertifikat'],
            "userkey" => $token['user_key'],
            "ket" => $data['ket'],
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_sertifikat', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_sertifikat', 'id', $id, 'C', json_encode($data_insert), 'Insert Data Sertifikat', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            } 
    }

    public function edit_data_sert()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sertifikat
                    WHERE id_sertifikat = '$input->id_sertifikat'
                    ORDER BY cd DESC";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } 
        else 
        {
            return $this->app->respons_data(array(), 'Data gagal diload.', 200);
        }
    }

    public function update_sertifikat()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "no_sertifikat" => $data['no_sertifikat'],
            "nama_sertifikat" => $data['nama_sertifikat'],
            "tanggal_sertifikat" => $data['tanggal_sertifikat'],
            "tanggal_berlaku" => $data['tanggal_berlaku'],
            "penyelenggara" => $data['penyelenggara'],
            "nm_file" => $data['nm_file'],
            "nm_folder" => $data['nm_folder'],
            "tingkat_sertifikat" => $data['tingkat_sertifikat'],
            "jenis_sertifikat" => $data['jenis_sertifikat'],
            "userkey" => $token['user_key'],
            "ket" => $data['ket'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_sertifikat', $data_update, array('id_sertifikat' => $data['id_sertifikat']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_sertifikat', 'id_sertifikat', $data['id_sertifikat'], 'U', json_encode($data_update), 'Update Data Sertifikat', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function delete_sertifikat()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_sertifikat', array('id_sertifikat' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_sertifikat', 'id_sertifikat', $data['id'], 'D', json_encode($data_test), 'Delete Data Sertifikat', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }


    
    //---------------------- SK

        public function edit_sk() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_sk
                WHERE id_sk = '$data[id_sk]'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function insert_sk()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "jenis_sk" => $data['jenis_sk'],
            "judul_sk" => $data['judul_sk'],
            "no_sk" => $data['no_sk'],
            "tanggal_sk" => $data['tanggal_sk'],
            "tmt_sk" => $data['tmt_sk'],
            "exp_sk" => $data['exp_sk'],
            "userkey" => $token['user_key'],
            "file_sk" => $data['file_sk'],
            "folder_sk" => $data['folder_sk'],
            "status" => '',
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_sk', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_sk', 'id', $id, 'C', json_encode($data_insert), 'Insert Data SK Panitia', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            } 
    }

    public function insert_sk_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "jenis_sk" => $data['jenis_sk'],
            "judul_sk" => $data['judul_sk'],
            "no_sk" => $data['no_sk'],
            "tanggal_sk" => $data['tanggal_sk'],
            "tmt_sk" => $data['tmt_sk'],
            "exp_sk" => $data['exp_sk'],
            "userkey" => $token['user_key'],
            "file_sk" => $data['file_sk'],
            "folder_sk" => $data['folder_sk'],
            "fak" => $data['flus'],
            "tmt_usm" => $data['tmt_usm'],
            "status_kerja" => $data['status_kerja'],
            "jenis_pekerjaan" => $data['jenis_pekerjaan'],
            "status" => '',
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_sk', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_sk', 'id', $id, 'C', json_encode($data_insert), 'Insert Data SK Pegawai', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            } 
    }

    public function get_sk()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$user_key'
                    AND jenis_sk = '$input->jenis_sk'
                    ORDER BY cd DESC";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sk_jns()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$user_key'
                    AND jenis_sk = '$input->jenis_sk'
                    AND (tanggal_sk BETWEEN  '$input->awal' AND '$input->akhir')
                    ORDER BY cd DESC";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sk_pegawai()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        // $sql    = "SELECT * from kinerja_sk 
        //             WHERE userkey = '$user_key'
        //             AND jenis_sk = 'pegawai'
        //             AND exp_sk >= '$input->akhir'
        //             ORDER BY cd DESC";

         $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$user_key'
                    AND jenis_sk = 'pegawai'
                    ORDER BY exp_sk DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_sk_panitia()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

         $sql    = "SELECT * from kinerja_sk 
                    WHERE userkey = '$user_key'
                    AND jenis_sk = 'panitia'
                    AND (tanggal_sk BETWEEN  '$input->awal' AND '$input->akhir')
                    ORDER BY tanggal_sk DESC
                    LIMIT 1";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function update_sk_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_update = array(
            "jenis_sk" => $data['jenis_sk'],
            "judul_sk" => $data['judul_sk'],
            "no_sk" => $data['no_sk'],
            "tanggal_sk" => $data['tanggal_sk'],
            "tmt_sk" => $data['tmt_sk'],
            "exp_sk" => $data['exp_sk'],
            "userkey" => $token['user_key'],
            "file_sk" => $data['file_sk'],
            "folder_sk" => $data['folder_sk'],
            "fak" => $data['flus'],
            "tmt_usm" => $data['tmt_usm'],
            "status_kerja" => $data['status_kerja'],
            "jenis_pekerjaan" => $data['jenis_pekerjaan'],
            "status" => '',
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->update('kinerja_sk', $data_update, array('id_sk' => $data['id_sk']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_sk', 'id_sk', $id, 'C', json_encode($data_update), 'Update Data SK Pegawai', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            } 
    }

    public function delete_sk()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_sk', array('id_sk' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_sk', 'id_ijazah', $data['id_sk'], 'D', json_encode($data_test), 'Delete Data SK', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }



    //--------------------------ijazah

    public function get_ijazah()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_ijazah
                    WHERE userkey = '$user_key'
                    ORDER BY cd DESC";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_data_ijazah()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];

        $sql = "SELECT * from kinerja_ijazah where userkey = '$user_key' order by tanggal_ijazah DESC LIMIT 1";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

     public function insert_ijz()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "jenjang_pendidikan" => $data['jenjang_pendidikan'],
            "tanggal_ijazah" => $data['tanggal_ijazah'],
            "nama_instansi" => $data['nama_instansi'],
            "no_ijazah" => $data['no_ijazah'],
            "tahun_masuk" => $data['tahun_masuk'],
            "tahun_lulus" => $data['tahun_lulus'],
            "jurusan" => $data['jurusan'],
            "tempat_belajar" => $data['tempat_belajar'],
            "userkey" => $token['user_key'],
            "file" => $data['file'],
            "status" => '',
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_ijazah', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_ijazah', 'id', $id, 'C', json_encode($data_insert), 'Insert Data Ijazah', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }   
    }

    
     public function get_ijazah_edit()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];

        $sql = "SELECT  * from kinerja_ijazah where userkey = '$user_key' AND id_ijazah = '$data->id_ijazah'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function update_ijazah()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "jenjang_pendidikan" => $data['jenjang_pendidikan'],
            "tanggal_ijazah" => $data['tanggal_ijazah'],
            "nama_instansi" => $data['nama_instansi'],
            "no_ijazah" => $data['no_ijazah'],
            "tahun_masuk" => $data['tahun_masuk'],
            "tahun_lulus" => $data['tahun_lulus'],
            "jurusan" => $data['jurusan'],
            "tempat_belajar" => $data['tempat_belajar'],
            "file" => $data['file'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_ijazah', $data_update, array('id_ijazah' => $data['id_ijazah']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_ijazah', 'id_ijazah', $data['id_ijazah'], 'U', json_encode($data_update), 'Update Data Ijazah', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

     public function delete_ijazah()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_ijazah', array('id_ijazah' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_ijazah', 'id_ijazah', $data['id'], 'D', json_encode($data_test), 'Delete Data Ijazah', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }

//-------------- event

    public function get_semua_event()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_event
                    WHERE (tanggal_mulai BETWEEN '$input->awal' AND '$input->akhir')
                    ORDER BY tanggal_mulai ASC";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_event_wajib()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        // $sql    = "SELECT a.id_event, 
        //                   a.nama_event, 
        //                   a.tanggal_mulai, 
        //                   a.tanggal_selesai, 
        //                   a.jam_mulai, 
        //                   a.jam_selesai, 
        //                   a.lokasi_event, 
        //                   b.user_key, 
        //                   b.jam_finger
        //                 FROM kinerja_event a
        //                 LEFT JOIN kinerja_event_peserta b ON a.id_event = b.id_event 
        //                 WHERE a.sub_unsur = 'Integritas' 
        //                 and (a.tanggal_mulai BETWEEN '$input->awal' AND '$input->akhir')
        //                 and b.user_key = '$user_key'";
        $sql    = "SELECT   ke.id_event,
                            ke.nama_event,
                            ke.tanggal_mulai,
                            ke.tanggal_selesai,
                            ep.id,
                            ep.file_foto,
                            ep.nama_folder,
                            ep.jam_finger
                        FROM
                            kinerja_event AS ke
                        LEFT JOIN (
                            SELECT
                                kinerja_event.id_event,
                                kinerja_event.nama_event,
                                kinerja_event.tanggal_mulai,
                                kinerja_event.tanggal_selesai,
                                kinerja_event.jam_mulai,
                                kinerja_event.jam_selesai,
                                kinerja_event.lokasi_event,
                                kinerja_event_peserta.file_foto,
                                kinerja_event_peserta.nama_folder,
                                kinerja_event_peserta.user_key,
                                kinerja_event_peserta.id,
                                kinerja_event_peserta.jam_finger
                            FROM
                                kinerja_event
                            JOIN kinerja_event_peserta ON kinerja_event.id_event = kinerja_event_peserta.id_event
                            WHERE
                                kinerja_event_peserta.user_key = '$user_key'
                        ) AS ep ON ke.id_event = ep.id_event
                        WHERE
                            (
                                ke.tanggal_mulai BETWEEN '$input->awal'
                                AND '$input->akhir'
                            )
                        AND ke.sub_unsur = 'Integritas'";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_event_pst()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT * from kinerja_event_peserta
                    WHERE id = ?";

        $query  = $this->db->query($sql, array($input->id)); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } 
        else 
        {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function delete_foto()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;

        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_event_peserta', array('id' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_event_peserta', 'id', $data['id'], 'D', json_encode($data_test), 'Delete Data Foto', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }

    public function get_event_pilihan()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        // $sql    = "SELECT * from kinerja_event
        //             WHERE (tanggal_mulai BETWEEN '$input->awal' AND '$input->akhir')
        //             AND ket = '2'";

        $sql    = "SELECT   ke.id_event,
                            ke.nama_event,
                            ke.tanggal_mulai,
                            ke.tanggal_selesai,
                            ep.file_foto,
                            ep.id,
                            ep.nama_folder
                        FROM
                            kinerja_event AS ke
                        LEFT JOIN (
                            SELECT
                                kinerja_event.id_event,
                                kinerja_event.nama_event,
                                kinerja_event.tanggal_mulai,
                                kinerja_event.tanggal_selesai,
                                kinerja_event.jam_mulai,
                                kinerja_event.jam_selesai,
                                kinerja_event.lokasi_event,
                                kinerja_event.sub_unsur,
                                kinerja_event_peserta.file_foto,
                                kinerja_event_peserta.nama_folder,
                                kinerja_event_peserta.user_key,
                                kinerja_event_peserta.id
                            FROM
                                kinerja_event
                            JOIN kinerja_event_peserta ON kinerja_event.id_event = kinerja_event_peserta.id_event
                            WHERE
                                kinerja_event_peserta.user_key = '$user_key'
                        ) AS ep ON ke.id_event = ep.id_event
                        WHERE
                            (
                                ke.tanggal_mulai BETWEEN '$input->awal'
                                AND '$input->akhir'
                            )
                        AND ke.sub_unsur = 'Harmoni'";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

    public function get_event_user()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT   ke.id_event,
                            ke.nama_event,
                            ke.tanggal_mulai,
                            ke.tanggal_selesai,
                            ep.file_foto,
                            ep.nama_folder
                        FROM
                            kinerja_event AS ke
                        JOIN (
                            SELECT
                                kinerja_event.id_event,
                                kinerja_event.nama_event,
                                kinerja_event.tanggal_mulai,
                                kinerja_event.tanggal_selesai,
                                kinerja_event.jam_mulai,
                                kinerja_event.jam_selesai,
                                kinerja_event.lokasi_event,
                                kinerja_event.sub_unsur,
                                kinerja_event_peserta.file_foto,
                                kinerja_event_peserta.nama_folder,
                                kinerja_event_peserta.user_key
                            FROM
                                kinerja_event
                            JOIN kinerja_event_peserta ON kinerja_event.id_event = kinerja_event_peserta.id_event
                            WHERE
                                kinerja_event_peserta.user_key = '$user_key'
                        ) AS ep ON ke.id_event = ep.id_event
                        WHERE
                            (
                                ke.tanggal_mulai BETWEEN '$input->awal'
                                AND '$input->akhir'
                            )
                        AND ke.sub_unsur = 'Harmoni'";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

     public function get_periode_asid()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_periode where kategori = 'Kependidikan' and id_periode = '$data->periode' order by penilaian_mulai DESC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } else {
            return array();
        }
    }



//================
     public function get_data_periode()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_periode where kategori = 'Kependidikan' and id_periode = '$data->id' order by penilaian_mulai DESC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }



    public function get_unit()
    {
        $sql    = "SELECT * from kinerja_unit";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } 
        else 
        {
            return $this->app->respons_data(array(), 'Data gagal diload.', 200);
        }
    }

    public function get_prodi()
    {
        $sql    = "SELECT * from program_studi";
        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } 
        else 
        {
            return $this->app->respons_data(array(), 'Data gagal diload.', 200);
        }
    }



    public function cek_data_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"));

        // $sql = "SELECT  * from kinerja_mst_pegawai where user_key = '$data->user_key'";
        $sql = "SELECT  kinerja_mst_pegawai.id_pegawai,
                        kinerja_mst_pegawai.id_peg_isdm,
                        kinerja_mst_pegawai.user_key,
                        kinerja_mst_pegawai.nama_doskar,
                        kinerja_mst_pegawai.nis,
                        kinerja_mst_pegawai.id_unit,
                        kinerja_mst_pegawai.nama_unit,
                        kinerja_mst_pegawai.prodi,
                        kinerja_mst_pegawai.pekerjaan,
                        kinerja_mst_pegawai.status_kerja,
                        kinerja_mst_pegawai.jabatan,
                        kinerja_mst_pegawai.jabatan_struktural,
                        kinerja_mst_pegawai.gol,
                        kinerja_mst_pegawai.tmt_usm,
                        kinerja_mst_pegawai.jenis_pegawai,
                        kinerja_jenis_pegawai.nama_jenis_pegawai
                        FROM
                        kinerja_mst_pegawai
                        JOIN kinerja_jenis_pegawai ON kinerja_mst_pegawai.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        where kinerja_mst_pegawai.user_key = '$data->user_key'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function cek_jenis_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  * from kinerja_jabatan where nama_jabatan = '$data->pekerjaan'";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "id_peg_isdm" => $data['id_doskar'],
            "user_key" => $data['user_key'],
            "nama_doskar" => $data['nama_doskar'],
            "nis" => $data['nis'],
            "nama_unit" => $data['nama_unit'],
            "pekerjaan" => $data['pekerjaan'],
            "jabatan" => $data['jabatan'],
            "gol" => $data['gol'],
            "tmt_usm" => $data['tmt_usm'],
            "status_kerja" => $data['status_kerja'],
            "jenis_pegawai" => $data['jenis_pegawai'],
            "ket" => $data['ket'],
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

        // if ($this->cek_data_pegawai(array("user_key" => $data['userkey']))) {
        //     return $this->app->respons_data(array(), 'Data Pegawai sudah ada', 200);
        // } else {
            if ($this->db->insert('kinerja_mst_pegawai', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_mst_pegawai', 'id_pegawai', $id, 'C', json_encode($data_insert), 'Insert Data Pegawai', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }
        // }
        
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
         $data_update = array(
            // "id_peg_isdm" => $data['id_doskar'],
            "nama_doskar" => $data['nama_doskar'],
            "nis" => $data['nis'],
            "nama_unit" => $data['nama_unit'],
            "pekerjaan" => $data['pekerjaan'],
            "jabatan" => $data['jabatan'],
            "gol" => $data['gol'],
            "tmt_usm" => $data['tmt_usm'],
            "status_kerja" => $data['status_kerja'],
            "jenis_pegawai" => $data['jenis_pegawai'],
            "ket" => $data['ket'],
            "ud" => date("Y-m-d h:i:s"),
            "u" => $token['user_key']
        );

            if ($this->db->update('kinerja_mst_pegawai', $data_update, array('user_key' => $data['user_key']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_mst_pegawai', 'user_key', $data['user_key'], 'U', json_encode($data_update), 'Update Data Pegawai', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function get_skp_peg() {
        $data = json_decode(file_get_contents("php://input"), true);
        // $sql = "SELECT  kinerja_skp.id_skp,
        //                 kinerja_skp.rincian_indikator,
        //                 kinerja_skp.skor,
        //                 kinerja_skp.bobot,
        //                 kinerja_skp.syarat,
        //                 kinerja_skp.ket,
        //                 kinerja_skp.periode,
        //                 kinerja_indikator_utama.indikator_utama,
        //                 kinerja_sub_indikator.nama_sub_indikator,
        //                 kinerja_indikator_penilaian.nama_ind_penilaian,
        //                 kinerja_jenis_pegawai.nama_jenis_pegawai,
        //                 kinerja_periode.nama_periode
        //                 FROM
        //                 kinerja_skp
        //                 LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
        //                 LEFT JOIN kinerja_jabatan ON kinerja_jenis_pegawai.id_jenis_pegawai = kinerja_jabatan.id_jenis_pegawai
        //                 LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
        //                 LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
        //                 LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
        //                 LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
        //                 WHERE kinerja_jabatan.nama_jabatan = '$data[jabatan]' ORDER by kinerja_skp.id_skp ASC";

          $sql = "SELECT  kinerja_skp.id_skp,
                        kinerja_skp.rincian_indikator,
                        kinerja_skp.skor,
                        kinerja_skp.bobot,
                        kinerja_skp.syarat,
                        kinerja_skp.ket,
                        kinerja_skp.periode,
                        kinerja_skp.urutan,
                        kinerja_indikator_utama.indikator_utama,
                        kinerja_sub_indikator.nama_sub_indikator,
                        kinerja_indikator_penilaian.nama_ind_penilaian,
                        kinerja_jenis_pegawai.nama_jenis_pegawai,
                        kinerja_jenis_pegawai.id_jenis_pegawai,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_skp
                        LEFT JOIN kinerja_jenis_pegawai ON kinerja_skp.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        LEFT JOIN kinerja_indikator_utama ON kinerja_skp.id_indikator_utama = kinerja_indikator_utama.id_indikator_utama
                        LEFT JOIN kinerja_sub_indikator ON kinerja_skp.id_sub_indikator = kinerja_sub_indikator.id_sub_indikator
                        LEFT JOIN kinerja_indikator_penilaian ON kinerja_skp.id_ind_penilaian = kinerja_indikator_penilaian.id
                        LEFT JOIN kinerja_periode ON kinerja_skp.periode = kinerja_periode.id_periode
                        WHERE kinerja_skp.jenis_pegawai = '$data[jabatan]' ORDER by kinerja_skp.urutan ASC";

        $query = $this->db->query($sql);
       
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }
    
   
    public function get_lpp()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];
        // $tanggal = $data->tahun . '-' . $data->bulan;

        $sql = "SELECT  * from kinerja_lpp where 
                        userkey = '$user_key' AND
                        (periode_lpp BETWEEN '$data->awal' AND '$data->akhir') ";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function insert_foto()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "id_event" => $data['id_event'],
            "user_key" => $token['user_key'],
            "nama_folder" => $data['nama_folder'],
            "file_foto" => $data['file_foto'],
            "dc" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

            if ($this->db->insert('kinerja_event_peserta', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_event_peserta', 'id_event', $id, 'C', json_encode($data_insert), 'Insert Data Pegawai', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            }
        
    }

    public function get_list_rekan()
    {
        $token  = $this->tkn;
        $data = json_decode(file_get_contents("php://input"), true);
        $sql    = "SELECT a.penilai_user_key, a.dinilai_user_key, a.dinilai_nama, a.dinilai_fak_unit, a.kategori, IF (b.dinilai_user_key IS NOT NULL OR b.dinilai_user_key = '', 'Sudah Dinilai', 'Belum Dinilai') status
                        FROM kinerja_asesor a
                            LEFT JOIN
                            (
                                SELECT * 
                                FROM kinerja_nilai a 
                                GROUP BY a.dinilai_user_key, a.penilai_user_key
                            ) b ON a.penilai_user_key = b.penilai_user_key AND a.dinilai_user_key = b.dinilai_user_key
                        WHERE a.penilai_user_key = '$token[user_key]'  and a.id_periode = '$data[id_periode]'
                        ORDER BY a.dinilai_nama ";

        $query  = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function get_list_tanya()
    {
        $token  = $this->tkn;
        $data = json_decode(file_get_contents("php://input"), true);
         $sql    = "SELECT a.id_pertanyaan, a.pertanyaan, a.jenis, a.kategori, FORMAT(b.nilai, 0) nilai
                        FROM kinerja_pernyataan a
                        LEFT JOIN                      (
                                 SELECT * FROM kinerja_nilai WHERE penilai_user_key = ?
                                                                            AND dinilai_user_key = ?
                             ) b ON a.id_pertanyaan = b.id_pertanyaan
                        WHERE a.jenis_pegawai = '$data[jenis_peg]'
                            AND a.id_periode = '$data[id_periode]'
                        ORDER BY a.urutan";

        $query  = $this->db->query($sql, array($token['user_key'], $post['dinilai']));
        $result = $query->result_array();
        $query->free_result();
      
        $data   = array('data' => $result, 'status' => $this->_cek_data($token['user_key'], $data['dinilai']));
        return $this->app->respons_data($data, 'Data berhasil diload '. $token['user_key'] .' | '. $data['dinilai'].'', 200);
    }

    public function _cek_data($penilai = '', $dinilai = '')
    {
        $sql    = "SELECT * 
                    FROM kinerja_nilai a 
                    WHERE a.penilai_user_key = ?
                        AND a.dinilai_user_key = ? ";

        $query  = $this->db->query($sql, array($penilai, $dinilai));
        $result = $query->row_array();
        $query->free_result();
        
        if(empty($result)) {
            return "open";
        } else {
            return "close";
        }
    }

    public function simpan_kuesioner()
    {
        $post       = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $token      = $this->tkn;

        // cek data sudah input atau belum
        
        $cek = $this->_cek_data($token['user_key'], $post['dinilai']);

        if ($cek == 'open') {
            $data_simpan = array();
            foreach ($post['nilai'] as $key => $value) {
                $data_simpan[] = array('dinilai_user_key'    => $post['dinilai'],
                                'penilai_user_key'  => $token['user_key'],
                                'id_pertanyaan'     => $key, 
                                'nilai'             => $value['nilai'],
                                'c'                 => $token['username'],
                                'dc'                => date('Y-m-d H:i:s') );
            }

            $this->db->trans_start();
            $this->db->insert_batch('kinerja_nilai', $data_simpan);
            $this->db->trans_complete();

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                $this->log_app->log_data($token['user_key'], 'kinerja_nilai', '', '', 'C', json_encode($data_simpan), 'batch input penilaian kinerja rekan kerja', 'log_kinerja');
                
                return $this->app->respons_data(true, 'Data berhasil diload', 200);
            } else {
                $this->db->trans_rollback();
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            }
        } else {

            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        }

    }

    public function get_nilai_kuesioner()
    {
        $token  = $this->tkn;
        $data = json_decode(file_get_contents("php://input"), true);
        // $sql    = "SELECT   Sum(IFNULL(nilai,0)) AS jm_nilai,
        //                     Count(a.nilai) AS jm_tny,
        //                     (SUM(IFNULL(nilai,0))/COUNT(nilai)) AS rerata,
        //                     b.jenis,
        //                     b.kategori,
        //                     a.id_nilai,
        //                     a.dinilai_user_key,
        //                     a.penilai_user_key,
        //                     a.id_pertanyaan,
        //                     a.jumlah,
        //                     a.nilai,
        //                     a.link_dokumen,
        //                     a.c,
        //                     a.dc,
        //                     b.periode_penilaian
        //                     FROM kinerja_nilai a 
        //                             LEFT JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        //                     WHERE b.jenis='Tenaga Kependidikan' and dinilai_user_key = '$token[user_key]'
        //                     and b.periode_penilaian = '$data[periode]'
        //                     GROUP BY a.dinilai_user_key,a.penilai_user_key
        //                     ORDER BY a.dinilai_user_key,b.kategori,a.dc ";
        $sql = "SELECT sum(rerata) as total_rerata,
                        count(penilai_user_key) as jml_penilai,
                        SUM(rerata)/COUNT(penilai_user_key) as rerata_akhir
                         from (SELECT   Sum(IFNULL(nilai,0)) AS jm_nilai,
                                                    Count(a.id_pertanyaan) AS jm_tny,
                                                    (SUM(IFNULL(nilai,0))/COUNT(nilai)) AS rerata,
                                                    b.jenis,
                                                    b.kategori,
                                                    a.id_nilai,
                                                    a.dinilai_user_key,
                                                    a.penilai_user_key,
                                                    a.id_pertanyaan,
                                                    a.jumlah,
                                                    a.nilai,
                                                    a.link_dokumen,
                                                    a.c,
                                                    a.dc,
                                                    b.id_periode
                                                    FROM kinerja_nilai a 
                                                            LEFT JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
                                                    WHERE b.jenis='Tenaga Kependidikan' 
                                                    and a.dinilai_user_key = '$token[user_key]'
                                                    and b.id_periode = '$data[periode]'
                                                    GROUP BY a.dinilai_user_key, a.penilai_user_key
                                                    ORDER BY a.dinilai_user_key,b.kategori,a.dc) as datane";

        $query  = $this->db->query($sql);
        $result = $query->row_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert_ajuan()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "id_skp" => $data['id_skp'],
            "user_key" => $data['user_key'],
            "skor" => $data['skor'],
            "bobot" => $data['bobot'],
            "nilai" => $data['nilai'],
            "total_nilai" => $data['total_nilai'],
            "id_periode" => $data['periode_penilaian'],
            "ket" => 0,
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

        if ($this->cek_insert_skp($data['periode_penilaian'],$data['id_skp']) == false) {
            return $this->app->respons_data(false, 'Data sudah diajukan', 200);
        } else {

            if ($this->db->insert('kinerja_skp_pegawai', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_skp_pegawai', 'id', $id, 'C', json_encode($data_insert), 'Insert Data SKP Pegawai', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal disimpan', 200);
            }
        }
        
    }

     public function cek_insert_skp($id = '', $skp ='')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT * FROM kinerja_skp_pegawai WHERE periode_penilaian = '$id' AND id_skp = '$skp' AND user_key = '$userkey'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function cek_skp_pegawai()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT * FROM kinerja_skp_pegawai WHERE periode_penilaian = '$data[periode]' AND user_key = '$userkey'";
       
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
           $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
              return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function get_presensi()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "SELECT COUNT(a.tanggal) as harikerja, 
                          SUM(if(a.hadir = 1, 1, 0)) as hadir,
                          (SUM(if(a.hadir = 1, 1, 0))/count(a.tanggal))*100 as persentes,
                          a.user_key
                            from (SELECT * 
                            FROM finger_transport a
                            WHERE a.user_key = '$user_key'
                            AND a.tanggal BETWEEN '$input->awal' AND '$input->akhir'
                            AND DATE_FORMAT(a.tanggal, '%w') BETWEEN '1' AND '5') as a";

        $query  = $this->db->query($sql); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }

        public function get_working_days()
    {
        $token  = $this->tkn;
        $input  = json_decode(file_get_contents("php://input"));
        $user_key = $token['user_key'];

        $sql    = "WITH RECURSIVE tanggal(t) AS (
                      SELECT '$input->awal' AS t
                      UNION ALL
                      SELECT t + INTERVAL 1 DAY FROM tanggal WHERE t < '$input->akhir'
                    )
                    SELECT COUNT(*) AS jumlah_hari_kerja
                    FROM tanggal
                    WHERE DAYOFWEEK(t) BETWEEN 2 AND 6
                      AND t NOT IN (SELECT tanggal_libur FROM kinerja_libur)";

        $query  = $this->db->query($sql, array()); 

        if ($query->num_rows() > 0)
        {
            $result = $query->row_array();
            $query->free_result();
             return $result;
        } 
        else 
        {
            return array();
        }
    }



}