<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_lpp extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];

        $sql = "SELECT  kinerja_lpp.id_lpp,
                        kinerja_lpp.userkey,
                        kinerja_lpp.periode_lpp,
                        kinerja_lpp.tugas,
                        kinerja_lpp.status,
                        kinerja_lpp.nama_folder,
                        kinerja_lpp.nama_file,
                        kinerja_lpp.ket,
                        kinerja_lpp.cd,
                        kinerja_lpp.c,
                        YEAR(STR_TO_DATE(periode_lpp, '%Y-%m')) AS order_year
                        FROM
                            kinerja_lpp
                            WHERE userkey = '$user_key'
                            AND YEAR(STR_TO_DATE(periode_lpp, '%Y-%m')) = '$data->tahun'
                        ORDER BY
                            periode_lpp DESC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function cek_insert($id = '')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT id_lpp, userkey FROM kinerja_lpp WHERE periode_lpp = '$id' AND userkey = '$userkey'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "userkey" => $token['user_key'],
            "periode_kinerja" => $data['periode_kinerja'],
            "periode_lpp" => $data['periode_lpp'],
            "tugas" => $data['tugas'],
            "nama_folder" => $data['nama_folder'],
            "nama_file" => $data['nama_file'],
            "cd" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );

        if ($this->cek_insert($data['periode_lpp']) == false) {
            return $this->app->respons_data(false, 'Laporan sudah ada', 200);
        } else {
            if ($this->db->insert('kinerja_lpp', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_lpp', 'id', $id, 'C', json_encode($data_insert), 'Insert Data LPP', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }
        }
        
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_lpp
                WHERE id_lpp = ?";
        $query = $this->db->query($sql, $data['id']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "tugas" => $data['tugas'],
            "nama_folder" => $data['nama_folder'],
            "nama_file" => $data['nama_file'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_lpp', $data_update, array('id_lpp' => $data['id_lpp']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_lpp', 'id_lpp', $data['id_lpp'], 'U', json_encode($data_update), 'Update Data LPP', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_lpp WHERE id_lpp = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_lpp', array('id_lpp' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_lpp', 'id_lpp', $data['id'], 'D', json_encode($data_test), 'Delete Data LPP', 'log_sia');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }
}