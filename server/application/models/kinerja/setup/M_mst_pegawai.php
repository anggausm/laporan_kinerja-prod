<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_mst_pegawai extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  kinerja_mst_pegawai.id_pegawai,
                        kinerja_mst_pegawai.id_peg_isdm,
                        kinerja_mst_pegawai.user_key,
                        kinerja_mst_pegawai.nama_doskar,
                        kinerja_mst_pegawai.nis,
                        kinerja_mst_pegawai.id_unit,
                        kinerja_mst_pegawai.nama_unit,
                        kinerja_mst_pegawai.prodi,
                        kinerja_mst_pegawai.pekerjaan,
                        kinerja_mst_pegawai.status_kerja,
                        kinerja_mst_pegawai.jabatan,
                        kinerja_mst_pegawai.jabatan_struktural,
                        kinerja_mst_pegawai.gol,
                        kinerja_mst_pegawai.tmt_usm,
                        kinerja_mst_pegawai.jenis_pegawai,
                        kinerja_mst_pegawai.ket,
                        kinerja_jenis_pegawai.nama_jenis_pegawai
                        FROM
                        kinerja_mst_pegawai
                        JOIN kinerja_jenis_pegawai ON kinerja_mst_pegawai.jenis_pegawai = kinerja_jenis_pegawai.id_jenis_pegawai
                        ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

     public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_mst_pegawai
                WHERE user_key = ?";
        $query = $this->db->query($sql, $data['user_key']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

        public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "jenis_pegawai" => $data['jenis_pegawai'],
            // "ket" => $data['ket'],
            "u" => $token['user_key'],
            "ud" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_mst_pegawai', $data_update, array('user_key' => $data['user_key']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_mst_pegawai', 'user_key', $data['user_key'], 'U', json_encode($data_update), 'Update Data Pegawai', 'log_sso');
                return $this->app->respons_data(true, 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(false, 'Data gagal update', 200);
            }
    }



    //=unused 

    public function cek_insert($id = '')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT id_jabatan, nama_jabatan, id_jenis_pegawai FROM kinerja_jabatan WHERE nama_jabatan = '$id'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "nama_jabatan" => $data['nama_jabatan'],
            "id_jenis_pegawai" => $data['id_jenis_pegawai'],
            "ket" => $data['ket'],
            "dc" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );


            if ($this->db->insert('kinerja_jabatan', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_jabatan', 'id_jabatan', $id, 'C', json_encode($data_insert), 'Insert Data Jenis Pegawai', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }

        
    }

   



    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_jabatan WHERE id_jabatan = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_jabatan', array('id_jabatan' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_jabatan', 'id_jabatan', $data['id'], 'D', json_encode($data_test), 'Delete Data Event', 'log_sso');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }
}