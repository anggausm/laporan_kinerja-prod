<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_asesor extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

    public function get_data()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $periode = $this->db->select('id_periode,nama_periode,kategori')->get_where('kinerja_periode', array('bulan_tahun' => $data['bulan_tahun']))->result_array();
        $result['periode'] = $periode;
        $id_periode_list = array_column($periode, 'id_periode');
        $id_periode_list = empty($id_periode_list) ? array('aa') : $id_periode_list;
        $this->db->from('kinerja_asesor');
        $this->db->where_in('id_periode', $id_periode_list);
        $query = $this->db->get();
        $result['asesor'] = $query->result_array();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
    public function get_periode()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $this->db->select('bulan_tahun,nama_periode');
        $this->db->from('kinerja_periode');
        $this->db->group_by('bulan_tahun');
        $this->db->order_by('id_periode', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }

    public function insert()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('bulan_tahun', 'Bulan Tahun', 'required');

        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == false) {
            return $this->app->respons_data(false, validation_errors(), 200);
        }
        if ($data['dinilai_jenis_pekerjaan'] == 'Tenaga Pendidik') {
            $kategori_periode = 'Dosen';
        } else {
            $kategori_periode = 'Kependidikan';
        }
        $periode = $this->db->get_where('kinerja_periode', array('bulan_tahun' => $data['bulan_tahun'], 'kategori' => $kategori_periode))->row_array();
        if (empty($periode)) {
            return $this->app->respons_data(false, "Data periode tidak ada, untuk $data[bulan_tahun] kategori $kategori_periode", 200);
        }

        $data['kategori'] = empty($data['kategori']) ? 'teman' : $data['kategori'];
        if ($data['dinilai_id_doskar'] == $data['penilai_id_doskar']) {
            return $this->app->respons_data(false, 'Data gagal disimpan, yang dinilai dan penilai tidak boleh sama', 200);
        }
        $check_exist = $this->db->get_where('kinerja_asesor', array('dinilai_id_doskar' => $data['dinilai_id_doskar'], 'penilai_id_doskar' => $data['penilai_id_doskar'], 'id_periode' => $periode['id_periode']))->row_array();
        if (!empty($check_exist)) {
            return $this->app->respons_data(false, 'Data gagal disimpan, data sudah ada', 200);
        }
        $jm_asesor = $this->db->get_where('kinerja_asesor', array('dinilai_id_doskar' => $data['dinilai_id_doskar'], 'kategori' => 'teman', 'id_periode' => $periode['id_periode']))->num_rows();
        if ($jm_asesor >= 3 && $data['kategori'] == 'teman') {
            return $this->app->respons_data(false, 'Data gagal disimpan, data asesor rekan kerja maksimal 3', 200);
        }
        $this->db->trans_start();
        $datas = array(
            'dinilai_id_doskar' => $data['dinilai_id_doskar'],
            'dinilai_user_key' => $data['dinilai_user_key'],
            'dinilai_nama' => $data['dinilai_nama'],
            'dinilai_fak_unit' => $data['dinilai_fak_unit'],
            'dinilai_nama_unit' => $data['dinilai_nama_unit'],
            'penilai_id_doskar' => $data['penilai_id_doskar'],
            'dinilai_jenis_pekerjaan' => $data['dinilai_jenis_pekerjaan'],
            'dinilai_status_kerja' => $data['dinilai_status_kerja'],
            'penilai_user_key' => $data['penilai_user_key'],
            'penilai_nama' => $data['penilai_nama'],
            'penilai_fak_unit' => $data['penilai_fak_unit'],
            'penilai_nama_unit' => $data['penilai_nama_unit'],
            'penilai_jenis_pekerjaan' => $data['penilai_jenis_pekerjaan'],
            'penilai_status_kerja' => $data['penilai_status_kerja'],
            'kategori' => $data['kategori'],
            'id_periode' => $periode['id_periode'],
            'c' => $token['user_key'],
            'dc' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('kinerja_asesor', $datas);
        $id_asesor = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal disimpan', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_asesor', 'id_asesor', $id_asesor, 'C', json_encode($datas), '', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
        }
    }

    public function delete()
    {
        $token = $this->tkn;
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $data_log = $this->db->get_where('kinerja_asesor', array('id_asesor' => $data['id_asesor']))->row_array();
        $this->db->trans_start();
        $this->db->delete('kinerja_asesor', array('id_asesor' => $data['id_asesor']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return $this->app->respons_data(false, 'Data gagal dihapus', 200);
        } else {
            $this->db->trans_commit();
            $this->log_app->log_data($token['user_key'], 'kinerja_asesor', 'id_asesor', $data['id_asesor'], 'D', json_encode($data_log), '', 'log_kinerja');
            return $this->app->respons_data(true, 'Data berhasil dihapus', 200);
        }
    }
}
