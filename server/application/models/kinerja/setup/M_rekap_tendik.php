<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_rekap_tendik extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $data = json_decode(file_get_contents("php://input"));

        $sql = "SELECT  kinerja_rekap_nilai.id,
                        kinerja_rekap_nilai.user_key,
                        kinerja_rekap_nilai.id_periode,
                        kinerja_rekap_nilai.skor,
                        kinerja_rekap_nilai.nilai,
                        kinerja_rekap_nilai.kategori,
                        kinerja_rekap_nilai.`status`,
                        kinerja_rekap_nilai.ket,
                        kinerja_mst_pegawai.nama_doskar,
                        kinerja_mst_pegawai.nama_unit,
                        kinerja_mst_pegawai.pekerjaan,
                        kinerja_periode.nama_periode
                        FROM
                        kinerja_rekap_nilai
                        JOIN kinerja_mst_pegawai ON kinerja_rekap_nilai.user_key = kinerja_mst_pegawai.user_key
                        JOIN kinerja_periode ON kinerja_rekap_nilai.id_periode = kinerja_periode.id_periode
                        WHERE kinerja_rekap_nilai.id_periode = '$data->periode'
                        ";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function cek_insert($id = '')
    {
         $token = $this->tkn;
         $userkey = $token['user_key'];
        $sql = "SELECT id_lpp, userkey FROM kinerja_lpp WHERE periode_lpp = '$id' AND userkey = '$userkey'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insert_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
       
        $data_insert = array(
            "nama_event" => $data['nama_event'],
            "tanggal_mulai" => $data['tgl_awal'],
            "tanggal_selesai" => $data['tgl_akhir'],
            "jam_mulai" => $data['jam_awal'],
            "jam_selesai" => $data['jam_awal'],
            "lokasi_event" => $data['lokasi_event'],
            "ket" => $data['ket'],
            "dc" => date("Y-m-d h:i:s"),
            "c" => $token['user_key']
        );


            if ($this->db->insert('kinerja_event', $data_insert)) {
                $id = $this->db->insert_id();
                $this->log_app->log_data($token['user_key'], 'kinerja_event', 'id_event', $id, 'C', json_encode($data_insert), 'Insert Data Event', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal disimpan', 200);
            }

        
    }

    public function edit() {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT *
                FROM kinerja_event
                WHERE id_event = ?";
        $query = $this->db->query($sql, $data['id']);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }
    }

    public function update_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $token = $this->tkn;
                
        $data_update = array(
            "nama_event" => $data['nama_event'],
            "tanggal_mulai" => $data['tgl_awal'],
            "tanggal_selesai" => $data['tgl_akhir'],
            "jam_mulai" => $data['jam_awal'],
            "jam_selesai" => $data['jam_akhir'],
            "lokasi_event" => $data['lokasi_event'],
            "ket" => $data['ket'],
            "u" => $token['user_key'],
            "du" => date("Y-m-d h:i:s")
        );

            if ($this->db->update('kinerja_event', $data_update, array('id_event' => $data['id_event']))) {
                $this->log_app->log_data($token['user_key'], 'kinerja_event', 'id_event', $data['id_event'], 'U', json_encode($data_update), 'Update Data LPP', 'log_sso');
                return $this->app->respons_data(array(), 'Data berhasil update', 200);
            } else {
                return $this->app->respons_data(array(), 'Data gagal update', 200);
            }
    }

    public function get_data_edit($id = '')
    {
        $data = json_decode(file_get_contents("php://input"), true);
        $sql = "SELECT * FROM kinerja_lpp WHERE id_lpp = '$id'";
        
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return true;
        } else {
            return false;
        }
    }

    public function delete_data()
    {
        $data = json_decode(file_get_contents("php://input"), true);

        $token = $this->tkn;
        $data_test = $this->get_data_edit($data['id']);
        
        if (empty($data)) {
            return $this->app->respons_data(array(), 'Data kosong', 200);
        }
        if ($this->db->delete('kinerja_lpp', array('id_lpp' => $data['id']))) {
            $this->log_app->log_data($token['user_key'], 'kinerja_lpp', 'id_lpp', $data['id'], 'D', json_encode($data_test), 'Delete Data LPP', 'log_sia');
            return $this->app->respons_data(array(), 'Data berhasil dihapus', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal dihapus', 200);
        }
    }
}