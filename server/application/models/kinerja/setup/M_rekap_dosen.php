<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_rekap_dosen extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

    public function get_data()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();
        $sql = "SELECT a.id_nilai,a.dinilai_user_key,a.penilai_user_key
        ,SUM(CASE WHEN b.urutan LIKE '1%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_akademik
        ,SUM(CASE WHEN b.urutan LIKE '2%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_pendidikan
        ,SUM(CASE WHEN b.urutan LIKE '3%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_penelitian
        ,SUM(CASE WHEN b.urutan LIKE '4%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_pkm
        ,SUM(CASE WHEN b.urutan LIKE '5%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_penunjang
        ,SUM(CASE WHEN b.urutan LIKE '6%' THEN IFNULL(a.nilai,0) ELSE 0 END) nilai_core_value
        ,SUM(CASE WHEN b.urutan LIKE '6%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_core_value
        ,SUM(IFNULL(a.nilai,0)) total_nilai
        ,SUM(CASE WHEN b.urutan LIKE '1%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_akademik
        ,SUM(CASE WHEN b.urutan LIKE '2%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_pendidikan
        ,SUM(CASE WHEN b.urutan LIKE '3%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_penelitian
        ,SUM(CASE WHEN b.urutan LIKE '4%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_pkm
        ,SUM(CASE WHEN b.urutan LIKE '5%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_penunjang
        ,SUM(CASE WHEN b.urutan LIKE '6%' THEN IFNULL(a.nilai_pimpinan,0) ELSE 0 END) nilai_pimpinan_core_value
        ,SUM(CASE WHEN b.urutan LIKE '6%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_core_value
        ,SUM(IFNULL(a.nilai_pimpinan,0)) total_pimpinan_nilai
        ,SUM(CASE WHEN b.urutan LIKE '1%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_akademik
        ,SUM(CASE WHEN b.urutan LIKE '2%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_pendidikan
        ,SUM(CASE WHEN b.urutan LIKE '3%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_penelitian
        ,SUM(CASE WHEN b.urutan LIKE '4%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_pkm
        ,SUM(CASE WHEN b.urutan LIKE '5%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_penunjang
        ,SUM(CASE WHEN b.urutan LIKE '6%' THEN IFNULL(a.nilai_univ,0) ELSE 0 END) nilai_univ_core_value
        ,SUM(IFNULL(a.nilai_univ,0)) total_univ_nilai
        FROM kinerja_nilai a
        JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        WHERE b.kategori='Dosen'
        GROUP BY a.dinilai_user_key 
        ";
        $query = $this->db->query($sql);
        $result['rekap'] = $query->result_array();
        $query->free_result();

        $sql = "SELECT c.nama_doskar,d.nama_doskar as penilai_nama,a.*
        FROM kinerja_nilai a
        JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        LEFT JOIN doskar_usm c ON a.dinilai_user_key=c.user_key
        LEFT JOIN doskar_usm d ON a.penilai_user_key=d.user_key
        WHERE b.kategori='Dosen' AND a.nilai > 0
        ";
        $query = $this->db->query($sql);
        // $result['cek_link'] = $query->result_array();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
}
