<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_rekap_kependidikan extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('form_validation');
    }

    public function get_data()
    {
        $data = is_array(json_decode(file_get_contents("php://input"), true)) ? json_decode(file_get_contents("php://input"), true) : array();

        // $sql = "SELECT SUM(IFNULL(nilai,0)) jm_nilai,c.nama_doskar,c.id_doskar,b.jenis,b.kategori,d.nama_doskar AS nama_penilai,a.*  
        // FROM kinerja_nilai a 
        // JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        // LEFT JOIN doskar_usm c ON a.dinilai_user_key=c.user_key 
        // LEFT JOIN doskar_usm d ON a.penilai_user_key=d.user_key 
        // WHERE b.jenis='Tenaga Kependidikan'
        // GROUP BY a.dinilai_user_key,a.penilai_user_key
        // ORDER BY a.dinilai_user_key,b.kategori,a.dc";

        $sql = "SELECT SUM(IFNULL(nilai,0)) jm_nilai,b.jenis,b.kategori,a.*  
        FROM kinerja_nilai a 
        JOIN kinerja_pernyataan b ON a.id_pertanyaan=b.id_pertanyaan
        WHERE b.jenis='Tenaga Kependidikan'
        GROUP BY a.dinilai_user_key,a.penilai_user_key
        ORDER BY a.dinilai_user_key,b.kategori,a.dc";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $this->app->respons_data($result, 'Data berhasil diload', 200);
    }
}
