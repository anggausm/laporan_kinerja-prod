<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_jafa extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {

    }

    public function default()
    {
        $sql    = "SELECT a.id, a.ajuan_jafa, IF(b.j_data IS NULL, '0', b.j_data) j_peserta
                    FROM jafa_jenis_ajuan a
                        LEFT JOIN
                        (
                            SELECT a.id_jafa_ajuan, COUNT(*) j_data -- c.nama_doskar, a.bidang_ilmu, b.nama_jafa, b.kum
                            FROM jafa_data_ajuan a 
                                JOIN jafa_jenis b ON a.id_jafa = b.id_jafa
                                JOIN doskar_usm c ON a.user_key = c.user_key
                            GROUP BY a.id_jafa_ajuan
                        ) b ON a.id = b.id_jafa_ajuan
                    ORDER BY a.urutan";

        $query  = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('rs' => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('rs' => ''), 'data gagal diload', 200);
        }
    }

    public function dosen_usm($data = array())
    {
        $sql = "SELECT user_key, nama_doskar FROM doskar_usm ORDER BY nama_doskar";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function jabfung($data = array())
    {
        $sql = "SELECT id_jafa, CONCAT(nama_jafa, ' (', kum, ')') nama_jafa FROM jafa_jenis ORDER BY id_jafa";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function jenis_ajuan($data = array())
    {
        $sql = "SELECT * FROM jafa_jenis_ajuan ORDER BY urutan";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function jenis_pengajuan($data = array())
    {
        $data   = json_decode(file_get_contents("php://input"));
        $id     = $data->id;
        $sql    = "SELECT * FROM jafa_jenis_ajuan WHERE id = ?";
        $query  = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function insert($data = array())
    { 
        $data           = json_decode(file_get_contents("php://input"));
        $token          = $this->tkn;
        $user_key       = $token['user_key'];
        $tgl            = date('Y-m-d h:i:s');

        $sql = "INSERT INTO jafa_data_ajuan (id_data, user_key, bidang_ilmu, id_jafa, id_jafa_ajuan, c, dc) 
                    VALUE (?, ?, ?, ?, ?, ?, ?)";

        if ($this->db->query($sql, array('', $data->user_key, $data->bidang_ilmu, $data->id_jafa, '1', $user_key, $tgl))) 
        {
            $id_data    = $this->get_id();
            $this->log_app->log_data($user_key, 'jafa_data_ajuan', 'id_data', $id_data, 'C', json_encode(array('id_data' => $id_data, 'user_key' => $data->user_key, 'bidang_ilmu' => $data->bidang_ilmu, 'id_jafa' => $data->id_jafa, 'id_jafa_ajuan' => '1')), ' ', 'log_pengajuan_jafa');
            return $this->app->respons_data(array('id_data' => $id_data), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan simpan', 200);
        }
    }

    public function get_id()
    {
        $sql = "SELECT id_data FROM jafa_data_ajuan ORDER BY id_data DESC LIMIT 1";
        $query = $this->db->query($sql, array());
        $result = $query->row_array();
        $query->free_result();
        return $result['id_data'];
    }

    public function detail()
    {
        $data   = json_decode(file_get_contents("php://input"));
        $id     = $data->id_jenis;
        $sql    = "SELECT a.id_data, a.id_jafa_ajuan, c.nama_doskar, a.bidang_ilmu, b.nama_jafa, b.kum, c.nidn
                    FROM jafa_data_ajuan a 
                        JOIN jafa_jenis b ON a.id_jafa = b.id_jafa
                        JOIN doskar_usm c ON a.user_key = c.user_key
                    WHERE a.id_jafa_ajuan = ?";

        $query  = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data(array('rs' => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('rs' => ''), 'data gagal diload', 200);
        }
    }

    public function edit()
    {
        $data   = json_decode(file_get_contents("php://input"));
        $id     = $data->id_data;
        $sql    = "SELECT id_data, user_key, bidang_ilmu, id_jafa, id_jafa_ajuan
                    FROM jafa_data_ajuan
                    WHERE id_data = ?";

        $query  = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data(array('rs' => $result), 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array('rs' => ''), 'data gagal diload', 200);
        }
    }

    public function update($data = array())
    { 
        $data           = json_decode(file_get_contents("php://input"));
        $token          = $this->tkn;
        $user_key       = $token['user_key'];

        $sql = "UPDATE jafa_data_ajuan 
                SET user_key        = ?, 
                    bidang_ilmu     = ?, 
                    id_jafa         = ?, 
                    id_jafa_ajuan   = ?, 
                    u               = ?
                WHERE id_data = ? ";
        if ($this->db->query($sql, array($data->user_key, $data->bidang_ilmu, $data->id_jafa, $data->id_jafa_ajuan, $user_key, $data->id_data))) 
        {
            $this->log_app->log_data($user_key, 'jafa_data_ajuan', 'id_data', $data->id_data, 'U', 
                json_encode(array('id_data'         => $id_data, 
                                    'user_key'      => $data->user_key, 
                                    'bidang_ilmu'   => $data->bidang_ilmu, 
                                    'id_jafa'       => $data->id_jafa, 
                                    'id_jafa_ajuan' => $data->id_jafa_ajuan, 
                                    'u'             => $user_key )), 
                ' ', 'log_pengajuan_jafa');
            return $this->app->respons_data(array(), 'Data berhasil simpan', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal disimpan simpan', 200);
        }
    }


}
