<?php

/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_versi extends CI_Model
{

    //construktor
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }
    public function get_data()
    {
        $sql = "SELECT versi, tgl_rilis, url_download, keterangan FROM `versi_app` WHERE aktif ='1'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
}
