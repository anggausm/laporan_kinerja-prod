<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_ijin_histori extends CI_Model
{
    //construktor
    public $tb_absensi_histori = 'absensi_histori';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $sql = "SELECT a.id, c.nama_doskar, a.user_key, a.tanggal_awal, a.tanggal_akhir, a.keterangan, a.status, b.jenis_absensi
                FROM absensi_ijin a
                    JOIN absensi_jenis b    ON a.jenis_ijin = b.id_jenis
                    JOIN doskar_usm c       ON a.user_key   = c.user_key
                WHERE (a.status     = ? OR a.status = ?)
                    AND b.kategori  = ?
                ORDER BY a.id DESC";
        $query = $this->db->query($sql, array('Acc', 'Ditolak', 'Ijin'));

        if ($query->num_rows() > 0) 
        {
            $result = $query->result_array();
            $query->free_result();
            // $rs = array('rs' => $result);
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }  
    }
}