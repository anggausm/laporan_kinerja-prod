<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_cuti extends CI_Model
{
    //construktor
    public $tb_absensi_histori = 'absensi_histori';
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

    }
    
    public function get_data()
    {
        $sql = "SELECT a.id, c.nama_doskar, a.user_key, a.tanggal_awal, a.tanggal_akhir, a.keterangan, a.status, b.jenis_absensi
                FROM absensi_cuti a
                    JOIN absensi_jenis b    ON a.jenis_cuti = b.id_jenis
                    JOIN doskar_usm c       ON a.user_key   = c.user_key
                WHERE a.status      = ?
                    AND b.kategori  = ?
                ORDER BY a.id DESC";
        $query = $this->db->query($sql, array('Menunggu', 'Cuti'));

        if ($query->num_rows() > 0) 
        {
            $result = $query->result_array();
            $query->free_result();
            // $rs = array('rs' => $result);
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'Data gagal diload', 200);
        }  
    }

    public function doskar_usm($data = array())
    {
        // $sql = "SELECT user_key, nama_doskar FROM doskar_usm ORDER BY nama_doskar";
        $sql    = "SELECT a.user_key, a.nama_doskar FROM doskar_usm a
                        JOIN absensi_peserta b ON a.user_key = b.user_key
                    WHERE a.aktif       = ?
                        AND b.status    = ?
                    ORDER BY a.nama_doskar";
        $query  = $this->db->query($sql, array('Aktif', 'Finjer'));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function get_doskar($user_key = '')
    {
        $sql = "SELECT user_key, shift_finjer FROM doskar_usm WHERE user_key = '$user_key'";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function jenis_cuti($data = array())
    {
        $sql = "SELECT id_jenis, jenis_absensi FROM absensi_jenis WHERE status = ? AND kategori = ?";
        $query = $this->db->query($sql, array('Aktif', 'Cuti'));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function insert()
    {
        $input      = file_get_contents("php://input");
        $data       = json_decode($input, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];
        $tahun_awal = substr($data['tanggal_awal'], 0,4);
        $tahun_akhir= substr($data['tanggal_akhir'], 0,4);
        $tahun      = substr($data['tanggal_awal'], 0,4);

        // cek tanggal pengajuan
        if ($data['tanggal_akhir'] >= $data['tanggal_awal']) 
        {

            // CEK TAHUN PENGAJUNA
            if ($tahun_awal == $tahun_akhir) 
            {
                // cek banyak ijin dalam sebulan
                // $banyak_cuti    = $this->cek_cuti($data);
                // $max_cuti       = $this->max_cuti($data);

                // if ($banyak_cuti < $max_cuti) 
                // { 
                $sql    = "INSERT INTO absensi_cuti (id, user_key, tanggal_awal, tanggal_akhir, tahun,
                                                    jenis_cuti, keterangan, status, c, dc)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())";
                $query  = $this->db->query($sql, array('', $data['user_key'], $data['tanggal_awal'], $data['tanggal_akhir'], $tahun,
                                                            $data['jenis_cuti'], $data['keterangan'], 'Menunggu', $user_key));
                if($query)
                {
                    // INSERT LOG
                    $data_log   = array('user_key'          => $data['user_key'], 
                                            'tanggal_awal'  => $data['tanggal_awal'],
                                            'tanggal_akhir' => $data['tanggal_akhir'],
                                            'jenis_cuti'    => $data['jenis_cuti'],
                                            'keterangan'    => $data['keterangan'],
                                            'tahun'         => $tahun,
                                            'status'        => 'Menunggu',
                                            'c'             => $user_key);
                    $dt_log     = json_encode($data_log);
                    $id_cuti    = $this->id_cuti_akhir();

                    $this->log_app->log_data($user_key, 'absensi_cuti', 'id', $id_cuti, 'C', $dt_log, ' ', 'log_absensi_cuti');

                    return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
                }
                else
                {
                    return $this->app->respons_data(false, 'Data GAGAL disimpan', 200);
                }
            }
            else
            {
                return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>Tahun pengajuan awal dan akhir harus sama.', 200);
            }
        }
        else
        {
            return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>Tanggal Akhir tidak boleh kurang dari Tanggal Awal', 200);
        }  
    }
/*
    public function cek_cuti($data = '')
    {
        $bt         = substr($data['tanggal_awal'], 0,7);
        $user_key   = $data['user_key'];
        $sql        = "SELECT SUM((DATEDIFF(a.tanggal_akhir, a.tanggal_awal) + 1)) jml
                        FROM absensi_ijin a
                        WHERE a.bulan_tahun     = ?
                            AND a.user_key      = ?
                            AND (a.status       = ? OR a.status = ?)
                        GROUP BY a.bulan_tahun";
        $query  = $this->db->query($sql, array($bt, $user_key, 'Menunggu', 'Acc'));
        $result = $query->row_array();
        return $result['jml'];
    }

    public function max_cuti($data = '')
    {
        $sql    = "SELECT max FROM absensi_jenis a WHERE a.id_jenis = ?";
        $query  = $this->db->query($sql, $data['jenis_ijin']);
        $result = $query->row_array();
        return $result['max'];
    }
*/
    public function edit() 
    {
        $input      = file_get_contents("php://input");
        $data       = json_decode($input, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];

        $sql        = "SELECT id, user_key, tanggal_awal, tanggal_akhir, jenis_cuti, keterangan, status
                        FROM absensi_cuti
                        WHERE id = ?";
        $query      = $this->db->query($sql, $data['id']);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            $result;
        }
        $rs = array('rs' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function update()
    {
        $input      = file_get_contents("php://input");
        $data       = json_decode($input, true);
        $token      = $this->tkn;
        $user_key   = $token['user_key'];

        $tahun_awal = substr($data['tanggal_awal'], 0,4);
        $tahun_akhir= substr($data['tanggal_akhir'], 0,4);
        $tahun      = substr($data['tanggal_awal'], 0,4);
        // $date_now   = date('Y-m-d');

        // CEK TANGGAL PENGAJUAN TIDAK BOLEH SAMA DENGAN TANGGAL SEKARANG
        // if ($data['tanggal_awal'] < $date_now && $data['tanggal_akhir'] < $date_now) 
        // {
            
            // cek tanggal pengajuan
            if ($data['tanggal_akhir'] >= $data['tanggal_awal']) 
            {
                // CEK TAHUN PENGAJUNA
                if ($tahun_awal == $tahun_akhir) 
                {
                    // cek banyak ijin dalam sebulan
                    // $banyak_ijin    = $this->cek_ijin($data);
                    // $max_ijin       = $this->max_ijin($data);

                    // if ($banyak_ijin <= $max_ijin) 
                    // {
                        $sql    = "UPDATE absensi_cuti
                                    SET user_key        = ?,
                                        tanggal_awal    = ?,
                                        tanggal_akhir   = ?,
                                        tahun           = ?,
                                        jenis_cuti      = ?, 
                                        keterangan      = ?,
                                        status          = ?, 
                                        u               = ?
                                    WHERE id            = ? ";
                        $query  = $this->db->query($sql, 
                                    array($data['user_key'], 
                                            $data['tanggal_awal'], 
                                            $data['tanggal_akhir'], 
                                            $tahun,
                                            $data['jenis_cuti'], 
                                            $data['keterangan'], 
                                            $data['status'], 
                                            $user_key,
                                            $data['id']));
                        if($query)
                        {
                            // insert histori absen
                            if ($data['status'] == 'ACC') 
                            {
                                $this->absensi_histori($data);
                            }

                            // INSERT LOG
                            $this->log_app->log_data($user_key, 'absensi_cuti', 'id', $data['id'], 'U', $input, ' ', 'log_absensi_cuti');

                            return $this->app->respons_data(true, 'Data berhasil disimpan', 200);
                        }
                        else
                        {
                            return $this->app->respons_data(false, 'Data GAGAL disimpan', 200);
                        }
                    // }
                    // else
                    // {
                    //     return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>
                    //         Akumulasi ijin pada bulan ini sudah '.$banyak_ijin.' kali. <br>
                    //         Maksimal ijin dalam satu bulan adalah '.$max_ijin.' kali.  <br>
                    //         Silahkan cek kembali tanggal pengajuan saat ini.', 200);
                    // }
                }
                else
                {
                    return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>Tahun pengajuan awal dan akhir harus sama.', 200);
                }
            }
            else
            {
                return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>Tanggal Akhir tidak boleh kurang dari Tanggal Awal', 200);
            }
            
        // }
        // else
        // {
        //     return $this->app->respons_data(false, 'Data GAGAL disimpan,<br>Tanggal pengajuan tidak boleh sama dengan tanggal saat ini.', 200);
        // } 
    }

    public function absensi_histori($data = '')
    {
        // ambil tanggal ijin 
        $awal   = $data['tanggal_awal'];
        $akhir  = $data['tanggal_akhir'];
        while (strtotime($awal) <= strtotime($akhir)) 
        {
            // cek tanggal
            $cek_tanggal    = $this->tanggal_cuti($awal);
            if ($cek_tanggal == '1') 
            {
                // cek histori
                $cek_histori = $this->cek_histori($data['user_key'], $awal);
                if ($cek_histori == '0') 
                {
                    # insert histori
                    $this->insert_histori($data['user_key'], $awal);
                }
                else
                {
                    # update histori
                    $this->update_histori($data['user_key'], $awal, $data['id']);
                }
            }

            $awal = date ("Y-m-d", strtotime("+1 day", strtotime($awal))); //looping tambah 1 date
        }
    }

    public function jenis_absensi($jenis = '')
    {
        $sql    = "SELECT a.jenis_absensi, a.field FROM absensi_jenis a WHERE a.id_jenis = ?";
        $query  = $this->db->query($sql, $jenis);
        $result = $query->row_array();
        return $result;
    }

    public function tanggal_cuti($tanggal = '')
    {
        $sql    = "SELECT a.tanggal
                    FROM wfh_kalender a
                    WHERE a.tanggal = ?
                        AND (a.hari = 'Senin' OR a.hari = 'Selasa'  OR a.hari = 'Rabu' OR a.hari = 'Kamis' OR a.hari = 'Jumat')
                        AND a.status = '0'
                    ORDER BY a.tanggal";
        $query  = $this->db->query($sql, $tanggal);
        if ($query->num_rows() > 0) {
            $result = '1';
            return $result;
        } else {
            $result = '0';
            return $result;
        }
    }

    public function cek_histori($user_key = '', $tanggal = '')
    {
        $sql    = "SELECT * 
                    FROM absensi_histori a 
                    WHERE a.user_key = ?
                        AND a.tanggal = ? ";
        $query  = $this->db->query($sql, array($user_key, $tanggal));
        if ($query->num_rows() > 0) {
            $result = '1';
            return $result;
        } else {
            $result = '0';
            return $result;
        }
    }

    public function insert_histori($user_key = '', $tanggal = '')
    {
        $token      = $this->tkn;
        $user_token = $token['user_key'];
        $tarif      = $this->tarif_transport($user_key);

        $dt                     = array();
        $dt['id']               = '';
        $dt['user_key']         = $user_key;
        $dt['id_absensi']       = '';
        $dt['tanggal']          = $tanggal;
        $dt['telat_masuk']      = '0';
        $dt['telat_pulang']     = '0';
        $dt['bolos']            = '0';
        $dt['hadir']            = '0';
        $dt['status_transport'] = '1';
        $dt['tarif']            = $tarif;
        $dt['area_masuk']       = '0';
        $dt['area_pulang']      = '0';
        $dt['ijin_masuk']       = '0';
        $dt['ijin_pulang']      = '0';
        $dt['cuti']             = '1';
        $dt['tugas']            = '0';
        $dt['sakit']            = '0';
        $dt['hari_khusus']      = '0';
        

        // PROSES INSERT DATA HISTORI ABSENSI
        if($this->db->insert($this->tb_absensi_histori, $dt))
        {
            $dt_log     = json_encode($dt);
            $id_histori = $this->id_histori_akhir();
            // insert log
            $this->log_app->log_data($user_token, 'absensi_histori', 'id', $id_histori, 'C', $dt_log, ' ', 'log_absensi_histori');
        }
    }

    public function update_histori($user_key, $tanggal, $id)
    {
        $token      = $this->tkn;
        $user_token = $token['user_key'];
        $tarif      = $this->tarif_transport($user_key);

        $dt                     = array();
        $dt['status_transport'] = '1';
        $dt['tarif']            = $tarif;
        $dt['cuti']             = '1';

        $this->db->where('id', $id);
        if($this->db->update($this->tb_absensi_histori, $dt))
        {
            // insert log
            $dt_log = json_encode($dt);
            $this->log_app->log_data($user_token, 'absensi_histori', 'id', $id, 'U', $dt_log, ' ', 'log_absensi_histori');
        }
    }

    public function tarif_transport($user_key)
    {
        $sql    = "SELECT b.biaya_transport
                    FROM absensi_peserta a
                        JOIN absensi_tarif b ON a.id_tarif = b.id_tarif
                    WHERE a.user_key = ? ";
        $query  = $this->db->query($sql, $user_key);
        $result = $query->row_array();
        return $result['biaya_transport'];
    }

    public function id_cuti_akhir()
    {
        $sql    = "SELECT id FROM absensi_cuti ORDER BY id DESC LIMIT 1 ";
        $query  = $this->db->query($sql);
        $result = $query->row_array();
        return $result['id'];
    }

    public function id_histori_akhir()
    {
        $sql    = "SELECT id FROM absensi_histori ORDER BY id DESC LIMIT 1 ";
        $query  = $this->db->query($sql);
        $result = $query->row_array();
        return $result['id'];
    }

    public function get_id_histori($user_key, $tanggal)
    {
        $sql    = "SELECT a.id FROM absensi_histori a WHERE a.user_key = ? AND a.tanggal = ? ";
        $query  = $this->db->query($sql, array($user_key, $tanggal));
        $result = $query->row_array();
        return $result;
    }

    public function get_absensi_histori($id)
    {
        $sql    = "SELECT a.telat_masuk, a.telat_pulang FROM absensi_histori a WHERE a.id = ? ";
        $query  = $this->db->query($sql, $id);
        $result = $query->row_array();
        return $result;
    }
}