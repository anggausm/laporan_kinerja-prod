<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_dosen extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
    }
    public function posisi_user($user_key = '')
    {

        $sql = "SELECT a.*, b.id_struktural,  b.kode_sto, b.`level` FROM
                (SELECT b.id_struktural, b.unit_bagian, b.nama_sub_struktural, b.kode_sto, b.`level`, a.*
                FROM (SELECT id_sub_struktural, user_key, fakultas, kode_khusus, keterangan
                FROM struktural_anggota WHERE user_key ='$user_key' and id_sub_struktural !='79')a
                JOIN struktural_sub b on a.id_sub_struktural = b.id_sub_struktural)a
                LEFT JOIN (SELECT id_struktural, id_sub_struktural, kode_sto, `level` FROM `struktural_sub` WHERE `level` ='2' or `level` ='1')b
                on a.id_sub_struktural = b.id_sub_struktural";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function rektorat()
    {
        $sql = "SELECT a.*, b.nama_doskar, b.nickname, b.nidn FROM (SELECT a.*, b.user_key, b.fakultas, b.kode_khusus FROM (
            SELECT id_sub_struktural, id_struktural, nama_sub_struktural, kode_sto, `level`
            FROM struktural_sub  WHERE `level` ='1' or `level` ='2' )a
            JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural   )a
            JOIN doskar_usm b on a.user_key = b.user_key
            ORDER BY a.fakultas ASC,  a.kode_khusus DESC , a.id_struktural, a.`level`, a.kode_sto ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function angota_fakultas($kode_sto = '', $fakultas = '', $kode_khusus = '')
    {
        if (empty($kode_khusus)) {
            $cek = "";
        } else {
            $cek = "and  kode_khusus ='$kode_khusus'";
        }
        if (empty($kode_sto)) {
            return array();
        }
        $sql = "SELECT a.*, b.nama_doskar, if(z_st_user ='B', SUBSTR(a.user_key,1,8),'*sama dengan password SIA') pas, b.nickname, b.nidn FROM (SELECT a.*, b.user_key FROM (
            SELECT id_sub_struktural, id_struktural, nama_sub_struktural, kode_sto, `level`
            FROM struktural_sub WHERE `kode_sto` like '$kode_sto%')a
            JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural WHERE fakultas ='$fakultas' $cek )a
            JOIN doskar_usm b on a.user_key = b.user_key
            ORDER BY a.`level`, a.kode_sto ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function angota_bauk_baak_unit($kode_sto = '')
    {
        if (empty($kode_sto)) {
            return array();
        }
        $sql = "SELECT a.*, b.nama_doskar, if(z_st_user ='B', SUBSTR(a.user_key,1,8),'*sama dengan password SIA') pas,  b.nickname, b.nidn FROM (SELECT a.*, b.user_key FROM (
                SELECT id_sub_struktural, id_struktural, nama_sub_struktural, kode_sto, `level`
                FROM struktural_sub WHERE `kode_sto` like '$kode_sto%')a
                JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural )a
                JOIN doskar_usm b on a.user_key = b.user_key
                ORDER BY a.`level`, a.kode_sto ASC";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_angggota()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key'];

        $posisi_user = $this->posisi_user($user_key);

        if ($posisi_user['id_struktural'] == '1') {
            //REKTORAT
            $rs_data = $this->rektorat();
        } elseif ($posisi_user['id_struktural'] == '4') {
            // FAKULTAS
            $rs_data = $this->angota_fakultas($posisi_user['kode_sto'] . '.', $posisi_user['fakultas'], $posisi_user['kode_khusus']);
        } elseif ($posisi_user['id_struktural'] == '5') {
            // PASCA
            $rs_data = $this->angota_fakultas($posisi_user['kode_sto'] . '.', $posisi_user['fakultas'], $posisi_user['kode_khusus']);
        } else {
            $rs_data = $this->angota_bauk_baak_unit($posisi_user['kode_sto']);
        }

        if ($posisi_user['level'] == '3') {
            $result = array('posisi_user' => $posisi_user, 'rs_data' => array());
        } else {
            $result = array('posisi_user' => $posisi_user, 'rs_data' => $rs_data);
        }
        return $this->app->respons_data($result, 'Daat berhasil di load', 200);
    }
}
