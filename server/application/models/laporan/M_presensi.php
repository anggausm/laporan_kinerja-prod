<?php
/*
 * copyright : Wahyu Budi Santosa
 * email : wahyubudisantosa@gmail.com
 * @dudoks
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class M_presensi extends CI_Model
{
//construktor
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
    }
    public function get_level_akun($user_key = '')
    {
        $sql = "SELECT a.*, c.id_struktural, b.id_sub_struktural, fakultas, kode_khusus, keterangan , c.unit_bagian, c.nama_sub_struktural, kode_sto, `level`, level_pimpinan FROM (
				SELECT user_key, nis, nidn, nama_doskar FROM doskar_usm WHERE user_key = ? )a
				JOIN (
				SELECT user_key, id_sub_struktural, fakultas, kode_khusus, keterangan
				FROM struktural_anggota WHERE keterangan !='Dosen' and id_sub_struktural !='60' and id_sub_struktural !='70'
				) b on a.user_key = b.user_key
				JOIN struktural_sub c on b.id_sub_struktural = c.id_sub_struktural
				";
        $query = $this->db->query($sql, array($user_key));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_angota_biro($kode_sto = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%' and level_pimpinan !='')a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_kabag($kode_sto = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
                (SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%' )a
                JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key = c.user_key )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_dekan($kode_sto = '', $fakultas = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%'  and level_pimpinan !='')a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key
				WHERE b.fakultas = ? )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_wadek1($kode_sto = '', $fakultas = '')
    {
        $date = date('Y-m-d');
        if ($fakultas == 'A' or $fakultas == 'D' or $fakultas == 'F') {
            $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
                (SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '4.%' )a
                JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key = c.user_key
                WHERE b.fakultas = ?
               )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
            $query = $this->db->query($sql, array($fakultas));
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $query->free_result();
                return $result;
            } else {
                return array();
            }
        }

    }
    public function get_angota_wadek2($kode_sto = '', $fakultas = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
                (SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%' )a
                JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key = c.user_key
                WHERE b.fakultas = ? )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_angota_kaprogdi($kode_sto = '', $kode_khusus = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%')a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key
				WHERE kode_khusus ='$kode_khusus'
				)a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY c.nama_doskar";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_ktu($kode_sto = '', $fakultas = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%'   )a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key
				WHERE b.fakultas = ?  )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key  ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_upt_lembaga($kode_sto = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto like '$kode_sto%')a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key
                 ORDER BY a.kode_sto ";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_anggota_wr2()
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
				(SELECT id_sub_struktural, kode_sto, nama_sub_struktural
				FROM `struktural_sub`
				WHERE id_struktural !='4' and  id_struktural !='5' and level_pimpinan BETWEEN 1 and 2
				)a
				JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
				JOIN doskar_usm c on b.user_key = c.user_key
				 )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key ORDER BY a.kode_sto ";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota_kepegawaian($kode_sto = '')
    {
        $date = date('Y-m-d');
        $sql = "SELECT a.*,DATE_FORMAT(b.tanggal,'%d-%m-%Y') tanggal, b.jam_masuk, b.jam_pulang FROM (SELECT a.*, keterangan,b.user_key, c.nama_doskar FROM
                (SELECT id_sub_struktural, kode_sto, nama_sub_struktural FROM struktural_sub WHERE kode_sto not like '1%'  )a
                JOIN struktural_anggota b on a.id_sub_struktural = b.id_sub_struktural
                JOIN doskar_usm c on b.user_key = c.user_key )a
LEFT JOIN (SELECT tanggal, user_key, jam_pulang, jam_masuk FROM wfh_absensi WHERE tanggal ='$date')b on a.user_key = b.user_key
                 ORDER BY a.kode_sto";
        $query = $this->db->query($sql, array($fakultas));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_angota()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_key = $token['user_key']; //
        $user = $this->get_level_akun($user_key);
//fakultas kode_khusus kode_sto
        if ($user['id_sub_struktural'] == '5' or $user['id_sub_struktural'] == '20') {
            //Biro
            $anggota = $this->get_angota_biro($user['kode_sto'] . '.');
        } elseif ($user['id_sub_struktural'] == '54') {
            //Dekan
            $anggota = $this->get_angota_dekan($user['kode_sto'] . '.', $user['fakultas']);
        } elseif ($user['id_sub_struktural'] == '55') {
            //wakil dekan 1
            $anggota = $this->get_angota_wadek1($user['kode_sto'] . '.', $user['fakultas']);
        } elseif ($user['id_sub_struktural'] == '56') {
            //wakil dekan 2
            $anggota = $this->get_angota_wadek2($user['kode_sto'] . '.', $user['fakultas']);
        } elseif ($user['id_sub_struktural'] == '59') {
            //kaprogdi
            $anggota = $this->get_angota_kaprogdi($user['kode_sto'] . '.', $user['kode_khusus']);
        } elseif ($user['id_sub_struktural'] == '62') {
            //ktu
            $anggota = $this->get_angota_ktu($user['kode_sto'] . '.', $user['fakultas']);
        } elseif ($user['id_sub_struktural'] == '76' or $user['id_sub_struktural'] == '83') {
            //lembaga dan upt
            $anggota = $this->get_angota_upt_lembaga($user['kode_sto'] . '.');
        } elseif ($user['id_struktural'] == '8' and $user['level'] == '2') {
            //lembaga dan upt
            $anggota = $this->get_angota_upt_lembaga($user['kode_sto'] . '.');
        } elseif ($user['id_sub_struktural'] == '3') {
            //wr 2
            $anggota = $this->get_anggota_wr2();
        } elseif ($user['id_sub_struktural'] == '21' or $user['id_sub_struktural'] == '22') {
            $anggota = $this->get_angota_kepegawaian();
        } elseif ($user['id_sub_struktural'] == '45') {
            $anggota = $this->get_angota_kabag($user['kode_sto'] . '.');
        } elseif ($user['id_sub_struktural'] == '6') {
            //kabag akademik
            $anggota = $this->get_angota_kabag($user['kode_sto'] . '.');
        } else {
            // $user_key = "15467578-8312-11ea-a817-1cb72c27dd68";
            // $user = $this->get_level_akun($user_key);
            // $anggota = $this->get_angota_upt_lembaga($user['kode_sto'] . '.', $user['fakultas']);
        }
        $result = array('prodil' => $user, 'anggota' => $anggota);
        return $this->app->respons_data($result, 'Daat berhasil di load', 200);
    }

    /*
    Get Presensi
     */
    public function detail_anggota($user_anggota = '')
    {
        $sql = "SELECT user_key, nis, nidn, nama_doskar, handphone FROM doskar_usm WHERE  user_key = ? ";
        $query = $this->db->query($sql, array($user_anggota));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function jabatan($id_sub_struktural = '')
    {
        $sql = "SELECT id_sub_struktural, unit_bagian, nama_sub_struktural, kode_sto FROM struktural_sub WHERE id_sub_struktural = ? ";
        $query = $this->db->query($sql, array($id_sub_struktural));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_data_presensi_default()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $user_anggota = $data->user_key;
        $user = $this->detail_anggota($user_anggota);

        $jabatan = $this->jabatan($data->id_sub_struktural);

        if (empty($data->bulan) or empty($data->tahun)) {
            $bulan = date('Y-m');
        } else {
            $bulan = $data->tahun . '-' . $data->bulan;
        }

        $sql = "SELECT a.*, b.id_absen, b.jm_task,  b.jam_masuk, b.jam_pulang, b.kordinat_masuk, b.kordinat_pulang FROM (
				SELECT tanggal, hari, if(hari ='Minggu', '#f4433', if(hari ='Sabtu','#fb6d18', '')) warna FROM wfh_kalender WHERE tanggal like '$bulan%')a
				LEFT JOIN (SELECT a.*, b.jm_task FROM (SELECT id_absen, tanggal, jam_masuk, jam_pulang, kordinat_masuk, kordinat_pulang
				 FROM wfh_absensi WHERE user_key = '$user_anggota'  and tanggal like '$bulan%')a
				left JOIN (
				SELECT id_absen, COUNT(*) jm_task FROM wfh_my_task  WHERE date_c like '$bulan%'  GROUP BY id_absen)b on a.id_absen = b.id_absen)b on a.tanggal = b.tanggal
				ORDER BY a.tanggal ASC";

        $query = $this->db->query($sql, array());
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $result;
        }
        $rs = array('user' => $user, 'jabatan' => $jabatan, 'result' => $result);
        return $this->app->respons_data($rs, 'Data berhasil diload', 200);
    }

    public function detail_task()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_absen = $data->id_absen;
        $sql = "SELECT id_task, id_absen, judul_kerjaan, keterangan, type_riport, dokumen, link, jam_riport , cek, keterangan_cek FROM wfh_my_task WHERE id_absen = ? ";
        $query = $this->db->query($sql, array($id_absen));
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }

    public function download_file()
    {
        $data = json_decode(file_get_contents("php://input"));
        $token = $this->tkn;
        $id_task = $data->id_task;
        $sql = "SELECT id_task, REPLACE(id_task,'-','')jdl_file, judul_kerjaan, dokumen FROM wfh_my_task WHERE id_task = ? ";
        $query = $this->db->query($sql, array($id_task));
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $this->app->respons_data($result, 'Data berhasil diload', 200);
        } else {
            return $this->app->respons_data(array(), 'data gagal diload', 200);
        }
    }
}
