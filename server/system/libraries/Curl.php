<?php

/*
 * Author : Wahyu Budi Santosa
 * ak : dudoks
 */
defined('BASEPATH') or exit('No direct script access allowed');

class CI_Curl
{

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function index()
    {
        print "CURL Dudoks 1.0";
    }

    public function request($method = '', $url = '', $tkn = '', $json_data = '')
    {
        $token = $this->CI->session->userdata($tkn);
        $headers = array(
            "Content-type:  application/json;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"run\"",
            "Authorization:$token ",
            "X-HTTP-Method-Override: $method",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, false); //di set false agar ouputan header yang di kirim tidak di ikutkan
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function upload($url = '', $file = '', $token = '')
    {
        $token = $this->CI->session->userdata($token);
        $headers = array(
            "Authorization:$token ",
        );
        $ch = curl_init();
        $cfile = new CURLFile($file['tmp_name'], $file['type'], $file['name']);
        $data = array('file' => $cfile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true); // required as of PHP 5.6.0
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, false); //di set false agar ouputan header yang di kirim tidak di ikutkan
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    public function request_manual_token($method = '', $url = '', $token = '', $json_data = '')
    {

        $headers = array(
            "Content-type:  application/json;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: \"run\"",
            "Authorization:$token ",
            "X-HTTP-Method-Override: $method",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, false); //di set false agar ouputan header yang di kirim tidak di ikutkan
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
