<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * LIbrary Api CI
 */
require_once APPPATH . '/libraries/JWT.php';
require_once APPPATH . '/libraries/ExpiredException.php';
use \Firebase\JWT\JWT;

class CI_Token
{

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function index()
    {

    }
    public function get_input()
    {
//di gunakan untuk mengambil json yang di kirim
        return json_decode(file_get_contents("php://input"));
    }

    public function base_server()
    {
        return "http://192.168.176.101/index.php/";
    }

    public function globalKey()
    {
        return $secret = '91f2b932-dd2aed7b-bd8e7f29-5d428100';
       //  return $secret = 'd6654c3e-dafe7010-0b6b1873-b7ca22b9';
    }
    public function server_key()
    {
        return $secret = '98654375831928833';
    }

    public function roots_level()
    {
        return $level_key = "2f2b7b9a-7008-11ea-9214-1cb72c27dd68";
    }
    public function id_aplikasi()
    {
        //80841925443002
        return $id_aplikasi = "98654375831928832";
    }

    public function respons_data($data, $pesan, $status)
    {
//response data json
        $arr = array('result' => $data, 'message' => $pesan, 'status' => $status);
        $this->CI->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // ->_display();
    }

    public function GlobalToken($payload)
    {
        try {
            $token = JWT::encode($payload, $this->globalKey());
            return $token;
        } catch (Exception $e) {
            return $this->respons_data(array('success' => false, 'pesan' => 'Maaf Token gagal di buat'), 'Maaf proses falidasi login gagal, silahkan ulangi sekali lagi', 200);
        }
    }

    public function checkGlobalToken()
    {
        $jwt = $this->CI->input->get_request_header('Authorization'); 
        try {
            $decoded = JWT::decode($jwt, $this->globalKey(), array('HS256'));
            if (empty($decoded)) {
                //return array('status' => '0', 'userKey' => '', 'levelKey' => '');
                return show_error("Akses anda belum bisa kami teruskan, silahkan login terlebih dahulu di sistem kami", '205', $heading = 'Maaf..');
            } 
            return json_decode(json_encode($decoded), true);
            //array('id_server' => $decoded->id_server, 'status' => '1', 'id_app' => $decoded->id_app, 'username' => $decoded->username, 'user_key' => $decoded->user_key, 'level_key' => $decoded->level_key);
        } catch (Exception $e) {
            return show_error("Akses anda belum bisa kami teruskan, silahkan login terlebih dahulu di sistem kami", '205', $heading = 'Maaf..');
            //return array('status' => '0', 'userKey' => '', 'levelKey' => '');
        }
    }

    public function cekToken()
    {
        $cek = $this->checkGlobalToken();
        if ($cek['status'] == '0') {
            return show_error("Akses anda belum bisa kami teruskan, silahkan login terlebih dahulu di sistem kami", '205', $heading = 'Maaf..');
        }
    }

    public function cek_akses_token()
    {
        $cek = $this->checkGlobalToken();

        if ($this->id_aplikasi() == $cek['id_app']) {

        } else {
            return show_error("Akses anda belum bisa kami teruskan, silahkan login terlebih dahulu di sistem kami", '205', $heading = 'Maaf..');
        }

    }

    public function cek_akses_app($token = '')
    {
        $id_app = $this->id_aplikasi();
        if (empty($token)) {
            return show_error("Akses anda belum bisa kami teruskan, anda belum memiliki akses di aplikasi ini,  ", '205', $heading = 'Maaf..');
        }
        if ($id_app == $token['id_app']) {

        } else {
            return show_error("Akses anda belum bisa kami teruskan, anda belum memiliki akses di aplikasi ini,", '205', $heading = 'Maaf..');
        }

    }

}
