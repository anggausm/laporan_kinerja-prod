<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * LIbrary Api CI
 */
require_once APPPATH . '/libraries/JWT.php';
require_once APPPATH . '/libraries/ExpiredException.php';

class CI_App
{
    /*
     * Mengambil data json
     */

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function get_input()
    {
//di gunakan untuk mengambil json yang di kirim
        return json_decode(file_get_contents("php://input"));
    }

//server 

// 172.16.97.100
// 192.168.176.100
 

// #backend
// 172.16.97.101
// 192.168.176.101 cd


    public function sso_url()
    {
        //172.16.97.205
       // return "http://172.16.99.205/index.php/";
         return "http://192.168.176.205/index.php/";
    }

    public function server_log()
    {
       // return "http://172.16.99.208/index.php/api/log";
    }
    
    public function respons_get($data = '')
    {
//response data json
        // $this->CI = & get_instance();
        $arr = array('result' => $data, 'message' => $data['pesan'], 'status' => $data['status']);
        $this->CI->output
            ->set_status_header($data['status'])
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // ->_display();
    }

    public function uuid()
    {
        $sql = "SELECT UUID() id";
        $query = $this->CI->db->query($sql);
        $result = $query->row_array();
        return $result['id'];
    }

    public function uuid_short()
    {
        $sql = "SELECT UUID_SHORT() id";
        $query = $this->CI->db->query($sql);
        $result = $query->row_array();
        return $result['id'];
    }

    public function cek_akses($api_url = '', $crud = '')
    {
        $this->CI->load->model('root/m_akses');
        $token = $this->CI->token->checkGlobalToken();
        if (empty($token) or $token == null) {

        } else {
            $cek = $this->app_cek_akses($token['id_app'], $api_url, $crud, $token['user_key'], $token['level_key']);

            if ($cek) {
            } else {
                return show_error('User yang anda gunakan belum memiliki akses di sistem kami.', '201', 'Maaf..');
                exit();
            }
        }
    }

    public function app_cek_akses($app_id = '', $menu_key = '', $crud = '', $user_key = '', $level_key = '')
    {
        //$token['id_app'], $api_url, $crud, $token['user_key'], $token['level_key']
        $sql = "SELECT * FROM (
                        SELECT id_levakses, menu_key, id_aplikasi  FROM `sso_level_akses`
                        WHERE level_id = '$level_key'
                        and menu_key ='$menu_key' and akses_$crud ='1' and id_aplikasi ='$app_id'
                )a
                 JOIN (
                            SELECT menu_key, id_aplikasi FROM sso_menu WHERE st ='1'
                            UNION
                            SELECT menu_key, id_aplikasi FROM sso_menu_sub WHERE st ='1'
                )b on a.menu_key = b.menu_key and a.id_aplikasi = b.id_aplikasi";
        $query = $this->CI->db->query($sql, array());
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function respons_data($data, $pesan, $status)
    {
//response data json
        $arr = array('result' => $data, 'message' => $pesan, 'status' => $status);
        $this->CI->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        // ->_display();
    }

    public function cekRequest($request)
    {
// digunakan untuk mengecek reques apa yang di ijinkan untuk mengakses halaman ini.
        error_reporting('All');
        $cek_rq = $_SERVER['REQUEST_METHOD'];
        if ($cek_rq == $request) {
            return true;
        } else {
            return show_error('Maaf untuk Request data anda belum bisa kami proses.', '201', 'Reques di tolak...');
            exit();
        }
    }

    public function headersAllow()
    {
        //==== ALLOWING CORS
        header('Access-Control-Allow-Origin: * '); //tanda bintang berarti semua client frond bisa mengakses data
        header('Access-Control-Allow-Methods: PATCH, GET, PUT, POST, DELETE');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
    }

}
